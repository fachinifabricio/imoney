

   MEMBER('imoney.clw')                                    ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('radsqlb.inc'),ONCE

                     MAP
                       INCLUDE('IMONE007.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('IMONE004.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseMovDocumentos PROCEDURE                              ! Generated from procedure template - Window

LOC:valor            DECIMAL(12,2)                         !
LOC:TipoContaDoc     STRING(1)                             !
LOC:TipoContaMov     STRING(1)                             !
LOC:TipoValorDoc     BYTE                                  !
LOC:TipoValorMov     BYTE                                  !
qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
BR1:View             VIEW(movdocumentos)
                       PROJECT(MOV:id)
                       PROJECT(MOV:d_datamovimentacao)
                       PROJECT(MOV:valormovimento)
                       PROJECT(MOV:descricao)
                       PROJECT(MOV:idconta)
                       JOIN(CON:con_pk_id,MOV:idconta),INNER
                         PROJECT(CON:descricaoconta)
                         PROJECT(CON:id)
                       END
                     END
Queue:RADSQLBrowse   QUEUE                            !Queue declaration for browse/combo box using ?lstMovDocumentos
MOV:id                 LIKE(MOV:id)                   !List box control field - type derived from field
Unknown                STRING(1)                      !List box control field - unable to determine correct data type
MOV:d_datamovimentacao LIKE(MOV:d_datamovimentacao)   !List box control field - type derived from field
MOV:valormovimento     LIKE(MOV:valormovimento)       !List box control field - type derived from field
MOV:descricao          LIKE(MOV:descricao)            !List box control field - type derived from field
CON:descricaoconta     LIKE(CON:descricaoconta)       !List box control field - type derived from field
CON:id                 LIKE(CON:id)                   !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Window               WINDOW('i-Money'),AT(,,471,267),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,SYSTEM,GRAY,DOUBLE,AUTO
                       IMAGE('window.ico'),AT(2,2),USE(?Image1)
                       PROMPT('Movimenta��o do Documento'),AT(26,5),USE(?proTitulo),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(2,23,468,2),USE(?Panel1)
                       LIST,AT(2,27,467,195),USE(?lstMovDocumentos),IMM,FLAT,VSCROLL,MSG('Scrolling records...'),FORMAT('[0D(2)F~ID~L@n15.`0b@292L(2)F~Plano de Contas~@s70@50CF~Data~@d06b@52R(2)F~Valor' &|
   '~@n13.`2@/211L(2)_F~Descri��o~@s50@200L(2)_F~Conta~@s50@]F'),FROM(Queue:RADSQLBrowse)
                       BUTTON('&Incluir'),AT(313,226,50,15),USE(?btnIncluir),TRN,FLAT,LEFT,ICON('incluir.ico')
                       BUTTON('&Alterar'),AT(366,226,50,15),USE(?btnAlterar),TRN,FLAT,LEFT,ICON('alterar.ico')
                       BUTTON('&Excluir'),AT(418,226,50,15),USE(?btnExcluir),TRN,FLAT,LEFT,ICON('excluir.ico')
                       PANEL,AT(2,243,468,2),USE(?Panel1:2)
                       BUTTON('&Fechar'),AT(418,249,50,15),USE(?btnFechar),TRN,FLAT,LEFT,ICON('fechar.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Reset                  PROCEDURE(BYTE Force=0),DERIVED     ! Method added to host embed code
SetAlerts              PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
BR1                  CLASS(EXTSQLB)
GenerateSelectStatement PROCEDURE(),DERIVED                ! Method added to host embed code
RunForm                PROCEDURE(Byte in_Request),DERIVED  ! Method added to host embed code
SetRange               PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseMovDocumentos')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?btnFechar,RequestCancelled)             ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?btnFechar,RequestCompleted)             ! Add the close control to the window manger
  END
  COMPILE('C55',_C55_)
    Self.Filesopened = 1
  C55
  OMIT('C55',_C55_)
    Filesopened = 1
  C55
  Relate:movdocumentos.Open()
  Access:movdocumentos.UseFile()
  Relate:contas.Open()
  Access:contas.UseFile()
  Relate:aliasplanocontas.SetOpenRelated()
  Relate:aliasplanocontas.Open                             ! File aliasplanocontas used by this procedure, so make sure it's RelationManager is open
  Access:documentos.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
  BR1.RefreshHotkey = F5Key
  BR1.Setlistcontrol (?lstMovDocumentos,10)
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>BrowseMovDocumentos<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>BrowseMovDocumentos<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  BR1.PopUpActive = True
  BR1.Init(Access:movdocumentos)
  BR1.useansijoin=1
  BR1.MaintainProcedure = 'UpdateMovDocumentos'
  BR1.SetupdateButtons (?btnIncluir,?btnAlterar,?btnExcluir,0)
  SELF.SetAlerts()
  ?lstMovDocumentos{PROP:From} = Queue:RADSQLBrowse
  BR1.AddTable('movdocumentos',1,0,'movdocumentos')  ! Add the table to the list of tables
    BR1.AddTable('contas',2,0,'contas')
  BR1.Listqueue      &= Queue:RADSQLBrowse
  BR1.Position       &= Queue:RADSQLBrowse.ViewPosition
  BR1.ColumnCharAsc   = ''
  BR1.ColumnCharDes   = ''
  BR1.AddFieldpairs(Queue:RADSQLBrowse.MOV:id,MOV:id,'movdocumentos','id','N',0,'B','A','M','N',1,1)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.MOV:descricao,MOV:descricao,'movdocumentos','descricao','A',0,'B','A','M','N',4,5)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.MOV:d_datamovimentacao,MOV:d_datamovimentacao,'movdocumentos','d_datamovimentacao','D',0,'B','A','M','N',5,3)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.MOV:valormovimento,MOV:valormovimento,'movdocumentos','valormovimento','N',0,'B','A','M','N',6,4)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.CON:id,CON:id,'contas','id','N',0,'B','A','M','N',1,7)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.CON:descricaoconta,CON:descricaoconta,'contas','descricaoconta','A',0,'B','A','M','N',2,6)
  BR1.AddJoinFields('movdocumentos','idconta','contas','id',BR1:view{prop:inner,1},0,CON:con_pk_id)
  BR1.Addviewfield('movdocumentos','idconta',7)   ! Not in queue from MOV:idconta
  BR1.Managedview &=BR1:View
  BR1.Openview()
  BR1.SetFileLoad()
  BR1.SetForceFetch (1)
  BR1.SetColColor = 0
  BR1.Resetsort(1)
  EnterByTabManager.Init(False)
  BR1.RefreshMethod = 1
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  COMPILE('C55',_C55_)
  if Self.filesopened
  C55
  OMIT('C55',_C55_)
  If filesopened then
  C55
    Relate:movdocumentos.Close
    Relate:contas.Close
  END
    Relate:aliasplanocontas.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF Window{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
     If force = 1  then
        BR1.ResetBrowse (1)
     end


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  BR1.SetAlerts()


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
  BR1.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('BrowseMovDocumentos')
          End
        End
      End
      Close(HWGroups)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BR1.GenerateSelectStatement PROCEDURE


  CODE
  PARENT.GenerateSelectStatement
  Self.ViewSqlStatement = |
  'SELECT A.ID, A.IDPLANO, A.DESCRICAO, A.D_DATAMOVIMENTACAO, A.VALORMOVIMENTO, A.IDCONTA, B.ID, '&|
  'B.DESCRICAOCONTA, C.ID, C.DESCRICAOPLANO FROM MOVDOCUMENTOS A '&|
  'JOIN CONTAS B ON A.IDCONTA=B.ID '&|
  'JOIN PLANOCONTAS C ON A.IDPLANO=C.ID'


BR1.RunForm PROCEDURE(Byte in_Request)


  CODE
     GlobalRequest = in_Request
  PARENT.RunForm(in_Request)
  !If 1 = 2
     if in_Request = ChangeRecord or in_Request = DeleteRecord or in_Request = ViewRecord
        if Access:movdocumentos.Fetch(MOV:mov_pk_id) <> Level:Benign ! Fetch movdocumentos on key 
          Message('Error on primary key fetch MOV:mov_pk_id|Error:'& Error() &' FileError:'& FileError() &'|SQL:'& movdocumentos{Prop:SQL})
          GlobalResponse = RequestCancelled
          Return
        end
     else
          !Prime fields for insert from related browse on documentos
        MOV:iddocumento = DOC:id
     end
  UpdateMovDocumentos 
     BR1.Response = Globalresponse
  
   BR1.ListControl{Prop:Selected} = BR1.CurrentChoice
   Display
  !End
  !If Self.Request <> DeleteRecord
  !  if in_Request = ChangeRecord or in_Request = ViewRecord
  !    if Access:movdocumentos.Fetch(MOV:mov_pk_id) <> Level:Benign ! Fetch movdocumentos on key
  !      Message('Error on primary key fetch MOV:mov_pk_id|Error:'& Error() &' FileError:'& FileError() &'|SQL:'& movdocumentos{Prop:SQL})
  !      GlobalResponse = RequestCancelled
  !      Return
  !    end
  !  else
  !    !Prime fields for insert from related browse on documentos
  !    MOV:iddocumento = DOC:id
  !  end
  !  UpdateMovDocumentos 
  !  BR1.Response = Globalresponse
  !  
  !  BR1.ListControl{Prop:Selected} = BR1.CurrentChoice
  !  Display
  !Else
  !  if Access:movdocumentos.Fetch(MOV:mov_pk_id) <> Level:Benign ! Fetch movdocumentos on key
  !    Message('Error on primary key fetch MOV:mov_pk_id|Error:'& Error() &' FileError:'& FileError() &'|SQL:'& movdocumentos{Prop:SQL})
  !    GlobalResponse = RequestCancelled
  !    Return
  !  end
  !  APC:id = DOC:idplano
  !  Get(aliasplanocontas,APC:pla_pk_id)
  !  !
  !  LOC:valor = MOV:valormovimento
  !  LOC:TipoContaDoc = APC:tipolancamento
  !  LOC:TipoContaMov = PLA:tipolancamento
  !  LOC:TipoValorDoc = APC:tipovalor
  !  LOC:TipoValorMov = PLA:tipovalor
  !  If Relate:movdocumentos.Delete() = Level:Benign
  !    If (LOC:TipoContaMov = 'D' And LOC:TipoContaDoc = 'D') Or (LOC:TipoContaMov = 'C' And LOC:TipoContaDoc = 'C')
  !      If LOC:TipoValorMov = 2
  !        DOC:valormulta += MOV:valormovimento
  !      ElsIf LOC:TipoValorMov = 3
  !        DOC:valorjuros += MOV:valormovimento
  !      ElsIf LOC:TipoValorMov = 4
  !        DOC:valoroutrosacrescimos += MOV:valormovimento
  !      End
  !    ElsIf (LOC:TipoContaMov = 'D' And LOC:TipoContaDoc = 'C') Or (LOC:TipoContaMov = 'C' And LOC:TipoContaDoc = 'D')
  !      If LOC:TipoValorMov = 1
  !        DOC:valordesconto += MOV:valormovimento
  !      ElsIf LOC:TipoValorMov = 5
  !        DOC:valorloutrosdescontos += MOV:valormovimento
  !      ElsIf LOC:TipoValorMov = 6
  !        DOC:valorpago += MOV:valormovimento
  !      End
  !    End
  !    Put(documentos)
  !  End
  !End


BR1.SetRange PROCEDURE


  CODE
     !! Set the range limit
     Self.Rangefilter =  Self.Getalias('movdocumentos') & '.IDDOCUMENTO=' & DOC:ID  ! DOC:
  PARENT.SetRange

UpdateMovDocumentos PROCEDURE                              ! Generated from procedure template - Window

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Valor            DECIMAL(12,2)                         !
qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
History::MOV:Record  LIKE(MOV:RECORD),THREAD
QuickWindow          WINDOW('i-Money'),AT(,,334,140),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,HLP('UpdateMovDocumentos'),SYSTEM,GRAY,DOUBLE,AUTO
                       ENTRY(@s70),AT(81,36,250,10),USE(PLA:descricaoplano),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Descri��o do lan�amento no plano de contas'),TIP('Descri��o do lan�amento no plano de contas'),REQ,READONLY
                       ENTRY(@s50),AT(4,58,327,10),USE(MOV:descricao),FLAT,MSG('Descri��o do movimento'),TIP('Descri��o do movimento'),REQ
                       ENTRY(@d06b),AT(4,80,73,10),USE(MOV:d_datamovimentacao),FLAT,MSG('Data'),TIP('Data'),REQ
                       ENTRY(@n13.`2),AT(81,80,80,10),USE(MOV:valormovimento),FLAT,DECIMAL(12),MSG('Valor do movimento'),TIP('Valor do movimento')
                       BUTTON('Button 5'),AT(187,78,73,15),USE(?Button5)
                       ENTRY(@n15.`0b),AT(3,102,58,10),USE(MOV:idconta),FLAT,DECIMAL(14),MSG('Identificador da conta'),TIP('Identificador da conta')
                       BUTTON,AT(65,101,12,12),USE(?lkpConta),TRN,FLAT,ICON('busca.ico')
                       ENTRY(@s50),AT(81,102,250,10),USE(CON:descricaoconta),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Descri��o da conta'),TIP('Descri��o da conta'),READONLY
                       BUTTON('&Gravar'),AT(214,122,55,15),USE(?btnGravar),TRN,FLAT,LEFT,MSG('Accept data and close the window'),TIP('Accept data and close the window'),ICON('gravar.ico'),DEFAULT
                       BUTTON('&Cancelar'),AT(274,122,55,15),USE(?btnCancelar),TRN,FLAT,LEFT,MSG('Cancel operation'),TIP('Cancel operation'),ICON('cancelar.ico')
                       IMAGE('window.ico'),AT(2,2),USE(?Image1)
                       PROMPT('Movimenta��o do Documento'),AT(26,5),USE(?Prompt6),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(2,23,329,2),USE(?Panel1)
                       PROMPT('Plano:'),AT(4,27),USE(?MOV:idplano:Prompt),TRN
                       PROMPT('Descri��o:'),AT(4,48),USE(?MOV:descricao:Prompt),TRN
                       PROMPT('Data:'),AT(4,71),USE(?MOV:d_datamovimentacao:Prompt),TRN
                       PROMPT('Valor:'),AT(81,71),USE(?MOV:valormovimento:Prompt),TRN
                       PROMPT('Conta:'),AT(3,92),USE(?MOV:idconta:Prompt),TRN
                       PANEL,AT(2,116,329,2),USE(?Panel1:2)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Reset                  PROCEDURE(BYTE Force=0),DERIVED     ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'Visualizando'
  OF InsertRecord
    ActionMessage = 'Incluindo'
  OF ChangeRecord
    ActionMessage = 'Alterando'
  END
  QuickWindow{Prop:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateMovDocumentos')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PLA:descricaoplano
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(MOV:Record,History::MOV:Record)
  SELF.AddHistoryField(?MOV:descricao,4)
  SELF.AddHistoryField(?MOV:d_datamovimentacao,5)
  SELF.AddHistoryField(?MOV:valormovimento,6)
  SELF.AddHistoryField(?MOV:idconta,7)
  SELF.AddUpdateFile(Access:movdocumentos)
  SELF.AddItem(?btnCancelar,RequestCancelled)              ! Add the cancel control to the window manager
  Relate:aliasplanocontas.SetOpenRelated()
  Relate:aliasplanocontas.Open                             ! File aliasplanocontas used by this procedure, so make sure it's RelationManager is open
  Access:documentos.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:movdocumentos
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.InsertAction = Insert:Batch
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?btnGravar
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  !Posiciona na conta do Documento
  APC:id = DOC:idplano
  Get(aliasplanocontas,APC:pla_pk_id)
  !Verifica se � altera��o e grava valores nas vari�veis locais para ser usado no TakeCompleted
  If Self.Request = ChangeRecord
    LOC:Valor = MOV:valormovimento
  End
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>UpdateMovDocumentos<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>UpdateMovDocumentos<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?PLA:descricaoplano{PROP:ReadOnly} = True
    ?MOV:descricao{PROP:ReadOnly} = True
    ?MOV:d_datamovimentacao{PROP:ReadOnly} = True
    ?MOV:valormovimento{PROP:ReadOnly} = True
    DISABLE(?Button5)
    ?MOV:idconta{PROP:ReadOnly} = True
    DISABLE(?lkpConta)
    ?CON:descricaoconta{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:aliasplanocontas.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  CON:id = MOV:idconta                                     ! Assign linking field value
  Access:contas.Fetch(CON:con_pk_id)
  PLA:id = MOV:idplano                                     ! Assign linking field value
  Access:planocontas.Fetch(PLA:pla_pk_id)
  PARENT.Reset(Force)


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    BrowseContas
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?btnGravar
      SetNullFields(movdocumentos)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?MOV:valormovimento
      IF Access:movdocumentos.TryValidateField(6)          ! Attempt to validate MOV:valormovimento in movdocumentos
        SELECT(?MOV:valormovimento)
        QuickWindow{Prop:AcceptAll} = False
        CYCLE
      ELSE
        FieldColorQueue.Feq = ?MOV:valormovimento
        GET(FieldColorQueue, FieldColorQueue.Feq)
        IF ~ERRORCODE()
          ?MOV:valormovimento{Prop:FontColor} = FieldColorQueue.OldColor
          DELETE(FieldColorQueue)
        END
      END
    OF ?MOV:idconta
      IF MOV:idconta OR ?MOV:idconta{Prop:Req}
        CON:id = MOV:idconta
        IF Access:contas.TryFetch(CON:con_pk_id)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            MOV:idconta = CON:id
          ELSE
            SELECT(?MOV:idconta)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?lkpConta
      ThisWindow.Update
      CON:id = MOV:idconta
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        MOV:idconta = CON:id
      END
      ThisWindow.Reset(1)
    OF ?btnGravar
      ThisWindow.Update
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeCompleted()
  !If Self.Response = RequestCompleted
  !  If Self.Request = InsertRecord
  !    If (PLA:tipolancamento = 'D' And APC:tipolancamento = 'D') Or (PLA:tipolancamento = 'C' And APC:tipolancamento = 'C')
  !      If PLA:tipovalor = 2
  !        DOC:valormulta += MOV:valormovimento
  !      ElsIf PLA:tipovalor = 3
  !        DOC:valorjuros += MOV:valormovimento
  !      ElsIf PLA:tipovalor = 4
  !        DOC:valoroutrosacrescimos += MOV:valormovimento
  !      End
  !    ElsIf (PLA:tipolancamento = 'D' And APC:tipolancamento = 'C') Or (PLA:tipolancamento = 'C' And APC:tipolancamento = 'D')
  !      If PLA:tipovalor = 1
  !        DOC:valordesconto += MOV:valormovimento
  !      ElsIf PLA:tipovalor = 5
  !        DOC:valorloutrosdescontos += MOV:valormovimento
  !      ElsIf PLA:tipovalor = 6
  !        DOC:valorpago += MOV:valormovimento
  !      End
  !    End
  !    Put(documentos)
  !  ElsIf Self.Request = ChangeRecord
  !    If (PLA:tipolancamento = 'D' And APC:tipolancamento = 'D') Or (PLA:tipolancamento = 'C' And APC:tipolancamento = 'C')
  !      If PLA:tipovalor = 2
  !        DOC:valormulta = (DOC:valormulta - LOC:Valor) + MOV:valormovimento
  !      ElsIf PLA:tipovalor = 3
  !        DOC:valorjuros = (DOC:valorjuros - LOC:Valor) + MOV:valormovimento
  !      ElsIf PLA:tipovalor = 4
  !        DOC:valoroutrosacrescimos = (DOC:valoroutrosacrescimos - LOC:Valor) + MOV:valormovimento
  !      End
  !    ElsIf (PLA:tipolancamento = 'D' And APC:tipolancamento = 'C') Or (PLA:tipolancamento = 'C' And APC:tipolancamento = 'D')
  !      If PLA:tipovalor = 1
  !        DOC:valordesconto = (DOC:valordesconto - LOC:Valor) + MOV:valormovimento
  !      ElsIf PLA:tipovalor = 5
  !        DOC:valorloutrosdescontos = (DOC:valorloutrosdescontos - LOC:Valor) + MOV:valormovimento
  !      ElsIf PLA:tipovalor = 6
  !        DOC:valorpago = (DOC:valorpago - LOC:Valor) + MOV:valormovimento
  !      End
  !    End
  !    Put(documentos)
  !  End
  !End
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('UpdateMovDocumentos')
          End
        End
      End
      Close(HWGroups)
    OF EVENT:OpenWindow
      !If Self.Request = ChangeRecord
      !  Disable(?MOV:idplano)
      !  Disable(?lkpPlano)
      !End
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

