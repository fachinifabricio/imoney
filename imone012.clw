

   MEMBER('imoney.clw')                                    ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('radsqlb.inc'),ONCE

                     MAP
                       INCLUDE('IMONE012.INC'),ONCE        !Local module procedure declarations
                     END


BrowseEmpresas PROCEDURE                                   ! Generated from procedure template - Window

LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
BR1:View             VIEW(empresa)
                       PROJECT(EMP:codempresa)
                       PROJECT(EMP:razaosocial)
                       PROJECT(EMP:cnpj)
                     END
Queue:RADSQLBrowse   QUEUE                            !Queue declaration for browse/combo box using ?List
EMP:codempresa         LIKE(EMP:codempresa)           !List box control field - type derived from field
EMP:razaosocial        LIKE(EMP:razaosocial)          !List box control field - type derived from field
EMP:cnpj               LIKE(EMP:cnpj)                 !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Window               WINDOW('i-Money'),AT(,,341,254),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,SYSTEM,GRAY,DOUBLE,AUTO
                       IMAGE('window.ico'),AT(4,2),USE(?imgLogo)
                       PROMPT('Empresas'),AT(28,5),USE(?proTitulo),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(2,23,336,2),USE(?Panel1)
                       LIST,AT(4,27,333,100),USE(?List),IMM,FLAT,VSCROLL,MSG('Scrolling records...'),FORMAT('28R(2)F~C�digo~@n3@202L(2)F~Raz�o Social~@s50@72R(4)F~CNPJ~@s18@'),FROM(Queue:RADSQLBrowse)
                       BUTTON('&Incluir'),AT(166,132,55,15),USE(?Insert),FLAT,LEFT,ICON('incluir.ico')
                       BUTTON('&Alterar'),AT(224,132,55,15),USE(?Change),FLAT,LEFT,ICON('alterar.ico')
                       BUTTON('&Excluir'),AT(282,132,55,15),USE(?Delete),FLAT,LEFT,ICON('excluir.ico')
                       PANEL,AT(4,150,336,2),USE(?Panel1:2)
                       PROMPT('Telefone:'),AT(5,155),USE(?EMP:telefone:Prompt),TRN
                       PROMPT('e-mail:'),AT(102,155),USE(?EMP:email:Prompt),TRN
                       ENTRY(@s14),AT(5,165,92,10),USE(EMP:telefone),SKIP,FLAT,MSG('N�mero do telefone'),TIP('N�mero do telefone'),UPR,READONLY
                       ENTRY(@s40),AT(102,165,236,10),USE(EMP:email),SKIP,FLAT,MSG('e-mail'),TIP('e-mail'),UPR,READONLY
                       PROMPT('Logradouro:'),AT(5,181),USE(?EMP:logradouro:Prompt),TRN
                       PROMPT('N�mero:'),AT(278,181),USE(?EMP:numero:Prompt),TRN
                       ENTRY(@s50),AT(5,191,267,10),USE(EMP:logradouro),SKIP,FLAT,MSG('C�digo da cidade segundo o cadastro de CEP<180>s'),TIP('C�digo da cidade segundo o cadastro de CEP<180>s'),REQ,UPR,READONLY
                       ENTRY(@s10),AT(278,191,60,10),USE(EMP:numero),SKIP,FLAT,MSG('N�mero residencial do im�vel'),TIP('N�mero residencial do im�vel'),REQ,UPR,READONLY
                       PROMPT('CEP:'),AT(5,207),USE(?EMP:cep:Prompt),TRN
                       PROMPT('Bairro:'),AT(102,207),USE(?EMP:bairro:Prompt),TRN
                       ENTRY(@p#####-###pb),AT(5,216,92,10),USE(EMP:cep),SKIP,FLAT,MSG('CEP fornecido pelo correio'),TIP('CEP fornecido pelo correio'),UPR,READONLY
                       ENTRY(@s20),AT(102,216,236,10),USE(EMP:bairro),SKIP,FLAT,MSG('Bairro'),TIP('Bairro'),UPR,READONLY
                       PANEL,AT(4,230,336,2),USE(?Panel1:3)
                       BUTTON('&Fechar'),AT(282,236,55,15),USE(?btnFechar),FLAT,LEFT,ICON('fechar.ico'),STD(STD:Close),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Reset                  PROCEDURE(BYTE Force=0),DERIVED     ! Method added to host embed code
SetAlerts              PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
BR1                  CLASS(EXTSQLB)
RunForm                PROCEDURE(Byte in_Request),DERIVED  ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseEmpresas')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?imgLogo
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  !message(GLO:Conexao,,,,,2)
  COMPILE('C55',_C55_)
    Self.Filesopened = 1
  C55
  OMIT('C55',_C55_)
    Filesopened = 1
  C55
  Relate:empresa.Open()
  Access:empresa.UseFile()
  SELF.Open(Window)                                        ! Open window
  BR1.RefreshHotkey = F5Key
  BR1.Setlistcontrol (?List,10)
  Do DefineListboxStyle
  BR1.PopUpActive = True
  BR1.Init(Access:empresa)
  BR1.useansijoin=1
  BR1.MaintainProcedure = 'UpdateEmpresa'
  BR1.SetupdateButtons (?Insert,?Change,?Delete,0)
  SELF.SetAlerts()
  ?List{PROP:From} = Queue:RADSQLBrowse
  BR1.AddTable('empresa',1,0,'empresa')  ! Add the table to the list of tables
  BR1.Listqueue      &= Queue:RADSQLBrowse
  BR1.Position       &= Queue:RADSQLBrowse.ViewPosition
  BR1.ColumnCharAsc   = ''
  BR1.ColumnCharDes   = ''
  BR1.AddFieldpairs(Queue:RADSQLBrowse.EMP:codempresa,EMP:codempresa,'empresa','codempresa','N',0,'B','A','M','N',1,1)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.EMP:razaosocial,EMP:razaosocial,'empresa','razaosocial','A',0,'B','A','M','N',2,2)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.EMP:cnpj,EMP:cnpj,'empresa','cnpj','A',0,'B','A','M','N',3,3)
  BR1.Managedview &=BR1:View
  BR1.Openview()
  BR1.SetFileLoad()
  BR1.SetForceFetch (1)
  BR1.SetColColor = 0
  BR1.Resetsort(1)
  EnterByTabManager.Init(False)
  BR1.RefreshMethod = 1
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  COMPILE('C55',_C55_)
  if Self.filesopened
  C55
  OMIT('C55',_C55_)
  If filesopened then
  C55
    Relate:empresa.Close
  END
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF Window{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
     If force = 1  then
        BR1.ResetBrowse (1)
     end


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  BR1.SetAlerts()


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
  BR1.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BR1.RunForm PROCEDURE(Byte in_Request)


  CODE
     GlobalRequest = in_Request
  PARENT.RunForm(in_Request)
     if in_Request = ChangeRecord or in_Request = DeleteRecord or in_Request = ViewRecord
        if Access:empresa.Fetch(EMP:emp_codempresa) <> Level:Benign ! Fetch empresa on key 
          Message('Error on primary key fetch EMP:emp_codempresa|Error:'& Error() &' FileError:'& FileError() &'|SQL:'& empresa{Prop:SQL})
          GlobalResponse = RequestCancelled
          Return
        end
     else
     end
  UpdateEmpresa 
     BR1.Response = Globalresponse
  
   BR1.ListControl{Prop:Selected} = BR1.CurrentChoice
   Display

UpdateEmpresa PROCEDURE                                    ! Generated from procedure template - Window

ActionMessage        CSTRING(40)                           !
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
Window               WINDOW('i-Money'),AT(,,343,156),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,SYSTEM,GRAY,DOUBLE,AUTO
                       IMAGE('window.ico'),AT(4,2),USE(?imgLogo)
                       PROMPT('Empresas'),AT(28,5),USE(?proTitulo),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(2,23,336,2),USE(?Panel1)
                       PROMPT('Raz�o Social:'),AT(5,29),USE(?EMP:razaosocial:Prompt)
                       PROMPT('CNPJ:'),AT(246,29),USE(?EMP:cnpj:Prompt)
                       ENTRY(@s50),AT(5,39,236,10),USE(EMP:razaosocial),FLAT,MSG('Raz�o Social'),TIP('Raz�o Social'),UPR
                       ENTRY(@s18),AT(246,39,92,10),USE(EMP:cnpj),FLAT,MSG('N�mero do CNPJ'),TIP('N�mero do CNPJ'),UPR
                       PROMPT('Telefone:'),AT(5,54),USE(?EMP:telefone:Prompt),TRN
                       PROMPT('e-mail:'),AT(102,54),USE(?EMP:email:Prompt),TRN
                       ENTRY(@s14),AT(5,64,92,10),USE(EMP:telefone),FLAT,MSG('N�mero do telefone'),TIP('N�mero do telefone'),UPR
                       ENTRY(@s40),AT(102,64,236,10),USE(EMP:email),FLAT,MSG('e-mail'),TIP('e-mail'),UPR
                       PROMPT('Logradouro:'),AT(5,80),USE(?EMP:logradouro:Prompt),TRN
                       PROMPT('N�mero:'),AT(278,80),USE(?EMP:numero:Prompt),TRN
                       ENTRY(@s50),AT(5,90,267,10),USE(EMP:logradouro),FLAT,MSG('C�digo da cidade segundo o cadastro de CEP<180>s'),TIP('C�digo da cidade segundo o cadastro de CEP<180>s'),REQ,UPR
                       ENTRY(@s10),AT(278,90,60,10),USE(EMP:numero),FLAT,MSG('N�mero residencial do im�vel'),TIP('N�mero residencial do im�vel'),REQ,UPR
                       PROMPT('CEP:'),AT(5,106),USE(?EMP:cep:Prompt),TRN
                       PROMPT('Bairro:'),AT(102,106),USE(?EMP:bairro:Prompt),TRN
                       ENTRY(@p#####-###pb),AT(5,115,92,10),USE(EMP:cep),FLAT,MSG('CEP fornecido pelo correio'),TIP('CEP fornecido pelo correio'),UPR
                       ENTRY(@s20),AT(102,115,236,10),USE(EMP:bairro),FLAT,MSG('Bairro'),TIP('Bairro'),UPR
                       PANEL,AT(4,129,336,2),USE(?Panel1:3)
                       BUTTON('Gravar'),AT(225,137,55,15),USE(?btnGravar),FLAT,LEFT,ICON('gravar.ico'),DEFAULT,REQ
                       BUTTON('&Cancelar'),AT(283,137,55,15),USE(?btnCancelar),FLAT,LEFT,ICON('cancelar.ico'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateEmpresa')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?imgLogo
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:empresa)
  Relate:empresa.SetOpenRelated()
  Relate:empresa.Open                                      ! File empresa used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:empresa
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.OkControl = ?btnGravar
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:empresa.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?btnGravar
      DOC:d_datapagamento = DataServidor()
      EMP:t_horacadastro  = HoraServidor()
      EMP:usuario = GLO:nomeusuario
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?btnGravar
      ThisWindow.Update
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

