

   MEMBER('imoney.clw')                                    ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('radsqlb.inc'),ONCE

                     MAP
                       INCLUDE('IMONE002.INC'),ONCE        !Local module procedure declarations
                     END


BrowseBancos PROCEDURE                                     ! Generated from procedure template - Window

qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
BR1:View             VIEW(bancos)
                       PROJECT(BAN:numerobanco)
                       PROJECT(BAN:nomedobanco)
                       PROJECT(BAN:sigladobanco)
                       PROJECT(BAN:id)
                     END
Queue:RADSQLBrowse   QUEUE                            !Queue declaration for browse/combo box using ?lstBancos
BAN:numerobanco        LIKE(BAN:numerobanco)          !List box control field - type derived from field
BAN:nomedobanco        LIKE(BAN:nomedobanco)          !List box control field - type derived from field
BAN:sigladobanco       LIKE(BAN:sigladobanco)         !List box control field - type derived from field
BAN:id                 LIKE(BAN:id)                   !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Window               WINDOW('i-Money'),AT(,,314,231),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('principal.ico'),SYSTEM,GRAY,DOUBLE,AUTO
                       IMAGE('window.ico'),AT(2,2),USE(?Image1)
                       PROMPT('Bancos'),AT(26,5),USE(?Prompt1),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(2,22,309,2),USE(?Panel1)
                       LIST,AT(2,26,309,163),USE(?lstBancos),IMM,FLAT,VSCROLL,MSG('Scrolling records...'),FORMAT('33C_F~N�mero~@n03b@207L(2)_F~Nome~@s50@20L(2)_F~Sigla~@s5@0D(2)_F~ID~L@n15.`0b@'),FROM(Queue:RADSQLBrowse)
                       BUTTON('&Incluir'),AT(158,193,50,15),USE(?btnIncluir),TRN,FLAT,LEFT,ICON('incluir.ico')
                       BUTTON('&Alterar'),AT(210,193,50,15),USE(?btnAlterar),TRN,FLAT,LEFT,ICON('alterar.ico')
                       BUTTON('&Excluir'),AT(262,193,50,15),USE(?btnExcluir),TRN,FLAT,LEFT,ICON('excluir.ico')
                       PANEL,AT(2,210,309,2),USE(?Panel1:2)
                       BUTTON('&Fechar'),AT(255,215,55,15),USE(?btnFechar),TRN,FLAT,LEFT,ICON('fechar.ico')
                       BUTTON('&Selecionar'),AT(2,193,60,15),USE(?btnSelecionar),TRN,FLAT,HIDE,LEFT,ICON('selecionar.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Reset                  PROCEDURE(BYTE Force=0),DERIVED     ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED ! Method added to host embed code
SetAlerts              PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
BR1                  CLASS(EXTSQLB)
RunForm                PROCEDURE(Byte in_Request),DERIVED  ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseBancos')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?btnFechar,RequestCancelled)             ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?btnFechar,RequestCompleted)             ! Add the close control to the window manger
  END
  COMPILE('C55',_C55_)
    Self.Filesopened = 1
  C55
  OMIT('C55',_C55_)
    Filesopened = 1
  C55
  Relate:bancos.Open()
  Access:bancos.UseFile()
  SELF.Open(Window)                                        ! Open window
  BR1.RefreshHotkey = F5Key
  BR1.Setlistcontrol (?lstBancos,10)
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>BrowseBancos<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>BrowseBancos<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  If Self.Request = SelectRecord then BR1.Selecting = 1.
  BR1.PopUpActive = True
  BR1.Init(Access:bancos)
  BR1.useansijoin=1
  BR1.MaintainProcedure = 'UpdateBancos'
  BR1.SetupdateButtons (?btnIncluir,?btnAlterar,?btnExcluir,0)
  BR1.Selectcontrol = ?btnSelecionar
  if Self.Request = Selectrecord
    Unhide(?btnSelecionar)
  else
    Disable(?btnSelecionar)  !Needed if using Toolbar
  end
  SELF.SetAlerts()
  ?lstBancos{PROP:From} = Queue:RADSQLBrowse
  BR1.AddTable('bancos',1,0,'bancos')  ! Add the table to the list of tables
  BR1.Listqueue      &= Queue:RADSQLBrowse
  BR1.Position       &= Queue:RADSQLBrowse.ViewPosition
  BR1.ColumnCharAsc   = ''
  BR1.ColumnCharDes   = ''
  BR1.AddFieldpairs(Queue:RADSQLBrowse.BAN:id,BAN:id,'bancos','id','N',0,'B','A','M','N',1,4)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.BAN:numerobanco,BAN:numerobanco,'bancos','numerobanco','N',0,'B','A','M','N',2,1)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.BAN:nomedobanco,BAN:nomedobanco,'bancos','nomedobanco','A',0,'B','A','I','N',3,2)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.BAN:sigladobanco,BAN:sigladobanco,'bancos','sigladobanco','A',0,'B','A','M','N',4,3)
  BR1.Managedview &=BR1:View
  BR1.Openview()
  BR1.SetFileLoad()
  BR1.SetForceFetch (1)
  BR1.SetColColor = 0
  BR1.Resetsort(1)
  EnterByTabManager.Init(False)
  BR1.RefreshMethod = 1
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  COMPILE('C55',_C55_)
  if Self.filesopened
  C55
  OMIT('C55',_C55_)
  If filesopened then
  C55
    Relate:bancos.Close
  END
  END
  if  BR1.Response = RequestCompleted then Globalresponse = RequestCompleted; ReturnValue = RequestCompleted.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF Window{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
     If force = 1  then
        BR1.ResetBrowse (1)
     end


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF BR1.Response <> 0 then ReturnValue = BR1.Response.
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF BR1.Response <> 0 then ReturnValue = BR1.Response.
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  BR1.SetAlerts()


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
  BR1.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('BrowseBancos')
          End
        End
      End
      Close(HWGroups)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BR1.RunForm PROCEDURE(Byte in_Request)


  CODE
     GlobalRequest = in_Request
  PARENT.RunForm(in_Request)
     if in_Request = ChangeRecord or in_Request = DeleteRecord or in_Request = ViewRecord
        if Access:bancos.Fetch(BAN:ban_pk_id) <> Level:Benign ! Fetch bancos on key 
          Message('Error on primary key fetch BAN:ban_pk_id|Error:'& Error() &' FileError:'& FileError() &'|SQL:'& bancos{Prop:SQL})
          GlobalResponse = RequestCancelled
          Return
        end
     else
     end
  UpdateBancos 
     BR1.Response = Globalresponse
  
   BR1.ListControl{Prop:Selected} = BR1.CurrentChoice
   Display

UpdateBancos PROCEDURE                                     ! Generated from procedure template - Window

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
History::BAN:Record  LIKE(BAN:RECORD),THREAD
QuickWindow          WINDOW('i-Money'),AT(,,306,70),FONT('Tahoma',8,COLOR:Black,FONT:regular),CENTER,IMM,ICON('principal.ico'),HLP('UpdateBancos'),SYSTEM,GRAY,DOUBLE,AUTO
                       IMAGE('window.ico'),AT(2,1),USE(?Image1)
                       PROMPT('Bancos'),AT(26,5),USE(?Prompt4),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(2,21,302,2),USE(?Panel1)
                       PROMPT('Banco:'),AT(3,25),USE(?BAN:numerobanco:Prompt),TRN
                       ENTRY(@n03b),AT(3,35,43,10),USE(BAN:numerobanco),FLAT,MSG('N�mero do banco'),TIP('N�mero do banco'),REQ
                       PROMPT('Nome:'),AT(51,25,22,10),USE(?BAN:nomedobanco:Prompt),TRN
                       ENTRY(@s50),AT(51,35,204,10),USE(BAN:nomedobanco),FLAT,MSG('Nome do banco'),TIP('Nome do banco'),REQ
                       PROMPT('Sigla:'),AT(260,25,22,10),USE(?BAN:sigladobanco:Prompt),TRN
                       ENTRY(@s5),AT(260,35,43,10),USE(BAN:sigladobanco),FLAT,MSG('Sigla do banco'),TIP('Sigla do banco')
                       PANEL,AT(2,48,302,2),USE(?Panel1:2)
                       BUTTON('&Gravar'),AT(189,53,55,15),USE(?btnGravar),FLAT,LEFT,MSG('Grava o registro'),TIP('Grava o registro'),ICON('gravar.ico'),DEFAULT
                       BUTTON('&Cancelar'),AT(248,53,55,15),USE(?btnCancelar),FLAT,LEFT,MSG('Cancela a opera��o'),TIP('Cancela a opera��o'),ICON('cancelar.ico')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'Visualizando'
  OF InsertRecord
    ActionMessage = 'Incluindo'
  OF ChangeRecord
    ActionMessage = 'Alterando'
  END
  QuickWindow{Prop:StatusText,2} = ActionMessage           ! Display status message in status bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateBancos')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(BAN:Record,History::BAN:Record)
  SELF.AddHistoryField(?BAN:numerobanco,2)
  SELF.AddHistoryField(?BAN:nomedobanco,3)
  SELF.AddHistoryField(?BAN:sigladobanco,4)
  SELF.AddUpdateFile(Access:bancos)
  SELF.AddItem(?btnCancelar,RequestCancelled)              ! Add the cancel control to the window manager
  Relate:bancos.Open                                       ! File bancos used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:bancos
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?btnGravar
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>UpdateBancos<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>UpdateBancos<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?BAN:numerobanco{PROP:ReadOnly} = True
    ?BAN:nomedobanco{PROP:ReadOnly} = True
    ?BAN:sigladobanco{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:bancos.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?btnGravar
      SetNullFields(bancos)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?btnGravar
      ThisWindow.Update
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('UpdateBancos')
          End
        End
      End
      Close(HWGroups)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

