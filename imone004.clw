

   MEMBER('imoney.clw')                                    ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('radsqlb.inc'),ONCE
   INCLUDE('radsqldr.inc'),ONCE

                     MAP
                       INCLUDE('IMONE004.INC'),ONCE        !Local module procedure declarations
                     END


BrowseContas PROCEDURE                                     ! Generated from procedure template - Window

qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
BR1:View             VIEW(contas)
                       PROJECT(CON:id)
                       PROJECT(CON:agenciabancaria)
                       PROJECT(CON:numeroconta)
                       PROJECT(CON:descricaoconta)
                       PROJECT(CON:idempresa)
                       PROJECT(CON:numerodobanco)
                       JOIN(EMP:emp_codempresa,CON:idempresa),INNER
                         PROJECT(EMP:razaosocial)
                         PROJECT(EMP:codempresa)
                       END
                       JOIN(BAN:ban_numerodobanco,CON:numerodobanco),INNER
                         PROJECT(BAN:nomedobanco)
                         PROJECT(BAN:numerobanco)
                       END
                     END
Queue:RADSQLBrowse   QUEUE                            !Queue declaration for browse/combo box using ?lstContas
CON:id                 LIKE(CON:id)                   !List box control field - type derived from field
BAN:nomedobanco        LIKE(BAN:nomedobanco)          !List box control field - type derived from field
CON:agenciabancaria    LIKE(CON:agenciabancaria)      !List box control field - type derived from field
CON:numeroconta        LIKE(CON:numeroconta)          !List box control field - type derived from field
CON:descricaoconta     LIKE(CON:descricaoconta)       !List box control field - type derived from field
EMP:razaosocial        LIKE(EMP:razaosocial)          !List box control field - type derived from field
EMP:codempresa         LIKE(EMP:codempresa)           !Related join file key field - type derived from field
BAN:numerobanco        LIKE(BAN:numerobanco)          !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Window               WINDOW('i-Money'),AT(,,398,223),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('principal.ico'),SYSTEM,GRAY,DOUBLE,AUTO
                       IMAGE('window.ico'),AT(3,2),USE(?Image1)
                       PROMPT('Contas'),AT(28,5),USE(?Prompt1),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(2,22,394,2),USE(?Panel1)
                       LIST,AT(3,26,391,154),USE(?lstContas),IMM,FLAT,VSCROLL,MSG('Scrolling records...'),FORMAT('[39R(2)F~ID~C(0)@n7.`0b@205L(2)F~Banco~@s50@45L(2)F~Ag�ncia~@s10@80L(2)F~Conta~@' &|
   's20@/206L(2)_F@s50@200L(2)_F@s50@]F'),FROM(Queue:RADSQLBrowse)
                       BUTTON('&Selecionar'),AT(4,183,60,15),USE(?btnSelecionar),TRN,FLAT,HIDE,LEFT,ICON('selecionar.ico')
                       BUTTON('&Incluir'),AT(239,183,50,15),USE(?btnIncluir),TRN,FLAT,LEFT,ICON('incluir.ico')
                       BUTTON('&Alterar'),AT(292,183,50,15),USE(?btnAlterar),TRN,FLAT,LEFT,ICON('alterar.ico')
                       BUTTON('&Excluir'),AT(344,183,50,15),USE(?btnExcluir),TRN,FLAT,LEFT,ICON('excluir.ico')
                       PANEL,AT(2,200,394,2),USE(?Panel1:2)
                       BUTTON('&Fechar'),AT(344,205,50,15),USE(?btnFechar),TRN,FLAT,LEFT,ICON('fechar.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Reset                  PROCEDURE(BYTE Force=0),DERIVED     ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED ! Method added to host embed code
SetAlerts              PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
BR1                  CLASS(EXTSQLB)
RunForm                PROCEDURE(Byte in_Request),DERIVED  ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseContas')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?btnFechar,RequestCancelled)             ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?btnFechar,RequestCompleted)             ! Add the close control to the window manger
  END
  COMPILE('C55',_C55_)
    Self.Filesopened = 1
  C55
  OMIT('C55',_C55_)
    Filesopened = 1
  C55
  Relate:contas.Open()
  Access:contas.UseFile()
  Relate:empresa.Open()
  Access:empresa.UseFile()
  Relate:bancos.Open()
  Access:bancos.UseFile()
  SELF.Open(Window)                                        ! Open window
  BR1.RefreshHotkey = F5Key
  BR1.Setlistcontrol (?lstContas,10)
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>BrowseContas<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>BrowseContas<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  If Self.Request = SelectRecord then BR1.Selecting = 1.
  BR1.PopUpActive = True
  BR1.Init(Access:contas)
  BR1.useansijoin=1
  BR1.MaintainProcedure = 'UpdateContas'
  BR1.SetupdateButtons (?btnIncluir,?btnAlterar,?btnExcluir,0)
  BR1.Selectcontrol = ?btnSelecionar
  if Self.Request = Selectrecord
    Unhide(?btnSelecionar)
  else
    Disable(?btnSelecionar)  !Needed if using Toolbar
  end
  SELF.SetAlerts()
  ?lstContas{PROP:From} = Queue:RADSQLBrowse
  BR1.AddTable('contas',1,0,'contas')  ! Add the table to the list of tables
    BR1.AddTable('empresa',2,0,'empresa')
    BR1.AddTable('bancos',3,0,'bancos')
  BR1.Listqueue      &= Queue:RADSQLBrowse
  BR1.Position       &= Queue:RADSQLBrowse.ViewPosition
  BR1.ColumnCharAsc   = ''
  BR1.ColumnCharDes   = ''
  BR1.AddFieldpairs(Queue:RADSQLBrowse.CON:id,CON:id,'contas','id','N',0,'B','A','M','N',1,1)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.CON:descricaoconta,CON:descricaoconta,'contas','descricaoconta','A',0,'B','A','M','N',2,5)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.CON:agenciabancaria,CON:agenciabancaria,'contas','agenciabancaria','A',0,'B','A','M','N',4,3)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.CON:numeroconta,CON:numeroconta,'contas','numeroconta','A',0,'B','A','M','N',5,4)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.EMP:codempresa,EMP:codempresa,'empresa','codempresa','N',0,'B','A','M','N',1,7)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.EMP:razaosocial,EMP:razaosocial,'empresa','razaosocial','A',0,'B','A','M','N',2,6)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.BAN:numerobanco,BAN:numerobanco,'bancos','numerobanco','N',0,'B','A','M','N',2,8)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.BAN:nomedobanco,BAN:nomedobanco,'bancos','nomedobanco','A',0,'B','A','M','N',3,2)
  BR1.AddJoinFields('contas','idempresa','empresa','codempresa',BR1:view{prop:inner,1},0,EMP:emp_codempresa)
  BR1.Addviewfield('contas','idempresa',9)   ! Not in queue from CON:idempresa
  BR1.AddJoinFields('contas','numerodobanco','bancos','numerobanco',BR1:view{prop:inner,2},0,BAN:ban_numerodobanco)
  BR1.Addviewfield('contas','numerodobanco',3)   ! Not in queue from CON:numerodobanco
  BR1.Managedview &=BR1:View
  BR1.Openview()
  BR1.SetFileLoad()
  BR1.SetForceFetch (1)
  BR1.SetColColor = 0
  BR1.Resetsort(1)
  EnterByTabManager.Init(False)
  BR1.RefreshMethod = 1
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  COMPILE('C55',_C55_)
  if Self.filesopened
  C55
  OMIT('C55',_C55_)
  If filesopened then
  C55
    Relate:contas.Close
    Relate:empresa.Close
    Relate:bancos.Close
  END
  END
  if  BR1.Response = RequestCompleted then Globalresponse = RequestCompleted; ReturnValue = RequestCompleted.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF Window{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
     If force = 1  then
        BR1.ResetBrowse (1)
     end


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF BR1.Response <> 0 then ReturnValue = BR1.Response.
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF BR1.Response <> 0 then ReturnValue = BR1.Response.
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  BR1.SetAlerts()


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
  BR1.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('BrowseContas')
          End
        End
      End
      Close(HWGroups)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BR1.RunForm PROCEDURE(Byte in_Request)


  CODE
     GlobalRequest = in_Request
  PARENT.RunForm(in_Request)
     if in_Request = ChangeRecord or in_Request = DeleteRecord or in_Request = ViewRecord
        if Access:contas.Fetch(CON:con_pk_id) <> Level:Benign ! Fetch contas on key 
          Message('Error on primary key fetch CON:con_pk_id|Error:'& Error() &' FileError:'& FileError() &'|SQL:'& contas{Prop:SQL})
          GlobalResponse = RequestCancelled
          Return
        end
     else
     end
  UpdateContas 
     BR1.Response = Globalresponse
  
   BR1.ListControl{Prop:Selected} = BR1.CurrentChoice
   Display
  If Self.Request = InsertRecord And Self.Response = RequestCompleted
    Post(Event:ScrollBottom,?lstContas)
    Display(?lstContas)
  End

UpdateContas PROCEDURE                                     ! Generated from procedure template - Window

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
RADSQLDrop6Locator CString(256)
RADSQLDrop6:View     VIEW(bancos)
                       PROJECT(BAN:nomedobanco)
                       PROJECT(BAN:numerobanco)
                       PROJECT(BAN:id)
                     END
Queue:RADSQLDrop     QUEUE                            !Queue declaration for browse/combo box using ?BAN:nomedobanco
BAN:nomedobanco        LIKE(BAN:nomedobanco)          !List box control field - type derived from field
BAN:numerobanco        LIKE(BAN:numerobanco)          !Browse hot field - type derived from field
BAN:id                 LIKE(BAN:id)                   !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
RADSQLDrop7Locator CString(256)
RADSQLDrop7:View     VIEW(empresa)
                       PROJECT(EMP:razaosocial)
                       PROJECT(EMP:codempresa)
                     END
Queue:RADSQLDrop:1   QUEUE                            !Queue declaration for browse/combo box using ?EMP:razaosocial
EMP:razaosocial        LIKE(EMP:razaosocial)          !List box control field - type derived from field
EMP:codempresa         LIKE(EMP:codempresa)           !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::CON:Record  LIKE(CON:RECORD),THREAD
QuickWindow          WINDOW('i-Money'),AT(,,207,140),FONT('Tahoma',8,COLOR:Black,FONT:regular),CENTER,IMM,HLP('UpdateContas'),SYSTEM,GRAY,DOUBLE,AUTO
                       IMAGE('window.ico'),AT(3,2),USE(?Image1)
                       PROMPT('Contas'),AT(28,5),USE(?Prompt5),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(2,22,203,2),USE(?Panel1)
                       PROMPT('Banco:'),AT(4,27),USE(?Prompt6),TRN
                       COMBO(@s255),AT(4,37,198,10),USE(BAN:nomedobanco),IMM,FLAT,VSCROLL,FORMAT('200L(2)F@s50@'),DROP(10),FROM(Queue:RADSQLDrop),MSG('Nome do banco')
                       PROMPT('Ag�ncia:'),AT(4,49),USE(?CON:agenciabancaria:Prompt),TRN
                       PROMPT('Conta:'),AT(59,49),USE(?CON:numeroconta:Prompt),TRN
                       ENTRY(@s10),AT(4,59,50,10),USE(CON:agenciabancaria),FLAT,MSG('Ag�ncia banc�ria, caso seja uma conta banc�ria'),TIP('Ag�ncia banc�ria, caso seja uma conta banc�ria')
                       ENTRY(@s20),AT(59,59,143,10),USE(CON:numeroconta),FLAT,MSG('N�mero da conta, caso seja uma ag�ncia banc�ria'),TIP('N�mero da conta, caso seja uma ag�ncia banc�ria')
                       PROMPT('Descri��o:'),AT(4,72),USE(?CON:descricaoconta:Prompt),TRN
                       ENTRY(@s50),AT(4,81,198,10),USE(CON:descricaoconta),FLAT,MSG('Descri��o da conta'),TIP('Descri��o da conta'),REQ
                       PROMPT('Empresa:'),AT(4,94),USE(?CON:descricaoconta:Prompt:2),TRN
                       COMBO(@s255),AT(4,103,198,10),USE(EMP:razaosocial),IMM,FLAT,VSCROLL,FORMAT('200L(2)F@s50@'),DROP(10),FROM(Queue:RADSQLDrop:1),MSG('Raz�o Social')
                       PANEL,AT(2,117,203,2),USE(?Panel1:2)
                       BUTTON('&Gravar'),AT(91,122,55,15),USE(?btnGravar),TRN,FLAT,LEFT,MSG('Grava o registro'),TIP('Grava o registro'),ICON('gravar.ico'),DEFAULT
                       BUTTON('&Cancelar'),AT(149,122,55,15),USE(?btnCancelar),TRN,FLAT,LEFT,MSG('Cancela a opera��o'),TIP('Cancela a opera��o'),ICON('cancelar.ico')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Reset                  PROCEDURE(BYTE Force=0),DERIVED     ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
SetAlerts              PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END

RADSQLDrop6          CLASS(RADSQLDrop)
GenerateSelectStatement PROCEDURE(),DERIVED                ! Method added to host embed code
                     END

RADSQLDrop7          CLASS(RADSQLDrop)
GenerateSelectStatement PROCEDURE(),DERIVED                ! Method added to host embed code
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'Visualizando'
  OF InsertRecord
    ActionMessage = 'Incluindo'
  OF ChangeRecord
    ActionMessage = 'Alterando'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateContas')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(CON:Record,History::CON:Record)
  SELF.AddHistoryField(?CON:agenciabancaria,4)
  SELF.AddHistoryField(?CON:numeroconta,5)
  SELF.AddHistoryField(?CON:descricaoconta,2)
  SELF.AddUpdateFile(Access:contas)
  SELF.AddItem(?btnCancelar,RequestCancelled)              ! Add the cancel control to the window manager
  COMPILE('C55',_C55_)
    Self.Filesopened = 1
  C55
  OMIT('C55',_C55_)
    Filesopened = 1
  C55
  Relate:bancos.Open()
  Access:bancos.UseFile()
  Relate:empresa.Open()
  Access:empresa.UseFile()
  Relate:contas.SetOpenRelated()
  Relate:contas.Open                                       ! File contas used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:contas
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?btnGravar
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>UpdateContas<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>UpdateContas<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    DISABLE(?BAN:nomedobanco)
    ?CON:agenciabancaria{PROP:ReadOnly} = True
    ?CON:numeroconta{PROP:ReadOnly} = True
    ?CON:descricaoconta{PROP:ReadOnly} = True
    DISABLE(?EMP:razaosocial)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  RADSQLDrop6.Listqueue      &= Queue:RADSQLDrop
  RADSQLDrop6.Position       &= Queue:RADSQLDrop.ViewPosition
  RADSQLDrop6.Setlistcontrol(?BAN:nomedobanco)
  RADSQLDrop6.RefreshHotkey = F5Key
  RADSQLDrop6.AddDropVariables(?BAN:nomedobanco,0,BAN:nomedobanco,'BAN:numerobanco',BAN:numerobanco,CON:numerodobanco)
  RADSQLDrop6.Init(Access:bancos)
  RADSQLDrop6.AddField(BAN:numerobanco,CON:numerodobanco)
     RADSQLDrop6.useansijoin=1
  RADSQLDrop7.Listqueue      &= Queue:RADSQLDrop:1
  RADSQLDrop7.Position       &= Queue:RADSQLDrop:1.ViewPosition
  RADSQLDrop7.Setlistcontrol(?EMP:razaosocial)
  RADSQLDrop7.RefreshHotkey = F5Key
  RADSQLDrop7.AddDropVariables(?EMP:razaosocial,0,EMP:razaosocial,'EMP:codempresa',EMP:codempresa,CON:idempresa)
  RADSQLDrop7.Init(Access:empresa)
  RADSQLDrop7.AddField(EMP:codempresa,CON:idempresa)
     RADSQLDrop7.useansijoin=1
  SELF.SetAlerts()
  ?BAN:nomedobanco{PROP:From} = Queue:RADSQLDrop
  RADSQLDrop6.AddTable('bancos',1,0,'BANCOS')  ! Add the table to the list of tables
  RADSQLDrop6.ColumnCharAsc   = ''
  RADSQLDrop6.ColumnCharDes   = ''
  RADSQLDrop6.AddFieldpairs(Queue:RADSQLDrop.BAN:id,BAN:id,'bancos','id | READONLY','N',0,'B','A','I','N',1,3)
  RADSQLDrop6.AddFieldpairs(Queue:RADSQLDrop.BAN:numerobanco,BAN:numerobanco,'bancos','numerobanco','N',0,'B','A','I','N',2,2)
  RADSQLDrop6.AddFieldpairs(Queue:RADSQLDrop.BAN:nomedobanco,BAN:nomedobanco,'bancos','nomedobanco','A',0,'A','A','I','N',3,1)
  RADSQLDrop6.Addindexfunction('UPPER(%)')
  RADSQLDrop6.Managedview &=RADSQLDrop6:View
  RADSQLDrop6.Openview
  RADSQLDrop6.SetFileLoad
  ?EMP:razaosocial{PROP:From} = Queue:RADSQLDrop:1
  RADSQLDrop7.AddTable('empresa',1,0,'EMPRESA')  ! Add the table to the list of tables
  RADSQLDrop7.ColumnCharAsc   = ''
  RADSQLDrop7.ColumnCharDes   = ''
  RADSQLDrop7.AddFieldpairs(Queue:RADSQLDrop:1.EMP:codempresa,EMP:codempresa,'empresa','codempresa | READONLY','N',0,'B','A','I','N',1,2)
  RADSQLDrop7.AddFieldpairs(Queue:RADSQLDrop:1.EMP:razaosocial,EMP:razaosocial,'empresa','razaosocial','A',0,'A','A',' ','N',2,1)
  RADSQLDrop7.Addindexfunction('UPPER(%)')
  RADSQLDrop7.Managedview &=RADSQLDrop7:View
  RADSQLDrop7.Openview
  RADSQLDrop7.SetFileLoad
  RADSQLDrop6.SetColColor = 0
  RADSQLDrop6.Resetsort(1,1)
  RADSQLDrop7.SetColColor = 0
  RADSQLDrop7.Resetsort(1,1)
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  COMPILE('C55',_C55_)
  if Self.filesopened
  C55
  OMIT('C55',_C55_)
  If filesopened then
  C55
  Relate:bancos.Close
  END
  COMPILE('C55',_C55_)
  if Self.filesopened
  C55
  OMIT('C55',_C55_)
  If filesopened then
  C55
  Relate:empresa.Close
  END
    Relate:contas.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
  If force = 2
    RADSQLDrop6.ResetBrowse (1)
  end
  If force = 2
    RADSQLDrop7.ResetBrowse (1)
  end


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  RADSQLDrop6.SetAlerts
  RADSQLDrop7.SetAlerts


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?btnGravar
      SetNullFields(contas)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?btnGravar
      ThisWindow.Update
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
  RADSQLDrop6.TakeEvent()
  RADSQLDrop7.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('UpdateContas')
          End
        End
      End
      Close(HWGroups)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


RADSQLDrop6.GenerateSelectStatement PROCEDURE


  CODE
  PARENT.GenerateSelectStatement
  Self.ViewSqlStatement = 'SELECT A.ID, A.NUMEROBANCO, A.NOMEDOBANCO FROM BANCOS A'


RADSQLDrop7.GenerateSelectStatement PROCEDURE


  CODE
  PARENT.GenerateSelectStatement
  self.viewsqlstatement = |
    'SELECT A.CODEMPRESA, A.RAZAOSOCIAL FROM EMPRESA A'

