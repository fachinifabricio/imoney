

   MEMBER('imoney.clw')                                    ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('radsqlb.inc'),ONCE

                     MAP
                       INCLUDE('IMONE006.INC'),ONCE        !Local module procedure declarations
                     END


BrowseFavorecidos PROCEDURE                                ! Generated from procedure template - Window

LOC:TipoBusca        BYTE(1)                               !
LOC:Busca            CSTRING(101)                          !
LOC:SQL              CSTRING(501)                          !
qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
BR1:View             VIEW(favorecidos)
                       PROJECT(FAV:id)
                       PROJECT(FAV:razaosocial)
                       PROJECT(FAV:nomefantasia)
                       PROJECT(FAV:tipocliente)
                       PROJECT(FAV:cpf_cnpj)
                       PROJECT(FAV:rg_ie)
                       PROJECT(FAV:endereco)
                       PROJECT(FAV:numero)
                       PROJECT(FAV:complemento)
                       PROJECT(FAV:cidade)
                       PROJECT(FAV:estado)
                       PROJECT(FAV:telefone1)
                       PROJECT(FAV:telefone2)
                       PROJECT(FAV:telefone3)
                     END
Queue:RADSQLBrowse   QUEUE                            !Queue declaration for browse/combo box using ?lstFavorecidos
FAV:id                 LIKE(FAV:id)                   !List box control field - type derived from field
FAV:razaosocial        LIKE(FAV:razaosocial)          !List box control field - type derived from field
FAV:nomefantasia       LIKE(FAV:nomefantasia)         !List box control field - type derived from field
FAV:tipocliente        LIKE(FAV:tipocliente)          !List box control field - type derived from field
FAV:cpf_cnpj           LIKE(FAV:cpf_cnpj)             !Browse hot field - type derived from field
FAV:rg_ie              LIKE(FAV:rg_ie)                !Browse hot field - type derived from field
FAV:endereco           LIKE(FAV:endereco)             !Browse hot field - type derived from field
FAV:numero             LIKE(FAV:numero)               !Browse hot field - type derived from field
FAV:complemento        LIKE(FAV:complemento)          !Browse hot field - type derived from field
FAV:cidade             LIKE(FAV:cidade)               !Browse hot field - type derived from field
FAV:estado             LIKE(FAV:estado)               !Browse hot field - type derived from field
FAV:telefone1          LIKE(FAV:telefone1)            !Browse hot field - type derived from field
FAV:telefone2          LIKE(FAV:telefone2)            !Browse hot field - type derived from field
FAV:telefone3          LIKE(FAV:telefone3)            !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Window               WINDOW('i-Money'),AT(,,452,328),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('principal.ico'),SYSTEM,GRAY,DOUBLE,AUTO
                       IMAGE('window.ico'),AT(2,2),USE(?Image1)
                       PROMPT('Clientes e Fornecedores'),AT(26,5),USE(?Prompt1),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(2,22,448,2),USE(?Panel1)
                       LIST,AT(3,46,446,115),USE(?lstFavorecidos),IMM,FLAT,VSCROLL,MSG('Scrolling records...'),FORMAT('26R(5)_F~C�digo~C(0)@n5.`0@181L(2)_F~Raz�o~@s50@180L(2)_F~Fantasia~@s50@4C_F~Tip' &|
   'o~@s1@'),FROM(Queue:RADSQLBrowse)
                       BUTTON('&Selecionar'),AT(3,167,60,15),USE(?btnSelecionar),TRN,FLAT,HIDE,LEFT,ICON('selecionar.ico')
                       BUTTON('&Incluir'),AT(291,167,50,15),USE(?btnIncluir),TRN,FLAT,LEFT,ICON('incluir.ico')
                       BUTTON('&Alterar'),AT(345,167,50,15),USE(?btnAlterar),TRN,FLAT,LEFT,ICON('alterar.ico')
                       BUTTON('&Excluir'),AT(397,167,50,15),USE(?btnExcluir),TRN,FLAT,LEFT,ICON('excluir.ico')
                       PANEL,AT(2,184,446,2),USE(?Panel1:2)
                       PROMPT('Nome Fantasia:'),AT(5,189),USE(?FAV:nomefantasia:Prompt)
                       ENTRY(@s200),AT(4,198,199,10),USE(FAV:nomefantasia),FLAT,READONLY
                       PROMPT('CPF/CNPJ:'),AT(207,189),USE(?FAV:cpf_cnpj:Prompt),TRN,RIGHT
                       ENTRY(@s18),AT(207,198,119,10),USE(FAV:cpf_cnpj),SKIP,FLAT,FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('CPF/CNPJ do cliente/fornecedor'),TIP('CPF/CNPJ do cliente/fornecedor'),READONLY
                       PROMPT('RG/IE:'),AT(329,189),USE(?FAV:rg_ie:Prompt),TRN
                       ENTRY(@s18),AT(329,198,119,10),USE(FAV:rg_ie),SKIP,FLAT,FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('RG/IE do cliente/fornecedor'),TIP('RG/IE do cliente/fornecedor'),READONLY
                       PROMPT('Endere�o:'),AT(5,211,35,10),USE(?FAV:endereco:Prompt),TRN
                       ENTRY(@s60),AT(4,221,271,10),USE(FAV:endereco),SKIP,FLAT,FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('Endere�o do cliente/fornecedor'),TIP('Endere�o do cliente/fornecedor'),READONLY
                       PROMPT('N�mero:'),AT(279,211),USE(?FAV:numero:Prompt),TRN
                       ENTRY(@s15),AT(279,221,62,10),USE(FAV:numero),SKIP,FLAT,FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('N�mero do cliente/fornecedor'),TIP('N�mero do cliente/fornecedor'),READONLY
                       PROMPT('CEP:'),AT(344,211),USE(?FAV:cep:Prompt),TRN,LEFT
                       PROMPT('Bairro:'),AT(4,235),USE(?FAV:bairro:Prompt),TRN,LEFT
                       ENTRY(@s30),AT(4,245,138,10),USE(FAV:bairro),SKIP,FLAT,LEFT,UPR,READONLY
                       ENTRY(@s9),AT(344,221,104,10),USE(FAV:cep),SKIP,FLAT,RIGHT,READONLY
                       PROMPT('Complemento:'),AT(153,235),USE(?FAV:complemento:Prompt),TRN
                       ENTRY(@s30),AT(151,245,119,10),USE(FAV:complemento),SKIP,FLAT,FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('Complemento do cliente/fornecedor'),TIP('Complemento do cliente/fornecedor'),READONLY
                       PROMPT('Cidade:'),AT(279,235),USE(?FAV:cidade:Prompt),TRN
                       ENTRY(@s30),AT(279,245,119,10),USE(FAV:cidade),SKIP,FLAT,FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('Cidade do cliente/fornecedor'),TIP('Cidade do cliente/fornecedor'),READONLY
                       PROMPT('Estado:'),AT(408,235),USE(?FAV:estado:Prompt),TRN
                       ENTRY(@s2),AT(407,245,40,10),USE(FAV:estado),SKIP,FLAT,FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('Estado do cliente/fornecedor'),TIP('Estado do cliente/fornecedor'),READONLY
                       PROMPT('Telefones:'),AT(4,258),USE(?FAV:telefone1:Prompt),TRN
                       ENTRY(@s20),AT(4,267,109,10),USE(FAV:telefone1),SKIP,FLAT,FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('Telefone do cliente/fornecedor'),TIP('Telefone do cliente/fornecedor'),READONLY
                       ENTRY(@s20),AT(117,267,110,10),USE(FAV:telefone2),SKIP,FLAT,FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('Telefone do cliente/fornecedor'),TIP('Telefone do cliente/fornecedor'),READONLY
                       ENTRY(@s20),AT(232,267,109,10),USE(FAV:telefone3),SKIP,FLAT,FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('Telefone do cliente/fornecedor'),TIP('Telefone do cliente/fornecedor'),READONLY
                       PROMPT('Website:'),AT(4,281),USE(?FAV:url:Prompt)
                       PROMPT('Email:'),AT(230,281),USE(?FAV:email:Prompt)
                       ENTRY(@s200),AT(230,291,218,10),USE(FAV:email),FLAT,READONLY
                       ENTRY(@s200),AT(4,291,218,10),USE(FAV:url),FLAT,READONLY
                       PANEL,AT(2,305,446,2),USE(?Panel1:3)
                       BUTTON('&Fechar'),AT(397,310,50,15),USE(?btnFechar),TRN,FLAT,LEFT,ICON('fechar.ico')
                       ENTRY(@s100),AT(111,28,319,10),USE(LOC:Busca),FLAT
                       SHEET,AT(3,27,445,15),USE(?Sheet1)
                         TAB('Raz�o'),USE(?Tab2)
                         END
                         TAB('Fantasia'),USE(?Tab3)
                         END
                         TAB('C�digo'),USE(?Tab1)
                         END
                       END
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Reset                  PROCEDURE(BYTE Force=0),DERIVED     ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED ! Method added to host embed code
SetAlerts              PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
BR1                  CLASS(EXTSQLB)
RunForm                PROCEDURE(Byte in_Request),DERIVED  ! Method added to host embed code
SetRange               PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeEvent              PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseFavorecidos')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?btnFechar,RequestCancelled)             ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?btnFechar,RequestCompleted)             ! Add the close control to the window manger
  END
  COMPILE('C55',_C55_)
    Self.Filesopened = 1
  C55
  OMIT('C55',_C55_)
    Filesopened = 1
  C55
  Relate:favorecidos.Open()
  Access:favorecidos.UseFile()
  SELF.Open(Window)                                        ! Open window
  COMPILE ('**CW7**',_CWVER_=7000)
  ?Sheet1{PROP:TabSheetStyle} = TabStyle:Boxed
  !**CW7**
  BR1.RefreshHotkey = F5Key
  BR1.Setlistcontrol (?lstFavorecidos,10)
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>BrowseFavorecidos<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>BrowseFavorecidos<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  If Self.Request = SelectRecord then BR1.Selecting = 1.
  BR1.PopUpActive = True
  BR1.Init(Access:favorecidos)
  BR1.useansijoin=1
  BR1.MaintainProcedure = 'UpdateFavorecidos'
  BR1.SetupdateButtons (?btnIncluir,?btnAlterar,?btnExcluir,0)
  BR1.Selectcontrol = ?btnSelecionar
  if Self.Request = Selectrecord
    Unhide(?btnSelecionar)
  else
    Disable(?btnSelecionar)  !Needed if using Toolbar
  end
  SELF.SetAlerts()
  ?lstFavorecidos{PROP:From} = Queue:RADSQLBrowse
  BR1.AddTable('favorecidos',1,0,'favorecidos')  ! Add the table to the list of tables
  BR1.Listqueue      &= Queue:RADSQLBrowse
  BR1.Position       &= Queue:RADSQLBrowse.ViewPosition
  BR1.ColumnCharAsc   = ''
  BR1.ColumnCharDes   = ''
  BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:id,FAV:id,'favorecidos','id','N',0,'B','A','M','N',1,1)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:nomefantasia,FAV:nomefantasia,'favorecidos','nomefantasia','A',0,'B','A','M','N',2,3)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:razaosocial,FAV:razaosocial,'favorecidos','razaosocial','A',0,'B','A','M','N',3,2)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:tipocliente,FAV:tipocliente,'favorecidos','tipocliente','A',0,'B','A','M','N',4,4)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:cpf_cnpj,FAV:cpf_cnpj,'favorecidos','cpf_cnpj','A',0,'B','A','M','N',5,5)
  !BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:cpf_cnpj,FAV:cpf_cnpj,'','','A',1,'N','N','M','N',5,0) ! Hotfield
  BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:rg_ie,FAV:rg_ie,'favorecidos','rg_ie','A',0,'B','A','M','N',6,6)
  !BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:rg_ie,FAV:rg_ie,'','','A',1,'N','N','M','N',6,0) ! Hotfield
  BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:endereco,FAV:endereco,'favorecidos','endereco','A',0,'B','A','M','N',7,7)
  !BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:endereco,FAV:endereco,'','','A',1,'N','N','M','N',7,0) ! Hotfield
  BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:numero,FAV:numero,'favorecidos','numero','A',0,'B','A','M','N',8,8)
  !BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:numero,FAV:numero,'','','A',1,'N','N','M','N',8,0) ! Hotfield
  BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:complemento,FAV:complemento,'favorecidos','complemento','A',0,'B','A','M','N',9,9)
  !BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:complemento,FAV:complemento,'','','A',1,'N','N','M','N',9,0) ! Hotfield
  BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:cidade,FAV:cidade,'favorecidos','cidade','A',0,'B','A','M','N',10,10)
  !BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:cidade,FAV:cidade,'','','A',1,'N','N','M','N',10,0) ! Hotfield
  BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:estado,FAV:estado,'favorecidos','estado','A',0,'B','A','M','N',11,11)
  !BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:estado,FAV:estado,'','','A',1,'N','N','M','N',11,0) ! Hotfield
  BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:telefone1,FAV:telefone1,'favorecidos','telefone1','A',0,'B','A','M','N',12,12)
  !BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:telefone1,FAV:telefone1,'','','A',1,'N','N','M','N',12,0) ! Hotfield
  BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:telefone2,FAV:telefone2,'favorecidos','telefone2','A',0,'B','A','M','N',13,13)
  !BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:telefone2,FAV:telefone2,'','','A',1,'N','N','M','N',13,0) ! Hotfield
  BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:telefone3,FAV:telefone3,'favorecidos','telefone3','A',0,'B','A','M','N',14,14)
  !BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:telefone3,FAV:telefone3,'','','A',1,'N','N','M','N',14,0) ! Hotfield
  BR1.Managedview &=BR1:View
  BR1.Openview()
  BR1.SetFileLoad()
  BR1.SetForceFetch (1)
  BR1.SetColColor = 0
       BR1.Resetsort(BR1.GetColumnNr ('favorecidos','razaosocial'))
  EnterByTabManager.Init(False)
  BR1.RefreshMethod = 1
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  COMPILE('C55',_C55_)
  if Self.filesopened
  C55
  OMIT('C55',_C55_)
  If filesopened then
  C55
    Relate:favorecidos.Close
  END
  END
  if  BR1.Response = RequestCompleted then Globalresponse = RequestCompleted; ReturnValue = RequestCompleted.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF Window{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
     If force = 1  then
        BR1.ResetBrowse (1)
     end


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF BR1.Response <> 0 then ReturnValue = BR1.Response.
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF BR1.Response <> 0 then ReturnValue = BR1.Response.
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  BR1.SetAlerts()


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LOC:Busca
      If LOC:Busca
        BR1.ResetBrowse(1)
      End
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
  BR1.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      IF choice(?sheet1) = 1
        BR1.Resetsort(BR1.GetColumnNr ('favorecidos','razaosocial'))
      ELSIF choice(?sheet1) = 2
        BR1.Resetsort(BR1.GetColumnNr ('favorecidos','nomefantasia'))
      ELSIF choice(?sheet1) = 3
        BR1.Resetsort(BR1.GetColumnNr ('favorecidos','id'))
      ELSE
        BR1.Resetsort(BR1.GetColumnNr ('favorecidos','razaosocial'))
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('BrowseFavorecidos')
          End
        End
      End
      Close(HWGroups)
    OF EVENT:OpenWindow
      select(?LOC:Busca)
      display
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BR1.RunForm PROCEDURE(Byte in_Request)


  CODE
     GlobalRequest = in_Request
  PARENT.RunForm(in_Request)
     if in_Request = ChangeRecord or in_Request = DeleteRecord or in_Request = ViewRecord
        if Access:favorecidos.Fetch(FAV:cli_pk_id) <> Level:Benign ! Fetch favorecidos on key FAV:cli_razaosocial
          Message('Error on primary key fetch FAV:cli_pk_id|Error:'& Error() &' FileError:'& FileError() &'|SQL:'& favorecidos{Prop:SQL})
          GlobalResponse = RequestCancelled
          Return
        end
     else
       IF    choice(?sheet1) = 1
       ELSIF choice(?sheet1) = 2
       ELSIF choice(?sheet1) = 3
       END
     end
  UpdateFavorecidos 
     BR1.Response = Globalresponse
  
   BR1.ListControl{Prop:Selected} = BR1.CurrentChoice
   Display
  If Self.Request = InsertRecord And Self.Response = RequestCompleted
    Select(?Tab2)
    Post(Event:ScrollBottom,?lstFavorecidos)
    Display(?lstFavorecidos)
  End


BR1.SetRange PROCEDURE


  CODE
  IF choice(?sheet1) = 1
    SELF.Rangefilter = ''
    SELF.OrderByClause = SELF.GetAlias('favorecidos') & '.razaosocial ASC'              
    SELF.SetFilter ('')
  ELSIF choice(?sheet1) = 2
    SELF.Rangefilter = ''
    SELF.OrderByClause = SELF.GetAlias('favorecidos') & '.nomefantasia ASC'              
    SELF.SetFilter ('')
  ELSIF choice(?sheet1) = 3
    SELF.Rangefilter = ''
    SELF.OrderByClause = SELF.GetAlias('favorecidos') & '.id ASC'              
    SELF.SetFilter ('')
  ELSE
    SELF.RangeFilter = ''
    SELF.OrderByClause = SELF.GetAlias('favorecidos') & '.razaosocial ASC'                  
    SELF.SetFilter ('')
  END
  PARENT.SetRange
  Self.RangeFilter = ''
  If LOC:Busca
    If Choice(?Sheet1) = 1
      LOC:SQL = 'to_ascii(lower(a.razaosocial)) ilike <39>%'&clip(LOC:Busca)&'%<39>'
    ElsIf Choice(?Sheet1) = 2
      LOC:SQL = 'to_ascii(lower(a.nomefantasia)) ilike <39>%'&clip(LOC:Busca)&'%<39>'
    Else
      a# = LOC:Busca
      LOC:SQL = 'a.id = '&a#
    End
  else
    clear(LOC:SQL)
  End
  
  Self.RangeFilter = Self.RangeFilter & Clip(LOC:SQL)


BR1.TakeEvent PROCEDURE


  CODE
  ! in case that tabs are used to sort and filter data in the browse
  CASE EVENT()
  OF Event:AlertKey
    IF KEYCODE() = MouseLeft2 AND ?lstFavorecidos{PROPLIST:MouseDownRow} = 0
      RETURN
    END
  END
  PARENT.TakeEvent

UpdateFavorecidos PROCEDURE                                ! Generated from procedure template - Window

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
History::FAV:Record  LIKE(FAV:RECORD),THREAD
QuickWindow          WINDOW('i-Money'),AT(,,325,264),FONT('Tahoma',8,COLOR:Black,FONT:regular),CENTER,IMM,ICON('principal.ico'),HLP('UpdateFavorecidos'),SYSTEM,GRAY,DOUBLE,AUTO
                       ENTRY(@s60),AT(3,35,318,10),USE(FAV:razaosocial),FLAT,SCROLL,MSG('Raz�o social do cliente/fornecedor'),TIP('Raz�o social do cliente/fornecedor'),REQ
                       ENTRY(@s200),AT(3,57,318,10),USE(FAV:nomefantasia),FLAT
                       LIST,AT(3,78,65,10),USE(FAV:tipocliente),FLAT,VSCROLL,LEFT(1),MSG('Tipo de cliente (F-F�sico J-Jur�dico)'),TIP('Tipo de cliente (F-F�sico J-Jur�dico)'),DROP(5),FROM('F�SICA|#F|JUR�DICA|#J')
                       ENTRY(@s18),AT(73,78,122,10),USE(FAV:cpf_cnpj),FLAT,MSG('CPF/CNPJ do cliente/fornecedor'),TIP('CPF/CNPJ do cliente/fornecedor')
                       ENTRY(@s18),AT(199,78,122,10),USE(FAV:rg_ie),FLAT,MSG('RG/IE do cliente/fornecedor'),TIP('RG/IE do cliente/fornecedor')
                       ENTRY(@s60),AT(3,101,249,10),USE(FAV:endereco),FLAT,SCROLL,MSG('Endere�o do cliente/fornecedor'),TIP('Endere�o do cliente/fornecedor')
                       ENTRY(@s15),AT(257,101,64,10),USE(FAV:numero),FLAT,MSG('N�mero do cliente/fornecedor'),TIP('N�mero do cliente/fornecedor')
                       ENTRY(@p#####-###pb),AT(3,123,101,10),USE(FAV:cep),FLAT,RIGHT
                       ENTRY(@s30),AT(109,123,211,10),USE(FAV:bairro),FLAT,LEFT,UPR
                       ENTRY(@s30),AT(3,146,153,10),USE(FAV:complemento),FLAT,SCROLL,MSG('Complemento do cliente/fornecedor'),TIP('Complemento do cliente/fornecedor')
                       ENTRY(@s30),AT(161,146,124,10),USE(FAV:cidade),FLAT,MSG('Cidade do cliente/fornecedor'),TIP('Cidade do cliente/fornecedor')
                       ENTRY(@s2),AT(289,146,31,10),USE(FAV:estado),FLAT,MSG('Estado do cliente/fornecedor'),TIP('Estado do cliente/fornecedor'),UPR
                       ENTRY(@s20),AT(3,169,101,10),USE(FAV:telefone1),FLAT,MSG('Telefone do cliente/fornecedor'),TIP('Telefone do cliente/fornecedor')
                       ENTRY(@s20),AT(111,169,101,10),USE(FAV:telefone2),FLAT,MSG('Telefone do cliente/fornecedor'),TIP('Telefone do cliente/fornecedor')
                       ENTRY(@s20),AT(219,169,101,10),USE(FAV:telefone3),FLAT,MSG('Telefone do cliente/fornecedor'),TIP('Telefone do cliente/fornecedor')
                       ENTRY(@s200),AT(3,193,318,10),USE(FAV:url),FLAT
                       ENTRY(@s200),AT(3,217,318,10),USE(FAV:email),FLAT
                       BUTTON('&Gravar'),AT(207,238,55,15),USE(?btnGravar),TRN,FLAT,LEFT,MSG('Grava o registro'),TIP('Grava o registro'),ICON('gravar.ico'),DEFAULT
                       BUTTON('&Cancelar'),AT(266,238,55,15),USE(?btnCancelar),TRN,FLAT,LEFT,MSG('Cancela a opera��o'),TIP('Cancela a opera��o'),ICON('cancelar.ico')
                       IMAGE('window.ico'),AT(2,2),USE(?Image1)
                       PROMPT('Clientes e Fornecedores'),AT(26,5),USE(?Prompt13),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(2,22,320,2),USE(?Panel1)
                       PROMPT('Raz�o:'),AT(3,26),USE(?FAV:razaosocial:Prompt),TRN
                       PROMPT('Nome Fantasia:'),AT(3,48),USE(?FAV:nomefantasia:Prompt),TRN
                       PROMPT('Pessoa:'),AT(3,69),USE(?FAV:tipocliente:Prompt),TRN
                       PROMPT('CPF/CNPJ:'),AT(73,69),USE(?FAV:cpf_cnpj:Prompt),TRN
                       PROMPT('RG/IE:'),AT(199,69),USE(?FAV:rg_ie:Prompt),TRN
                       PROMPT('Endere�o:'),AT(3,91),USE(?FAV:endereco:Prompt),TRN
                       PROMPT('N�mero:'),AT(257,91),USE(?FAV:numero:Prompt),TRN
                       PROMPT('CEP:'),AT(3,114),USE(?FAV:cep:Prompt),TRN,LEFT
                       PROMPT('Bairro:'),AT(109,114),USE(?FAV:bairro:Prompt),TRN,LEFT
                       PROMPT('Complemento:'),AT(3,136),USE(?FAV:complemento:Prompt),TRN
                       PROMPT('Cidade:'),AT(161,136),USE(?FAV:cidade:Prompt),TRN
                       PROMPT('Estado:'),AT(289,136),USE(?FAV:estado:Prompt),TRN
                       PROMPT('Telefones:'),AT(3,159),USE(?FAV:telefone1:Prompt),TRN
                       PROMPT('Website:'),AT(3,183),USE(?FAV:url:Prompt)
                       PROMPT('Email:'),AT(3,207),USE(?FAV:email:Prompt)
                       PANEL,AT(2,232,320,2),USE(?Panel1:2)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'Visualizando'
  OF InsertRecord
    ActionMessage = 'Incluindo'
  OF ChangeRecord
    ActionMessage = 'Alterando'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateFavorecidos')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?FAV:razaosocial
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(FAV:Record,History::FAV:Record)
  SELF.AddHistoryField(?FAV:razaosocial,3)
  SELF.AddHistoryField(?FAV:nomefantasia,2)
  SELF.AddHistoryField(?FAV:tipocliente,4)
  SELF.AddHistoryField(?FAV:cpf_cnpj,5)
  SELF.AddHistoryField(?FAV:rg_ie,6)
  SELF.AddHistoryField(?FAV:endereco,7)
  SELF.AddHistoryField(?FAV:numero,8)
  SELF.AddHistoryField(?FAV:cep,19)
  SELF.AddHistoryField(?FAV:bairro,20)
  SELF.AddHistoryField(?FAV:complemento,9)
  SELF.AddHistoryField(?FAV:cidade,10)
  SELF.AddHistoryField(?FAV:estado,11)
  SELF.AddHistoryField(?FAV:telefone1,12)
  SELF.AddHistoryField(?FAV:telefone2,13)
  SELF.AddHistoryField(?FAV:telefone3,14)
  SELF.AddHistoryField(?FAV:url,21)
  SELF.AddHistoryField(?FAV:email,22)
  SELF.AddUpdateFile(Access:favorecidos)
  SELF.AddItem(?btnCancelar,RequestCancelled)              ! Add the cancel control to the window manager
  Relate:favorecidos.Open                                  ! File favorecidos used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:favorecidos
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?btnGravar
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>UpdateFavorecidos<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>UpdateFavorecidos<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?FAV:razaosocial{PROP:ReadOnly} = True
    ?FAV:nomefantasia{PROP:ReadOnly} = True
    DISABLE(?FAV:tipocliente)
    ?FAV:cpf_cnpj{PROP:ReadOnly} = True
    ?FAV:rg_ie{PROP:ReadOnly} = True
    ?FAV:endereco{PROP:ReadOnly} = True
    ?FAV:numero{PROP:ReadOnly} = True
    ?FAV:cep{PROP:ReadOnly} = True
    ?FAV:bairro{PROP:ReadOnly} = True
    ?FAV:complemento{PROP:ReadOnly} = True
    ?FAV:cidade{PROP:ReadOnly} = True
    ?FAV:estado{PROP:ReadOnly} = True
    ?FAV:telefone1{PROP:ReadOnly} = True
    ?FAV:telefone2{PROP:ReadOnly} = True
    ?FAV:telefone3{PROP:ReadOnly} = True
    ?FAV:url{PROP:ReadOnly} = True
    ?FAV:email{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:favorecidos.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?btnGravar
      SetNullFields(favorecidos)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?FAV:cpf_cnpj
      If ~0{prop:acceptall}
        If FAV:tipocliente = 'F'
          If FAV:cpf_cnpj
            if Chk_CIC(Format(TiraCaracteres(FAV:cpf_cnpj),@n011))
              FAV:cpf_cnpj = Format(FAV:cpf_cnpj,@p###.###.###-##pb)
            Else
              Message('CPF inv�lido!','Aten��o',Icon:Asterisk,'OK',,2)
              Clear(FAV:cpf_cnpj)
              Select(?FAV:cpf_cnpj)
            End
          End
        Else
          If FAV:cpf_cnpj
            if Chk_CGC(Format(TiraCaracteres(FAV:cpf_cnpj),@n014))
              FAV:cpf_cnpj = Format(FAV:cpf_cnpj,@p##.###.###/####-##pb)
            Else
              Message('CNPJ inv�lido!','Aten��o',Icon:Asterisk,'OK',,2)
              Clear(FAV:cpf_cnpj)
              Select(?FAV:cpf_cnpj)
            End
          End
        End
      End
    OF ?btnGravar
      ThisWindow.Update
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      If Self.Request = InsertRecord
          
          FAV:id = FunNumSequencial('favorecidos','id')
          
      End
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('UpdateFavorecidos')
          End
        End
      End
      Close(HWGroups)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

