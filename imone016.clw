

   MEMBER('imoney.clw')                                    ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('IMONE016.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('IMONE004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE008.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE012.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE013.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseExtratoConta22022011 PROCEDURE                       ! Generated from procedure template - Window

LOC:DataInicial      DATE                                  !
LOC:DataFinal        DATE                                  !
qExtrato             QUEUE,PRE(QEX)                        !
idmov                DECIMAL(15)                           !
datamov              DATE                                  !
icone                LONG                                  !
descricao            CSTRING(51)                           !
valor                CSTRING(21)                           !
tipomov              STRING(1)                             !
numdocumento         CSTRING(21)                           !
numdocpagamento      CSTRING(21)                           !
seq                  LONG                                  !
id_documento         REAL                                  !
                     END                                   !
LOC:SaldoFinal       DECIMAL(15,2)                         !
LOC:RazaoSocial      CSTRING(101)                          !
LOC:EnderecoBairro   CSTRING(101)                          !
LOC:CepTelefone      CSTRING(101)                          !
LOC:Data             DATE                                  !
LOC:Hora             STRING(20)                            !
loc:busca_doc        STRING(20)                            !
loc:busca_data_nao_conciliados LONG                        !
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
fExtrato  File,Driver('ODBC','/TurboSql=1'),PRE(FEX),Name('consultasql'),Owner(GLO:conexao)
Record      Record
campo1        decimal(15)
campo2        date
campo3        cstring(51)
campo4        decimal(15,2)
campo5        cstring(2)
campo6        byte
campo7        cstring(21)
campo8        cstring(21)
campo9        real
            End
          End
Window               WINDOW('i-Money'),AT(,,505,368),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,SYSTEM,GRAY,DOUBLE,AUTO
                       IMAGE('window.ico'),AT(2,2),USE(?imgLogo)
                       PROMPT('Extrato da Conta'),AT(26,5),USE(?proTitulo),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(2,23,500,2),USE(?Panel1)
                       GROUP('Filtros'),AT(3,26,498,69),USE(?Group1),BOXED,TRN
                         ENTRY(@n15.`0b),AT(32,39,54,10),USE(CON:id),FLAT,DECIMAL(2),MSG('Identifica��o da conta'),TIP('Identifica��o da conta'),REQ
                         BUTTON,AT(89,37,12,12),USE(?lkpConta),TRN,FLAT,ICON('busca.ico')
                         GROUP('Per�odo (Documentos Conciliados)'),AT(8,64,158,26),USE(?Group2),BOXED,TRN
                           PROMPT('De:'),AT(19,74),USE(?LOC:DataInicial:Prompt),TRN
                           ENTRY(@d06b),AT(32,74,52,10),USE(LOC:DataInicial),FLAT
                           PROMPT('At�:'),AT(88,74),USE(?LOC:DataFinal:Prompt),TRN
                           ENTRY(@d06b),AT(106,74,52,10),USE(LOC:DataFinal),FLAT
                         END
                         BUTTON('Atualizar'),AT(169,70,68,17),USE(?btnAtualizar),FLAT,LEFT,ICON('selecionar.ico')
                         PROMPT('Conta:'),AT(8,39),USE(?CON:id:Prompt),TRN
                         ENTRY(@s50),AT(105,39,390,10),USE(CON:descricaoconta),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Descri��o da conta'),TIP('Descri��o da conta'),REQ,READONLY
                         STRING('Banco:'),AT(8,52),USE(?String1)
                         ENTRY(@s50),AT(32,52,249,9),USE(BAN:nomedobanco),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Nome do banco'),TIP('Nome do banco'),REQ,READONLY
                         PROMPT('Ag�ncia:'),AT(286,52),USE(?CON:agenciabancaria:Prompt),TRN
                         ENTRY(@s10),AT(316,52,60,10),USE(CON:agenciabancaria),SKIP,FLAT,RIGHT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Ag�ncia banc�ria, caso seja uma conta banc�ria'),TIP('Ag�ncia banc�ria, caso seja uma conta banc�ria'),READONLY
                         PROMPT('Conta:'),AT(381,52),USE(?CON:numeroconta:Prompt),TRN
                         ENTRY(@s20),AT(405,52,90,10),USE(CON:numeroconta),SKIP,FLAT,RIGHT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('N�mero da conta, caso seja uma ag�ncia banc�ria'),TIP('N�mero da conta, caso seja uma ag�ncia banc�ria'),READONLY
                       END
                       GROUP('Busca'),AT(3,95,498,38),USE(?Group5),BOXED
                         GROUP('Por N<186> Doc. Pagamento'),AT(10,103,170,26),USE(?Group4),BOXED
                           ENTRY(@s20),AT(18,114,91,10),USE(loc:busca_doc),FLAT,UPR
                           BUTTON('Buscar'),AT(118,110,56,15),USE(?Button7),FLAT,LEFT,ICON('busca.ico')
                         END
                         GROUP('Por Data (Inclusive n�o conciliados)'),AT(323,103,170,26),USE(?Group6),BOXED
                           ENTRY(@d06b),AT(330,114,91,10),USE(loc:busca_data_nao_conciliados),FLAT
                           BUTTON('Buscar'),AT(426,110,56,15),USE(?Button7:2),FLAT,LEFT,ICON('busca.ico')
                         END
                       END
                       LIST,AT(4,135,497,193),USE(?lstExtrato),FLAT,VSCROLL,FORMAT('60L_FI~Data~L(11)@d06b@#2#57L(1)_F~N<186> Documento~L(12)@s20@#7#180L(4)_F~Descri��o' &|
   '~S(120)@s50@#4#84R_F~Valor~@s20@#5#84R_F~N<186> Doc. Pagamento~@s20@#8#4C_F~Tipo~C(4' &|
   ')@s1@#6#'),FROM(qExtrato)
                       PANEL,AT(2,331,500,2),USE(?Panel1:2)
                       GROUP('Lan�amento Avulso'),AT(169,337,177,30),USE(?Group3),BOXED
                         BUTTON('Inclui&r'),AT(189,348,56,15),USE(?btnLancamento),TRN,FLAT,LEFT,ICON('incluir.ico')
                         BUTTON('E&xcluir'),AT(267,348,56,15),USE(?Button6),DISABLE,FLAT,LEFT,ICON('excluir.ico')
                       END
                       BUTTON('&Imprimir'),AT(4,347,63,15),USE(?btnImprimir),FLAT,LEFT,ICON('imprimir.ico')
                       BUTTON('Concilia��o por lote'),AT(68,348,89,15),USE(?Button8),FLAT,LEFT,ICON('principal.ico')
                       BUTTON('Cancelar Documento'),AT(355,344,88,19),USE(?Button9),DISABLE,FLAT,LEFT,ICON('cancelar.ico')
                       BUTTON('&Fechar'),AT(451,351,50,15),USE(?btnFechar),TRN,FLAT,LEFT,ICON('fechar.ico'),DEFAULT
                     END

Report               REPORT,AT(240,1365,7750,9823),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',8,,,CHARSET:ANSI),THOUS
                       HEADER,AT(240,260,7750,1115),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         BOX,AT(3208,83,4458,354),USE(?Box1:4),ROUND,COLOR(COLOR:Black),FILL(COLOR:Gray)
                         BOX,AT(83,83,3052,635),USE(?Box1:2),ROUND,COLOR(COLOR:Black),FILL(COLOR:Gray)
                         BOX,AT(3188,52,4458,354),USE(?Box1:3),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(63,52,3052,635),USE(?Box1),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         STRING('Extrato da Conta'),AT(3198,73,4438,281),USE(?String17),TRN,CENTER,FONT('Arial',18,,FONT:bold,CHARSET:ANSI)
                         STRING(@s50),AT(73,135,3031,177),USE(EMP:razaosocial),TRN,CENTER,FONT(,8,,FONT:bold)
                         STRING(@s50),AT(94,323,2979,177),USE(LOC:EnderecoBairro),TRN,CENTER,FONT(,7,,)
                         STRING(@s50),AT(94,500,2979,177),USE(LOC:CepTelefone),TRN,CENTER,FONT(,7,,)
                         LINE,AT(94,1052,7563,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING(@d06b),AT(5385,469),USE(LOC:DataInicial),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING(@d06b),AT(6750,469),USE(LOC:DataFinal),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING('at�'),AT(6500,469),USE(?String32),TRN,FONT('Verdana',8,,FONT:bold,CHARSET:ANSI)
                         STRING('De:'),AT(5156,469),USE(?String31),TRN,FONT('Verdana',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Data'),AT(83,896),USE(?String18:8),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('N<186> Documento'),AT(750,896),USE(?String18:9),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Descri��o'),AT(1646,896),USE(?String18:10),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Valor'),AT(5854,896),USE(?String18:11),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('N<186> Doc. Pagamento'),AT(6271,896),USE(?String18:12),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Tipo'),AT(7406,896),USE(?String18:7),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                       END
detail1                DETAIL,AT(,,,177),USE(?Detail1)
                         STRING(@d06b),AT(94,10),USE(QEX:datamov),LEFT
                         STRING(@s20),AT(750,10,802,177),USE(QEX:numdocumento),LEFT
                         STRING(@s50),AT(1646,10,3167,177),USE(QEX:descricao),LEFT
                         STRING(@s1),AT(7469,10),USE(QEX:tipomov),CENTER
                         STRING(@s20),AT(6271,10,1063,177),USE(QEX:numdocpagamento),RIGHT(2)
                         STRING(@s20),AT(4906,10,1250,177),USE(QEX:valor),RIGHT(2)
                       END
                       FOOTER,AT(240,11177,7750,354),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Data:'),AT(2010,21),USE(?ReportDatePrompt),TRN,FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING(@d06b),AT(2333,21),USE(LOC:Data),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING('Hora:'),AT(3417,21),USE(?ReportTimePrompt),TRN,FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING(@T01B),AT(3750,21),USE(LOC:Hora),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING('Usu�rio:'),AT(83,10),USE(?String24),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s20),AT(531,10),USE(GLO:nomeusuario),FONT('Arial',8,,,CHARSET:ANSI)
                         LINE,AT(94,-10,7563,0),USE(?Line1:2),COLOR(COLOR:Black)
                         STRING(@pP�gina <.<<#p),AT(6906,198,729,146),PAGENO,USE(?PageCount),RIGHT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Interfocus Tecnologia'),AT(6604,10),USE(?String23),TRN,FONT('Times New Roman',8,,FONT:bold+FONT:italic,CHARSET:ANSI)
                       END
                     END
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
CarregaExtrato Routine
  !No Extrato � feita a invers�o de contas, toda conta cr�dito � d�bito e a d�bito � cr�dito. Somente para os que tem documento
  sq# = 0
  Free(qExtrato)
  Clear(LOC:SaldoFinal)
  !Busca o Saldo Inicial
  Clear(FEX:Record)

  ! A1, Inicio - 19/05/2009 - Fabricio Fachini

  ! C�digo Antigo, Inicio
     !  fExtrato{prop:sql} = |
     !  'select mvd.idconta, 0, <39>Saldo Inicial<39>, sum(mvd.valormovimento), pla.tipolancamento, '&|
     !  'case when mvd.iddocumento is null Then 0 Else 1 End as doc '&|
     !  'from movdocumentos mvd '&|
     !  'Join planocontas pla on (pla.id = mvd.idplano) '&|
     !  'Where mvd.d_dataconciliacao < <39>'&Format(LOC:DataInicial,@d012)&'<39> and mvd.idconta = '&CON:id&' '&|
     !  'Group By mvd.idconta, pla.tipolancamento, doc'
  ! C�digo Antigo, FIM          doctos.idplano       'Join documentos doctos on (doctos.id = mvd.iddocumento) ' &|

  ! C�digo Novo, Inicio
  fExtrato{prop:sql} = |
  'select mvd.idconta, 0, <39>Saldo Inicial<39>, sum(mvd.valormovimento), pla.tipolancamento, '&|
  'case when mvd.iddocumento is null Then 0 Else 1 End as doc ' &|
  'from movdocumentos mvd ' &|
  'Join planocontas pla on (pla.id = mvd.idplano) ' &|
  'Where mvd.d_dataconciliacao < <39>'&Format(LOC:DataInicial,@d012)&'<39> and ' &|
  'mvd.idconta = ' &CON:id& ' '&|
  'Group By mvd.idconta, pla.tipolancamento, doc '
  ! C�digo Novo, Fim
  !Message(fExtrato{prop:sql},,,,,2)

  ! A1, FIM - 19/05/2009 - Fabricio Fachini
  !stop('1-LOC:SaldoFinal: ' & LOC:SaldoFinal)
  Loop
    next(fExtrato)
    If Error() Then Break .

    !If (FEX:campo5 = 'C' And FEX:campo6) Or (FEX:campo5 = 'D' and ~FEX:campo6)
    If FEX:campo5 = 'D'
      LOC:SaldoFinal -= FEX:campo4
    Else
      LOC:SaldoFinal += FEX:campo4
    End
  End
  !stop('2-LOC:SaldoFinal: ' & LOC:SaldoFinal)
  !Inclui o Saldo Inicial
  do Busca_Ultimo_Reg_Queue
  Clear(qExtrato)
  QEX:seq = sq# + 1
  QEX:icone = 0
  QEX:descricao = 'Saldo Inicial'
  QEX:valor = Format(LOC:SaldoFinal,@n15.`2)
  If LOC:SaldoFinal >= 0
    QEX:tipomov = 'C'
  Else
    QEX:tipomov = 'D'
  End
  Add(qExtrato)
  !Busca os movimentos conciliados
  Clear(FEX:Record)

  ! A2, Inicio - 19/05/2009 - Fabricio Fachini

  ! C�digo Antigo, Inicio
  !fExtrato{prop:sql} = |
  !'select mvd.id, mvd.d_dataconciliacao, mvd.descricao, mvd.valormovimento, pla.tipolancamento, '&|
  !'case when mvd.iddocumento is null Then 0 Else 1 End as doc, dc.numdocumento, mvd.numdocpagamento '&|
  !'from movdocumentos mvd '&|
  !'left join documentos dc on dc.id = mvd.iddocumento '&|
  !'Join planocontas pla on (pla.id = mvd.idplano) '&|
  !'Where mvd.d_dataconciliacao between <39>'&Format(LOC:DataInicial,@d012)&'<39> and <39>'&Format(LOC:DataFinal,@d012)&'<39> '&|
  !'and mvd.idconta = '&CON:id&' order by mvd.d_dataconciliacao asc'
  ! C�digo Antigo, Fim

  ! C�digo Novo, Inicio
  fExtrato{prop:sql} = |
  'select mvd.id, mvd.d_dataconciliacao, mvd.descricao, mvd.valormovimento, pla.tipolancamento, '&|
  'case when dc.id is null Then 0 Else 1 End as doc, dc.numdocumento, mvd.numdocpagamento, '&|
  'dc.id from movdocumentos mvd '&|
  'left join documentos dc on dc.id = mvd.iddocumento '&|
  'left Join planocontas pla on pla.id = mvd.idplano '&|
  'Where mvd.d_dataconciliacao between <39>' &Format(LOC:DataInicial,@d012)&'<39> and <39>'&Format(LOC:DataFinal,@d012)&'<39> '&|
  'and mvd.idconta = ' & CON:id & ' '&|
  'order by mvd.d_dataconciliacao asc'
  ! C�digo Novo, Fim
  !Message(fExtrato{prop:sql},,,,,2)
  ! A2, Fim - 19/05/2009 - Fabricio Fachini

  Loop
    Next(fExtrato)
    If Error() Then Break .

    do Busca_Ultimo_Reg_Queue
    Clear(qExtrato)
    QEX:seq = sq# + 1

    QEX:idmov = FEX:campo1
    QEX:datamov = FEX:campo2
    if FEX:campo6 = 1
      QEX:icone = 3
    else
      QEX:icone = 2
    end
    QEX:descricao = FEX:campo3
    QEX:valor = Format(FEX:campo4,@n15.`2)

    !If (FEX:campo5 = 'C' And FEX:campo6) Or (FEX:campo5 = 'D' And FEX:campo6 = 0)
    !stop(FEX:campo5)
    If FEX:campo5 = 'D'
      QEX:tipomov = 'D'
      LOC:SaldoFinal += FEX:campo4 * (-1)
    Else
      QEX:tipomov = 'C'
      LOC:SaldoFinal += FEX:campo4
    End

    QEX:numdocumento = FEX:campo7
    QEX:numdocpagamento = FEX:campo8
    QEX:id_documento = FEX:campo9
    Add(qExtrato)
  End
  !Inclui Saldo Final, 2 Linhas em Branco e 1 linha de t�tulo de n�o conciliados
  
  Loop a# = 1 To 4

    do Busca_Ultimo_Reg_Queue
    Clear(qExtrato)
    QEX:seq = sq# + 1

    If a# = 1
      QEX:icone = 0
      QEX:descricao = 'Saldo Final'
      If LOC:SaldoFinal >= 0
        QEX:tipomov = 'C'
      Else
        QEX:tipomov = 'D'
      End
      QEX:valor = Format(LOC:SaldoFinal,@n15.`2)
    ElsIf a# <> 4
      QEX:icone = 0
    Else a# = 4
      QEX:icone = 0
      QEX:descricao = 'N � O   C O N C I L I A D O S'

    End
    Add(qExtrato)
  End
  Clear(LOC:SaldoFinal)
  !Busca os movimentos n�o consolidados
  Clear(FEX:Record)
  fExtrato{prop:sql} = |
  'select mvd.id, mvd.d_datamovimentacao, mvd.descricao, mvd.valormovimento, pla.tipolancamento, '&|
  'case when mvd.iddocumento is null Then 0 Else 1 End as doc, dc.numdocumento, mvd.numdocpagamento '&|
  'from movdocumentos mvd '&|
  'left join documentos dc on dc.id = mvd.iddocumento '&|
  'left Join planocontas pla on (pla.id = mvd.idplano) '&|
  'Where mvd.d_dataconciliacao is null and mvd.idconta = '&CON:id&' order by mvd.d_datamovimentacao asc'
  !message(fExtrato{prop:sql},,,,,2)
  Loop
    Next(fExtrato)
    If Error() Then Break .

    do Busca_Ultimo_Reg_Queue
    Clear(qExtrato)
    QEX:seq = sq# + 1

    QEX:idmov = FEX:campo1
    QEX:datamov = FEX:campo2
    QEX:icone = 1
    QEX:descricao = FEX:campo3
    QEX:valor = Format(FEX:campo4,@n15.`2)

    !If (FEX:campo5 = 'C' And FEX:campo6) or (FEX:campo5 = 'D' And FEX:campo6 = 0)
    If FEX:campo5 = 'D'
      QEX:tipomov = 'D'
      LOC:SaldoFinal += FEX:campo4 * (-1)
    Else
      QEX:tipomov = 'C'
      LOC:SaldoFinal += FEX:campo4
    End

    QEX:numdocumento    = FEX:campo7
    QEX:numdocpagamento = FEX:campo8
    Add(qExtrato)
  End

  !Inclui Saldo n�o consolidado

  do Busca_Ultimo_Reg_Queue
  Clear(qExtrato)
  QEX:seq = sq# + 1

  QEX:icone = 0
  QEX:descricao = 'Saldo N�o Conciliado'
  QEX:valor = Format(LOC:SaldoFinal,@n15.`2)
  If LOC:SaldoFinal >= 0
    QEX:tipomov = 'C'
  Else
    QEX:tipomov = 'D'
  End
  Add(qExtrato)
  !
  !Display(?lstExtrato)

  ! Posicionando registro
  QEX:descricao = 'N � O   C O N C I L I A D O S'
  get(qExtrato, QEX:descricao)
  select(?lstExtrato, QEX:seq + 1)
  display

!  IMPRIME A QUEUE EM UM ARQUIVOS ( NA PASTAS DO EXECUT�VEL )
!  remove('testequeue.txt')
!  sort(qExtrato,1)
!  loop bg# = 1 to records(qExtrato)
!    get(qExtrato, bg#)
!    if error() then break end
!    lineprint(QEX:seq &' - '& QEX:idmov &' - '& format(QEX:datamov,@d06b) &' - '& QEX:descricao &' - '& QEX:valor,'testequeue.txt')
!  end
!CarregaExtrato Routine
!  !No Extrato � feita a invers�o de contas, toda conta cr�dito � d�bito e a d�bito � cr�dito. Somente para os que tem documento
!  sq# = 0
!  Free(qExtrato)
!  Clear(LOC:SaldoFinal)
!  !Busca o Saldo Inicial
!  Clear(FEX:Record)
!
!  ! A1, Inicio - 19/05/2009 - Fabricio Fachini
!
!  ! C�digo Antigo, Inicio
!     !  fExtrato{prop:sql} = |
!     !  'select mvd.idconta, 0, <39>Saldo Inicial<39>, sum(mvd.valormovimento), pla.tipolancamento, '&|
!     !  'case when mvd.iddocumento is null Then 0 Else 1 End as doc '&|
!     !  'from movdocumentos mvd '&|
!     !  'Join planocontas pla on (pla.id = mvd.idplano) '&|
!     !  'Where mvd.d_dataconciliacao < <39>'&Format(LOC:DataInicial,@d012)&'<39> and mvd.idconta = '&CON:id&' '&|
!     !  'Group By mvd.idconta, pla.tipolancamento, doc'
!  ! C�digo Antigo, FIM
!
!  ! C�digo Novo, Inicio
!  fExtrato{prop:sql} = |
!  'select mvd.idconta, 0, <39>Saldo Inicial<39>, sum(mvd.valormovimento), pla.tipolancamento, '&|
!  'case when mvd.iddocumento is null Then 0 Else 1 End as doc ' &|
!  'from movdocumentos mvd ' &|
!  'Join documentos doctos on (doctos.id = mvd.iddocumento) ' &|
!  'Join planocontas pla on (pla.id = doctos.idplano) ' &|
!  'Where mvd.d_dataconciliacao < <39>'&Format(LOC:DataInicial,@d012)&'<39> and ' &|
!  'mvd.idconta = ' &CON:id& ' '&|
!  'Group By mvd.idconta, pla.tipolancamento, doc '
!  ! C�digo Novo, Fim
!  !Message(fExtrato{prop:sql},,,,,2)
!
!  ! A1, FIM - 19/05/2009 - Fabricio Fachini
!
!  Loop
!    next(fExtrato)
!    If Error() Then Break .
!
!    If (FEX:campo5 = 'C' And FEX:campo6) Or (FEX:campo5 = 'D' and ~FEX:campo6)
!      LOC:SaldoFinal -= FEX:campo4
!    Else
!      LOC:SaldoFinal += FEX:campo4
!    End
!  End
!  !Inclui o Saldo Inicial
!  do Busca_Ultimo_Reg_Queue
!  Clear(qExtrato)
!  QEX:seq = sq# + 1
!  QEX:icone = 2
!  QEX:descricao = 'Saldo Inicial'
!  QEX:valor = Format(LOC:SaldoFinal,@n15.`2)
!  If LOC:SaldoFinal >= 0
!    QEX:tipomov = 'C'
!  Else
!    QEX:tipomov = 'D'
!  End
!  Add(qExtrato)
!  !Busca os movimentos conciliados
!  Clear(FEX:Record)
!
!  ! A2, Inicio - 19/05/2009 - Fabricio Fachini
!
!  ! C�digo Antigo, Inicio
!  !fExtrato{prop:sql} = |
!  !'select mvd.id, mvd.d_dataconciliacao, mvd.descricao, mvd.valormovimento, pla.tipolancamento, '&|
!  !'case when mvd.iddocumento is null Then 0 Else 1 End as doc, dc.numdocumento, mvd.numdocpagamento '&|
!  !'from movdocumentos mvd '&|
!  !'left join documentos dc on dc.id = mvd.iddocumento '&|
!  !'Join planocontas pla on (pla.id = mvd.idplano) '&|
!  !'Where mvd.d_dataconciliacao between <39>'&Format(LOC:DataInicial,@d012)&'<39> and <39>'&Format(LOC:DataFinal,@d012)&'<39> '&|
!  !'and mvd.idconta = '&CON:id&' order by mvd.d_dataconciliacao asc'
!  ! C�digo Antigo, Fim
!
!  ! C�digo Novo, Inicio
!  fExtrato{prop:sql} = |
!  'select mvd.id, mvd.d_dataconciliacao, mvd.descricao, mvd.valormovimento, pla.tipolancamento, '&|
!  'case when mvd.iddocumento is null Then 0 Else 1 End as doc, dc.numdocumento, mvd.numdocpagamento '&|
!  'from movdocumentos mvd '&|
!  'left join documentos dc on dc.id = mvd.iddocumento '&|
!  'left Join planocontas pla on (pla.id = dc.idplano) '&|
!  'Where mvd.d_dataconciliacao between <39>' &Format(LOC:DataInicial,@d012)&'<39> and <39>'&Format(LOC:DataFinal,@d012)&'<39> '&|
!  'and mvd.idconta = ' & CON:id & ' '&|
!  'order by mvd.d_dataconciliacao asc'
!  ! C�digo Novo, Fim
!  !Message(fExtrato{prop:sql},,,,,2)
!  ! A2, Fim - 19/05/2009 - Fabricio Fachini
!
!  Loop
!    Next(fExtrato)
!    If Error() Then Break .
!
!    do Busca_Ultimo_Reg_Queue
!    Clear(qExtrato)
!    QEX:seq = sq# + 1
!
!    QEX:idmov = FEX:campo1
!    QEX:datamov = FEX:campo2
!    QEX:icone = 2
!    QEX:descricao = FEX:campo3
!    QEX:valor = Format(FEX:campo4,@n15.`2)
!
!    If (FEX:campo5 = 'C' And FEX:campo6) Or (FEX:campo5 = 'D' And FEX:campo6 = 0)
!      QEX:tipomov = 'D'
!      LOC:SaldoFinal += FEX:campo4 * (-1)
!    Else
!      QEX:tipomov = 'C'       
!      LOC:SaldoFinal += FEX:campo4
!    End
!
!    QEX:numdocumento = FEX:campo7
!    QEX:numdocpagamento = FEX:campo8
!    Add(qExtrato)
!  End
!  !Inclui Saldo Final, 2 Linhas em Branco e 1 linha de t�tulo de n�o conciliados
!  
!  Loop a# = 1 To 4
!
!    do Busca_Ultimo_Reg_Queue
!    Clear(qExtrato)
!    QEX:seq = sq# + 1
!
!    If a# = 1
!      QEX:icone = 2
!      QEX:descricao = 'Saldo Final'
!      If LOC:SaldoFinal >= 0
!        QEX:tipomov = 'C'
!      Else
!        QEX:tipomov = 'D'
!      End
!      QEX:valor = Format(LOC:SaldoFinal,@n15.`2)
!    ElsIf a# <> 4
!      QEX:icone = 2
!    Else a# = 4
!      QEX:icone = 2
!      QEX:descricao = 'N � O   C O N C I L I A D O S'
!
!    End
!    Add(qExtrato)
!  End
!  Clear(LOC:SaldoFinal)
!  !Busca os movimentos n�o consolidados
!  Clear(FEX:Record)
!  fExtrato{prop:sql} = |
!  'select mvd.id, mvd.d_datamovimentacao, mvd.descricao, mvd.valormovimento, pla.tipolancamento, '&|
!  'case when mvd.iddocumento is null Then 0 Else 1 End as doc, dc.numdocumento, mvd.numdocpagamento '&|
!  'from movdocumentos mvd '&|
!  'left join documentos dc on dc.id = mvd.iddocumento '&|
!  'left Join planocontas pla on (pla.id = dc.idplano) '&|
!  'Where mvd.d_dataconciliacao is null and mvd.idconta = '&CON:id&' order by mvd.d_datamovimentacao asc'
!  !message(fExtrato{prop:sql},,,,,2)
!  Loop
!    Next(fExtrato)
!    If Error() Then Break .
!
!    do Busca_Ultimo_Reg_Queue
!    Clear(qExtrato)
!    QEX:seq = sq# + 1
!
!    QEX:idmov = FEX:campo1
!    QEX:datamov = FEX:campo2
!    QEX:icone = 1
!    QEX:descricao = FEX:campo3
!    QEX:valor = Format(FEX:campo4,@n15.`2)
!
!    If (FEX:campo5 = 'C' And FEX:campo6) or (FEX:campo5 = 'D' And FEX:campo6 = 0)
!      QEX:tipomov = 'D'
!      LOC:SaldoFinal += FEX:campo4 * (-1)
!    Else
!      QEX:tipomov = 'D'
!      LOC:SaldoFinal += FEX:campo4
!    End
!
!    QEX:numdocumento    = FEX:campo7
!    QEX:numdocpagamento = FEX:campo8
!    Add(qExtrato)
!  End
!
!  !Inclui Saldo n�o consolidado
!
!  do Busca_Ultimo_Reg_Queue
!  Clear(qExtrato)
!  QEX:seq = sq# + 1
!
!  QEX:icone = 2
!  QEX:descricao = 'Saldo N�o Conciliado'
!  QEX:valor = Format(LOC:SaldoFinal,@n15.`2)
!  If LOC:SaldoFinal >= 0
!    QEX:tipomov = 'C'
!  Else
!    QEX:tipomov = 'D'
!  End
!  Add(qExtrato)
!  !
!  !Display(?lstExtrato)
!
!  ! Posicionando registro
!  QEX:descricao = 'N � O   C O N C I L I A D O S'
!  get(qExtrato, QEX:descricao)
!  select(?lstExtrato, QEX:seq + 1)
!  display
!
!!  IMPRIME A QUEUE EM UM ARQUIVOS ( NA PASTAS DO EXECUT�VEL )
!!  remove('testequeue.txt')
!!  sort(qExtrato,1)
!!  loop bg# = 1 to records(qExtrato)
!!    get(qExtrato, bg#)
!!    if error() then break end
!!    lineprint(QEX:seq &' - '& QEX:idmov &' - '& format(QEX:datamov,@d06b) &' - '& QEX:descricao &' - '& QEX:valor,'testequeue.txt')
!!  end
!!
!!  
Busca_Ultimo_Reg_Queue routine

  sort(qExtrato, QEX:seq)
  loop z# = 1 to records(qExtrato)
       get(qExtrato, z#)
       if errorcode() then break end
       sq# = QEX:seq
  end

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseExtratoConta22022011')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?imgLogo
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:bancos.SetOpenRelated()
  Relate:bancos.Open                                       ! File bancos used by this procedure, so make sure it's RelationManager is open
  Access:contas.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:movdocumentos.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:empresa.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  Open(fExtrato,42h)
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:bancos.Close
  Close(fExtrato)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    BrowseContas
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button7
      ! Busca DOC
      
      QEX:numdocpagamento = loc:busca_doc
      get(qExtrato, QEX:numdocpagamento)
      
      select(?lstExtrato, QEX:seq)
      display
    OF ?Button7:2
      ! Busca DOC por data
      
      QEX:datamov = loc:busca_data_nao_conciliados
      get(qExtrato, QEX:datamov)
      
      select(?lstExtrato, QEX:seq)
      display
    OF ?btnLancamento
      If ~CON:id
        Message('N�o foi selecionada a conta!','Aten��o',Icon:Asterisk,'OK',,2)
        Cycle
      End
    OF ?btnImprimir
      !!!Seta as Configura��es do Report
      Previewer.INIT(PrintPreviewQueue)
      Previewer.AllowUserZoom = True
      Previewer.Maximize = True
      
      Open(Report)
      Settarget(Report)
      Report{PROP:Preview} = PrintPreviewQueue.Filename
      
      !L� a Queue e Imprime
      Loop a# = 1 To Records(qExtrato)
        Get(qExtrato,a#)
        Print(RPT:Detail1)
      End
      
      LOC:Data = DataServidor()
      LOC:Hora = HoraServidor()
      
      Endpage(Report)
      If Previewer.Display() Then Report{ PROP:FlushPreview } = True .
      Settarget()
      Close(Report)
      Free(PrintPreviewQueue)
    OF ?Button9
      ! Cancela/Zera valor do documento
        if QEX:idmov
          case message('O documento receber� valor 0(ZERO).||Deseja realmente cancelar o documento?','Aten��o',icon:question,'&Sim|&N�o',2)
          of 1
            Clear(FEX:Record)
            fExtrato{prop:sql} = |
              'update public.movdocumentos set valormovimento = 0 where id = ' & QEX:idmov
      
            ! Message(fExtrato{prop:sql},,,,,2)
      
            do CarregaExtrato
      
            message('Documento Cancelado!','Aten��o',icon:exclamation)
          end
      
        else
          message('Selecione um documento','Aten��o',icon:exclamation)
        end
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CON:id
      IF CON:id OR ?CON:id{Prop:Req}
        CON:id = CON:id
        IF Access:contas.TryFetch(CON:con_pk_id)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            CON:id = CON:id
          ELSE
            SELECT(?CON:id)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      BAN:numerobanco = CON:numerodobanco
      Get(bancos,BAN:ban_numerodobanco)
      
      !if ~LOC:DataInicial or ~LOC:Datafinal
        !Passa data dos �ltimos 10 dias para o extrato
        LOC:DataFinal = LOC:Data
        LOC:DataInicial = LOC:DataFinal - 10
      !end
      
      If LOC:DataInicial And LOC:DataFinal
        Do CarregaExtrato
      Else
        Free(qExtrato)
      End
      Display
    OF ?lkpConta
      ThisWindow.Update
      CON:id = CON:id
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        CON:id = CON:id
      END
      ThisWindow.Reset(1)
      Post(Event:Accepted,?CON:id)
    OF ?btnAtualizar
      ThisWindow.Update
      If ~CON:id
        Message('Selecione a conta!','Aten��o',Icon:Asterisk,'OK',,2)
      ElsIf ~LOC:DataInicial Or ~LOC:DataFinal
        Message('Per�odo informado inv�lido!','Aten��o',Icon:Asterisk,'OK',,2)
      Else
        Do CarregaExtrato
      End
    OF ?lstExtrato
      ! Accept do brw, habilita btn excluir lanc avulso
      get(qExtrato,choice(?lstExtrato))
      
      Clear(FEX:Record)
      fExtrato{prop:sql} = |
              'select pmov.tipobaixa from public.movdocumentos pmov where pmov.id = ' & QEX:idmov
      
      !message(fExtrato{prop:sql},,,,,2)
      
      Next(fExtrato)
      
      if sub(FEX:campo1,1,1) = '4'
         enable(?Button6)
      else
         disable(?Button6)
      end
      
      if sub(FEX:campo1,1,1) = '4' or sub(FEX:campo1,1,1) = '1'
        enable(?Button9)
      else
        disable(?Button9)
      end
      
      display
      
      If KeyCode() = MouseLeft2
        Get(qExtrato,Choice(?lstExtrato))
        !Se �cone 1, quer dizer que � n�o conciliado
        If QEX:icone = 1
          !Posiciona no registro
          MOV:id = QEX:idmov
          Get(movdocumentos,MOV:mov_pk_id)
          If ~Error()
            !Chama a Janela de Data de Concilia��o e caso retorne a data, concilia o movimento
            MOV:d_dataconciliacao = WindowDataConciliacao()
            If MOV:d_dataconciliacao
              Put(movdocumentos)
              Post(Event:Accepted,?btnAtualizar)
            End
          End
        elsif QEX:icone = 3
            winVisualizaDocumento(QEX:id_documento)
        elsif QEX:icone = 2
            GlobalRequest = ViewRecord
            UpdateLancamentoAvulso(QEX:idmov)
        End
      End
      clear(movdocumentos)
    OF ?btnLancamento
      ThisWindow.Update
      GlobalRequest = InsertRecord
      UpdateLancamentoAvulso()
      ThisWindow.Reset
      Post(Event:Accepted,?btnAtualizar)
    OF ?Button6
      ThisWindow.Update
      !Excluir lanc avulso
      
      case message('Deseja realmente excluir o lan�amento?','Aten��o',icon:question,'&Sim|&N�o',2)
      of 1
      
          Clear(FEX:Record)
          fExtrato{prop:sql} = |
                  'delete from public.movdocumentos pmov where pmov.id = ' & QEX:idmov
      
          Do CarregaExtrato
      
          disable(?Button6)
      
      end
    OF ?Button8
      ThisWindow.Update
      Conciliacao_LOTE()
      ThisWindow.Reset
      do CarregaExtrato
    OF ?btnFechar
      ThisWindow.Update
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      Clear(CON:Record)
      Clear(BAN:Record)
      Clear(MOV:Record)
      
      ?lstExtrato{prop:iconlist,1} = path()&'\uncheck.ico'
      !?lstExtrato{prop:iconlist,2} = path()&'\check.ico'
      ?lstExtrato{prop:iconlist,2} = path()&'\lupazinha.ico'
      ?lstExtrato{prop:iconlist,3} = path()&'\lupazinha.ico'
      ?lstExtrato{prop:lineheight} = 10
      
      !!!Seta as Vari�veis do Report
      Clear(LOC:RazaoSocial)
      Clear(LOC:EnderecoBairro)
      Clear(LOC:CepTelefone)
      
      EMP:codempresa = 1
      get(empresa,EMP:emp_codempresa)
      
      LOC:RazaoSocial    = EMP:razaosocial
      LOC:EnderecoBairro = EMP:logradouro&', '&EMP:numero&'   '&EMP:bairro
      LOC:CepTelefone    = EMP:cep&'   '&EMP:telefone
      LOC:Data           = DataServidor()
      LOC:Hora           = HoraServidor()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

