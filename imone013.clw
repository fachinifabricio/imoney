

   MEMBER('imoney.clw')                                    ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('radsqlb.inc'),ONCE

                     MAP
                       INCLUDE('IMONE013.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('IMONE003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE006.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE009.INC'),ONCE        !Req'd for module callout resolution
                     END


visualizarbaixas PROCEDURE (prt:iddoc)                     ! Generated from procedure template - Window

Loc:ValorPuro        DECIMAL(7,2)                          !
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
BR1:View             VIEW(movdocumentos)
                       PROJECT(MOV:d_datamovimentacao)
                       PROJECT(MOV:valormulta)
                       PROJECT(MOV:valorjuros)
                       PROJECT(MOV:valordesconto)
                       PROJECT(MOV:valoroutrosacrescimos)
                       PROJECT(MOV:valormovimento)
                       PROJECT(MOV:tipobaixa)
                       PROJECT(MOV:id)
                       PROJECT(MOV:iddocumento)
                       JOIN(DOC:doc_pk_id,MOV:iddocumento),INNER
                         PROJECT(DOC:d_datavencimento)
                         PROJECT(DOC:id)
                       END
                     END
Queue:RADSQLBrowse   QUEUE                            !Queue declaration for browse/combo box using ?List
DOC:d_datavencimento   LIKE(DOC:d_datavencimento)     !List box control field - type derived from field
MOV:d_datamovimentacao LIKE(MOV:d_datamovimentacao)   !List box control field - type derived from field
Loc:ValorPuro          LIKE(Loc:ValorPuro)            !List box control field - type derived from local data
MOV:valormulta         LIKE(MOV:valormulta)           !List box control field - type derived from field
MOV:valorjuros         LIKE(MOV:valorjuros)           !List box control field - type derived from field
MOV:valordesconto      LIKE(MOV:valordesconto)        !List box control field - type derived from field
MOV:valoroutrosacrescimos LIKE(MOV:valoroutrosacrescimos) !List box control field - type derived from field
MOV:valormovimento     LIKE(MOV:valormovimento)       !List box control field - type derived from field
MOV:tipobaixa          LIKE(MOV:tipobaixa)            !List box control field - type derived from field
MOV:id                 LIKE(MOV:id)                   !Primary key field - type derived from field
DOC:id                 LIKE(DOC:id)                   !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Window               WINDOW('i-Money'),AT(,,489,267),FONT('MS Sans Serif',8,,FONT:regular),CENTER,GRAY
                       IMAGE('window.ico'),AT(2,2),USE(?Image1)
                       PROMPT('Visualizando Baixas'),AT(27,3),USE(?Prompt1),FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,24,480,2),USE(?Panel1)
                       LIST,AT(5,29,477,209),USE(?List),IMM,FLAT,VSCROLL,MSG('Scrolling records...'),FORMAT('51C(2)|M~Vencimento~@d06b@53C(2)|M~Pagamento~@d06b@45R(2)|M~Valor~C@n10.2@40R(2)' &|
   '|M~Multa~C@n8`2@40R(2)|M~Juros~C@n8.2@40R(2)|M~Desconto~C@n7`2@56R(2)|M~Outros A' &|
   'cresc.~C@n9`2@52R(2)|M~Valor Pago~C@n10.`2@70C(2)|M~Baixa~@s15@'),FROM(Queue:RADSQLBrowse)
                       PANEL,AT(4,241,480,2),USE(?Panel1:2)
                       BUTTON('&Fechar'),AT(429,249,47,14),USE(?CancelButton),FLAT,LEFT,ICON('fechar.ico'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Reset                  PROCEDURE(BYTE Force=0),DERIVED     ! Method added to host embed code
SetAlerts              PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
BR1                  CLASS(EXTSQLB)
SetQueueRecord         PROCEDURE(),DERIVED                 ! Method added to host embed code
SetRange               PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('visualizarbaixas')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  COMPILE('C55',_C55_)
    Self.Filesopened = 1
  C55
  OMIT('C55',_C55_)
    Filesopened = 1
  C55
  Relate:movdocumentos.Open()
  Access:movdocumentos.UseFile()
  Relate:documentos.Open()
  Access:documentos.UseFile()
  SELF.Open(Window)                                        ! Open window
  BR1.RefreshHotkey = F5Key
  BR1.Setlistcontrol (?List,10)
  Do DefineListboxStyle
  BR1.PopUpActive = True
  BR1.SQLRecordLimit = 'prt:iddoc'
  BR1.Init(Access:movdocumentos)
  BR1.useansijoin=1
  SELF.SetAlerts()
  ?List{PROP:From} = Queue:RADSQLBrowse
  BR1.SetSortColColor(-1)
  BR1.AddTable('movdocumentos',1,0,'movdocumentos')  ! Add the table to the list of tables
    BR1.AddTable('documentos',2,0,'documentos')
  BR1.Listqueue      &= Queue:RADSQLBrowse
  BR1.Position       &= Queue:RADSQLBrowse.ViewPosition
  BR1.ColumnCharAsc   = ''
  BR1.ColumnCharDes   = ''
  BR1.AddFieldpairs(Queue:RADSQLBrowse.MOV:id,MOV:id,'movdocumentos','id','N',0,'B','A','M','N',1,10)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.MOV:d_datamovimentacao,MOV:d_datamovimentacao,'movdocumentos','d_datamovimentacao','D',0,'B','A','M','N',5,2)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.MOV:valormovimento,MOV:valormovimento,'movdocumentos','valormovimento','N',0,'B','A','M','N',6,8)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.MOV:valordesconto,MOV:valordesconto,'movdocumentos','valordesconto','N',0,'B','A','M','N',13,6)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.MOV:valormulta,MOV:valormulta,'movdocumentos','valormulta','N',0,'B','A','M','N',14,4)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.MOV:valorjuros,MOV:valorjuros,'movdocumentos','valorjuros','N',0,'B','A','M','N',15,5)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.MOV:valoroutrosacrescimos,MOV:valoroutrosacrescimos,'movdocumentos','valoroutrosacrescimos','N',0,'B','A','M','N',16,7)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.MOV:tipobaixa,MOV:tipobaixa,'movdocumentos','tipobaixa','A',0,'B','A','M','N',18,9)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.DOC:id,DOC:id,'documentos','id','N',0,'B','A','M','N',1,11)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.DOC:d_datavencimento,DOC:d_datavencimento,'documentos','d_datavencimento','D',0,'B','A','M','N',8,1)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.Loc:ValorPuro,Loc:ValorPuro,'','','A',0,'B','A','M','N',0,3) ! Formula added
  BR1.AddJoinFields('movdocumentos','iddocumento','documentos','id',BR1:view{prop:inner,1},0,DOC:doc_pk_id)
  BR1.Addviewfield('movdocumentos','iddocumento',2)   ! Not in queue from MOV:iddocumento
  BR1.Managedview &=BR1:View
  BR1.Openview()
  BR1.SetForceFetch (1)
  BR1.Datepicture = '@d06b'
  BR1.Resetsort(1)
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  COMPILE('C55',_C55_)
  if Self.filesopened
  C55
  OMIT('C55',_C55_)
  If filesopened then
  C55
    Relate:movdocumentos.Close
    Relate:documentos.Close
  END
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF Window{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
     If force = 1  then
        BR1.ResetBrowse (1)
     end


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  BR1.SetAlerts()


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
  BR1.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BR1.SetQueueRecord PROCEDURE


  CODE
        Loc:ValorPuro = (MOV:valormovimento + MOV:valoroutrosacrescimos) - (MOV:valormulta + MOV:valorjuros + MOV:valordesconto)
  PARENT.SetQueueRecord


BR1.SetRange PROCEDURE


  CODE
  PARENT.SetRange
  Self.RangeFilter = 'a.iddocumento = ' & prt:iddoc

WindowEscolherAno PROCEDURE                                ! Generated from procedure template - Window

LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
Window               WINDOW('Escolher Ano'),AT(,,109,75),FONT('MS Sans Serif',8,,FONT:regular),CENTER,GRAY
                       SPIN(@n4),AT(15,8,78,28),USE(Glo:ano),CENTER,FONT('Arial',24,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(0804000H),RANGE(2000,2999),STEP(1)
                       BUTTON('OK'),AT(32,50,45,14),USE(?Button1),STD(STD:Close),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('WindowEscolherAno')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Glo:ano
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      Glo:ano = year(today())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

WindowAvisoDocumentos PROCEDURE                            ! Generated from procedure template - Window

qDocumentos          QUEUE,PRE(QDO)                        !
Display              CSTRING(51)                           !
icone                LONG                                  !
nivel                LONG                                  !
numdocumento         CSTRING(31)                           !
valordocumento       DECIMAL(10,2)                         !
Vencimento           DATE                                  !
                     END                                   !
qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
Window               WINDOW('i-Money'),AT(,,463,280),FONT('Tahoma',8,,,CHARSET:ANSI),CENTER,IMM,SYSTEM,GRAY,DOUBLE,AUTO
                       IMAGE('window.ico'),AT(2,2),USE(?imgLogo)
                       PROMPT('Alerta de Documentos'),AT(28,5),USE(?Prompt1),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(2,23,457,2),USE(?Panel1)
                       LIST,AT(3,27,456,229),USE(?lstDocumentos),FLAT,VSCROLL,FORMAT('237L(2)|FIT~Favorecido~@s50@#1#85L(2)|F~Documento~@s20@#4#53C|F~Vencimento~@d06b' &|
   '@#6#56R(4)|F~Valor~C(0)@n-14.`2@#5#'),FROM(qDocumentos)
                       PANEL,AT(2,258,457,2),USE(?Panel1:2)
                       BUTTON('&Fechar'),AT(403,263,55,15),USE(?btnFechar),TRN,FLAT,LEFT,ICON('fechar.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
BuscaDocumentos routine
  data
fAlertas  File,Driver('ODBC','/TurboSql=1'),PRE(FAL),Name('consultasql'),Owner(GLO:conexao)
Record      Record
tipo          cstring(16),name('campo1')
favorecido    cstring(51),name('campo1')
documento     cstring(31),name('campo1')
vencimento    date,name('campo1')
valor         decimal(15,2),name('campo1')
            End
          End
RTN:tipo  cstring(16)
RTN:ValorCreditos decimal(15,2)
RTN:ValorDebitos  decimal(15,2)
  code
    !Abre o arquivo
    open(fAlertas,42h)
    !
    clear(RTN:tipo)
    RTN:ValorCreditos = 0
    RTN:ValorDebitos = 0
    !
    clear(FAL:Record)
    fAlertas{prop:sql} = |
      'select '&|
      'case when pl.tipolancamento = <39>D<39> then <39>D � B I T O<39> else <39>C R � D I T O<39> end as tipo, '&|
      'fav.razaosocial, d.numdocumento, d.d_datavencimento, d.valordocumento '&|
      'from public.documentos d '&|
      'join public.planocontas pl on pl.id = d.idplano '&|
      'join public.favorecidos fav on fav.id = d.idcliente '&|
      'where d.d_datavencimento <= current_date and d.d_datapagamento is null '&|
      ' and d.idempresa = '&GLO:CodEmpresa&' '&|
      'order by tipo, d.d_datavencimento'
    loop
      next(fAlertas)
      if error() then break .

      if RTN:tipo <> FAL:tipo
        !Adicona na Queue o N�vel 1 da Tree
        clear(qDocumentos)
        QDO:Display = FAL:tipo
        if FAL:tipo = 'D � B I T O'
          QDO:icone = 1
        else
          QDO:icone = 2
        end
        QDO:nivel = 1
        add(qDocumentos)

        RTN:tipo = FAL:tipo
      end

      Clear(qDocumentos)
      if FAL:tipo = 'D � B I T O'
        QDO:icone = 1
        RTN:ValorDebitos += FAL:valor
      else
        QDO:icone = 2
        RTN:ValorCreditos += FAL:valor
      end
      QDO:display = FAL:favorecido
      QDO:nivel = 2
      QDO:numdocumento = FAL:documento
      QDO:valordocumento = FAL:valor
      QDO:Vencimento = FAL:vencimento
      add(qDocumentos)
    end

    QDO:Display = 'D � B I T O'
    get(qDocumentos,+QDO:Display)
    QDO:valordocumento = RTN:ValorDebitos
    put(qDocumentos)

    QDO:Display = 'C R � D I T O'
    get(qDocumentos,+QDO:Display)
    QDO:valordocumento = RTN:ValorCreditos
    put(qDocumentos)

    Display
    close(fAlertas)

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('WindowAvisoDocumentos')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?imgLogo
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>WindowAvisoDocumentos<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>WindowAvisoDocumentos<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?btnFechar
      post(event:closewindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('WindowAvisoDocumentos')
          End
        End
      End
      Close(HWGroups)
    OF EVENT:OpenWindow
      ?lstDocumentos{prop:iconlist,1} = path()&'\debito.ico'
      ?lstDocumentos{prop:iconlist,2} = path()&'\credito.ico'
      do BuscaDocumentos
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

winVisualizaDocumento PROCEDURE (PAR:IdDocumento)          ! Generated from procedure template - Window

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Repetir          LONG                                  !
LOC:incremento       LONG                                  !
LOC:TipoIncremento   BYTE(1)                               !Tipo de Incremento
LOC:Vencimento       DATE                                  !
qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
QuickWindow          WINDOW('i-Money'),AT(,,312,213),FONT('MS Sans Serif',8,,FONT:regular),CENTER,IMM,HLP('UpdateDocumentos'),SYSTEM,GRAY,DOUBLE,AUTO
                       ENTRY(@n10.`0b),AT(3,36,58,10),USE(DOC:idcliente),SKIP,FLAT,RIGHT(2),FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('Identificador do cliente/fornecedor'),TIP('Identificador do cliente/fornecedor'),REQ,READONLY
                       ENTRY(@n15.`0b),AT(3,58,58,10),USE(DOC:idplano),SKIP,FLAT,RIGHT(2),FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('Identificador da conta no plano de contas'),TIP('Identificador da conta no plano de contas'),REQ,READONLY
                       ENTRY(@n15.`0b),AT(3,81,58,10),USE(DOC:idcentrocusto),SKIP,FLAT,RIGHT(2),FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('ID de controle da tabela de bancos'),TIP('ID de controle da tabela de bancos'),REQ,READONLY
                       ENTRY(@s20),AT(3,105,71,10),USE(DOC:numdocumento),SKIP,FLAT,FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('N�mero do documento'),TIP('N�mero do documento'),REQ,READONLY
                       ENTRY(@d06b),AT(76,105,55,10),USE(DOC:d_dataemissao),SKIP,FLAT,CENTER,FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('Data de emiss�o do documento'),TIP('Data de emiss�o do documento'),REQ,READONLY
                       ENTRY(@d06b),AT(133,105,55,10),USE(DOC:d_datavencimento),SKIP,FLAT,CENTER,FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('Data de vencimento do documento'),TIP('Data de vencimento do documento'),REQ,READONLY
                       ENTRY(@d06b),AT(190,105,55,10),USE(DOC:d_dataprovavel),SKIP,FLAT,CENTER,FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('Data de vencimento do documento'),TIP('Data de vencimento do documento'),REQ,READONLY
                       ENTRY(@n13.`2),AT(247,105,62,10),USE(DOC:valordocumento),SKIP,FLAT,RIGHT(2),FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('Valor do documento'),TIP('Valor do documento'),READONLY
                       PROMPT('Desconto:'),AT(3,118),USE(?DOC:valordesconto:Prompt),TRN
                       ENTRY(@n13.`2),AT(3,128,62,10),USE(DOC:valordesconto),SKIP,FLAT,DECIMAL(12),FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('Valor de desconto do documento'),TIP('Valor de desconto do documento'),READONLY
                       PROMPT('Juros:'),AT(128,118),USE(?DOC:valorjuros:Prompt),TRN
                       ENTRY(@n13.`2),AT(128,128,62,10),USE(DOC:valorjuros),SKIP,FLAT,DECIMAL(12),FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('Valor de juros do documento'),TIP('Valor de juros do documento'),READONLY
                       PROMPT('Multa:'),AT(69,118),USE(?DOC:valormulta:Prompt),TRN
                       ENTRY(@n13.`2),AT(69,128,55,10),USE(DOC:valormulta),SKIP,FLAT,DECIMAL(12),FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('Valor de mulya'),TIP('Valor de mulya'),READONLY
                       PROMPT('Outros Desc.:'),AT(194,118),USE(?DOC:valorloutrosdescontos:Prompt),TRN
                       ENTRY(@n13.`2),AT(194,128,55,10),USE(DOC:valorloutrosdescontos),SKIP,FLAT,DECIMAL(12),FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('Valor de outros descontos'),TIP('Valor de outros descontos'),READONLY
                       ENTRY(@s50),AT(3,152,305,10),USE(DOC:descricao),SKIP,FLAT,FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('Descri��o do documento'),TIP('Descri��o do documento'),REQ,READONLY
                       ENTRY(@d06b),AT(235,173,73,10),USE(DOC:d_cancelamento),SKIP,FLAT,CENTER,FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('Data de cancelamento integral do t�tulo'),TIP('Data de cancelamento integral do t�tulo'),READONLY
                       BUTTON('&Fechar'),AT(253,196,55,15),USE(?btnFechar),TRN,FLAT,LEFT,MSG('Cancel operation'),TIP('Cancel operation'),ICON('fechar.ico')
                       IMAGE('window.ico'),AT(3,2),USE(?Image1)
                       PROMPT('Documentos'),AT(29,5),USE(?Prompt8),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(3,22,307,2),USE(?Panel1)
                       PROMPT('Clientes e Fornecedores'),AT(3,27,86,10),USE(?DOC:idcliente:Prompt),TRN,LEFT
                       ENTRY(@s60),AT(65,36,243,10),USE(FAV:razaosocial),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Raz�o social do cliente/fornecedor'),TIP('Raz�o social do cliente/fornecedor'),REQ,READONLY
                       PROMPT('Plano:'),AT(3,49,39,10),USE(?DOC:idplano:Prompt),TRN,LEFT
                       ENTRY(@s70),AT(65,58,243,10),USE(PLA:descricaoplano),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Descri��o do lan�amento no plano de contas'),TIP('Descri��o do lan�amento no plano de contas'),REQ,READONLY
                       PROMPT('Centro de Custo:'),AT(3,72),USE(?DOC:idcentrocusto:Prompt),TRN
                       ENTRY(@s50),AT(65,81,243,10),USE(CCU:descricao),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Descri��o do lan�amento no plano de contas'),TIP('Descri��o do lan�amento no plano de contas'),REQ,READONLY
                       PROMPT('Documento:'),AT(3,96),USE(?DOC:numdocumento:Prompt),TRN,LEFT
                       PROMPT('Emiss�o:'),AT(76,96),USE(?DOC:d_dataemissao:Prompt),TRN
                       PROMPT('Vencimento:'),AT(133,96),USE(?DOC:d_datavencimento:Prompt),TRN
                       PROMPT('Previs�o:'),AT(190,96),USE(?DOC:d_datavencimento:Prompt:2),TRN
                       PROMPT('Valor:'),AT(248,96),USE(?DOC:valordocumento:Prompt),TRN
                       PROMPT('Descri��o:'),AT(3,142,39,10),USE(?DOC:descricao:Prompt),TRN,LEFT
                       PROMPT('Cancelamento:'),AT(235,164),USE(?DOC:d_dataemissao:Prompt:2),TRN
                       PROMPT('Pago:'),AT(253,118),USE(?DOC:valorpago:Prompt),TRN
                       ENTRY(@n13.`2),AT(253,128,55,10),USE(DOC:valorpago),SKIP,FLAT,DECIMAL(12),FONT(,,COLOR:Navy,,CHARSET:ANSI),MSG('Valor pago do documento'),TIP('Valor pago do documento'),READONLY
                       PANEL,AT(3,189,307,2),USE(?Panel1:2)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('winVisualizaDocumento')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DOC:idcliente
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?btnFechar,RequestCancelled)                ! Add the cancel control to the window manager
  Relate:centrocusto.SetOpenRelated()
  Relate:centrocusto.Open                                  ! File centrocusto used by this procedure, so make sure it's RelationManager is open
  Access:documentos.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:favorecidos.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:planocontas.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>winVisualizaDocumento<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>winVisualizaDocumento<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?DOC:idcliente{PROP:ReadOnly} = True
    ?DOC:idplano{PROP:ReadOnly} = True
    ?DOC:idcentrocusto{PROP:ReadOnly} = True
    ?DOC:numdocumento{PROP:ReadOnly} = True
    ?DOC:d_dataemissao{PROP:ReadOnly} = True
    ?DOC:d_datavencimento{PROP:ReadOnly} = True
    ?DOC:d_dataprovavel{PROP:ReadOnly} = True
    ?DOC:valordocumento{PROP:ReadOnly} = True
    ?DOC:valordesconto{PROP:ReadOnly} = True
    ?DOC:valorjuros{PROP:ReadOnly} = True
    ?DOC:valormulta{PROP:ReadOnly} = True
    ?DOC:valorloutrosdescontos{PROP:ReadOnly} = True
    ?DOC:descricao{PROP:ReadOnly} = True
    ?DOC:d_cancelamento{PROP:ReadOnly} = True
    ?FAV:razaosocial{PROP:ReadOnly} = True
    ?PLA:descricaoplano{PROP:ReadOnly} = True
    ?CCU:descricao{PROP:ReadOnly} = True
    ?DOC:valorpago{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:centrocusto.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      BrowseFavorecidos
      BrowsePlanoContas
      BrowseCentroDeCusto
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DOC:idcliente
      IF DOC:idcliente OR ?DOC:idcliente{Prop:Req}
        FAV:id = DOC:idcliente
        IF Access:favorecidos.TryFetch(FAV:cli_pk_id)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            DOC:idcliente = FAV:id
          ELSE
            SELECT(?DOC:idcliente)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?DOC:idplano
      IF DOC:idplano OR ?DOC:idplano{Prop:Req}
        PLA:id = DOC:idplano
        IF Access:planocontas.TryFetch(PLA:pla_pk_id)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            DOC:idplano = PLA:id
          ELSE
            SELECT(?DOC:idplano)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?DOC:idcentrocusto
      IF DOC:idcentrocusto OR ?DOC:idcentrocusto{Prop:Req}
        CCU:id = DOC:idcentrocusto
        IF Access:centrocusto.TryFetch(CCU:ccu_pk_id)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            DOC:idcentrocusto = CCU:id
          ELSE
            SELECT(?DOC:idcentrocusto)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?DOC:valordocumento
      IF Access:documentos.TryValidateField(9)             ! Attempt to validate DOC:valordocumento in documentos
        SELECT(?DOC:valordocumento)
        QuickWindow{Prop:AcceptAll} = False
        CYCLE
      ELSE
        FieldColorQueue.Feq = ?DOC:valordocumento
        GET(FieldColorQueue, FieldColorQueue.Feq)
        IF ~ERRORCODE()
          ?DOC:valordocumento{Prop:FontColor} = FieldColorQueue.OldColor
          DELETE(FieldColorQueue)
        END
      END
    OF ?DOC:valordesconto
      IF Access:documentos.TryValidateField(11)            ! Attempt to validate DOC:valordesconto in documentos
        SELECT(?DOC:valordesconto)
        QuickWindow{Prop:AcceptAll} = False
        CYCLE
      ELSE
        FieldColorQueue.Feq = ?DOC:valordesconto
        GET(FieldColorQueue, FieldColorQueue.Feq)
        IF ~ERRORCODE()
          ?DOC:valordesconto{Prop:FontColor} = FieldColorQueue.OldColor
          DELETE(FieldColorQueue)
        END
      END
    OF ?DOC:valorjuros
      IF Access:documentos.TryValidateField(12)            ! Attempt to validate DOC:valorjuros in documentos
        SELECT(?DOC:valorjuros)
        QuickWindow{Prop:AcceptAll} = False
        CYCLE
      ELSE
        FieldColorQueue.Feq = ?DOC:valorjuros
        GET(FieldColorQueue, FieldColorQueue.Feq)
        IF ~ERRORCODE()
          ?DOC:valorjuros{Prop:FontColor} = FieldColorQueue.OldColor
          DELETE(FieldColorQueue)
        END
      END
    OF ?DOC:valormulta
      IF Access:documentos.TryValidateField(13)            ! Attempt to validate DOC:valormulta in documentos
        SELECT(?DOC:valormulta)
        QuickWindow{Prop:AcceptAll} = False
        CYCLE
      ELSE
        FieldColorQueue.Feq = ?DOC:valormulta
        GET(FieldColorQueue, FieldColorQueue.Feq)
        IF ~ERRORCODE()
          ?DOC:valormulta{Prop:FontColor} = FieldColorQueue.OldColor
          DELETE(FieldColorQueue)
        END
      END
    OF ?DOC:valorloutrosdescontos
      IF Access:documentos.TryValidateField(15)            ! Attempt to validate DOC:valorloutrosdescontos in documentos
        SELECT(?DOC:valorloutrosdescontos)
        QuickWindow{Prop:AcceptAll} = False
        CYCLE
      ELSE
        FieldColorQueue.Feq = ?DOC:valorloutrosdescontos
        GET(FieldColorQueue, FieldColorQueue.Feq)
        IF ~ERRORCODE()
          ?DOC:valorloutrosdescontos{Prop:FontColor} = FieldColorQueue.OldColor
          DELETE(FieldColorQueue)
        END
      END
    OF ?DOC:valorpago
      IF Access:documentos.TryValidateField(16)            ! Attempt to validate DOC:valorpago in documentos
        SELECT(?DOC:valorpago)
        QuickWindow{Prop:AcceptAll} = False
        CYCLE
      ELSE
        FieldColorQueue.Feq = ?DOC:valorpago
        GET(FieldColorQueue, FieldColorQueue.Feq)
        IF ~ERRORCODE()
          ?DOC:valorpago{Prop:FontColor} = FieldColorQueue.OldColor
          DELETE(FieldColorQueue)
        END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('winVisualizaDocumento')
          End
        End
      End
      Close(HWGroups)
    OF EVENT:OpenWindow
      DOC:id = PAR:IdDocumento
      get(documentos,DOC:doc_pk_id)
      if error()
        Message('Erro ao localizar documento!','Aten��o',Icon:Question,'OK',,2)
        post(event:closewindow)
      else
        FAV:id = DOC:idcliente
        get(favorecidos,FAV:cli_pk_id)
        PLA:id = DOC:idplano
        get(planocontas,PLA:pla_pk_id)
        CCU:id = DOC:idcentrocusto
        get(centrocusto,CCU:ccu_pk_id)
      end
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

