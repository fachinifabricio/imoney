

   MEMBER('imoney.clw')                                    ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('radsqldr.inc'),ONCE

                     MAP
                       INCLUDE('IMONE017.INC'),ONCE        !Local module procedure declarations
                     END


winSelecionarEmpresa PROCEDURE                             ! Generated from procedure template - Window

LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
RADSQLDrop1Locator CString(256)
RADSQLDrop1:View     VIEW(empresa)
                       PROJECT(EMP:razaosocial)
                       PROJECT(EMP:codempresa)
                     END
Queue:RADSQLDrop     QUEUE                            !Queue declaration for browse/combo box using ?EMP:razaosocial
EMP:razaosocial        LIKE(EMP:razaosocial)          !List box control field - type derived from field
EMP:codempresa         LIKE(EMP:codempresa)           !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Window               WINDOW('i-Money'),AT(,,369,70),FONT('MS Sans Serif',8,,FONT:regular),CENTER,IMM,SYSTEM,GRAY,DOUBLE,AUTO
                       IMAGE('window.ico'),AT(2,2),USE(?Image1)
                       STRING('Empresa'),AT(26,5),USE(?String1),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(2,23,364,2),USE(?Panel1)
                       COMBO(@s255),AT(3,29,361,15),USE(EMP:razaosocial),IMM,FLAT,VSCROLL,FONT(,11,,FONT:bold,CHARSET:ANSI),FORMAT('200L(2)|@s50@'),DROP(10),FROM(Queue:RADSQLDrop),MSG('Raz�o Social')
                       PANEL,AT(2,47,364,2),USE(?Panel1:2)
                       BUTTON('&OK'),AT(310,52,55,15),USE(?btnOK),TRN,FLAT,LEFT,ICON('selecionar.ico'),STD(STD:Close),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Reset                  PROCEDURE(BYTE Force=0),DERIVED     ! Method added to host embed code
SetAlerts              PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
RADSQLDrop1          CLASS(RADSQLDrop)
GenerateSelectStatement PROCEDURE(),DERIVED                ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('winSelecionarEmpresa')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  COMPILE('C55',_C55_)
    Self.Filesopened = 1
  C55
  OMIT('C55',_C55_)
    Filesopened = 1
  C55
  Relate:empresa.Open()
  Access:empresa.UseFile()
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  RADSQLDrop1.Listqueue      &= Queue:RADSQLDrop
  RADSQLDrop1.Position       &= Queue:RADSQLDrop.ViewPosition
  RADSQLDrop1.Setlistcontrol(?EMP:razaosocial)
  RADSQLDrop1.RefreshHotkey = F5Key
  RADSQLDrop1.AddDropVariables(?EMP:razaosocial,0,EMP:razaosocial,'EMP:codempresa',EMP:codempresa,GLO:CodEmpresa)
  RADSQLDrop1.Init(Access:empresa)
  RADSQLDrop1.AddField(EMP:codempresa,GLO:CodEmpresa)
  RADSQLDrop1.AddField(EMP:razaosocial,GLO:NomeEmpresa)
     RADSQLDrop1.useansijoin=1
  SELF.SetAlerts()
  ?EMP:razaosocial{PROP:From} = Queue:RADSQLDrop
  RADSQLDrop1.AddTable('empresa',1,0,'EMPRESA')  ! Add the table to the list of tables
  RADSQLDrop1.ColumnCharAsc   = ''
  RADSQLDrop1.ColumnCharDes   = ''
  RADSQLDrop1.DefaultFill     = True
  RADSQLDrop1.AddFieldpairs(Queue:RADSQLDrop.EMP:codempresa,EMP:codempresa,'empresa','codempresa | READONLY','N',0,'B','A','I','N',1,2)
  RADSQLDrop1.AddFieldpairs(Queue:RADSQLDrop.EMP:razaosocial,EMP:razaosocial,'empresa','razaosocial','A',0,'A','A','I','N',2,1)
  RADSQLDrop1.Addindexfunction('UPPER(%)')
  RADSQLDrop1.Managedview &=RADSQLDrop1:View
  RADSQLDrop1.Openview
  RADSQLDrop1.SetFileLoad
  RADSQLDrop1.SetColColor = 0
  RADSQLDrop1.Resetsort(1,1)
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  COMPILE('C55',_C55_)
  if Self.filesopened
  C55
  OMIT('C55',_C55_)
  If filesopened then
  C55
  Relate:empresa.Close
  END
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF Window{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
  If force = 2
    RADSQLDrop1.ResetBrowse (1)
  end


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  RADSQLDrop1.SetAlerts


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
  RADSQLDrop1.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


RADSQLDrop1.GenerateSelectStatement PROCEDURE


  CODE
  PARENT.GenerateSelectStatement
  self.viewsqlstatement = |
  'SELECT A.CODEMPRESA, A.RAZAOSOCIAL FROM EMPRESA A'

