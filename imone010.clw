

   MEMBER('imoney.clw')                                    ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('radsqlb.inc'),ONCE
   INCLUDE('radsqldr.inc'),ONCE

                     MAP
                       INCLUDE('IMONE010.INC'),ONCE        !Local module procedure declarations
                     END


MOD:Busca   CSTRING(201)
MOD:GroupID   CSTRING(41)
MOD:ID   DECIMAL(15)
BrowseGroupsUsersControls PROCEDURE (prm:nameproced)       ! Generated from procedure template - Window

LOC:PopUp            CSTRING(5001)                         !
LOC:SQLGrupo         CSTRING(501)                          !
LOC:SQLUsuario       CSTRING(501)                          !
LOC:Access           STRING(10)                            !
LOC:InitialAccess    STRING(12)                            !
qControls            QUEUE,PRE(QCO)                        !
procedurename        CSTRING(51)                           !
controlname          CSTRING(41)                           !
a_access             STRING(3)                             !
access_icon          LONG                                  !
a_insert             STRING(3)                             !
insert_icon          LONG                                  !
a_change             STRING(3)                             !
change_icon          LONG                                  !
a_delete             STRING(3)                             !
delete_icon          LONG                                  !
idprocedure          DECIMAL(15)                           !
                     END                                   !
LOC:DataServidor     DATE                                  !
fLoadQueue File,Driver('ODBC','/TurboSql=1'),Owner(GLO:conexao),Name('consultasql'),Pre(FLQ)
Record        Record
campo1          Cstring(51)
campo2          Cstring(41)
campo3          Byte
campo4          Byte
campo5          Byte
campo6          Byte
campo7          Decimal(15)
              End
          End

fDataServidor File,Driver('ODBC','/TurboSql=1'),Owner(GLO:conexao),Name('consultasql'),Pre(FDS)
Record        Record
campo1          Date
              End
          End
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
BR2:View             VIEW(hwgroups)
                       PROJECT(HWG:namegroup)
                       PROJECT(HWG:accessgroup)
                       PROJECT(HWG:accessinitial)
                       PROJECT(HWG:id)
                     END
Queue:RADSQLBrowse:1 QUEUE                            !Queue declaration for browse/combo box using ?List:2
HWG:namegroup          LIKE(HWG:namegroup)            !List box control field - type derived from field
LOC:Access             LIKE(LOC:Access)               !List box control field - type derived from local data
LOC:InitialAccess      LIKE(LOC:InitialAccess)        !List box control field - type derived from local data
HWG:accessgroup        LIKE(HWG:accessgroup)          !Browse hot field - type derived from field
HWG:accessinitial      LIKE(HWG:accessinitial)        !Browse hot field - type derived from field
HWG:id                 LIKE(HWG:id)                   !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BR3:View             VIEW(hwusers)
                       PROJECT(HWU:nameuser)
                       PROJECT(HWU:login)
                       PROJECT(HWU:d_blockdate)
                       PROJECT(HWU:id)
                     END
Queue:RADSQLBrowse:2 QUEUE                            !Queue declaration for browse/combo box using ?List:3
HWU:nameuser           LIKE(HWU:nameuser)             !List box control field - type derived from field
HWU:nameuser_Icon      LONG                           !Entry's icon ID
HWU:login              LIKE(HWU:login)                !List box control field - type derived from field
HWU:d_blockdate        LIKE(HWU:d_blockdate)          !Browse hot field - type derived from field
LOC:DataServidor       LIKE(LOC:DataServidor)         !Browse hot field - type derived from local data
HWU:id                 LIKE(HWU:id)                   !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Window               WINDOW('HWSecurity'),AT(,,507,297),FONT('Tahoma',8,,FONT:regular),IMM,SYSTEM,GRAY,DOUBLE,AUTO
                       PANEL,AT(1,1,258,22),USE(?Panel1),BEVEL(-1,1)
                       STRING('Grupos'),AT(4,3,252,18),USE(?String1),CENTER,FONT('Haettenschweiler',18,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                       PANEL,AT(260,1,246,22),USE(?Panel1:3),BEVEL(-1,1)
                       STRING('Usu�rios'),AT(262,3,241,18),USE(?String1:3),CENTER,FONT('Haettenschweiler',18,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                       LIST,AT(3,25,253,96),USE(?List:2),IMM,FLAT,VSCROLL,ALRT(MouseRight),FORMAT('147L(2)|F~Nome~@s36@43L(2)|F~Acesso~C@s10@48L(2)|F~Acesso Inicial~C@s12@'),FROM(Queue:RADSQLBrowse:1)
                       LIST,AT(262,25,241,96),USE(?List:3),IMM,FLAT,VSCROLL,MSG('Scrolling records...'),ALRT(MouseRight),FORMAT('148L(1)|FI~Nome~L(2)@s36@71L(2)|F~Login~@s20@'),FROM(Queue:RADSQLBrowse:2)
                       PANEL,AT(3,123,253,24),USE(?Panel1:4),BEVEL(-1,1)
                       BOX,AT(6,125,248,20),USE(?Box1),COLOR(COLOR:Gray),FILL(COLOR:Silver)
                       BUTTON('&Incluir'),AT(75,128,50,14),USE(?btnIncluirGrupo),TRN,FLAT,LEFT,ICON('incluir.ico')
                       BUTTON('&Alterar'),AT(135,128,50,14),USE(?btnAlterarGrupo),TRN,FLAT,LEFT,ICON('alterar.ico')
                       BUTTON('&Excluir'),AT(195,128,50,14),USE(?btnExcluirGrupo),TRN,FLAT,LEFT,ICON('excluir.ico')
                       PANEL,AT(262,123,241,24),USE(?Panel1:5),BEVEL(-1,1)
                       BOX,AT(264,125,237,20),USE(?Box1:2),COLOR(COLOR:Gray),FILL(COLOR:Silver)
                       BUTTON('&Duplicar'),AT(15,128,50,14),USE(?btnDuplicarGrupo),TRN,FLAT,LEFT,ICON('duplicar.ico')
                       BUTTON('&Incluir'),AT(298,128,47,14),USE(?btnIncluirUsuario),TRN,FLAT,LEFT,ICON('incluir.ico')
                       BUTTON('&Alterar'),AT(362,128,47,14),USE(?btnAlterarUsuario),TRN,FLAT,LEFT,ICON('alterar.ico')
                       BUTTON('&Excluir'),AT(426,128,47,14),USE(?btnExcluirUsuario),TRN,FLAT,LEFT,ICON('excluir.ico')
                       PANEL,AT(1,148,505,22),USE(?Panel1:2),BEVEL(-1,1)
                       STRING('Controles'),AT(3,150,500,18),USE(?String1:2),CENTER,FONT('Haettenschweiler',18,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                       LIST,AT(3,172,500,106),USE(?List2),FLAT,VSCROLL,FORMAT('204L(3)|F~Nome da Procedure~L(1)@s50@#1#160L(3)|F~Nome do Controle~L(1)@s39@#2#3' &|
   '0L(3)|FI~Acesso~C(1)@s3@#3#0R(3)|F~access icon~L(1)@n-14@#4#30L(3)|FI~Incluir~C(' &|
   '1)@s3@#5#0R(3)|F~insert icon~L(1)@n-14@#6#30L(3)|FI~Alterar~C(1)@s3@#7#0R(3)|F~c' &|
   'hange icon~L(1)@n-14@#8#30L(3)|FI~Excluir~C(1)@s3@#9#0R(3)|F~delete icon~L(1)@n-' &|
   '14@#10#'),FROM(qControls)
                       BUTTON('Permiss�es - Cidades do Usu�rio'),AT(3,281,128,14),USE(?btnPermissaoCidadeUsuario),FLAT,HIDE,LEFT,ICON('login_button.ico')
                       BUTTON('&Sair'),AT(457,281,46,14),USE(?btnSair),TRN,FLAT,LEFT,ICON('cancelar.ico'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Reset                  PROCEDURE(BYTE Force=0),DERIVED     ! Method added to host embed code
SetAlerts              PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
BR2                  CLASS(EXTSQLB)
FetchRecord            PROCEDURE(Byte In_forcefetch),DERIVED ! Method added to host embed code
ResetChildren          PROCEDURE(),DERIVED                 ! Method added to host embed code
RunForm                PROCEDURE(Byte in_Request),DERIVED  ! Method added to host embed code
SetQueueRecord         PROCEDURE(),DERIVED                 ! Method added to host embed code
SetRange               PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeEvent              PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END

BR3                  CLASS(EXTSQLB)
FetchRecord            PROCEDURE(Byte In_forcefetch),DERIVED ! Method added to host embed code
RunForm                PROCEDURE(Byte in_Request),DERIVED  ! Method added to host embed code
SetQueueRecord         PROCEDURE(),DERIVED                 ! Method added to host embed code
SetRange               PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeEvent              PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
RefreshQueue Routine
    Free(qControls)
    Clear(FLQ:Record)
    fLoadQueue{Prop:SQL} = 'Select hwp.procedurename, hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, '&|
                           'hwa.a_delete, hwa.id from hwprocedures hwp '&|
                           'left join hwaccess hwa on hwp.id = hwa.idprocedure '&|
                           'where hwa.idgroup = ' & HWG:id & ' and hwp.procedurename = ''' & PRM:nameproced &''''

    Loop
        Next(fLoadQueue)
        If Error() Then Break .

        QCO:procedurename = FLQ:campo1
        QCO:controlname   = FLQ:campo2

        If FLQ:campo3
            QCO:access_icon = 1
            QCO:a_access    = 'Sim'
        Else
            QCO:access_icon = 2
            QCO:a_access    = 'N�o'
        End

        If FLQ:campo4
            QCO:insert_icon = 3
            QCO:a_insert    = 'Sim'
        Else
            QCO:insert_icon = 4
            QCO:a_insert    = 'N�o'
        End

        If FLQ:campo5
            QCO:change_icon = 5
            QCO:a_change    = 'Sim'
        Else
            QCO:change_icon = 6
            QCO:a_change    = 'N�o'
        End

        If FLQ:campo6
            QCO:delete_icon = 7
            QCO:a_delete    = 'Sim'
        Else
            QCO:delete_icon = 8
            QCO:a_delete    = 'N�o'
        End

        QCO:idprocedure = FLQ:campo7

        Add(qControls)
    End
    Display(?List2)

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseGroupsUsersControls')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  COMPILE('C55',_C55_)
    Self.Filesopened = 1
  C55
  OMIT('C55',_C55_)
    Filesopened = 1
  C55
  Relate:hwgroups.Open()
  Access:hwgroups.UseFile()
  Relate:hwusers.Open()
  Access:hwusers.UseFile()
  Relate:hwaccess.SetOpenRelated()
  Relate:hwaccess.Open                                     ! File hwaccess used by this procedure, so make sure it's RelationManager is open
  Access:hwprocedures.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  Open(fLoadQueue, 42h)
  SELF.Open(Window)                                        ! Open window
  BR2.RefreshHotkey = F5Key
  BR2.Setlistcontrol (?List:2,10)
  BR3.RefreshHotkey = F5Key
  BR3.Setlistcontrol (?List:3,10)
  Do DefineListboxStyle
  BR2.PopUpActive = False
  BR2.Init(Access:hwgroups)
  BR2.useansijoin=1
  BR3.PopUpActive = False
  BR3.Init(Access:hwusers)
  BR3.useansijoin=1
  BR2.MaintainProcedure = 'UpdateGroups'
  BR2.SetupdateButtons (?btnIncluirGrupo,?btnAlterarGrupo,?btnExcluirGrupo,0)
  BR3.MaintainProcedure = 'UpdateUsers'
  BR3.SetupdateButtons (?btnIncluirUsuario,?btnAlterarUsuario,?btnExcluirUsuario,0)
  SELF.SetAlerts()
  ?List:2{PROP:From} = Queue:RADSQLBrowse:1
  BR2.AddTable('hwgroups',1,0,'hwgroups')  ! Add the table to the list of tables
  BR2.Listqueue      &= Queue:RADSQLBrowse:1
  BR2.Position       &= Queue:RADSQLBrowse:1.ViewPosition
  BR2.ColumnCharAsc   = ''
  BR2.ColumnCharDes   = ''
  BR2.AddFieldpairs(Queue:RADSQLBrowse:1.HWG:id,HWG:id,'hwgroups','id','N',0,'B','A','M','N',1,6)
  BR2.AddFieldpairs(Queue:RADSQLBrowse:1.HWG:namegroup,HWG:namegroup,'hwgroups','namegroup','A',0,'B','A','M','N',2,1)
  BR2.AddFieldpairs(Queue:RADSQLBrowse:1.HWG:accessgroup,HWG:accessgroup,'hwgroups','accessgroup','N',0,'B','A','M','N',3,4)
  !BR2.AddFieldpairs(Queue:RADSQLBrowse:1.HWG:accessgroup,HWG:accessgroup,'','','A',1,'N','N','M','N',3,0) ! Hotfield
  BR2.AddFieldpairs(Queue:RADSQLBrowse:1.HWG:accessinitial,HWG:accessinitial,'hwgroups','accessinitial','N',0,'B','A','M','N',4,5)
  !BR2.AddFieldpairs(Queue:RADSQLBrowse:1.HWG:accessinitial,HWG:accessinitial,'','','A',1,'N','N','M','N',4,0) ! Hotfield
  BR2.AddFieldpairs(Queue:RADSQLBrowse:1.LOC:Access,LOC:Access,'','','A',0,'B','A','M','N',0,2) ! Formula added
  BR2.AddFieldpairs(Queue:RADSQLBrowse:1.LOC:InitialAccess,LOC:InitialAccess,'','','A',0,'B','A','M','N',0,3) ! Formula added
  BR2.Managedview &=BR2:View
  BR2.Openview()
  BR2.SetFileLoad()
  BR2.SetForceFetch (1)
  ?List:3{PROP:From} = Queue:RADSQLBrowse:2
    ?List:3{PROP:IconList,1} = '~verde.ico'
    ?List:3{PROP:IconList,2} = '~vermelha.ico'
  BR3.AddTable('hwusers',1,0,'hwusers')  ! Add the table to the list of tables
  BR3.Listqueue      &= Queue:RADSQLBrowse:2
  BR3.Position       &= Queue:RADSQLBrowse:2.ViewPosition
  BR3.ColumnCharAsc   = ''
  BR3.ColumnCharDes   = ''
  BR3.AddFieldpairs(Queue:RADSQLBrowse:2.HWU:id,HWU:id,'hwusers','id','N',0,'B','A','M','N',1,5)
  BR3.AddFieldpairs(Queue:RADSQLBrowse:2.HWU:nameuser,HWU:nameuser,'hwusers','nameuser','A',0,'B','A','M','N',2,1)
  BR3.AddFieldpairs(Queue:RADSQLBrowse:2.HWU:login,HWU:login,'hwusers','login','A',0,'B','A','M','N',3,2)
  BR3.AddFieldpairs(Queue:RADSQLBrowse:2.HWU:d_blockdate,HWU:d_blockdate,'hwusers','d_blockdate','D',0,'B','A','M','N',5,3)
  !BR3.AddFieldpairs(Queue:RADSQLBrowse:2.HWU:d_blockdate,HWU:d_blockdate,'','','A',1,'N','N','M','N',5,0) ! Hotfield
  BR3.Managedview &=BR3:View
  BR3.Openview()
  BR3.SetFileLoad()
  BR3.SetForceFetch (1)
  BR2.SetColColor = 0
  BR2.Resetsort(1)
  BR3.SetColColor = 0
  BR3.Resetsort(1)
  EnterByTabManager.Init(False)
  BR2.RefreshMethod = 1
  BR3.RefreshMethod = 1
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  COMPILE('C55',_C55_)
  if Self.filesopened
  C55
  OMIT('C55',_C55_)
  If filesopened then
  C55
    Relate:hwgroups.Close
  END
  COMPILE('C55',_C55_)
  if Self.filesopened
  C55
  OMIT('C55',_C55_)
  If filesopened then
  C55
    Relate:hwusers.Close
  END
    Relate:hwaccess.Close
  Close(fLoadQueue)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF Window{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
     If force = 1  then
        BR2.ResetBrowse (1)
     end
     If force = 1  then
        BR3.ResetBrowse (1)
     end


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  BR2.SetAlerts()
  BR3.SetAlerts()


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?btnDuplicarGrupo
      ThisWindow.Update
      WindowDuplicateGroups
      BR2.ResetBrowse(1)
      BR3.ResetBrowse(1)
    OF ?List2
      If KeyCode() = MouseLeft2
          AnyChange# = 0
      
          Get(qControls, Choice(?List2))
          If ?List2{PropList:MouseDownField} = 3
              If QCO:access_icon = 1
                  QCO:access_icon = 2
                  QCO:a_access    = 'N�o'
                  QCO:insert_icon = 4
                  QCO:a_insert    = 'N�o'
                  QCO:change_icon = 6
                  QCO:a_change    = 'N�o'
                  QCO:delete_icon = 8
                  QCO:a_delete    = 'N�o'
              Else
                  QCO:access_icon = 1
                  QCO:a_access    = 'Sim'
              End
              AnyChange# = 1
              Put(qControls)
          Elsif ?List2{PropList:MouseDownField} = 5
              If QCO:access_icon = 1
                  If QCO:insert_icon = 3
                      QCO:insert_icon = 4
                      QCO:a_insert    = 'N�o'
                  Else
                      QCO:insert_icon = 3
                      QCO:a_insert    = 'Sim'
                  End
                  AnyChange# = 1
                  Put(qControls)
              End
          Elsif ?List2{PropList:MouseDownField} = 7
              If QCO:access_icon = 1
                  If QCO:change_icon = 5
                      QCO:change_icon = 6
                      QCO:a_change    = 'N�o'
                  Else
                      QCO:change_icon = 5
                      QCO:a_change    = 'Sim'
                  End
                  AnyChange# = 1
                  Put(qControls)
              End
          Elsif ?List2{PropList:MouseDownField} = 9
              If QCO:access_icon = 1
                  If QCO:delete_icon = 7
                      QCO:delete_icon = 8
                      QCO:a_delete    = 'N�o'
                  Else
                      QCO:delete_icon = 7
                      QCO:a_delete    = 'Sim'
                  End
                  AnyChange# = 1
                  Put(qControls)
              End
          End
      
          If AnyChange# = 1
              HWA:id = QCO:idprocedure
              Get(hwaccess, HWA:hwa_pk_id)
              If ~Error()
                  If QCO:access_icon = 1
                      HWA:a_access = 1
                  Else
                      HWA:a_access = 0
                  End
      
                  If QCO:insert_icon = 3
                      HWA:a_insert = 1
                  Else
                      HWA:a_insert = 0
                  End
      
                  If QCO:change_icon = 5
                      HWA:a_change = 1
                  Else
                      HWA:a_change = 0
                  End
      
                  If QCO:delete_icon = 7
                      HWA:a_delete = 1
                  Else
                      HWA:a_delete = 0
                  End
                  Put(hwaccess)
                  AnyChange# = 0
              End
          End
      
          Display(?List2)
      End
    OF ?btnPermissaoCidadeUsuario
      ThisWindow.Update
      BrowseCityUser(HWU:login)
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
  BR2.TakeEvent()
  BR3.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List:2
    CASE EVENT()
    OF EVENT:AlertKey
      If KeyCode() = MouseRight
          LOC:Popup = '['&Prop:Icon&'('&Path()&'\Lupazinha.ico)]Procurar Usu�rio|' & |
                      '['&Prop:Icon&'('&Path()&'\Usuariozinho.ico)]Mostrar Todos'
          Execute Popup(LOC:PopUp)
              Begin
                  WindowSearchUser
              End
              Begin
                  MOD:Busca = 0
              End
          End
      
          BR2.ResetBrowse(1)
          BR3.ResetBrowse(1)
      End
    END
  OF ?List:3
    CASE EVENT()
    OF EVENT:AlertKey
      If KeyCode() = MouseRight
          LOC:Popup = '['&Prop:Icon&'('&Path()&'\Lupazinha.ico)]Procurar Usu�rio|' & |
                      '['&Prop:Icon&'('&Path()&'\Usuariozinho.ico)]Mostrar Todos'
          Execute Popup(LOC:PopUp)
              Begin
                  WindowSearchUser
              End
              Begin
                  MOD:Busca = 0
              End
          End
      
          BR2.ResetBrowse(1)
          BR3.ResetBrowse(1)
      End
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ?List2{Prop:IconList,1} = Path() & '\check.ico'
      ?List2{Prop:IconList,2} = Path() & '\uncheck.ico'
      ?List2{Prop:IconList,3} = Path() & '\check.ico'
      ?List2{Prop:IconList,4} = Path() & '\uncheck.ico'
      ?List2{Prop:IconList,5} = Path() & '\check.ico'
      ?List2{Prop:IconList,6} = Path() & '\uncheck.ico'
      ?List2{Prop:IconList,7} = Path() & '\check.ico'
      ?List2{Prop:IconList,8} = Path() & '\uncheck.ico'
      
      !!Buscar a data do servidor
      Open(fDataServidor, 42h)
      Clear(FDS:Record)
      fDataServidor{Prop:SQL} = 'Select current_date'
      Next(fDataServidor)
      LOC:DataServidor = FDS:campo1
      Close(fDataServidor)
      
      BR2.ResetBrowse(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BR2.FetchRecord PROCEDURE(Byte In_forcefetch)


  CODE
  PARENT.FetchRecord(In_forcefetch)
  Do RefreshQueue


BR2.ResetChildren PROCEDURE


  CODE
  PARENT.ResetChildren
  BR3.ResetBrowse(1)


BR2.RunForm PROCEDURE(Byte in_Request)


  CODE
     GlobalRequest = in_Request
  PARENT.RunForm(in_Request)
     if in_Request = ChangeRecord or in_Request = DeleteRecord or in_Request = ViewRecord
        if Access:hwgroups.Fetch(HWG:hwg_pk_id) <> Level:Benign ! Fetch hwgroups on key 
          Message('Error on primary key fetch HWG:hwg_pk_id|Error:'& Error() &' FileError:'& FileError() &'|SQL:'& hwgroups{Prop:SQL})
          GlobalResponse = RequestCancelled
          Return
        end
     else
     end
  UpdateGroups 
     BR2.Response = Globalresponse
  
   BR2.ListControl{Prop:Selected} = BR2.CurrentChoice
   Display


BR2.SetQueueRecord PROCEDURE


  CODE
        IF (HWG:accessgroup = 1)
          LOC:Access = 'Supervisor'
        ELSE
          IF (HWG:accessgroup = 2)
            LOC:Access = 'Operador'
          ELSE
            LOC:Access = 'Sem Acesso'
          END
        END
        IF (HWG:accessinitial = 1)
          LOC:InitialAccess = 'Acesso Total'
        ELSE
          LOC:InitialAccess = 'Sem Acesso'
        END
  PARENT.SetQueueRecord


BR2.SetRange PROCEDURE


  CODE
  PARENT.SetRange
  Self.RangeFilter = ''
  LOC:SQLGrupo = ''
  If MOD:Busca = 1
      LOC:SQLGrupo = 'a.id = '&MOD:groupid
  End
  Self.RangeFilter = Self.RangeFilter & Clip(LOC:SQLGrupo)


BR2.TakeEvent PROCEDURE


  CODE
  CASE EVENT()
  OF Event:AlertKey
    CASE KEYCODE()
    OF MouseLeft2
      RETURN
    END
  END
  PARENT.TakeEvent


BR3.FetchRecord PROCEDURE(Byte In_forcefetch)


  CODE
  PARENT.FetchRecord(In_forcefetch)
  !GLO:nomeusuario = HWU:login


BR3.RunForm PROCEDURE(Byte in_Request)


  CODE
     GlobalRequest = in_Request
  PARENT.RunForm(in_Request)
     if in_Request = ChangeRecord or in_Request = DeleteRecord or in_Request = ViewRecord
        if Access:hwusers.Fetch(HWU:hwu_pk_id) <> Level:Benign ! Fetch hwusers on key 
          Message('Error on primary key fetch HWU:hwu_pk_id|Error:'& Error() &' FileError:'& FileError() &'|SQL:'& hwusers{Prop:SQL})
          GlobalResponse = RequestCancelled
          Return
        end
     else
          !Prime fields for insert from related browse on hwgroups
        HWU:groupid = HWG:id
     end
  UpdateUsers 
     BR3.Response = Globalresponse
  
   BR3.ListControl{Prop:Selected} = BR3.CurrentChoice
   Display


BR3.SetQueueRecord PROCEDURE


  CODE
    IF (HWU:d_blockdate and (LOC:DataServidor > HWU:d_blockdate))
      Queue:RADSQLBrowse:2.HWU:nameuser_Icon = 2
    ELSE
      Queue:RADSQLBrowse:2.HWU:nameuser_Icon = 1
    END
  PARENT.SetQueueRecord


BR3.SetRange PROCEDURE


  CODE
     !! Set the range limit
     Self.Rangefilter =  Self.Getalias('hwusers') & '.GROUPID=' & HWG:ID  ! HWG:
  PARENT.SetRange
  LOC:SQLUsuario   = ''
  If MOD:Busca = 1
      Self.RangeFilter = ''
      LOC:SQLUsuario = 'a.id = '&MOD:id
  End
  Self.RangeFilter = Self.RangeFilter & Clip(LOC:SQLUsuario)


BR3.TakeEvent PROCEDURE


  CODE
  CASE EVENT()
  OF Event:AlertKey
    CASE KEYCODE()
    OF MouseLeft2
      RETURN
    END
  END
  PARENT.TakeEvent

UpdateGroups PROCEDURE                                     ! Generated from procedure template - Window

LOC:AccessGroup      BYTE                                  !
LOC:InitialAccess    BYTE                                  !
CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
fBigSerialGroup File,Driver('ODBC','/TurboSql=1'),Owner(GLO:conexao),Name('consultasql'),Pre(FBG)
Record        Record
campo1          Cstring(101)
              End
          End

fBigSerialAccess File,Driver('ODBC','/TurboSql=1'),Owner(GLO:conexao),Name('consultasql'),Pre(FBA)
Record        Record
campo1          Cstring(101)
              End
          End

fSelectProcedure File,Driver('ODBC','/TurboSql=1'),Owner(GLO:conexao),Name('consultasql'),Pre(FSP)
Record        Record
campo1          Decimal(15)
              End
          End
History::HWG:Record  LIKE(HWG:RECORD),THREAD
QuickWindow          WINDOW('HWSecurity'),AT(,,291,82),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,SYSTEM,GRAY,DOUBLE,AUTO
                       PANEL,AT(1,1,288,22),USE(?Panel1),BEVEL(-1,1)
                       STRING('Grupo'),AT(3,3,283,18),USE(?String1),CENTER,FONT('Haettenschweiler',18,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                       PANEL,AT(4,25,282,39),USE(?Panel1:2),BEVEL(-1,1)
                       BOX,AT(6,27,277,35),USE(?Box1),COLOR(COLOR:Gray),FILL(COLOR:Silver)
                       PROMPT('Nome:'),AT(8,32,43,10),USE(?HWG:namegroup:Prompt),TRN,RIGHT
                       ENTRY(@s40),AT(54,32,207,10),USE(HWG:namegroup),FLAT,REQ,UPR,MSG('Nome do grupo')
                       PROMPT('Acesso:'),AT(8,47,43,10),USE(?HWG:accessgroup:Prompt),TRN,RIGHT
                       LIST,AT(54,47,79,10),USE(HWG:accessgroup),FLAT,DROP(5),FROM('Supervisor|#1|Operator|#2|No Access|#3'),MSG('Grupo de acesso')
                       PROMPT('Acesso Inicial:'),AT(136,47),USE(?HWG:accessinitial:Prompt),TRN,RIGHT
                       LIST,AT(182,47,79,10),USE(HWG:accessinitial),FLAT,DROP(5),FROM('Total Access|#1|No Access|#2'),MSG('Acesso inicial')
                       STRING(''),AT(5,64,108,10),USE(?String2),TRN
                       BUTTON('&Gravar'),AT(180,66,49,14),USE(?btnGravar),TRN,FLAT,LEFT,MSG('Accept data and close the window'),TIP('Accept data and close the window'),ICON('gravar.ico')
                       BUTTON('&Cancelar'),AT(232,66,53,14),USE(?btnCancelar),TRN,FLAT,LEFT,MSG('Cancel operation'),TIP('Cancel operation'),ICON('cancelar.ico')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
PrimeFields            PROCEDURE(),PROC,DERIVED            ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Group'
  OF InsertRecord
    ActionMessage = 'Incluindo Novo Grupo'
  OF ChangeRecord
    ActionMessage = 'Alterando o Grupo'
  END
  QuickWindow{Prop:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateGroups')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(HWG:Record,History::HWG:Record)
  SELF.AddHistoryField(?HWG:namegroup,2)
  SELF.AddHistoryField(?HWG:accessgroup,3)
  SELF.AddHistoryField(?HWG:accessinitial,4)
  SELF.AddUpdateFile(Access:hwgroups)
  SELF.AddItem(?btnCancelar,RequestCancelled)              ! Add the cancel control to the window manager
  Relate:hwaccess.SetOpenRelated()
  Relate:hwaccess.Open                                     ! File hwaccess used by this procedure, so make sure it's RelationManager is open
  Access:hwprocedures.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:hwgroups
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?btnGravar
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>UpdateGroups<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>UpdateGroups<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?HWG:namegroup{PROP:ReadOnly} = True
    DISABLE(?HWG:accessgroup)
    DISABLE(?HWG:accessinitial)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:hwaccess.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    HWG:accessgroup = 2
    HWG:accessinitial = 1
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?HWG:accessgroup
      If HWG:accessgroup = 1
          HWG:accessinitial = 1
          Disable(?HWG:accessinitial)
      Elsif HWG:accessgroup = 2
          Enable(?HWG:accessinitial)
      Elsif HWG:accessgroup = 3
          HWG:accessinitial = 2
          Disable(?HWG:accessinitial)
      End
    OF ?btnGravar
      ThisWindow.Update
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If Self.Request = InsertRecord
      Open(fBigSerialGroup,42h)
      Clear(FBG:Record)
      fBigSerialGroup{Prop:SQL} = 'Select nextval(''hwgroups_id_seq'')'
      Next(fBigSerialGroup)
      HWG:id = FBG:campo1
      Close(fBigSerialGroup)
  End
  ReturnValue = PARENT.TakeCompleted()
  Open(fBigSerialAccess, 42h)
  Open(fSelectProcedure, 42h)
  
  If Self.Request = InsertRecord And Self.Response = RequestCompleted
      ?String2{Prop:Text} = 'Aguarde, processando...'
      Display
  
      Clear(FBA:Record)
      fBigSerialAccess{Prop:SQL} = 'select func_hw_incluiacesso('& HWG:id &', '& HWG:accessinitial &', '& 2 &')'
  
  !    Clear(FSP:Record)
  !    fSelectProcedure{Prop:SQL} = 'Select id from hwprocedures'
  !    Loop
  !        Next(fSelectProcedure)
  !        If Error() Then Break .
  !
  !        Clear(FBA:Record)
  !        fBigSerialAccess{Prop:SQL} = 'Select nextval(''hwaccess_id_seq'')'
  !        Next(fBigSerialAccess)
  !
  !        HWA:id          = FBA:campo1
  !        HWA:idprocedure = FSP:campo1
  !        HWA:idgroup     = HWG:id
  !
  !        If HWG:accessinitial = 1
  !            HWA:a_access = 1
  !            HWA:a_insert = 1
  !            HWA:a_change = 1
  !            HWA:a_delete = 1
  !        Else
  !            HWA:a_access = 0
  !            HWA:a_insert = 0
  !            HWA:a_change = 0
  !            HWA:a_delete = 0
  !        End
  !
  !        Add(hwaccess)
  !    End
  Elsif Self.Request = ChangeRecord And Self.Response = RequestCompleted
      ?String2{Prop:Text} = 'Aguarde, processando...'
      Display
  
      If HWG:accessgroup ~= LOC:AccessGroup Or HWG:accessinitial ~= LOC:InitialAccess
        Case Message('Deseja alterar os acessos j� existentes?','Aten��o',Icon:Question,'Sim|N�o',2,2)
        Of 1
          If HWG:accessinitial = 1                                  !! Update para atualizar o acesso
              Clear(FBA:Record) 
              fBigSerialAccess{Prop:SQL} = 'Update hwaccess set a_access = 1, a_insert = 1, a_change = 1, a_delete = 1' &|
                                           'where idgroup = ' & HWG:id
          Else
              Clear(FBA:Record) 
              fBigSerialAccess{Prop:SQL} = 'Update hwaccess set a_access = 0, a_insert = 0, a_change = 0, a_delete = 0' &|
                                           'where idgroup = ' & HWG:id
          End
        End
      End
  End
  
  ?String2{Prop:Text} = ''
  Display
  
  Close(fBigSerialAccess)
  Close(fSelectProcedure)
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('UpdateGroups')
          End
        End
      End
      Close(HWGroups)
    OF EVENT:OpenWindow
      LOC:AccessGroup   = HWG:accessgroup
      LOC:InitialAccess = HWG:accessinitial
      
      If HWG:accessgroup = 1
          HWG:accessinitial = 1
          Disable(?HWG:accessinitial)
      Elsif HWG:accessgroup = 2
          Enable(?HWG:accessinitial)
      Elsif HWG:accessgroup = 3
          HWG:accessinitial = 2
          Disable(?HWG:accessinitial)
      End
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

UpdateUsers PROCEDURE                                      ! Generated from procedure template - Window

LOC:ConfirmPassword  CSTRING(21)                           !
CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
RADSQLDrop4Locator CString(256)
RADSQLDrop4:View     VIEW(hwgroups)
                       PROJECT(HWG:namegroup)
                       PROJECT(HWG:id)
                     END
Queue:RADSQLDrop     QUEUE                            !Queue declaration for browse/combo box using ?HWG:namegroup
HWG:namegroup          LIKE(HWG:namegroup)            !List box control field - type derived from field
HWG:id                 LIKE(HWG:id)                   !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
fBigSerialUser File,Driver('ODBC','/TurboSql=1'),Owner(GLO:conexao),Name('consultasql'),Pre(FBU)
Record        Record
campo1          Decimal(15)
              End
          End
History::HWU:Record  LIKE(HWU:RECORD),THREAD
QuickWindow          WINDOW('HWSecurity'),AT(,,343,106),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,SYSTEM,GRAY,DOUBLE,AUTO
                       ENTRY(@s40),AT(31,34,157,10),USE(HWU:nameuser),FLAT,REQ,UPR,MSG('Nome do usu�rio')
                       COMBO(@s255),AT(215,34,119,10),USE(HWG:namegroup),IMM,FLAT,VSCROLL,FORMAT('160L(2)F@s40@'),DROP(10),FROM(Queue:RADSQLDrop),MSG('Nome do grupo')
                       ENTRY(@s20),AT(31,49,73,10),USE(HWU:login),FLAT,REQ,UPR,MSG('Login do usu�rio')
                       ENTRY(@s20),AT(140,49,73,10),USE(HWU:user_password),FLAT,REQ,PASSWORD,MSG('Senha do usu�rio')
                       ENTRY(@s20),AT(261,49,73,10),USE(LOC:ConfirmPassword),FLAT,PASSWORD
                       ENTRY(@d06b),AT(82,68,73,10),USE(HWU:d_blockdate),FLAT,MSG('Data de bloqueio')
                       BUTTON('&Gravar'),AT(231,90,49,14),USE(?btnGravar),TRN,FLAT,LEFT,MSG('Accept data and close the window'),TIP('Accept data and close the window'),ICON('gravar.ico')
                       BUTTON('&Cancelar'),AT(285,90,54,14),USE(?btnCancelar),TRN,FLAT,LEFT,MSG('Cancel operation'),TIP('Cancel operation'),ICON('cancelar.ico')
                       STRING('Grupo:'),AT(193,34),USE(?String2),TRN
                       BOX,AT(91,64,161,18),USE(?Box2),ROUND,COLOR(COLOR:Gray),FILL(COLOR:Silver)
                       PROMPT('Data de Desativa��o:'),AT(11,68),USE(?HWU:d_blockdate:Prompt),TRN
                       PANEL,AT(2,1,340,22),USE(?Panel1),BEVEL(-1,1)
                       STRING('Usu�rios'),AT(4,3,335,18),USE(?String1),CENTER,FONT('Haettenschweiler',18,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                       PANEL,AT(4,25,336,63),USE(?Panel1:2),BEVEL(-1,1)
                       BOX,AT(6,27,331,59),USE(?Box1),COLOR(COLOR:Gray),FILL(COLOR:Silver),LINEWIDTH(1)
                       PROMPT('Nome:'),AT(9,34),USE(?HWU:nameuser:Prompt),TRN,FONT(,,COLOR:Black,,CHARSET:ANSI)
                       PROMPT('Login:'),AT(11,49),USE(?HWU:login:Prompt),TRN
                       PROMPT('Password:'),AT(107,49),USE(?HWU:user_password:Prompt),TRN
                       PROMPT('Re-Password:'),AT(216,49),USE(?LOC:ConfirmPassword:Prompt),TRN
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Reset                  PROCEDURE(BYTE Force=0),DERIVED     ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
SetAlerts              PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END

RADSQLDrop4          RADSQLDrop
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View User'
  OF InsertRecord
    ActionMessage = 'Incluindo Novo Usu�rio'
  OF ChangeRecord
    ActionMessage = 'Alterando o Usu�rio'
  END
  QuickWindow{Prop:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateUsers')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?HWU:nameuser
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(HWU:Record,History::HWU:Record)
  SELF.AddHistoryField(?HWU:nameuser,2)
  SELF.AddHistoryField(?HWU:login,3)
  SELF.AddHistoryField(?HWU:user_password,4)
  SELF.AddHistoryField(?HWU:d_blockdate,5)
  SELF.AddUpdateFile(Access:hwusers)
  SELF.AddItem(?btnCancelar,RequestCancelled)              ! Add the cancel control to the window manager
  COMPILE('C55',_C55_)
    Self.Filesopened = 1
  C55
  OMIT('C55',_C55_)
    Filesopened = 1
  C55
  Relate:hwgroups.Open()
  Access:hwgroups.UseFile()
  Relate:hwusers.Open                                      ! File hwusers used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:hwusers
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?btnGravar
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  If Self.Request = ChangeRecord 
      Get(hwusers, HWU:hwu_pk_id)
      LOC:ConfirmPassword = HWU:user_password
  End
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>UpdateUsers<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>UpdateUsers<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?HWU:nameuser{PROP:ReadOnly} = True
    DISABLE(?HWG:namegroup)
    ?HWU:login{PROP:ReadOnly} = True
    ?HWU:user_password{PROP:ReadOnly} = True
    ?LOC:ConfirmPassword{PROP:ReadOnly} = True
    ?HWU:d_blockdate{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  RADSQLDrop4.Listqueue      &= Queue:RADSQLDrop
  RADSQLDrop4.Position       &= Queue:RADSQLDrop.ViewPosition
  RADSQLDrop4.Setlistcontrol(?HWG:namegroup)
  RADSQLDrop4.RefreshHotkey = F5Key
  RADSQLDrop4.AddDropVariables(?HWG:namegroup,0,HWG:namegroup,'HWG:id',HWG:id,HWU:groupid)
  RADSQLDrop4.Init(Access:hwgroups)
  RADSQLDrop4.AddField(HWG:id,HWU:groupid)
     RADSQLDrop4.useansijoin=1
  SELF.SetAlerts()
  ?HWG:namegroup{PROP:From} = Queue:RADSQLDrop
  RADSQLDrop4.AddTable('hwgroups',1,0,'HWGROUPS')  ! Add the table to the list of tables
  RADSQLDrop4.ColumnCharAsc   = ''
  RADSQLDrop4.ColumnCharDes   = ''
  RADSQLDrop4.AddFieldpairs(Queue:RADSQLDrop.HWG:id,HWG:id,'hwgroups','id','N',0,'B','A','I','N',1,2)
  RADSQLDrop4.AddFieldpairs(Queue:RADSQLDrop.HWG:namegroup,HWG:namegroup,'hwgroups','namegroup','A',0,'B','A','I','N',2,1)
  RADSQLDrop4.Managedview &=RADSQLDrop4:View
  RADSQLDrop4.Openview
  RADSQLDrop4.SetFileLoad
  RADSQLDrop4.SetForceFetch (1)
  RADSQLDrop4.SetColColor = 0
  RADSQLDrop4.Resetsort(1,1)
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  COMPILE('C55',_C55_)
  if Self.filesopened
  C55
  OMIT('C55',_C55_)
  If filesopened then
  C55
  Relate:hwgroups.Close
  END
    Relate:hwusers.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
  If force = 2
    RADSQLDrop4.ResetBrowse (1)
  end


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  RADSQLDrop4.SetAlerts


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?HWU:nameuser
      If ~0{Prop:acceptall}
          If Self.Request = InsertRecord
              If Instring(' ', HWU:nameuser, 1, 1)
                  HWU:login = Sub(HWU:nameuser, 1, Instring(' ', HWU:nameuser, 1, 1)-1)
              Else
                  HWU:login = HWU:nameuser
              End
              Display
          End
      End
    OF ?LOC:ConfirmPassword
      If Clip(LOC:ConfirmPassword) ~= Clip(HWU:user_password)
          Message('O Password n�o � id�ntico |Tente novamente!','HWSecurity',Icon:Exclamation,'OK')
          Clear(HWU:user_password)
          Clear(LOC:ConfirmPassword)
          Select(HWU:user_password)
          Display
      End
    OF ?btnGravar
      ThisWindow.Update
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If Self.Request = InsertRecord
      Open(fBigSerialUser,42h)
      Clear(FBU:Record)
      fBigSerialUser{Prop:SQL} = 'Select nextval(''hwusers_id_seq'')'
      Next(fBigSerialUser)
      HWU:id = FBU:campo1
      Close(fBigSerialUser)
  End
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
  RADSQLDrop4.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('UpdateUsers')
          End
        End
      End
      Close(HWGroups)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

WindowChangePassword PROCEDURE                             ! Generated from procedure template - Window

LOC:senha            CSTRING(21)                           !
LOC:NovaSenha        CSTRING(21)                           !
LOC:ReNovaSenha      CSTRING(21)                           !
qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
Window               WINDOW('Trocar Senha'),AT(,,255,89),FONT('Tahoma',8,COLOR:Navy,FONT:regular,CHARSET:ANSI),CENTER,IMM,SYSTEM,GRAY,DOUBLE,AUTO
                       IMAGE('EULA.JPG'),AT(3,2,62,85),USE(?Image1)
                       BOX,AT(67,2,185,85),USE(?Box1),COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(1)
                       PROMPT('Senha Atual:'),AT(73,14,67,10),USE(?LOC:senha:Prompt),TRN,RIGHT
                       ENTRY(@s20),AT(143,14,103,10),USE(LOC:senha),FLAT,PASSWORD
                       PROMPT('Nova Senha:'),AT(73,28,67,10),USE(?LOC:NovaSenha:Prompt),TRN,RIGHT
                       ENTRY(@s20),AT(143,28,103,10),USE(LOC:NovaSenha),FLAT,PASSWORD
                       PROMPT('Repete Nova Senha:'),AT(73,42),USE(?LOC:ReNovaSenha:Prompt),TRN,RIGHT
                       ENTRY(@s20),AT(143,42,103,10),USE(LOC:ReNovaSenha),FLAT,PASSWORD
                       BUTTON('OK'),AT(108,66,59,15),USE(?btnOK),TRN,FLAT,LEFT,FONT('Arial',9,,FONT:bold,CHARSET:ANSI),ICON('gravar.ico')
                       BUTTON('Cancelar'),AT(168,66,59,15),USE(?btnCancelar),TRN,FLAT,LEFT,FONT('Arial',9,,FONT:bold,CHARSET:ANSI),ICON('cancelar.ico'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('WindowChangePassword')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>WindowChangePassword<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>WindowChangePassword<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LOC:senha
      If LOC:senha ~= GLO:senhausuario
        Message('Senha inv�lida!','Senha',Icon:Exclamation,'OK',,2)
        Clear(LOC:Senha)
        Select(?LOC:Senha)
        Display
      End
    OF ?LOC:NovaSenha
      If LOC:NovaSenha = LOC:senha
        Message('A senha deve ser diferente da atual!','Senha',Icon:Exclamation,'OK',,2)
        Clear(LOC:NovaSenha)
        Select(?LOC:NovaSenha)
        Display
      ElsIf LOC:NovaSenha And LOC:ReNovaSenha
        If LOC:NovaSenha ~= LOC:ReNovaSenha
          Message('Senhas n�o conferem!','Senha',Icon:Exclamation,'OK',,2)
          Clear(LOC:NovaSenha)
          Select(?LOC:NovaSenha)
          Display
        End
      End
    OF ?LOC:ReNovaSenha
      If LOC:NovaSenha And LOC:ReNovaSenha
        If LOC:NovaSenha ~= LOC:ReNovaSenha
          Message('Senhas n�o conferem!','Senha',Icon:Exclamation,'OK',,2)
          Clear(LOC:ReNovaSenha)
          Select(?LOC:ReNovaSenha)
          Display
        End
      End
    OF ?btnOK
      ThisWindow.Update
      If LOC:senha And LOC:NovaSenha And LOC:ReNovaSenha
        Case Message('Deseja realmente alterar a senha?','Aten��o',Icon:Exclamation,'Sim|N�o',2,2)
          Of 1
            consultasql{prop:sql} = 'Update hwusers set user_password = <39>' & LOC:NovaSenha & '<39> where login = <39>' & GLO:nomeusuario & '<39>'
            post(event:closewindow)
        End
      End
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('WindowChangePassword')
          End
        End
      End
      Close(HWGroups)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

WindowSearchUser PROCEDURE                                 ! Generated from procedure template - Window

LOC:NomeUsuario      CSTRING(41)                           !
qResultados          QUEUE,PRE(QRE)                        !
Grupo                CSTRING(41)                           !
Usuario              CSTRING(41)                           !
GroupID              LONG                                  !
ID                   LONG                                  !
                     END                                   !
qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
fBuscaUser File,Driver('ODBC','/TurboSql=1'),Owner(GLO:conexao),Name('consultasql'),Pre(FBU)
Record        Record
campo1          CString(101)
campo2          CString(101)
campo3          Decimal(15)
campo4          Decimal(15)
              End
          End
Window               WINDOW('HWSecurity'),AT(,,315,121),FONT('MS Sans Serif',8,,FONT:regular),CENTER,IMM,SYSTEM,GRAY,DOUBLE,AUTO
                       PROMPT('Procurar Usu�rio:'),AT(15,7,67,9),USE(?LOC:NomeUsuario:Prompt),TRN,LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Gray)
                       ENTRY(@s40),AT(83,7,199,10),USE(LOC:NomeUsuario),FLAT,UPR
                       BUTTON,AT(284,5,14,13),USE(?btnBusca),TRN,FLAT,ICON('lupazinha.ico')
                       LIST,AT(3,22,308,81),USE(?lstResultados),FLAT,VSCROLL,FORMAT('147L(2)|F~Grupo~@s36@#1#160L(2)F~Usuario~@s36@#2#'),FROM(qResultados)
                       PANEL,AT(2,2,310,19),USE(?Panel1),BEVEL(-1,1)
                       BUTTON('Selecionar'),AT(194,105,57,14),USE(?btnSelecionar),DISABLE,TRN,FLAT,LEFT,ICON('selecionar.ico')
                       BUTTON('Cancelar'),AT(254,105,57,14),USE(?btnCancelar),TRN,FLAT,LEFT,ICON('cancelar.ico'),STD(STD:Close)
                       BOX,AT(5,4,303,14),USE(?Box1),COLOR(COLOR:Gray),FILL(COLOR:Gray)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('WindowSearchUser')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:NomeUsuario:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>WindowSearchUser<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>WindowSearchUser<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LOC:NomeUsuario
      Post(Event:Accepted, ?btnBusca)
      Display
    OF ?btnBusca
      ThisWindow.Update
      If Clip(LOC:NomeUsuario)
          Open(fBuscaUser,42h)
          Clear(FBU:Record)
      
          fBuscaUser{Prop:SQL} = 'select hwg.namegroup, hwu.login, hwu.groupid, hwu.id '&|
                                 'from hwusers hwu left join hwgroups hwg on hwg.id = hwu.groupid '&|
                                 'where lower(hwu.login) ilike <39>%'& Lower(LOC:NomeUsuario) &'%<39>'
          Loop
              Next(fBuscaUser)
              If Error() Then Break .
      
              QRE:Grupo   = Clip(FBU:campo1)
              QRE:Usuario = Clip(FBU:campo2)
              QRE:GroupID = FBU:campo3
              QRE:ID      = FBU:campo4
              Add(qResultados)
              
          End
      
          Close(fBuscaUser)
      
          If ~Records(qResultados)
              Message('Nenhum usu�rio foi encontrado!', 'HWSecurity', ICON:Exclamation)
              Disable(?btnSelecionar)
          Else
              Enable(?btnSelecionar)
          End
      End
      Display
    OF ?btnSelecionar
      ThisWindow.Update
      If Records(qResultados)
          Get(qResultados, Choice(?lstResultados))
      
          MOD:groupid = QRE:GroupID
          MOD:id      = QRE:ID
          MOD:Busca = 1
      End
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('WindowSearchUser')
          End
        End
      End
      Close(HWGroups)
    OF EVENT:OpenWindow
      MOD:Busca = 0
      Free(qResultados)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

BrowseCityUser PROCEDURE (prt:login)                       ! Generated from procedure template - Browse

QueueEmpresa         QUEUE,PRE(Qcid)                       !
codigoempresa        STRING(10)                            !
icone                LONG                                  !
nomeempresa          CSTRING(31)                           !
empresalong          LONG                                  !
                     END                                   !
qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
Window               WINDOW('HWSecurity'),AT(,,196,161),FONT('Tahoma',8,,),CENTER,SYSTEM,GRAY,DOUBLE
                       PROMPT('Empresa do Usu�rios'),AT(4,2,188,18),USE(?Prompt1),CENTER,FONT('Haettenschweiler',18,COLOR:White,),COLOR(COLOR:Gray)
                       PANEL,AT(1,0,194,22),USE(?Panel1),BEVEL(-1,1)
                       LIST,AT(3,24,189,119),USE(?ListEmpresa),FLAT,VSCROLL,FORMAT('52R(1)|I~C�digo~C(0)@s10@#1#0R~icone~L(2)@n-14@#2#120L(3)|~Nome~L(2)@s30@#3#'),FROM(QueueEmpresa)
                       BUTTON('&Fechar'),AT(142,145,50,14),USE(?btnFechar),FLAT,LEFT,ICON('cancelar.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
fturbo  FILE,DRIVER('ODBC','/Turbosql=1'),OWNER(GLO:conexao),PRE(FTU),Name('consultasql')
Record     Record
campo1       long
campo2       Cstring(31)
           End
        End

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseCityUser')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?btnFechar,RequestCancelled)             ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?btnFechar,RequestCompleted)             ! Add the close control to the window manger
  END
  Relate:empresa.SetOpenRelated()
  Relate:empresa.Open                                      ! File empresa used by this procedure, so make sure it's RelationManager is open
  Access:empresadousuario.UseFile                          ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>BrowseCityUser<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>BrowseCityUser<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:empresa.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ListEmpresa
      if keycode() = mouseleft2 then
        get(QueueEmpresa,choice(?ListEmpresa))
      
        USUE:codempresa = Qcid:Empresalong
        USUE:usuario   = upper(prt:Login)
        get(empresadousuario,USUE:usu_pk_usuariosporempresa)
        if error() then
          USUE:codempresa = Qcid:Empresalong
          USUE:usuario   = upper(prt:Login)
          access:empresadousuario.insert()
      
          Qcid:icone = 1
          put(QueueEmpresa)
        else
          USUE:codempresa = Qcid:Empresalong
          USUE:usuario   = upper(prt:Login)
          access:empresadousuario.deleterecord(0)
      
          Qcid:icone = 2
          put(QueueEmpresa)
        end
      end
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('BrowseCityUser')
          End
        End
      End
      Close(HWGroups)
    OF EVENT:OpenWindow
      open(fturbo,42h)
      clear(fturbo:record)
      ?ListEmpresa{prop:iconlist,1} = Path() & '\img\check.ico'
      ?ListEmpresa{prop:iconlist,2} = Path() & '\img\uncheck.ico'
      
      fturbo{prop:sql} = 'Select a.codempresa, a.razaosocial from empresa a'
      loop
        next(fturbo)
        if error() then break end
        Qcid:codigoempresa = ftu:campo1
        Qcid:icone         = 2
        Qcid:nomeempresa   = ftu:campo2
        Qcid:empresalong   = ftu:campo1
      
        USUE:codempresa = ftu:campo1
        USUE:usuario    = prt:login
        get(empresadousuario,USUE:usu_pk_usuariosporempresa)
        if ~error() then
            Qcid:icone = 1
        end
      
        add(QueueEmpresa)
      end
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

WindowDuplicateGroups PROCEDURE                            ! Generated from procedure template - Window

LOC:NomeGrupo        CSTRING(41)                           !
LOC:IdGrupoBase      DECIMAL(15)                           !
LOC:AcessoGrupoBase  BYTE                                  !
LOC:AcessoInicialGrupoBase BYTE                            !
qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
Window               WINDOW('HWSecurity'),AT(,,226,67),FONT('MS Sans Serif',8,,FONT:regular),CENTER,IMM,SYSTEM,GRAY,DOUBLE,AUTO
                       PANEL,AT(2,1,223,22),USE(?Panel1),BEVEL(-1,1)
                       STRING('Duplicar Grupo'),AT(4,3,218,18),USE(?String1),CENTER,FONT('Haettenschweiler',18,COLOR:White,FONT:regular,CHARSET:ANSI),COLOR(COLOR:Gray)
                       PANEL,AT(4,25,217,22),USE(?Panel1:2),BEVEL(-1,1)
                       PROMPT('Nome do Grupo:'),AT(12,31),USE(?LOC:NovoNome:Prompt),TRN
                       ENTRY(@s40),AT(66,31,144,10),USE(LOC:NomeGrupo),FLAT
                       BOX,AT(6,27,212,18),USE(?Box1),COLOR(COLOR:Silver),FILL(COLOR:Silver)
                       BUTTON('&Gravar'),AT(113,50,49,14),USE(?OkButton),TRN,FLAT,LEFT,ICON('gravar.ico')
                       BUTTON('&Cancelar'),AT(167,50,53,14),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelar.ico'),STD(STD:Close)
                     END

fDuplicarGrupo File,Driver('ODBC','/TurboSql=1'),Owner(GLO:conexao),Name('consultasql'),Pre(FDG)
Record        Record
campo1          Decimal(15)
              End
          End
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('WindowDuplicateGroups')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:hwgroups.SetOpenRelated()
  Relate:hwgroups.Open                                     ! File hwgroups used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>WindowDuplicateGroups<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>WindowDuplicateGroups<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:hwgroups.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      !! Duplicar o grupo, mantendo iguais os acessos
      Open(fDuplicarGrupo, 42h)
      
      LOC:IdGrupoBase            = HWG:id
      LOC:AcessoGrupoBase        = HWG:accessgroup
      LOC:AcessoInicialGrupoBase = HWG:accessinitial
      
      !! Gravar o novo grupo
      Clear(FDG:Record)
      fDuplicarGrupo{PROP:Sql} = 'select nextval(''hwgroups_id_seq'')'
      Next(fDuplicarGrupo)
      HWG:id            = FDG:campo1
      HWG:namegroup     = LOC:NomeGrupo
      HWG:accessgroup   = LOC:AcessoGrupoBase
      HWG:accessinitial = LOC:AcessoInicialGrupoBase
      Add(hwgroups)
      
      !! Incluir acessos iguais ao grupo base
      Clear(FDG:Record)
      fDuplicarGrupo{PROP:Sql} = 'insert into hwaccess (idprocedure, idgroup, a_access, a_insert, a_change, a_delete) '&|
                                 'select hwa.idprocedure, '& HWG:id &', hwa.a_access, hwa.a_insert, hwa.a_change, hwa.a_delete '&|
                                 'from hwaccess hwa '&|
                                 'where hwa.idgroup = '& LOC:IdGrupoBase
      
      Close(fDuplicarGrupo)
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('WindowDuplicateGroups')
          End
        End
      End
      Close(HWGroups)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

WindowLogin PROCEDURE (<PAR:Chamada>)                      ! Generated from procedure template - Window

LOC:Tentativas       BYTE                                  !
LOC:DataServidor     DATE                                  !
qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
Window               WINDOW('Login'),AT(,,255,82),FONT('Tahoma',8,COLOR:Navy,FONT:regular,CHARSET:ANSI),CENTER,IMM,ALRT(EscKey),ALRT(AltF4),GRAY,DOUBLE,AUTO
                       IMAGE('PID.JPG'),AT(2,2,62,78),USE(?Image1)
                       BOX,AT(67,2,185,78),USE(?Box1),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                       IMAGE('login.gif'),AT(73,6),USE(?Image2)
                       PROMPT('Login:'),AT(76,25,23,10),USE(?GLO:nomeusuario:Prompt),TRN,RIGHT
                       ENTRY(@s30),AT(101,25,144,10),USE(GLO:nomeusuario),FLAT,MSG('Login do usu�rio conectado ao sistema'),TIP('Login do usu�rio conectado ao sistema'),UPR
                       PROMPT('Senha:'),AT(76,39),USE(?GLO:senha:Prompt),TRN,RIGHT
                       ENTRY(@s20),AT(101,39,144,10),USE(GLO:senhausuario),FLAT,MSG('Login do usu�rio conectado ao sistema'),TIP('Login do usu�rio conectado ao sistema'),PASSWORD
                       BUTTON('OK'),AT(111,59,59,15),USE(?btnOK),TRN,FLAT,LEFT,FONT('Arial',9,,FONT:bold,CHARSET:ANSI),ICON('gravar.ico'),DEFAULT
                       BUTTON('Cancelar'),AT(176,59,59,15),USE(?btnCancelar),TRN,FLAT,LEFT,FONT('Arial',9,,FONT:bold,CHARSET:ANSI),ICON('cancelar.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('WindowLogin')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:consultasql.Open                                  ! File consultasql used by this procedure, so make sure it's RelationManager is open
  Relate:hwusers.Open                                      ! File hwusers used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>WindowLogin<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>WindowLogin<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:consultasql.Close
    Relate:hwusers.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?btnOK
      ThisWindow.Update
      !Clear(CSQ:Record)
      !consultasql{prop:sql} = 'Select nameuser From hwusers where lower(login) = <39>'&Lower(GLO:nomeusuario)&'<39>'
      HWU:login = GLO:nomeusuario
      Get(hwusers, HWU:hwu_login)
      If Error()
        If LOC:tentativas >= 3
          Message('Login inv�lido!|N�mero de tentativas esgotadas!','Senha',Icon:Exclamation,'OK',,2)
          Halt
        Else
          Message('Login inv�lido!','Login',Icon:Exclamation,'OK',,2)
          Clear(GLO:nomeusuario)
          Clear(GLO:senhausuario)
          Select(?GLO:nomeusuario)
        End
      Else
        If LOC:DataServidor <= HWU:d_blockdate
          Message('Usu�rio bloqueado para acesso no sistema!','Aten��o',Icon:Exclamation,'OK',,2)
          Clear(GLO:nomeusuario)
          Clear(GLO:senhausuario)
          Select(?GLO:nomeusuario)
        Else
          !stop(GLO:senhausuario &' - '& HWU:user_password)
          If GLO:senhausuario ~= HWU:user_password
            If LOC:tentativas >= 3
              Message('Senha inv�lida!|N�mero de tentativas esgotadas!','Senha',Icon:Exclamation,'OK',,2)
              Halt
            Else
              Message('Senha inv�lida!','Senha',Icon:Exclamation,'OK',,2)
              If ~PAR:Chamada = 1
                LOC:Tentativas += 1
                Clear(GLO:senhausuario)
                Select(?GLO:senhausuario)
              Else
                Clear(GLO:senhausuario)
                Select(?GLO:senhausuario)
              End
            End
          Else
            GLO:UsuarioConexao = lower(TrocaLetras(HWU:login))
            GLO:SenhaConexao = 'cgm@i745$passwd'!HWU:user_password&'@i745user'
            GLO:idgroup = HWU:groupid
            GLO:idhwuser = HWU:id
            Post(Event:CloseWindow)
            !0{prop:text} = 'Toca Im�veis'
            !WindowSelectEmpresa
            !Main
          End
        End
      End
    OF ?btnCancelar
      ThisWindow.Update
      Halt
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      Cycle
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('WindowLogin')
          End
        End
      End
      Close(HWGroups)
    OF EVENT:OpenWindow
      Clear(GLO:nomeusuario)
      Clear(GLO:senhausuario)
      Clear(GLO:idgroup)
      Clear(LOC:tentativas)
      LOC:DataServidor = DataServidor()
      If PAR:Chamada = 1
          Disable(?btnCancelar)
          Display
      End
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

