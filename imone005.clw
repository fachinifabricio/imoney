

   MEMBER('imoney.clw')                                    ! This is a MEMBER module


   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('abexcel.inc'),ONCE
   INCLUDE('radsqlb.inc'),ONCE
   INCLUDE('radsqldr.inc'),ONCE

                     MAP
                       INCLUDE('IMONE005.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('IMOFUNCOES.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE006.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE007.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE009.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE013.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseDocumentos PROCEDURE                                 ! Generated from procedure template - Window

LOC:favorecido       LONG                                  !
LOC:datainicio       DATE                                  !
LOC:DataFim          DATE                                  !
LOC:TipoData         BYTE(2)                               !
LOC:TipoDataiManager BYTE                                  !
LOC:situacao         BYTE(1)                               !
LOC:Filtro           CSTRING(5001)                         !
LOC:SaldoAberto      DECIMAL(12,2)                         !
queue:DocsImanager   QUEUE,PRE(imanager)                   !
id                   CSTRING(20)                           !
idcliente            CSTRING(20)                           !
idplanodocumento     CSTRING(20)                           !
numdocumento         CSTRING(20)                           !
descricao            CSTRING(20)                           !
d_dataemissao        CSTRING(20)                           !
d_datavencimento     CSTRING(20)                           !
valordocumento       CSTRING(20)                           !
d_datapagamento      CSTRING(20)                           !
valordesconto        CSTRING(20)                           !
valorjuros           CSTRING(20)                           !
valormulta           CSTRING(20)                           !
valoroutrosacrescimos CSTRING(20)                          !
valorloutrosdescontos CSTRING(20)                          !
valorpago            CSTRING(20)                           !
planoid              CSTRING(20)                           !
planodescricao       CSTRING(20)                           !
tipolancamento       CSTRING(20)                           !
codigocliente        CSTRING(20)                           !
razaosocial          CSTRING(20)                           !
d_dataprovavel       CSTRING(20)                           !
                     END                                   !
Loc:Totalizador      DECIMAL(20,2)                         !
fDocumentosPagar FILE,DRIVER('ODBC','/Turbosql=1'),OWNER(GLO:conexao),PRE(FDP),Name('consultasql')
Record      Record
campo1      long
campo2      long
campo3      long
campo4      cstring(21)
campo5      cstring(51)
campo6      date
campo7      date
campo8      decimal(10,2)
campo9      date
campo10     decimal(10,2)
campo11     decimal(10,2)
campo12     decimal(10,2)
campo13     decimal(10,2)
campo14     decimal(10,2)
campo15     decimal(10,2)
campo16     long
campo17     cstring(71)
campo18     cstring(2)
campo19     long
campo20     cstring(61)
campo21      date     ! Data Provavel
            End
        End
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
BR1:View             VIEW(documentos)
                       PROJECT(DOC:numdocumento)
                       PROJECT(DOC:descricao)
                       PROJECT(DOC:d_dataemissao)
                       PROJECT(DOC:d_dataprovavel)
                       PROJECT(DOC:d_datavencimento)
                       PROJECT(DOC:valordocumento)
                       PROJECT(DOC:d_datapagamento)
                       PROJECT(DOC:valorpago)
                       PROJECT(DOC:valororiginalbaixado)
                       PROJECT(DOC:id)
                       PROJECT(DOC:idcentrocusto)
                       PROJECT(DOC:idplano)
                       PROJECT(DOC:idcliente)
                       JOIN(CCU:ccu_pk_id,DOC:idcentrocusto),INNER
                         PROJECT(CCU:descricao)
                         PROJECT(CCU:id)
                       END
                       JOIN(PLA:pla_pk_id,DOC:idplano),INNER
                         PROJECT(PLA:id)
                         PROJECT(PLA:descricaoplano)
                         PROJECT(PLA:tipolancamento)
                       END
                       JOIN(FAV:cli_pk_id,DOC:idcliente),INNER
                         PROJECT(FAV:razaosocial)
                         PROJECT(FAV:id)
                       END
                     END
Queue:RADSQLBrowse   QUEUE                            !Queue declaration for browse/combo box using ?lstDocumentos
DOC:numdocumento       LIKE(DOC:numdocumento)         !List box control field - type derived from field
DOC:numdocumento_NormalFG LONG                        !Normal forground color
DOC:numdocumento_NormalBG LONG                        !Normal background color
DOC:numdocumento_SelectedFG LONG                      !Selected forground color
DOC:numdocumento_SelectedBG LONG                      !Selected background color
DOC:numdocumento_Icon  LONG                           !Entry's icon ID
DOC:descricao          LIKE(DOC:descricao)            !List box control field - type derived from field
DOC:descricao_NormalFG LONG                           !Normal forground color
DOC:descricao_NormalBG LONG                           !Normal background color
DOC:descricao_SelectedFG LONG                         !Selected forground color
DOC:descricao_SelectedBG LONG                         !Selected background color
FAV:razaosocial        LIKE(FAV:razaosocial)          !List box control field - type derived from field
FAV:razaosocial_NormalFG LONG                         !Normal forground color
FAV:razaosocial_NormalBG LONG                         !Normal background color
FAV:razaosocial_SelectedFG LONG                       !Selected forground color
FAV:razaosocial_SelectedBG LONG                       !Selected background color
CCU:descricao          LIKE(CCU:descricao)            !List box control field - type derived from field
CCU:descricao_NormalFG LONG                           !Normal forground color
CCU:descricao_NormalBG LONG                           !Normal background color
CCU:descricao_SelectedFG LONG                         !Selected forground color
CCU:descricao_SelectedBG LONG                         !Selected background color
DOC:d_dataemissao      LIKE(DOC:d_dataemissao)        !List box control field - type derived from field
DOC:d_dataemissao_NormalFG LONG                       !Normal forground color
DOC:d_dataemissao_NormalBG LONG                       !Normal background color
DOC:d_dataemissao_SelectedFG LONG                     !Selected forground color
DOC:d_dataemissao_SelectedBG LONG                     !Selected background color
DOC:d_dataprovavel     LIKE(DOC:d_dataprovavel)       !List box control field - type derived from field
DOC:d_dataprovavel_NormalFG LONG                      !Normal forground color
DOC:d_dataprovavel_NormalBG LONG                      !Normal background color
DOC:d_dataprovavel_SelectedFG LONG                    !Selected forground color
DOC:d_dataprovavel_SelectedBG LONG                    !Selected background color
DOC:d_datavencimento   LIKE(DOC:d_datavencimento)     !List box control field - type derived from field
DOC:d_datavencimento_NormalFG LONG                    !Normal forground color
DOC:d_datavencimento_NormalBG LONG                    !Normal background color
DOC:d_datavencimento_SelectedFG LONG                  !Selected forground color
DOC:d_datavencimento_SelectedBG LONG                  !Selected background color
DOC:valordocumento     LIKE(DOC:valordocumento)       !List box control field - type derived from field
DOC:valordocumento_NormalFG LONG                      !Normal forground color
DOC:valordocumento_NormalBG LONG                      !Normal background color
DOC:valordocumento_SelectedFG LONG                    !Selected forground color
DOC:valordocumento_SelectedBG LONG                    !Selected background color
DOC:d_datapagamento    LIKE(DOC:d_datapagamento)      !List box control field - type derived from field
DOC:d_datapagamento_NormalFG LONG                     !Normal forground color
DOC:d_datapagamento_NormalBG LONG                     !Normal background color
DOC:d_datapagamento_SelectedFG LONG                   !Selected forground color
DOC:d_datapagamento_SelectedBG LONG                   !Selected background color
DOC:valorpago          LIKE(DOC:valorpago)            !List box control field - type derived from field
DOC:valorpago_NormalFG LONG                           !Normal forground color
DOC:valorpago_NormalBG LONG                           !Normal background color
DOC:valorpago_SelectedFG LONG                         !Selected forground color
DOC:valorpago_SelectedBG LONG                         !Selected background color
LOC:SaldoAberto        LIKE(LOC:SaldoAberto)          !List box control field - type derived from local data
LOC:SaldoAberto_NormalFG LONG                         !Normal forground color
LOC:SaldoAberto_NormalBG LONG                         !Normal background color
LOC:SaldoAberto_SelectedFG LONG                       !Selected forground color
LOC:SaldoAberto_SelectedBG LONG                       !Selected background color
PLA:id                 LIKE(PLA:id)                   !List box control field - type derived from field
PLA:id_NormalFG        LONG                           !Normal forground color
PLA:id_NormalBG        LONG                           !Normal background color
PLA:id_SelectedFG      LONG                           !Selected forground color
PLA:id_SelectedBG      LONG                           !Selected background color
PLA:descricaoplano     LIKE(PLA:descricaoplano)       !List box control field - type derived from field
PLA:descricaoplano_NormalFG LONG                      !Normal forground color
PLA:descricaoplano_NormalBG LONG                      !Normal background color
PLA:descricaoplano_SelectedFG LONG                    !Selected forground color
PLA:descricaoplano_SelectedBG LONG                    !Selected background color
PLA:tipolancamento     LIKE(PLA:tipolancamento)       !Browse hot field - type derived from field
DOC:valororiginalbaixado LIKE(DOC:valororiginalbaixado) !Browse hot field - type derived from field
DOC:id                 LIKE(DOC:id)                   !Primary key field - type derived from field
CCU:id                 LIKE(CCU:id)                   !Related join file key field - type derived from field
FAV:id                 LIKE(FAV:id)                   !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
RADSQLDrop2Locator CString(256)
RADSQLDrop2:View     VIEW(aliasfavorecidos)
                       PROJECT(AFV:razaosocial)
                       PROJECT(AFV:id)
                     END
Queue:RADSQLDrop     QUEUE                            !Queue declaration for browse/combo box using ?AFV:razaosocial
AFV:razaosocial        LIKE(AFV:razaosocial)          !List box control field - type derived from field
AFV:id                 LIKE(AFV:id)                   !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Window               WINDOW('i-Money'),AT(,,679,436),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,GRAY,DOUBLE
                       ENTRY(@d06b),AT(103,27,60,10),USE(LOC:datainicio),FLAT,CENTER
                       ENTRY(@d06b),AT(183,27,60,10),USE(LOC:DataFim),FLAT,CENTER
                       IMAGE('window.ico'),AT(3,2),USE(?Image1)
                       PROMPT('Documentos'),AT(28,5),USE(?Prompt1),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(2,22,674,2),USE(?Panel1)
                       BUTTON,AT(651,26,20,11),USE(?btnAtualizar),TRN,FLAT,ICON('selecionar.ico')
                       SHEET,AT(2,41,674,14),USE(?Sheet2),BELOW,FONT(,8,,),SPREAD
                         TAB('Documentos do i-Money'),USE(?Tab4),FONT(,9,,),COLOR(080FFFFH)
                           SHEET,AT(2,27,674,14),USE(?Sheet1),COLOR(0E1E1E1H)
                             TAB('Todos'),USE(?Tab1)
                             END
                             TAB('Receber'),USE(?Tab2)
                             END
                             TAB('Pagar'),USE(?Tab3)
                             END
                           END
                           LIST,AT(244,27,69,10),USE(LOC:TipoData),FLAT,COLOR(COLOR:White),DROP(5),FROM('Emiss�o|#1|Vencimento|#2|Previs�o|#3|Pagamento|#4')
                           LIST,AT(355,27,74,10),USE(LOC:situacao),FLAT,COLOR(COLOR:White),DROP(5),FROM('Todos|#1|Pagos|#2|Em aberto|#3')
                           COMBO(@s255),AT(482,27,163,10),USE(AFV:razaosocial),IMM,FLAT,VSCROLL,COLOR(COLOR:White),FORMAT('240L(2)F@s60@'),DROP(10),FROM(Queue:RADSQLDrop),MSG('Raz�o social do cliente/fornecedor')
                           PROMPT('Fornecedores'),AT(433,32),USE(?Prompt5:2),TRN
                           LIST,AT(3,57,674,329),USE(?lstDocumentos),IMM,FLAT,VSCROLL,FONT('Arial',8,,FONT:regular,CHARSET:ANSI),COLOR(COLOR:White),MSG('Scrolling records...'),FORMAT('[66R(2)F*I~N<186> do Doc~@s20@204L(2)F*~Descri��o~@s50@222L(2)F*~Raz�o Social~@s100@' &|
   '200L(2)F*~Centro de Custo~@s50@/70C_F*~Emiss�o~@d06b@73C_F*~Previs�o~@d06b@45C_F' &|
   '*~Vencimento~@d06b@59R(2)_F*~Valor~@n10.`2@74C_F*~Pagamento~@d06b@37R(2)_F*~Pago' &|
   '~@n9.`2@64R(4)_F*~Saldo Aberto~R(2)@n10.`2@29R(2)_F*~Conta e~@n5@132L(2)_F*~Desc' &|
   'ri��o~@s30@]F'),FROM(Queue:RADSQLBrowse)
                           BUTTON('&Movimenta��es'),AT(357,417,78,15),USE(?btnMovimentacoes),TRN,FLAT,HIDE,LEFT,ICON('lancamentos.ico')
                           BUTTON('&Baixar Documento'),AT(4,394,90,15),USE(?btnBaixaTotal),DISABLE,FLAT,LEFT,ICON('lancamentos.ico')
                           BUTTON('Visualizar Baixas'),AT(103,394,90,15),USE(?BtnVisualizarBaixas),DISABLE,FLAT,LEFT,ICON('lancamentos.ico')
                           BUTTON('Estornar Baixas'),AT(202,394,90,15),USE(?btnExtornoBaixa),DISABLE,FLAT,LEFT,ICON('contrair_tudo.ico')
                           PROMPT('Clientes e'),AT(439,24),USE(?Prompt5),TRN
                           BUTTON('&Incluir'),AT(521,394,50,15),USE(?btnIncluir),TRN,FLAT,LEFT,ICON('incluir.ico')
                           PROMPT('Situa��o:'),AT(322,27),USE(?LOC:situacao:Prompt),TRN
                           BUTTON('&Alterar'),AT(573,394,50,15),USE(?btnAlterar),TRN,FLAT,LEFT,ICON('alterar.ico')
                           BUTTON('&Excluir'),AT(625,394,50,15),USE(?btnExcluir),TRN,FLAT,LEFT,ICON('excluir.ico')
                         END
                         TAB('Documentos do i-Manager'),USE(?Tab5),HIDE,FONT(,9,,),COLOR(0FF8000H)
                           PANEL,AT(3,387,477,24),USE(?Panel3),FILL(COLOR:Red),BEVEL(2,2)
                           LIST,AT(244,27,69,10),USE(LOC:TipoDataiManager),FLAT,COLOR(COLOR:White),DROP(5),FROM('Emiss�o|#1|Vencimento|#2|Pagamento|#3')
                           PROMPT('Visualiza��o dos documentos do i-Manager'),AT(7,392,470,13),USE(?Prompt7),TRN,CENTER,FONT(,12,COLOR:White,FONT:bold,CHARSET:ANSI)
                           LIST,AT(3,57,674,329),USE(?lstDocumentos:2),FLAT,VSCROLL,FONT('Arial',8,,FONT:regular,CHARSET:ANSI),COLOR(0D8FCFAH),MSG('Scrolling records...'),FORMAT('[56L(2)F~N<186> do Doc~@s20@#4#29R(2)F~Conta e~@n5@#3#171L(2)F~Descri��o~@s40@#5#45C' &|
   '(2)F~Emiss�o~@d06b@#6#45C(2)F~Vencimento~@d06b@#7#47R(2)F~Valor~@n10.`2@#8#45C(2' &|
   ')F~Pagamento~@d06b@42R(2)F~Valor Pago~@n10.`2@#15#44R(2)F~Multa~@n10.`2@#12#43R(' &|
   '2)F~Juros~@n10.`2@#11#42R(2)F~Desconto~@n10.`2@#10#44R(2)F~Acr�scimos~@n10.`2@/#' &|
   '13#135L(26)_F~Descri��o~@s35@#17#300L(2)_F~Identifica��o do Cliente~@s100@#20#]F'),FROM(queue:DocsImanager)
                         END
                       END
                       ENTRY(@n-20.`2),AT(302,390,184,20),USE(Loc:Totalizador),CENTER,FONT('Arial',16,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(0804000H),READONLY
                       PROMPT('De:'),AT(89,27),USE(?LOC:datainicio:Prompt),TRN
                       PROMPT('At�:'),AT(167,27),USE(?LOC:DataFim:Prompt),TRN
                       PANEL,AT(2,412,674,2),USE(?Panel1:2)
                       BUTTON('Documentos'),AT(3,417,65,15),USE(?Button9),FLAT,LEFT,ICON('imprimir.ico')
                       BUTTON('&Fechar'),AT(625,419,50,15),USE(?btnFechar),TRN,FLAT,LEFT,ICON('fechar.ico')
                       BUTTON('Nota Fiscal'),AT(69,417,65,15),USE(?btnNF),FLAT,HIDE,LEFT,ICON('imprimir.ico')
                       BUTTON('Excel'),AT(135,417,65,15),USE(?btnExcel),FLAT,LEFT,ICON('Excel.ico')
                       OLE,AT(207,418,10,10),USE(?SMExcel),TRN,HIDE
                       END
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Reset                  PROCEDURE(BYTE Force=0),DERIVED     ! Method added to host embed code
SetAlerts              PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
BR1                  CLASS(EXTSQLB)
GenerateSelectStatement PROCEDURE(),DERIVED                ! Method added to host embed code
ResetBrowse            PROCEDURE(Byte forcerefill),DERIVED ! Method added to host embed code
RunForm                PROCEDURE(Byte in_Request),DERIVED  ! Method added to host embed code
SetQueueRecord         PROCEDURE(),DERIVED                 ! Method added to host embed code
SetRange               PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeEvent              PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END

RADSQLDrop2          CLASS(RADSQLDrop)
GenerateSelectStatement PROCEDURE(),DERIVED                ! Method added to host embed code
                     END

MSExcel              MSExcelClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Totalizar routine
  Loc:Totalizador = 0
  if choice(?Sheet2) = 1
    loop a# = 1 to records(Queue:RADSQLBrowse)
      get(Queue:RADSQLBrowse, a#)
      if Queue:RADSQLBrowse.PLA:tipolancamento = 'C'
        Loc:Totalizador += Queue:RADSQLBrowse.DOC:valordocumento
      elsif Queue:RADSQLBrowse.PLA:tipolancamento = 'D'
        Loc:Totalizador -= Queue:RADSQLBrowse.DOC:valordocumento
      end
    end
  else
    loop a# = 1 to records(queue:DocsImanager)
      get(queue:DocsImanager,a#)
      Loc:Totalizador += imanager:valordocumento
    end
  end
  display(?Loc:Totalizador)
Carrega_DocsImanager routine

data

loc:view_where  cstring(2000)
Loc:queryString cstring(2000)


code

  Open(fDocumentosPagar,42h)

  If LOC:datainicio And LOC:DataFim and LOC:TipoDataiManager = 1
    loc:view_where = ' where a.d_dataemissao between <39>'&format(LOC:datainicio,@d012b)&'<39> and <39>'&format(LOC:datafim,@d012b)&'<39>'
    loc:view_where = loc:view_where & ' order by a.d_dataemissao'
  ElsIf LOC:datainicio And LOC:DataFim and LOC:TipoDataiManager = 2
    loc:view_where = ' where a.d_datavencimento between <39>'&format(LOC:datainicio,@d012b)&'<39> and <39>'&format(LOC:datafim,@d012b)&'<39>'
    loc:view_where = loc:view_where & ' order by a.d_datavencimento'
  ElsIf LOC:datainicio And LOC:DataFim and LOC:TipoDataiManager = 3
    loc:view_where = ' where a.d_datapagamento between <39>'&format(LOC:datainicio,@d012b)&'<39> and <39>'&format(LOC:datafim,@d012b)&'<39>'
    loc:view_where = loc:view_where & ' order by a.d_datapagamento'
  else
    LOC:datainicio = today()
    LOC:DataFim    = today()

    loc:view_where = ' where a.d_datapagamento between <39>'&format(LOC:datainicio,@d012b)&'<39> and <39>'&format(LOC:datafim,@d012b)&'<39>'
  End

  Loc:Totalizador = 0

  Loc:queryString =    'select * from (select ' &|
                       'a.id, a.idcliente, a.idplanodocumento, a.numdocumento, a.descricao, '&|
                       'a.d_dataemissao, a.d_datavencimento, a.valordocumento, a.d_datapagamento, '&|
                       'a.valordesconto, a.valorjuros, a.valormulta, a.valoroutrosacrescimos, '&|
                       'a.valorloutrosdescontos, a.valorpago, a.planoid, a.planodescricao, a.tipolancamento, ' &|
                       'a.codigocliente, a.razaosocial, a.d_dataprovavel ' &|
                       'from public.fun_carrega_view_docreceber_imanager a) as a ' &|
                       loc:view_where

  fDocumentosPagar{Prop:Sql} = Loc:queryString
  !message(fileerror() &'||'& fDocumentosPagar{Prop:Sql},,,,,2)

  free(queue:DocsImanager)
  loop
    next(fDocumentosPagar)
    if error() then break .
    clear(queue:DocsImanager)
    imanager:id                     = FDP:campo1
    imanager:idcliente              = FDP:campo2
    imanager:idplanodocumento       = FDP:campo3
    imanager:numdocumento           = FDP:campo4
    imanager:descricao              = FDP:campo5
    imanager:d_dataemissao          = FDP:campo6
    imanager:d_datavencimento       = FDP:campo7
    imanager:valordocumento         = FDP:campo8
    imanager:d_datapagamento        = FDP:campo9
    imanager:valordesconto          = FDP:campo10
    imanager:valorjuros             = FDP:campo11
    imanager:valormulta             = FDP:campo12
    imanager:valoroutrosacrescimos  = FDP:campo13
    imanager:valorloutrosdescontos  = FDP:campo14
    imanager:valorpago              = FDP:campo15
    imanager:planoid                = FDP:campo16
    imanager:planodescricao         = FDP:campo17
    imanager:tipolancamento         = FDP:campo18
    imanager:codigocliente          = FDP:campo19
    imanager:razaosocial            = FDP:campo20
    imanager:d_dataprovavel         = FDP:campo21
    add(queue:DocsImanager)
  end

  Do Totalizar
  close(fDocumentosPagar)

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseDocumentos')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:datainicio
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  COMPILE('C55',_C55_)
    Self.Filesopened = 1
  C55
  OMIT('C55',_C55_)
    Filesopened = 1
  C55
  Relate:documentos.Open()
  Access:documentos.UseFile()
  Relate:centrocusto.Open()
  Access:centrocusto.UseFile()
  Relate:planocontas.Open()
  Access:planocontas.UseFile()
  Relate:favorecidos.Open()
  Access:favorecidos.UseFile()
  Relate:aliasfavorecidos.Open()
  Access:aliasfavorecidos.UseFile()
  Relate:parametros.Open                                   ! File parametros used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
  COMPILE ('**CW7**',_CWVER_=7000)
  ?Sheet2{PROP:TabSheetStyle} = TabStyle:Colored
  ?Sheet1{PROP:TabSheetStyle} = TabStyle:Colored
  !**CW7**
  BR1.RefreshHotkey = F5Key
  BR1.Setlistcontrol (?lstDocumentos,10)
  Do DefineListboxStyle
  BR1.PopUpActive = False
  BR1.Init(Access:documentos)
  BR1.useansijoin=1
  RADSQLDrop2.Listqueue      &= Queue:RADSQLDrop
  RADSQLDrop2.Position       &= Queue:RADSQLDrop.ViewPosition
  RADSQLDrop2.Setlistcontrol(?AFV:razaosocial)
  RADSQLDrop2.RefreshHotkey = F5Key
  RADSQLDrop2.AddDropVariables(?AFV:razaosocial,0,AFV:razaosocial,'AFV:id',AFV:id,LOC:favorecido)
  RADSQLDrop2.Init(Access:aliasfavorecidos)
  RADSQLDrop2.AddField(AFV:id,LOC:favorecido)
     RADSQLDrop2.useansijoin=1
  BR1.MaintainProcedure = 'UpdateDocumentos'
  BR1.SetupdateButtons (?btnIncluir,?btnAlterar,?btnExcluir,0)
  SELF.SetAlerts()
  ?lstDocumentos{PROP:From} = Queue:RADSQLBrowse
    ?lstDocumentos{PROP:IconList,1} = '~credito.ico'
    ?lstDocumentos{PROP:IconList,2} = '~debito.ico'
  BR1.DoNotRetrieveAnyData=1
  BR1.AddTable('documentos',1,0,'documentos')  ! Add the table to the list of tables
    BR1.AddTable('centrocusto',2,0,'centrocusto')
    BR1.AddTable('planocontas',3,0,'planocontas')
    BR1.AddTable('favorecidos',4,0,'favorecidos')
  BR1.Listqueue      &= Queue:RADSQLBrowse
  BR1.Position       &= Queue:RADSQLBrowse.ViewPosition
  BR1.ColumnCharAsc   = ''
  BR1.ColumnCharDes   = ''
  BR1.AddFieldpairs(Queue:RADSQLBrowse.DOC:id,DOC:id,'documentos','id','N',0,'B','A','M','N',1,16)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.DOC:numdocumento,DOC:numdocumento,'documentos','numdocumento','A',0,'B','A','M','N',5,1)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.DOC:descricao,DOC:descricao,'documentos','descricao','A',0,'B','A','M','N',6,2)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.DOC:d_dataemissao,DOC:d_dataemissao,'documentos','d_dataemissao','D',0,'B','A','M','N',7,5)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.DOC:d_datavencimento,DOC:d_datavencimento,'documentos','d_datavencimento','D',0,'B','A','M','N',8,7)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.DOC:valordocumento,DOC:valordocumento,'documentos','valordocumento','N',0,'B','A','M','N',9,8)
  !BR1.AddFieldpairs(Queue:RADSQLBrowse.DOC:valordocumento,DOC:valordocumento,'','','A',1,'N','N','M','N',9,0) ! Hotfield
  BR1.AddFieldpairs(Queue:RADSQLBrowse.DOC:d_datapagamento,DOC:d_datapagamento,'documentos','d_datapagamento','D',0,'B','A','M','N',10,9)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.DOC:valorpago,DOC:valorpago,'documentos','valorpago','N',0,'B','A','M','N',16,10)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.DOC:valororiginalbaixado,DOC:valororiginalbaixado,'documentos','valororiginalbaixado','N',0,'B','A','M','N',21,15)
  !BR1.AddFieldpairs(Queue:RADSQLBrowse.DOC:valororiginalbaixado,DOC:valororiginalbaixado,'','','A',1,'N','N','M','N',21,0) ! Hotfield
  BR1.AddFieldpairs(Queue:RADSQLBrowse.DOC:d_dataprovavel,DOC:d_dataprovavel,'documentos','d_dataprovavel','D',0,'B','A','M','N',27,6)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.CCU:id,CCU:id,'centrocusto','id','N',0,'B','A','M','N',1,17)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.CCU:descricao,CCU:descricao,'centrocusto','descricao','A',0,'B','A','M','N',2,4)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.PLA:id,PLA:id,'planocontas','id','N',0,'B','A','M','N',1,12)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.PLA:descricaoplano,PLA:descricaoplano,'planocontas','descricaoplano','A',0,'B','A','M','N',2,13)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.PLA:tipolancamento,PLA:tipolancamento,'planocontas','tipolancamento','A',0,'B','A','M','N',3,14)
  !BR1.AddFieldpairs(Queue:RADSQLBrowse.PLA:tipolancamento,PLA:tipolancamento,'','','A',1,'N','N','M','N',3,0) ! Hotfield
  BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:id,FAV:id,'favorecidos','id','N',0,'B','A','M','N',1,18)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.FAV:razaosocial,FAV:razaosocial,'favorecidos','razaosocial','A',0,'B','A','M','N',3,3)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.LOC:SaldoAberto,LOC:SaldoAberto,'','','A',0,'B','A','M','N',0,11) ! Formula added
  BR1.AddJoinFields('documentos','idcentrocusto','centrocusto','id',BR1:view{prop:inner,1},0,CCU:ccu_pk_id)
  BR1.Addviewfield('documentos','idcentrocusto',2)   ! Not in queue from DOC:idcentrocusto
  BR1.AddJoinFields('documentos','idplano','planocontas','id',BR1:view{prop:inner,2},0,PLA:pla_pk_id)
  BR1.Addviewfield('documentos','idplano',4)   ! Not in queue from DOC:idplano
  BR1.AddJoinFields('documentos','idcliente','favorecidos','id',BR1:view{prop:inner,3},0,FAV:cli_pk_id)
  BR1.Addviewfield('documentos','idcliente',3)   ! Not in queue from DOC:idcliente
  BR1.Managedview &=BR1:View
  BR1.Openview()
  BR1.SetFileLoad()
  BR1.SetForceFetch (1)
  ?AFV:razaosocial{PROP:From} = Queue:RADSQLDrop
  RADSQLDrop2.AddTable('aliasfavorecidos',1,0,'FAVORECIDOS')  ! Add the table to the list of tables
  RADSQLDrop2.ColumnCharAsc   = ''
  RADSQLDrop2.ColumnCharDes   = ''
  RADSQLDrop2.AddFieldpairs(Queue:RADSQLDrop.AFV:id,AFV:id,'aliasfavorecidos','id | READONLY','N',0,'B','A','I','N',1,2)
  RADSQLDrop2.AddFieldpairs(Queue:RADSQLDrop.AFV:razaosocial,AFV:razaosocial,'aliasfavorecidos','razaosocial','A',0,'A','A','I','N',3,1)
  RADSQLDrop2.Addindexfunction('UPPER(%)')
  RADSQLDrop2.Managedview &=RADSQLDrop2:View
  RADSQLDrop2.Openview
  RADSQLDrop2.SetFileLoad
  BR1.SetColColor = 0
  BR1.Resetsort(1)
  RADSQLDrop2.SetColColor = 0
  RADSQLDrop2.Resetsort(1,1)
  EnterByTabManager.Init(False)
  BR1.RefreshMethod = 1
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  COMPILE('C55',_C55_)
  if Self.filesopened
  C55
  OMIT('C55',_C55_)
  If filesopened then
  C55
    Relate:documentos.Close
    Relate:centrocusto.Close
    Relate:planocontas.Close
    Relate:favorecidos.Close
  END
  COMPILE('C55',_C55_)
  if Self.filesopened
  C55
  OMIT('C55',_C55_)
  If filesopened then
  C55
  Relate:aliasfavorecidos.Close
  END
    Relate:parametros.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF Window{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
     If force = 1  then
        BR1.ResetBrowse (1)
     end
  If force = 2
    RADSQLDrop2.ResetBrowse (1)
  end


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  BR1.SetAlerts()
  RADSQLDrop2.SetAlerts


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?btnExtornoBaixa
      case message('Todas as baixas do documento selecionado ser�o exclu�das permanentemente!||O documento retornar� a seu estado inical - ABERTO!||Deseja realmente continuar?','Aten��o',icon:question,'&Sim|&N�o',2)
      of 1
      
          ! Apagar MovDocumentos
          !stop('Queue:RADSQLBrowse.DOC:id: ' & Queue:RADSQLBrowse.DOC:id)
          ConsultaSQL{prop:sql} = 'delete from movdocumentos where iddocumento = ' & Queue:RADSQLBrowse.DOC:id
          !message(ConsultaSQL{prop:sql},,,,,2)
          clear(ConsultaSQL)
          ! Update no Documentos
          ConsultaSQL{prop:sql} =   'update documentos ' &|
                                    'set d_datapagamento = null, ' &|
                                        'valorpago = 0, ' &|
                                        'valororiginalbaixado = 0, ' &|
                                        'valordesconto = 0, ' &|
                                        'valorjuros = 0, ' &|
                                        'valormulta = 0, ' &|
                                        'valoroutrosacrescimos = 0, ' &|
                                        'valorloutrosdescontos = 0 ' &|
                                    'where id = ' &  Queue:RADSQLBrowse.DOC:id
          !message(ConsultaSQL{prop:sql},,,,,2)
      
          BR1.ResetBrowse(1)
      end
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?btnAtualizar
      ThisWindow.Update
      if choice(?Sheet2) = 1
          If LOC:datainicio And LOC:DataFim and LOC:TipoData = 1
            LOC:Filtro = 'a.d_dataemissao between <39>'&Format(LOC:datainicio,@d012)&'<39> and <39>'&Format(LOC:DataFim,@d012)&'<39>'
          ElsIf LOC:datainicio And LOC:DataFim and LOC:TipoData = 2
            LOC:Filtro = 'a.d_datavencimento between <39>'&Format(LOC:datainicio,@d012)&'<39> and <39>'&Format(LOC:DataFim,@d012)&'<39>'
          ElsIf LOC:datainicio And LOC:DataFim and LOC:TipoData = 3
            LOC:Filtro = 'a.d_dataprovavel between <39>'&Format(LOC:datainicio,@d012)&'<39> and <39>'&Format(LOC:DataFim,@d012)&'<39>'
          Else
            Clear(LOC:Filtro)
          End
      
      !    If LOC:situacao = 2  ! A vencer
      !      If LOC:Filtro
      !        LOC:Filtro = LOC:Filtro&' and a.d_datavencimento >= current_date and a.d_datapagamento is null'
      !      Else
      !        LOC:Filtro = 'a.d_datavencimento >= current_date and a.d_datapagamento is null'
      !      End
          If LOC:situacao = 3 ! Em aberto
      !      If LOC:Filtro
      !        LOC:Filtro = LOC:Filtro&' and a.d_datavencimento < current_date and a.d_datapagamento is null'
      !      Else
      !        LOC:Filtro = 'a.d_datavencimento < current_date and a.d_datapagamento is null'
      !      End
            If LOC:Filtro
              LOC:Filtro = LOC:Filtro&' and a.d_datapagamento is null'
            Else
              LOC:Filtro = 'a.d_datapagamento is null'
            End
      
          ElsIf LOC:situacao = 2 ! Pagos
            If LOC:Filtro
              LOC:Filtro = LOC:Filtro&' and a.d_datapagamento is not null'
            Else
              LOC:Filtro = 'a.d_datapagamento is not null'
            End
          End
      
          If LOC:favorecido
            If LOC:Filtro
              LOC:Filtro = LOC:Filtro&' and a.idcliente = '&LOC:favorecido
            Else
              LOC:Filtro = 'a.idcliente = '&LOC:favorecido
            End
          End
      
          BR1.DoNotRetrieveAnyData = False
          BR1.ResetBrowse(1)
      
          !if LOC:situacao = 2 or LOC:situacao = 3
              !do Totalizar
              !?Loc:Totalizador{prop:hide} = 0
          !else
          !    ?Loc:Totalizador{prop:hide} = 1
          !end
          display
      else
          do Carrega_DocsImanager
          display
      end
    OF ?btnMovimentacoes
      ThisWindow.Update
      BrowseMovDocumentos()
      ThisWindow.Reset
    OF ?btnBaixaTotal
      ThisWindow.Update
      UpdateBaixa()
      ThisWindow.Reset
      BR1.ResetBrowse(1)
    OF ?BtnVisualizarBaixas
      ThisWindow.Update
      visualizarbaixas(Queue:RADSQLBrowse.DOC:id)
      ThisWindow.Reset
    OF ?btnAlterar
      ThisWindow.Update
       post(event:accepted,?btnAtualizar)
    OF ?Button9
      ThisWindow.Update
      RelatDocumentosPagar()
      ThisWindow.Reset
    OF ?btnFechar
      ThisWindow.Update
      Post(Event:CloseWindow)
    OF ?btnNF
      ThisWindow.Update
      open(fDocumentosPagar,42h)
      clear(FDP:Record)
      fDocumentosPagar{prop:sql} = |
        'select count(n.*) from temporarias.numeracaonf n where n.iddocumento = '&DOC:id&' and n.situacao = <39>N<39>'
      next(fDocumentosPagar)
      if FDP:campo1 = 0
        srcImprimeNotaFiscal(DOC:id,DOC:idempresa)
      else
        Message('A nota fiscal para o documento j� foi impressa!','Aten��o',icon:exclamation,'OK',,2)
      end
      close(fDocumentosPagar)
    OF ?btnExcel
      ThisWindow.Update
      if records(Queue:RADSQLBrowse)
        SETCURSOR(CURSOR:Wait)
      MSExcel.Init(Window,?SMExcel,1,1,1)
        !Monta o Cabe�alho
        MSExcel.CreateFile()
        MSExcel.Select('A1')
        MSExcel.Assign('Exporta��o de Documentos')
        MSExcel.Select('A2')
        MSExcel.Assign('Per�odo de: '&Format(LOC:datainicio,@d06b)&' at� '&Format(LOC:DataFim,@d06b))
        MSExcel.Select('B2')
        if LOC:TipoData = 1
          MSExcel.Assign('Filtrado por data de emiss�o')
        elsif LOC:TipoData = 2
          MSExcel.Assign('Filtrado por data de vencimento')
        elsif LOC:TipoData = 3
          MSExcel.Assign('Filtrado por data de previs�o')
        else
          MSExcel.Assign('Filtrado por data de pagamento')
        end
        MSExcel.Select('C2')
        if choice(?Sheet1) = 1
          MSExcel.Assign('Tipo documento: Todos')
        elsif choice(?Sheet1) = 2
          MSExcel.Assign('Tipo documento: Receber')
        else
          MSExcel.Assign('Tipo documento: Pagar')
        end
      
        MSExcel.Select('D2')
        if LOC:situacao = 1
          MSExcel.Assign('Situa��o: Todos')
        elsif LOC:situacao = 2
          MSExcel.Assign('Situa��o: Abertos')
        else
          MSExcel.Assign('Situa��o: Pagos')
        end
      
        MSExcel.Select('A4')
        MSExcel.Assign('Num.Documento')
        MSExcel.Select('B4')
        MSExcel.Assign('Decsri��o')
        MSExcel.Select('C4')
        MSExcel.Assign('Cliente/Fornecedor')
        MSExcel.Select('D4')
        MSExcel.Assign('Data de Emiss�o')
        MSExcel.Select('E4')
        MSExcel.Assign('Data de Previs�o')
        MSExcel.Select('F4')
        MSExcel.Assign('Data de Vencimento')
        MSExcel.Select('G4')
        MSExcel.Assign('Valor Documento')
        MSExcel.Select('H4')
        MSExcel.Assign('Data de Pagamento')
        MSExcel.Select('I4')
        MSExcel.Assign('Valor Pago')
        MSExcel.Select('J4')
        MSExcel.Assign('Saldo em Aberto')
        MSExcel.Select('K4')
        MSExcel.Assign('Plano de Contas')
        MSExcel.Select('L4')
        MSExcel.Assign('Centro de Custo')
      
        !Alimenta com os documentos
        loop a# = 1 to records(Queue:RADSQLBrowse)
          get(Queue:RADSQLBrowse,a#)
      
          MSExcel.Select('A'&a#+4)
          MSExcel.Assign(Queue:RADSQLBrowse.DOC:numdocumento)
          MSExcel.Select('B'&a#+4)
          MSExcel.Assign(Queue:RADSQLBrowse.DOC:descricao)
          MSExcel.Select('C'&a#+4)
          MSExcel.Assign(Queue:RADSQLBrowse.FAV:razaosocial)
          MSExcel.Select('D'&a#+4)
          MSExcel.Assign(format(Queue:RADSQLBrowse.DOC:d_dataemissao,@d10b))
          MSExcel.Select('E'&a#+4)
          MSExcel.Assign(format(Queue:RADSQLBrowse.DOC:d_dataprovavel,@d10b))
          MSExcel.Select('F'&a#+4)
          MSExcel.Assign(format(Queue:RADSQLBrowse.DOC:d_datavencimento,@d10b))
          MSExcel.Select('G'&a#+4)
          if Queue:RADSQLBrowse.PLA:tipolancamento = 'C'
            MSExcel.Assign(Queue:RADSQLBrowse.DOC:valordocumento)
          else
            MSExcel.Assign(Queue:RADSQLBrowse.DOC:valordocumento * (-1))
          end
          MSExcel.Select('H'&a#+4)
          MSExcel.Assign(format(Queue:RADSQLBrowse.DOC:d_datapagamento,@d10b))
          MSExcel.Select('I'&a#+4)
          if Queue:RADSQLBrowse.PLA:tipolancamento = 'C'
            MSExcel.Assign(Queue:RADSQLBrowse.DOC:valorpago)
          else
            MSExcel.Assign(Queue:RADSQLBrowse.DOC:valorpago * (-1))
          end
          MSExcel.Select('J'&a#+4)
          if Queue:RADSQLBrowse.PLA:tipolancamento = 'C'
            MSExcel.Assign(Queue:RADSQLBrowse.LOC:SaldoAberto)
          else
            MSExcel.Assign(Queue:RADSQLBrowse.LOC:SaldoAberto * (-1))
          end
          MSExcel.Select('K'&a#+4)
          MSExcel.Assign(Queue:RADSQLBrowse.PLA:descricaoplano)
          MSExcel.Select('L'&a#+4)
          MSExcel.Assign(Queue:RADSQLBrowse.CCU:descricao)
        end
      
        linha# = a# + 6
      
        MSExcel.Select('A'&linha#+1)
        MSExcel.Assign('Relat�rio extra�do em: '&Format(Today(),@d06b))
      
        SETCURSOR
      else
        message('N�o existem valores para gerar o arquivo!','Aten��o',Icon:Exclamation,'OK',1)
      end
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
  BR1.TakeEvent()
  RADSQLDrop2.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?Sheet1
      post(event:accepted,?btnAtualizar)
      Display()
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet2
      if choice(?sheet2) = 2
        ?Loc:Totalizador{prop:xpos} = 492
      else
        ?Loc:Totalizador{prop:xpos} = 302
      end
      Do Totalizar
      display
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
       !Verifica se est� liberado impress�o de nota fiscal
       PAR:id = 1
       get(parametros,PAR:par_pk_id)
       !stop(PAR:controlarnf)
      
       !if PAR:controlarnf
       !  WindowEscolherAno
       !  LOC:datainicio = date(1,1,glo:ano)
       !  LOC:DataFim    = date(12,31,glo:ano)
       !else
         LOC:datainicio = date(month(today()),1,year(today()))
         LOC:DataFim    = date(month(today())+1,1,year(today()))-1
       !end
       post(event:accepted,?btnAtualizar)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BR1.GenerateSelectStatement PROCEDURE


  CODE
  PARENT.GenerateSelectStatement
  !Self.ViewSqlStatement = 'SELECT A.ID, A.IDCLIENTE, A.IDPLANO, A.NUMDOCUMENTO, A.DESCRICAO, A.D_DATAEMISSAO, A.D_DATAVENCIMENTO, A.VALORDOCUMENTO, A.D_DATAPAGAMENTO, ' &|
  !                        'A.VALORPAGO, A.VALORORIGINALBAIXADO, A.D_DATAPROVAVEL, B.ID, B.DESCRICAOPLANO, B.TIPOLANCAMENTO, C.ID, C.RAZAOSOCIAL '&|
  !                        'FROM DOCUMENTOS A ' &|
  !                        'JOIN PLANOCONTAS B ON A.IDPLANO=B.ID '&|
  !                        'JOIN FAVORECIDOS C ON A.IDCLIENTE=C.ID'
  Self.ViewSqlStatement = 'SELECT A.ID, A.IDCENTROCUSTO, A.IDCLIENTE, A.IDPLANO, A.NUMDOCUMENTO, A.DESCRICAO, A.D_DATAEMISSAO, A.D_DATAVENCIMENTO, A.VALORDOCUMENTO, '&|
                          'A.D_DATAPAGAMENTO, A.VALORPAGO, A.VALORORIGINALBAIXADO, A.D_DATAPROVAVEL, B.ID, B.DESCRICAO, C.ID, C.DESCRICAOPLANO, C.TIPOLANCAMENTO, D.ID, D.RAZAOSOCIAL '&|
                          'FROM DOCUMENTOS A '&|
                          'JOIN CENTROCUSTO B ON A.IDCENTROCUSTO=B.ID '&|
                          'JOIN PLANOCONTAS C ON A.IDPLANO=C.ID '&|
                          'JOIN FAVORECIDOS D ON A.IDCLIENTE=D.ID'


BR1.ResetBrowse PROCEDURE(Byte forcerefill)


  CODE
  PARENT.ResetBrowse(forcerefill)
  Do Totalizar


BR1.RunForm PROCEDURE(Byte in_Request)


  CODE
     GlobalRequest = in_Request
  PARENT.RunForm(in_Request)
     if in_Request = ChangeRecord or in_Request = DeleteRecord or in_Request = ViewRecord
        if Access:documentos.Fetch(DOC:doc_pk_id) <> Level:Benign ! Fetch documentos on key 
          Message('Error on primary key fetch DOC:doc_pk_id|Error:'& Error() &' FileError:'& FileError() &'|SQL:'& documentos{Prop:SQL})
          GlobalResponse = RequestCancelled
          Return
        end
     else
     end
  UpdateDocumentos 
     BR1.Response = Globalresponse
  
   BR1.ListControl{Prop:Selected} = BR1.CurrentChoice
   Display


BR1.SetQueueRecord PROCEDURE


  CODE
  !LOC:SaldoAberto = (DOC:valordocumento + DOC:valorjuros + DOC:valormulta + DOC:valoroutrosacrescimos) - (DOC:valordesconto + DOC:valorloutrosdescontos + DOC:valorpago)
        LOC:SaldoAberto = (DOC:valordocumento - DOC:valororiginalbaixado)
  Queue:RADSQLBrowse.DOC:numdocumento_NormalFG = -1
  Queue:RADSQLBrowse.DOC:numdocumento_NormalBG = 8454143
  Queue:RADSQLBrowse.DOC:numdocumento_SelectedFG = -1
  Queue:RADSQLBrowse.DOC:numdocumento_SelectedBG = -1
    IF (PLA:tipolancamento = 'D')
      Queue:RADSQLBrowse.DOC:numdocumento_Icon = 2
    ELSE
      Queue:RADSQLBrowse.DOC:numdocumento_Icon = 1
    END
  Queue:RADSQLBrowse.DOC:descricao_NormalFG = -1
  Queue:RADSQLBrowse.DOC:descricao_NormalBG = 8454143
  Queue:RADSQLBrowse.DOC:descricao_SelectedFG = -1
  Queue:RADSQLBrowse.DOC:descricao_SelectedBG = -1
  Queue:RADSQLBrowse.FAV:razaosocial_NormalFG = -1
  Queue:RADSQLBrowse.FAV:razaosocial_NormalBG = 8454143
  Queue:RADSQLBrowse.FAV:razaosocial_SelectedFG = -1
  Queue:RADSQLBrowse.FAV:razaosocial_SelectedBG = -1
  Queue:RADSQLBrowse.CCU:descricao_NormalFG = -1
  Queue:RADSQLBrowse.CCU:descricao_NormalBG = 8454143
  Queue:RADSQLBrowse.CCU:descricao_SelectedFG = -1
  Queue:RADSQLBrowse.CCU:descricao_SelectedBG = -1
  Queue:RADSQLBrowse.DOC:d_dataemissao_NormalFG = -1
  Queue:RADSQLBrowse.DOC:d_dataemissao_NormalBG = -1
  Queue:RADSQLBrowse.DOC:d_dataemissao_SelectedFG = -1
  Queue:RADSQLBrowse.DOC:d_dataemissao_SelectedBG = -1
  Queue:RADSQLBrowse.DOC:d_dataprovavel_NormalFG = -1
  Queue:RADSQLBrowse.DOC:d_dataprovavel_NormalBG = -1
  Queue:RADSQLBrowse.DOC:d_dataprovavel_SelectedFG = -1
  Queue:RADSQLBrowse.DOC:d_dataprovavel_SelectedBG = -1
  Queue:RADSQLBrowse.DOC:d_datavencimento_NormalFG = -1
  Queue:RADSQLBrowse.DOC:d_datavencimento_NormalBG = -1
  Queue:RADSQLBrowse.DOC:d_datavencimento_SelectedFG = -1
  Queue:RADSQLBrowse.DOC:d_datavencimento_SelectedBG = -1
  Queue:RADSQLBrowse.DOC:valordocumento_NormalFG = -1
  Queue:RADSQLBrowse.DOC:valordocumento_NormalBG = -1
  Queue:RADSQLBrowse.DOC:valordocumento_SelectedFG = -1
  Queue:RADSQLBrowse.DOC:valordocumento_SelectedBG = -1
  Queue:RADSQLBrowse.DOC:d_datapagamento_NormalFG = -1
  Queue:RADSQLBrowse.DOC:d_datapagamento_NormalBG = -1
  Queue:RADSQLBrowse.DOC:d_datapagamento_SelectedFG = -1
  Queue:RADSQLBrowse.DOC:d_datapagamento_SelectedBG = -1
  Queue:RADSQLBrowse.DOC:valorpago_NormalFG = -1
  Queue:RADSQLBrowse.DOC:valorpago_NormalBG = -1
  Queue:RADSQLBrowse.DOC:valorpago_SelectedFG = -1
  Queue:RADSQLBrowse.DOC:valorpago_SelectedBG = -1
  Queue:RADSQLBrowse.LOC:SaldoAberto_NormalFG = -1
  Queue:RADSQLBrowse.LOC:SaldoAberto_NormalBG = -1
  Queue:RADSQLBrowse.LOC:SaldoAberto_SelectedFG = -1
  Queue:RADSQLBrowse.LOC:SaldoAberto_SelectedBG = -1
  Queue:RADSQLBrowse.PLA:id_NormalFG = -1
  Queue:RADSQLBrowse.PLA:id_NormalBG = -1
  Queue:RADSQLBrowse.PLA:id_SelectedFG = -1
  Queue:RADSQLBrowse.PLA:id_SelectedBG = -1
  Queue:RADSQLBrowse.PLA:descricaoplano_NormalFG = -1
  Queue:RADSQLBrowse.PLA:descricaoplano_NormalBG = -1
  Queue:RADSQLBrowse.PLA:descricaoplano_SelectedFG = -1
  Queue:RADSQLBrowse.PLA:descricaoplano_SelectedBG = -1
  PARENT.SetQueueRecord


BR1.SetRange PROCEDURE


  CODE
  PARENT.SetRange
  If Choice(?Sheet1) = 2
    If LOC:Filtro
      Self.RangeFilter = LOC:Filtro&' and c.tipolancamento = <39>C<39>'
    Else
      Self.RangeFilter = 'c.tipolancamento = <39>C<39>'
    End
  ElsIf Choice(?Sheet1) = 3
    If LOC:Filtro
      Self.RangeFilter = LOC:Filtro&' and c.tipolancamento = <39>D<39>'
    Else
      Self.RangeFilter = 'c.tipolancamento = <39>D<39>'
    End
  Else
    Self.RangeFilter = LOC:Filtro
  End
  
  self.rangefilter = self.rangefilter & ' and a.idempresa = '&GLO:CodEmpresa


BR1.TakeEvent PROCEDURE


  CODE
  PARENT.TakeEvent
  ! Vers�o 2.0 - 13/05/2008, Fabricio Fachini
  
  if ~Queue:RADSQLBrowse.LOC:SaldoAberto
    disable(?btnBaixaTotal)
  else
    enable(?btnBaixaTotal)
  end                                  
  
  
  if Queue:RADSQLBrowse.DOC:valororiginalbaixado
    disable(?btnAlterar)
    enable(?BtnVisualizarBaixas)
    enable(?btnExtornoBaixa)
  else
    enable(?btnAlterar)
    disable(?BtnVisualizarBaixas)
    disable(?btnExtornoBaixa)
  end
  
  !Libera��o de bot�o de notas
  if PLA:tipolancamento = 'C' and PAR:controlarnf
    Unhide(?btnNF)
  else
    Hide(?btnNF)
  end
  
  Display()
  
  ! Vers�o 1.0
  
  !if Queue:RADSQLBrowse.DOC:valorpago
  !  disable(?btnBaixaTotal)
  !else
  !  enable(?btnBaixaTotal)
  !end


RADSQLDrop2.GenerateSelectStatement PROCEDURE


  CODE
  PARENT.GenerateSelectStatement
  Self.ViewSqlStatement = |
  'SELECT A.ID, A.RAZAOSOCIAL FROM FAVORECIDOS A'

UpdateDocumentos PROCEDURE                                 ! Generated from procedure template - Window

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Repetir          LONG                                  !
LOC:incremento       LONG                                  !
LOC:TipoIncremento   BYTE(1)                               !Tipo de Incremento
LOC:Vencimento       DATE                                  !
Loc:Aux_Documento    CSTRING(100)                          !
qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
RADSQLDrop9Locator CString(256)
RADSQLDrop9:View     VIEW(empresa)
                       PROJECT(EMP:razaosocial)
                       PROJECT(EMP:codempresa)
                     END
Queue:RADSQLDrop     QUEUE                            !Queue declaration for browse/combo box using ?EMP:razaosocial
EMP:razaosocial        LIKE(EMP:razaosocial)          !List box control field - type derived from field
EMP:codempresa         LIKE(EMP:codempresa)           !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
fUpdate File,Driver('ODBC','/TurboSql=1'),Owner(GLO:conexao),Name('consultasql'),PRE(FUP)
Record    Record
campo1      Byte
          End
        End
History::DOC:Record  LIKE(DOC:RECORD),THREAD
QuickWindow          WINDOW('i-Money'),AT(,,312,229),FONT('MS Sans Serif',8,,FONT:regular),CENTER,IMM,HLP('UpdateDocumentos'),SYSTEM,GRAY,DOUBLE,AUTO
                       ENTRY(@n10.`0b),AT(3,36,58,10),USE(DOC:idcliente),FLAT,RIGHT(2),MSG('Identificador do cliente/fornecedor'),TIP('Identificador do cliente/fornecedor'),REQ
                       BUTTON,AT(63,35,12,12),USE(?lkpFavorecidos),TRN,FLAT,ICON('busca.ico')
                       ENTRY(@n15.`0b),AT(3,58,58,10),USE(DOC:idplano),FLAT,RIGHT(2),MSG('Identificador da conta no plano de contas'),TIP('Identificador da conta no plano de contas'),REQ
                       BUTTON,AT(63,57,12,12),USE(?lkpPlano),TRN,FLAT,ICON('busca.ico')
                       ENTRY(@n15.`0b),AT(3,81,58,10),USE(DOC:idcentrocusto),FLAT,RIGHT(2),MSG('ID de controle da tabela de bancos'),TIP('ID de controle da tabela de bancos'),REQ
                       BUTTON,AT(63,81,12,12),USE(?lkpCentroCusto),FLAT,ICON('busca.ico')
                       ENTRY(@s20),AT(3,105,71,10),USE(DOC:numdocumento),FLAT,MSG('N�mero do documento'),TIP('N�mero do documento'),REQ
                       ENTRY(@d06b),AT(76,105,55,10),USE(DOC:d_dataemissao),FLAT,CENTER,MSG('Data de emiss�o do documento'),TIP('Data de emiss�o do documento'),REQ
                       ENTRY(@d06b),AT(133,105,55,10),USE(DOC:d_datavencimento),FLAT,CENTER,MSG('Data de vencimento do documento'),TIP('Data de vencimento do documento'),REQ
                       ENTRY(@d06b),AT(190,105,55,10),USE(DOC:d_dataprovavel),FLAT,CENTER,MSG('Data de vencimento do documento'),TIP('Data de vencimento do documento'),REQ
                       ENTRY(@n13.`2),AT(247,105,62,10),USE(DOC:valordocumento),FLAT,RIGHT(2),MSG('Valor do documento'),TIP('Valor do documento')
                       ENTRY(@n10`2),AT(3,128,71,10),USE(DOC:indicemulta),FLAT,RIGHT,MSG('Utilizado para c�lculo de multas'),TIP('Utilizado para c�lculo de multas')
                       COMBO(@s29),AT(79,128,73,10),USE(DOC:tipomulta),FLAT,MSG('Utilizado para c�lculo de multas'),TIP('Utilizado para c�lculo de multas'),READONLY,DROP(10),FROM('Valor|#Valor|Dias|#Dias')
                       ENTRY(@n10`2),AT(157,128,73,10),USE(DOC:indicejuros),FLAT,RIGHT,MSG('Utilizado para c�lculo de juros, criado em 12/05/2009, Fabr�cio Fachini'),TIP('Utilizado para c�lculo de juros, criado em 12/05/2009, Fabr�cio Fachini')
                       COMBO(@s29),AT(235,128,73,10),USE(DOC:tipojuros),FLAT,MSG('Utilizado para c�lculo de juros'),TIP('Utilizado para c�lculo de juros'),READONLY,DROP(10),FROM('Valor|#Valor|Dias|#Dias')
                       ENTRY(@s50),AT(3,152,305,10),USE(DOC:descricao),FLAT,MSG('Descri��o do documento'),TIP('Descri��o do documento'),REQ
                       ENTRY(@n7.`0b),AT(3,173,42,10),USE(LOC:Repetir),FLAT,CENTER
                       ENTRY(@n7.`0b),AT(49,173,42,10),USE(LOC:incremento),FLAT,CENTER
                       LIST,AT(95,173,60,10),USE(LOC:TipoIncremento),FLAT,MSG('Tipo de Incremento'),TIP('Tipo de Incremento'),DROP(5),FROM('Dias|#1|Meses|#2|Anos|#3')
                       ENTRY(@d06b),AT(235,173,73,10),USE(DOC:d_cancelamento),FLAT,CENTER,MSG('Data de cancelamento integral do t�tulo'),TIP('Data de cancelamento integral do t�tulo')
                       PROMPT('Empresa:'),AT(3,184,33,10),USE(?LOC:Repetir:Prompt:2),TRN,LEFT
                       COMBO(@s255),AT(3,193,305,10),USE(EMP:razaosocial),IMM,FLAT,VSCROLL,FORMAT('200L(2)|F@s50@'),DROP(10),FROM(Queue:RADSQLDrop),MSG('Raz�o Social')
                       BUTTON('&Gravar'),AT(193,211,55,15),USE(?btnGravar),TRN,FLAT,LEFT,MSG('Accept data and close the window'),TIP('Accept data and close the window'),ICON('gravar.ico'),DEFAULT
                       BUTTON('&Cancelar'),AT(253,211,55,15),USE(?btnCancelar),TRN,FLAT,LEFT,MSG('Cancel operation'),TIP('Cancel operation'),ICON('cancelar.ico')
                       IMAGE('window.ico'),AT(3,2),USE(?Image1)
                       PROMPT('Documentos'),AT(29,5),USE(?Prompt8),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(3,22,307,2),USE(?Panel1)
                       PROMPT('Clientes e Fornecedores'),AT(3,27,86,10),USE(?DOC:idcliente:Prompt),TRN,LEFT
                       ENTRY(@s60),AT(79,36,229,10),USE(FAV:razaosocial),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Raz�o social do cliente/fornecedor'),TIP('Raz�o social do cliente/fornecedor'),REQ,READONLY
                       PROMPT('Plano:'),AT(3,49,39,10),USE(?DOC:idplano:Prompt),TRN,LEFT
                       ENTRY(@s70),AT(79,58,229,10),USE(PLA:descricaoplano),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Descri��o do lan�amento no plano de contas'),TIP('Descri��o do lan�amento no plano de contas'),REQ,READONLY
                       PROMPT('Centro de Custo:'),AT(3,72),USE(?DOC:idcentrocusto:Prompt),TRN
                       ENTRY(@s50),AT(79,81,229,10),USE(CCU:descricao),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Descri��o do lan�amento no plano de contas'),TIP('Descri��o do lan�amento no plano de contas'),REQ,READONLY
                       PROMPT('Documento:'),AT(3,96),USE(?DOC:numdocumento:Prompt),TRN,LEFT
                       PROMPT('Emiss�o:'),AT(76,96),USE(?DOC:d_dataemissao:Prompt),TRN
                       PROMPT('Vencimento:'),AT(133,96),USE(?DOC:d_datavencimento:Prompt),TRN
                       PROMPT('Previs�o:'),AT(190,96),USE(?DOC:d_datavencimento:Prompt:2),TRN
                       PROMPT('Valor:'),AT(248,96),USE(?DOC:valordocumento:Prompt),TRN
                       PROMPT('Tipo de Multa:'),AT(79,118),USE(?DOC:numdocumento:Prompt:3),TRN,LEFT
                       PROMPT('�ndice de Juros:'),AT(157,118),USE(?DOC:numdocumento:Prompt:4),TRN,LEFT
                       PROMPT('Tipo de Juros:'),AT(235,118),USE(?DOC:numdocumento:Prompt:5),TRN,LEFT
                       PROMPT('�ndice de Multa:'),AT(3,119),USE(?DOC:numdocumento:Prompt:2),TRN,LEFT
                       PROMPT('Descri��o:'),AT(3,142,39,10),USE(?DOC:descricao:Prompt),TRN,LEFT
                       PROMPT('Repetir:'),AT(3,164,33,10),USE(?LOC:Repetir:Prompt),TRN,LEFT
                       PROMPT('Incremento:'),AT(49,164),USE(?LOC:incremento:Prompt),TRN
                       PROMPT('Tipo:'),AT(95,164),USE(?LOC:TipoIncremento:Prompt),TRN
                       PROMPT('Cancelamento:'),AT(235,164),USE(?DOC:d_dataemissao:Prompt:2),TRN
                       PANEL,AT(3,206,307,2),USE(?Panel1:2)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
PrimeFields            PROCEDURE(),PROC,DERIVED            ! Method added to host embed code
Reset                  PROCEDURE(BYTE Force=0),DERIVED     ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED ! Method added to host embed code
SetAlerts              PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END

RADSQLDrop9          CLASS(RADSQLDrop)
GenerateSelectStatement PROCEDURE(),DERIVED                ! Method added to host embed code
SetRange               PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'Visualizando'
  OF InsertRecord
    ActionMessage = 'Incluindo'
  OF ChangeRecord
    ActionMessage = 'Alterando'
  END
  QuickWindow{Prop:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateDocumentos')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DOC:idcliente
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(DOC:Record,History::DOC:Record)
  SELF.AddHistoryField(?DOC:idcliente,3)
  SELF.AddHistoryField(?DOC:idplano,4)
  SELF.AddHistoryField(?DOC:idcentrocusto,2)
  SELF.AddHistoryField(?DOC:numdocumento,5)
  SELF.AddHistoryField(?DOC:d_dataemissao,7)
  SELF.AddHistoryField(?DOC:d_datavencimento,8)
  SELF.AddHistoryField(?DOC:d_dataprovavel,27)
  SELF.AddHistoryField(?DOC:valordocumento,9)
  SELF.AddHistoryField(?DOC:indicemulta,22)
  SELF.AddHistoryField(?DOC:tipomulta,23)
  SELF.AddHistoryField(?DOC:indicejuros,24)
  SELF.AddHistoryField(?DOC:tipojuros,25)
  SELF.AddHistoryField(?DOC:descricao,6)
  SELF.AddHistoryField(?DOC:d_cancelamento,26)
  SELF.AddUpdateFile(Access:documentos)
  SELF.AddItem(?btnCancelar,RequestCancelled)              ! Add the cancel control to the window manager
  COMPILE('C55',_C55_)
    Self.Filesopened = 1
  C55
  OMIT('C55',_C55_)
    Filesopened = 1
  C55
  Relate:empresa.Open()
  Access:empresa.UseFile()
  Relate:documentos.SetOpenRelated()
  Relate:documentos.Open                                   ! File documentos used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:documentos
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?btnGravar
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  Open(fUpdate,42h)
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>UpdateDocumentos<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>UpdateDocumentos<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?DOC:idcliente{PROP:ReadOnly} = True
    DISABLE(?lkpFavorecidos)
    ?DOC:idplano{PROP:ReadOnly} = True
    DISABLE(?lkpPlano)
    ?DOC:idcentrocusto{PROP:ReadOnly} = True
    DISABLE(?lkpCentroCusto)
    ?DOC:numdocumento{PROP:ReadOnly} = True
    ?DOC:d_dataemissao{PROP:ReadOnly} = True
    ?DOC:d_datavencimento{PROP:ReadOnly} = True
    ?DOC:d_dataprovavel{PROP:ReadOnly} = True
    ?DOC:valordocumento{PROP:ReadOnly} = True
    ?DOC:indicemulta{PROP:ReadOnly} = True
    DISABLE(?DOC:tipomulta)
    ?DOC:indicejuros{PROP:ReadOnly} = True
    DISABLE(?DOC:tipojuros)
    ?DOC:descricao{PROP:ReadOnly} = True
    ?LOC:Repetir{PROP:ReadOnly} = True
    ?LOC:incremento{PROP:ReadOnly} = True
    DISABLE(?LOC:TipoIncremento)
    ?DOC:d_cancelamento{PROP:ReadOnly} = True
    DISABLE(?EMP:razaosocial)
    ?FAV:razaosocial{PROP:ReadOnly} = True
    ?PLA:descricaoplano{PROP:ReadOnly} = True
    ?CCU:descricao{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  RADSQLDrop9.Listqueue      &= Queue:RADSQLDrop
  RADSQLDrop9.Position       &= Queue:RADSQLDrop.ViewPosition
  RADSQLDrop9.Setlistcontrol(?EMP:razaosocial)
  RADSQLDrop9.RefreshHotkey = F5Key
  RADSQLDrop9.AddDropVariables(?EMP:razaosocial,0,EMP:razaosocial,'EMP:codempresa',EMP:codempresa,DOC:idempresa)
  RADSQLDrop9.Init(Access:empresa)
  RADSQLDrop9.AddField(EMP:codempresa,DOC:idempresa)
     RADSQLDrop9.useansijoin=1
  SELF.SetAlerts()
  ?EMP:razaosocial{PROP:From} = Queue:RADSQLDrop
  RADSQLDrop9.AddTable('empresa',1,0,'EMPRESA')  ! Add the table to the list of tables
  RADSQLDrop9.ColumnCharAsc   = ''
  RADSQLDrop9.ColumnCharDes   = ''
  RADSQLDrop9.DefaultFill     = True
  RADSQLDrop9.AddFieldpairs(Queue:RADSQLDrop.EMP:codempresa,EMP:codempresa,'empresa','codempresa | READONLY','N',0,'B','A','I','N',1,2)
  RADSQLDrop9.AddFieldpairs(Queue:RADSQLDrop.EMP:razaosocial,EMP:razaosocial,'empresa','razaosocial','A',0,'A','A',' ','N',2,1)
  RADSQLDrop9.Addindexfunction('UPPER(%)')
  RADSQLDrop9.Managedview &=RADSQLDrop9:View
  RADSQLDrop9.Openview
  RADSQLDrop9.SetFileLoad
  RADSQLDrop9.SetColColor = 0
  RADSQLDrop9.Resetsort(1,1)
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  COMPILE('C55',_C55_)
  if Self.filesopened
  C55
  OMIT('C55',_C55_)
  If filesopened then
  C55
  Relate:empresa.Close
  END
    Relate:documentos.Close
  Close(fUpdate)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    LOC:Repetir = 1
  PARENT.PrimeFields


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  CCU:id = DOC:idcentrocusto                               ! Assign linking field value
  Access:centrocusto.Fetch(CCU:ccu_pk_id)
  PLA:id = DOC:idplano                                     ! Assign linking field value
  Access:planocontas.Fetch(PLA:pla_pk_id)
  FAV:id = DOC:idcliente                                   ! Assign linking field value
  Access:favorecidos.Fetch(FAV:cli_pk_id)
  PARENT.Reset(Force)
  If force = 2
    RADSQLDrop9.ResetBrowse (1)
  end


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      BrowseFavorecidos
      BrowsePlanoContas
      BrowseCentroDeCusto
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  RADSQLDrop9.SetAlerts


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?DOC:d_datavencimento
      if ~0{prop:acceptall}
        DOC:d_dataprovavel = DOC:d_datavencimento
        display
      end
    OF ?btnGravar
      if DOC:indicemulta
        ?DOC:tipomulta{prop:req} = 1
      else
        ?DOC:tipomulta{prop:req} = 0
      end
      
      if DOC:indicejuros
        ?DOC:tipojuros{prop:req} = 1
      else
        ?DOC:tipojuros{prop:req} = 0
      end
      
      If LOC:Repetir > 1 And ~LOC:incremento
        Message('Voc� deve informar um fator de incremento para os vencimentos!','Aten��o',Icon:Asterisk,'OK',,2)
        Cycle
      End
      
      If LOC:Repetir > 1 And LOC:incremento
        Loc:Aux_Documento  = DOC:numdocumento
        DOC:numdocumento   = DOC:numdocumento&'-1/'&LOC:Repetir
      
      end
      
      setnull(DOC:idnf)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DOC:idcliente
      IF DOC:idcliente OR ?DOC:idcliente{Prop:Req}
        FAV:id = DOC:idcliente
        IF Access:favorecidos.TryFetch(FAV:cli_pk_id)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            DOC:idcliente = FAV:id
          ELSE
            SELECT(?DOC:idcliente)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?lkpFavorecidos
      ThisWindow.Update
      FAV:id = DOC:idcliente
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        DOC:idcliente = FAV:id
      END
      ThisWindow.Reset(1)
    OF ?DOC:idplano
      IF DOC:idplano OR ?DOC:idplano{Prop:Req}
        PLA:id = DOC:idplano
        IF Access:planocontas.TryFetch(PLA:pla_pk_id)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            DOC:idplano = PLA:id
          ELSE
            SELECT(?DOC:idplano)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      If ~0{prop:AcceptAll}
        If PLA:tipovalor <> 7
          Message('Conta inv�lida, voc� deve selecionar uma conta do tipo "Valor Documento", corrija!','Aten��o',Icon:Asterisk,'OK',,2)
          Clear(PLA:descricaoplano)
          Clear(DOC:idplano)
          Select(?DOC:idplano)
        End
      End
    OF ?lkpPlano
      ThisWindow.Update
      PLA:id = DOC:idplano
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        DOC:idplano = PLA:id
      END
      ThisWindow.Reset(1)
      If ~0{prop:AcceptAll}
        Post(Event:Accepted,?DOC:idplano)
      End
    OF ?DOC:idcentrocusto
      IF DOC:idcentrocusto OR ?DOC:idcentrocusto{Prop:Req}
        CCU:id = DOC:idcentrocusto
        IF Access:centrocusto.TryFetch(CCU:ccu_pk_id)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            DOC:idcentrocusto = CCU:id
          ELSE
            SELECT(?DOC:idcentrocusto)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?lkpCentroCusto
      ThisWindow.Update
      CCU:id = DOC:idcentrocusto
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        DOC:idcentrocusto = CCU:id
      END
      ThisWindow.Reset(1)
      If ~0{prop:AcceptAll}
        Post(Event:Accepted,?DOC:idcentrocusto)
      End
    OF ?DOC:valordocumento
      IF Access:documentos.TryValidateField(9)             ! Attempt to validate DOC:valordocumento in documentos
        SELECT(?DOC:valordocumento)
        QuickWindow{Prop:AcceptAll} = False
        CYCLE
      ELSE
        FieldColorQueue.Feq = ?DOC:valordocumento
        GET(FieldColorQueue, FieldColorQueue.Feq)
        IF ~ERRORCODE()
          ?DOC:valordocumento{Prop:FontColor} = FieldColorQueue.OldColor
          DELETE(FieldColorQueue)
        END
      END
    OF ?btnGravar
      ThisWindow.Update
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeCompleted()
  !Se tiver gravado corretamente e for inclus�o de um novo documento, efetua o teste de duplicidade de boletos e incremento
  If Self.Response = RequestCompleted And Self.Request = InsertRecord
    If LOC:Repetir > 1
      b# = 2
      LOC:Vencimento = DOC:d_datavencimento
      Loop a# = 1 To LOC:Repetir - 1
        If LOC:TipoIncremento = 1
          LOC:Vencimento = LOC:Vencimento+(LOC:incremento)
        ElsIf LOC:TipoIncremento = 2
          LOC:Vencimento = Date(Month(LOC:Vencimento)+1,Day(LOC:Vencimento),Year(LOC:Vencimento))
        Else
          LOC:Vencimento = Date(Month(LOC:Vencimento),Day(LOC:Vencimento),Year(LOC:Vencimento)+LOC:incremento)
        End
        Clear(FUP:Record)
  !      fUpdate{prop:sql} = 'Insert into documentos(idcliente, idplano, numdocumento, descricao, d_dataemissao, d_datavencimento, d_dataprovavel, valordocumento, idcentrocusto) '&|
  !                          'values('&DOC:idcliente&','&DOC:idplano&',<39>'&DOC:numdocumento&'-'&a#&'<39>,<39>'&DOC:descricao&'<39>,<39>'&|
  !                          Format(DOC:d_dataemissao,@d012)&'<39>,<39>'&Format(LOC:Vencimento,@d012)&'<39>,<39>'&Format(LOC:Vencimento,@d012)&'<39>,'&|
  !                          DOC:valordocumento&','&DOC:idcentrocusto&')'
  
        fUpdate{prop:sql} = 'Insert into documentos(idcliente, idplano, numdocumento, descricao, d_dataemissao, d_datavencimento, d_dataprovavel, valordocumento, idcentrocusto, idempresa) '&|
                            'values('&DOC:idcliente&','&DOC:idplano&',<39>'&Loc:Aux_Documento&'-'&b#&'/'&LOC:Repetir&'<39>,<39>'&DOC:descricao&'<39>,<39>'&|
                            Format(DOC:d_dataemissao,@d012)&'<39>,<39>'&Format(LOC:Vencimento,@d012)&'<39>,<39>'&Format(LOC:Vencimento,@d012)&'<39>,'&|
                            DOC:valordocumento&','&DOC:idcentrocusto&','&DOC:idempresa&')'
  
        b# += 1
  
  
        !Message(fUpdate{prop:sql},,,,,2)
      End
    End
  End
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
  RADSQLDrop9.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('UpdateDocumentos')
          End
        End
      End
      Close(HWGroups)
    OF EVENT:OpenWindow
      If Self.Request <> InsertRecord
        Disable(?LOC:Repetir)
        Disable(?LOC:incremento)
        Disable(?LOC:TipoIncremento)
      End
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


RADSQLDrop9.GenerateSelectStatement PROCEDURE


  CODE
  PARENT.GenerateSelectStatement
  self.viewsqlstatement = |
    'SELECT A.CODEMPRESA, A.RAZAOSOCIAL FROM EMPRESA A'


RADSQLDrop9.SetRange PROCEDURE


  CODE
  PARENT.SetRange
  self.rangefilter = 'a.codempresa = '&GLO:CodEmpresa

UpdateBaixa PROCEDURE                                      ! Generated from procedure template - Form

ActionMessage        CSTRING(40)                           !
loc:tipobaixa        CSTRING(20)                           !
LOC:AlteracaoSaldo   DECIMAL(15,2)                         !
loc:valorpagar       DECIMAL(15,2)                         !
loc:valorbaixaparcialanterior DECIMAL(7,2)                 !
loc:valorjuros       DECIMAL(7,2)                          !
loc:valormulta       DECIMAL(7,2)                          !
loc:desconto         DECIMAL(7,2)                          !
loc:valoroutrosacrescimos DECIMAL(7,2)                     !
loc:ConciliarDocumentosAoExtrato BYTE                      !
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
Window               WINDOW('i-Money'),AT(,,341,256),FONT('MS Sans Serif',8,,FONT:regular),CENTER,IMM,SYSTEM,GRAY,DOUBLE,AUTO
                       ENTRY(@n-15.`2),AT(267,41,70,10),USE(loc:valorpagar),FLAT,RIGHT,FONT(,,,FONT:bold)
                       ENTRY(@d06b),AT(7,41,70,10),USE(DOC:d_dataemissao),SKIP,FLAT,MSG('Data de emiss�o do documento'),TIP('Data de emiss�o do documento'),REQ,READONLY
                       ENTRY(@d06b),AT(93,41,70,10),USE(DOC:d_datavencimento),SKIP,FLAT,MSG('Data de vencimento do documento'),TIP('Data de vencimento do documento'),REQ,READONLY
                       ENTRY(@n13.`2),AT(179,41,70,10),USE(DOC:valordocumento),SKIP,FLAT,DECIMAL(12),MSG('Valor do documento'),TIP('Valor do documento'),READONLY
                       ENTRY(@n7.`2),AT(14,80,44,10),USE(loc:valorjuros),FLAT,RIGHT(2),MSG('Valor de juros do documento'),TIP('Valor de juros do documento'),READONLY
                       ENTRY(@n10.`2),AT(71,80,36,10),USE(DOC:indicejuros),FLAT,RIGHT(2),MSG('Utilizado para c�lculo de juros, criado em 12/05/2009, Fabr�cio Fachini'),TIP('Utilizado para c�lculo de juros, criado em 12/05/2009, Fabr�cio Fachini'),READONLY
                       ENTRY(@s29),AT(120,80,41,10),USE(DOC:tipojuros),FLAT,MSG('Utilizado para c�lculo de juros'),TIP('Utilizado para c�lculo de juros'),READONLY
                       ENTRY(@n10.`2),AT(184,79,44,10),USE(loc:valormulta),FLAT,RIGHT(2),MSG('Valor de mulya'),TIP('Valor de mulya'),READONLY
                       ENTRY(@n8.`2),AT(241,79,36,10),USE(DOC:indicemulta),FLAT,RIGHT(2),MSG('Utilizado para c�lculo de multas'),TIP('Utilizado para c�lculo de multas'),READONLY
                       ENTRY(@s29),AT(290,79,41,10),USE(DOC:tipomulta),FLAT,MSG('Utilizado para c�lculo de multas'),TIP('Utilizado para c�lculo de multas'),READONLY
                       ENTRY(@n-10.`2),AT(7,107,62,10),USE(loc:desconto),FLAT,RIGHT(2),FONT(,,,FONT:bold),MSG('Valor de desconto do documento'),TIP('Valor de desconto do documento')
                       ENTRY(@n-10.`2),AT(80,107,62,10),USE(loc:valoroutrosacrescimos),FLAT,RIGHT(2),FONT(,,,FONT:bold),MSG('Valor de outros acr�scimos'),TIP('Valor de outros acr�scimos')
                       ENTRY(@n15.`0b),AT(15,146,70,10),USE(CON:id),FLAT,DECIMAL(12),MSG('Identifica��o da conta'),TIP('Identifica��o da conta'),REQ
                       ENTRY(@s50),AT(116,146,201,10),USE(CON:descricaoconta),SKIP,FLAT,MSG('Descri��o da conta'),TIP('Descri��o da conta'),READONLY
                       ENTRY(@n03b),AT(15,176,70,10),USE(CON:numerodobanco),SKIP,FLAT,RIGHT(1),MSG('N�mero do banco, caso seja uma conta banc�ria'),TIP('N�mero do banco, caso seja uma conta banc�ria'),READONLY
                       ENTRY(@s10),AT(89,176,70,10),USE(CON:agenciabancaria),SKIP,FLAT,MSG('Ag�ncia banc�ria, caso seja uma conta banc�ria'),TIP('Ag�ncia banc�ria, caso seja uma conta banc�ria'),READONLY
                       ENTRY(@s20),AT(163,176,70,10),USE(CON:numeroconta),SKIP,FLAT,MSG('N�mero da conta, caso seja uma ag�ncia banc�ria'),TIP('N�mero da conta, caso seja uma ag�ncia banc�ria'),READONLY
                       ENTRY(@d06b),AT(7,212,62,10),USE(DOC:d_datapagamento),FLAT,FONT(,,,FONT:bold),MSG('Data de pagamento'),TIP('Data de pagamento')
                       ENTRY(@n13.`2),AT(80,212,62,10),USE(DOC:valorpago),SKIP,FLAT,DECIMAL(12),FONT(,,,FONT:bold),MSG('Valor pago do documento'),TIP('Valor pago do documento'),READONLY
                       BUTTON('&Gravar'),AT(213,237,55,15),USE(?btnGravar),FLAT,LEFT,ICON('gravar.ico')
                       BUTTON('&Cancelar'),AT(278,237,55,15),USE(?btnCancelar),FLAT,LEFT,ICON('cancelar.ico'),STD(STD:Close)
                       CHECK(' Conciliar Documento ao Extrato'),AT(70,242),USE(loc:ConciliarDocumentosAoExtrato),FONT(,9,,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                       IMAGE('window.ico'),AT(3,2),USE(?Image1)
                       PROMPT('Baixa do Documento'),AT(29,5),USE(?Prompt8),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,22,333,2),USE(?Panel1)
                       PROMPT('Emiss�o:'),AT(7,30),USE(?DOC:d_dataemissao:Prompt),TRN
                       PROMPT('Vencimento:'),AT(93,30),USE(?DOC:d_datavencimento:Prompt),TRN
                       PROMPT('Total do Doc:'),AT(179,30),USE(?DOC:valordocumento:Prompt),TRN
                       PROMPT('Valor a pagar:'),AT(267,30),USE(?DOC:valordocumento:Prompt:2),TRN,FONT(,,,FONT:bold)
                       GROUP('Mora'),AT(7,56,160,38),USE(?Group2),BOXED
                       END
                       GROUP('Multa'),AT(177,56,160,38),USE(?Group2:2),BOXED
                         PROMPT('Valor:'),AT(184,69,44,10),USE(?DOC:valorjuros:Prompt:2),TRN
                         PROMPT('�ndice:'),AT(242,69),USE(?DOC:indicejuros:Prompt:2)
                         PROMPT('Tipo:'),AT(289,69),USE(?DOC:tipojuros:Prompt:2)
                       END
                       PROMPT('Valor:'),AT(14,70,44,10),USE(?DOC:valorjuros:Prompt),TRN
                       PROMPT('�ndice:'),AT(72,70),USE(?DOC:indicejuros:Prompt)
                       PROMPT('Tipo:'),AT(119,70),USE(?DOC:tipojuros:Prompt)
                       PROMPT('Desconto:'),AT(7,97,61,10),USE(?DOC:valordesconto:Prompt),TRN,FONT(,,,FONT:bold)
                       PROMPT('Outros Acr�scimos:'),AT(80,97),USE(?DOC:valoroutrosacrescimos:Prompt),TRN,FONT(,,,FONT:bold)
                       GROUP('Conta Banc�ria:'),AT(9,121,323,70),USE(?Group1),BOXED
                         BUTTON,AT(101,145,12,12),USE(?CallLookup),FLAT,ICON('busca.ico')
                         PROMPT('N<186> Doc. Pagamento:'),AT(247,164),USE(?MOV:numdocpagamento:Prompt),TRN,LEFT
                         ENTRY(@s20),AT(247,177,70,10),USE(MOV:numdocpagamento),FLAT,RIGHT(2),MSG('N�mero do Documento do Pagamento (N<186> Cheque ou Tranfer�ncia Banc�ria)'),TIP('N�mero do Documento do Pagamento (N<186> Cheque ou Tranfer�ncia Banc�ria)'),UPR
                       END
                       PROMPT('ID:'),AT(15,135),USE(?CON:id:Prompt)
                       PROMPT('N<186> Banco:'),AT(15,165),USE(?CON:numerodobanco:Prompt)
                       PROMPT('Ag�ncia:'),AT(89,165),USE(?CON:agenciabancaria:Prompt)
                       PROMPT('N<186> Conta:'),AT(163,165),USE(?CON:numeroconta:Prompt)
                       PROMPT('Data do Pagto:'),AT(7,202,62,10),USE(?DOC:d_datapagamento:Prompt),TRN,FONT(,,,FONT:bold)
                       PROMPT('Total a pagar:'),AT(81,202),USE(?DOC:valorpago:Prompt),TRN,FONT(,,,FONT:bold)
                       PANEL,AT(4,228,333,2),USE(?Panel1:2)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
CalculaValorPagamento Routine
  
  ! Inclus�o 1 - 13/05/2009, inclus�o de c�digo, Inicio
  
  if DOC:indicejuros
     if DOC:tipojuros = 'Valor'
        loc:valorjuros = DOC:indicejuros
     else ! DIA
        if DOC:d_datapagamento > DOC:d_datavencimento
           loc:valorjuros = ((loc:valorpagar/100) * DOC:indicejuros) * (DOC:d_datapagamento-DOC:d_datavencimento)
        end
     end
  end
  if DOC:indicemulta
     if DOC:tipomulta = 'Valor'
        loc:valormulta = DOC:indicemulta
     else ! DIA
        if DOC:d_datapagamento > DOC:d_datavencimento
           loc:valormulta = ((loc:valorpagar/100) * DOC:indicemulta) * (DOC:d_datapagamento-DOC:d_datavencimento)
        end
     end
  end
  ! Inclus�o 1 - 13/05/2009, inclus�o de c�digo, FIM

  ! Altera��o 1 - 13/05/2009, inclus�o de c�digo, Inicio
  !DOC:valorpago = (DOC:valordocumento-(DOC:valordesconto+DOC:valorloutrosdescontos)+(DOC:valorjuros+DOC:valormulta+DOC:valoroutrosacrescimos))
  DOC:valorpago = (loc:valorpagar+(loc:valorjuros+loc:valormulta+loc:valoroutrosacrescimos)-(loc:desconto))
  ! Altera��o 1 - 13/05/2009, inclus�o de c�digo, FIM

  Display

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateBaixa')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?loc:valorpagar
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:contas.SetOpenRelated()
  Relate:contas.Open                                       ! File contas used by this procedure, so make sure it's RelationManager is open
  Access:documentos.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:movdocumentos.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:contas.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    BrowseContas
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?loc:valorpagar
      if loc:valorpagar > (DOC:valordocumento - DOC:valororiginalbaixado)
         loc:valorpagar = DOC:valordocumento - DOC:valororiginalbaixado
         message('O valor informado � maior que o saldo em aberto','Aten��o',icon:exclamation)
      end
      Do CalculaValorPagamento
    OF ?loc:valorjuros
      Do CalculaValorPagamento
    OF ?loc:valormulta
      Do CalculaValorPagamento
    OF ?loc:desconto
      Do CalculaValorPagamento
    OF ?loc:valoroutrosacrescimos
      Do CalculaValorPagamento
    OF ?btnGravar
      If ~CON:id
        Message('Selecione a conta!','Aten��o',Icon:Asterisk,'OK',,2)
        select(?CON:id)
      elsif CON:id and CON:idempresa <> GLO:CodEmpresa
        Message('A conta selecionada n�o pertence a empresa, corrija!','Aten��o',Icon:Asterisk,'OK',,2)
        select(?CON:id)
      Else
      
        ! A2 - 13/05/2009, inclus�o de c�digo, Inicio
        !C�digo Original
        !ConsultaSQL{prop:sql} = 'insert into movdocumentos (iddocumento, idplano, descricao,d_datamovimentacao, valormovimento, '&|
        !                        'idconta, usuario) values ('&DOC:id&','&DOC:idplano&','&'<39>N� Documento: '&DOC:numdocumento&' - Valor Pago<39>,<39>'&|
        !                         format(DOC:d_datapagamento,@d012)&'<39>,'&DOC:valorpago&','&CON:id&',<39>'&GLO:nomeusuario&'<39>)'
        
        DOC:valororiginalbaixado = DOC:valororiginalbaixado + loc:valorpagar
      
        loc:tipobaixa = '1-Total'
        !stop(DOC:valororiginalbaixado &' - '& DOC:valorpago)                                       
        if DOC:valordocumento <> DOC:valorpago
          loc:tipobaixa = '2-Parcial'
        end
      
        if loc:ConciliarDocumentosAoExtrato  ! Doc J� Conciliado no Extrato
      
            ConsultaSQL{prop:sql} = 'insert into movdocumentos (iddocumento, descricao,d_datamovimentacao, valormovimento, '&|
                                    'idconta, usuario, valorjuros, valormulta, valoroutrosacrescimos, valordesconto, tipobaixa, idplano, d_dataconciliacao, numdocpagamento) '&|
                                    'values ('&DOC:id&','&'<39>N� Documento: '&DOC:numdocumento&' - Valor Pago<39>,<39>'&|
                                     format(DOC:d_datapagamento,@d012)&'<39>,'&DOC:valorpago&','&CON:id&',<39>'&GLO:nomeusuario&'<39>,'&|
                                     loc:valorjuros&','&loc:valormulta&','&loc:valoroutrosacrescimos&','&loc:desconto&|
                                    ',<39>'&loc:tipobaixa&'<39>, '& DOC:idplano &', <39>'& format(DOC:d_datapagamento,@d012) & '<39>,<39>'& MOV:numdocpagamento &'<39>)'
            if error() then stop(fileerror()) .
        else
      
            ConsultaSQL{prop:sql} = 'insert into movdocumentos (iddocumento, descricao,d_datamovimentacao, valormovimento, '&|
                                    'idconta, usuario, valorjuros, valormulta, valoroutrosacrescimos, valordesconto, tipobaixa, idplano, numdocpagamento) '&|
                                    'values ('&DOC:id&','&'<39>N� Documento: '&DOC:numdocumento&' - Valor Pago<39>,<39>'&|
                                     format(DOC:d_datapagamento,@d012)&'<39>,'&DOC:valorpago&','&CON:id&',<39>'&GLO:nomeusuario&'<39>,'&|
                                     loc:valorjuros&','&loc:valormulta&','&loc:valoroutrosacrescimos&','&loc:desconto&|
                                    ',<39>'&loc:tipobaixa&'<39>,<39> '& DOC:idplano & '<39>,<39>'& MOV:numdocpagamento &'<39>)'
            if error() then stop(fileerror()) .
        end
      
        !message(ConsultaSQL{prop:sql},,,,,2)
      
        DOC:valorpago = loc:valorbaixaparcialanterior + DOC:valorpago
      
        DOC:valordesconto = DOC:valordesconto + loc:desconto
        DOC:valorjuros    = DOC:valorjuros + loc:valorjuros
        DOC:valormulta    = DOC:valormulta + loc:valormulta
        DOC:valoroutrosacrescimos = DOC:valoroutrosacrescimos + loc:valoroutrosacrescimos
      
        !message(ConsultaSQL{prop:sql},,,,,2)
        ! A2 - 13/05/2009, inclus�o de c�digo, Fim
        put(documentos)
      
      
        Post(Event:CloseWindow)
      End
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CON:id
      IF CON:id OR ?CON:id{Prop:Req}
        CON:id = CON:id
        IF Access:contas.TryFetch(CON:con_pk_id)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            CON:id = CON:id
          ELSE
            SELECT(?CON:id)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      CON:id = CON:id
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        CON:id = CON:id
      END
      ThisWindow.Reset(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      Clear(CON:Record)
      
      ! Utilizado no btn gravar, para acumular valor pago (baixa parcial)
      loc:valorbaixaparcialanterior = DOC:valorpago
      
      MOV:iddocumento = DOC:id
      get(movdocumentos,MOV:mov_fk_documento)
      
      CON:id = MOV:idconta
      get(contas,CON:con_pk_id)
      
      DOC:d_datapagamento = DataServidor()
      
      ! 13/05/2009, inclus�o de c�digo, Inicio
      
      loc:valorpagar = DOC:valordocumento - DOC:valororiginalbaixado
      
      ! 13/05/2009, inclus�o de c�digo, Fim
      
      loc:ConciliarDocumentosAoExtrato = 1
      
      Do CalculaValorPagamento
      
      !Limpa campos
      clear(CON:id)
      clear(CON:descricaoconta)
      clear(CON:numerodobanco)
      clear(CON:agenciabancaria)
      clear(CON:numeroconta)
      clear(MOV:numdocpagamento)
      
      Display
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

RelatDocumentosPagar PROCEDURE                             ! Generated from procedure template - Report

Progress:Thermometer BYTE                                  !
LOC:popup            CSTRING(201)                          !
LOC:In               CSTRING(50001)                        !
LOC:ListarBaixas     BYTE                                  !
LOC:TipoData         BYTE(1)                               !
LOC:DataInicial      DATE                                  !
LOC:DataFinal        DATE                                  !
LOC:RazaoSocial      CSTRING(101)                          !
LOC:EnderecoBairro   CSTRING(101)                          !
LOC:CepTelefone      CSTRING(101)                          !
LOC:Data             DATE                                  !
LOC:Hora             STRING(20)                            !
LOC:TipoRelatorio    BYTE(1)                               !
LOC:TipoDocumento    BYTE(3)                               !
LOC:filtro           CSTRING(100001)                       !
LOC:ordem            BYTE(1)                               !
loc:subtotal         DECIMAL(10,2)                         !
loc:valor            DECIMAL(10,2)                         !
loc:totaloutrosacrescimos DECIMAL(7,2)                     !
loc:totaldesconto    DECIMAL(7,2)                          !
loc:totaljuros       DECIMAL(7,2)                          !
loc:totalmulta       DECIMAL(7,2)                          !
loc:totalvalor       DECIMAL(7,2)                          !
Loc:queryString      CSTRING(3000)                         !
loc:docsimanager     BYTE                                  !
loc:view_where       CSTRING(1000)                         !
LOC:idplano          REAL                                  !
qPlano               QUEUE,PRE(QPL)                        !
codigo               LONG                                  !
check                LONG                                  !
descricao            CSTRING(51)                           !
codigopai            LONG                                  !
Display              CSTRING(101)                          !
icone                LONG                                  !
tree                 LONG                                  !
                     END                                   !
Process:View         VIEW(empresa)
                     END
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
RADSQLDrop5Locator CString(256)
RADSQLDrop5:View     VIEW(favorecidos)
                       PROJECT(FAV:razaosocial)
                       PROJECT(FAV:id)
                     END
Queue:RADSQLDrop     QUEUE                            !Queue declaration for browse/combo box using ?FAV:razaosocial
FAV:razaosocial        LIKE(FAV:razaosocial)          !List box control field - type derived from field
FAV:id                 LIKE(FAV:id)                   !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
fDocumentosPagar FILE,DRIVER('ODBC','/Turbosql=1'),OWNER(GLO:conexao),PRE(FDP),Name('consultasql')
Record      Record
campo1      long
campo2      long
campo3      long
campo4      cstring(21)
campo5      cstring(51)
campo6      date
campo7      date
campo8      decimal(10,2)
campo9      date
campo10     decimal(10,2)
campo11     decimal(10,2)
campo12     decimal(10,2)
campo13     decimal(10,2)
campo14     decimal(10,2)
campo15     decimal(10,2)
campo16     long
campo17     cstring(71)
campo18     cstring(2)
campo19     long
campo20     cstring(61)
campo21      date     ! Data Provavel
            End
        End

fMoveDocs FILE,DRIVER('ODBC','/Turbosql=1'),OWNER(GLO:conexao),PRE(FMD),Name('consultasql')
Record      Record
campo1      date
campo2      decimal(10,2)
campo3      decimal(10,2)
campo4      decimal(10,2)
campo5      decimal(10,2)
campo6      decimal(10,2)
campo7      decimal(10,2)
campo8      cstring(61)
            End
        End


LOC:vlr_documento decimal(15,2)
LOC:vlr_multa decimal(15,2)
LOC:vlr_juros decimal(15,2)
LOC:vlr_desconto decimal(15,2)
LOC:vlr_outros decimal(15,2)
LOC:vlr_pago decimal(15,2)
ProgressWindow       WINDOW('i-Money'),AT(,,579,236),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,TIMER(1),GRAY,DOUBLE
                       IMAGE('window.ico'),AT(3,2),USE(?Image1)
                       STRING(''),AT(114,3,15,12),USE(?Progress:UserString),HIDE,CENTER
                       PROMPT('Relat�rio de Documentos'),AT(29,5),USE(?Prompt8),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       STRING(''),AT(199,4,15,12),USE(?Progress:PctText),HIDE,CENTER
                       PANEL,AT(3,22,573,2),USE(?Panel1)
                       OPTION('Tipo de Documentos'),AT(3,28,243,25),USE(LOC:TipoRelatorio),BOXED
                         RADIO(' A Pagar'),AT(58,39),USE(?LOC:TipoRelatorio:Radio4),VALUE('1')
                         RADIO(' A Receber'),AT(141,39),USE(?LOC:TipoRelatorio:Radio5),VALUE('2')
                       END
                       GROUP('Plano de Contas'),AT(253,28,321,180),USE(?Group4),BOXED
                         LIST,AT(259,39,310,164),USE(?lstPlano),FLAT,HVSCROLL,FONT('Courier New',8,,FONT:regular,CHARSET:ANSI),FORMAT('11L(7)I@n10@#1#320L(2)|IT@s100@#5#'),FROM(qPlano)
                       END
                       OPTION('Listar Documentos'),AT(3,56,243,39),USE(LOC:TipoDocumento),BOXED
                         RADIO(' Pagos'),AT(21,68),USE(?Option1:Radio1),TRN,VALUE('1')
                         RADIO(' Em Aberto'),AT(97,68),USE(?LOC:TipoDocumento:Radio2),TRN,VALUE('2')
                         RADIO(' Todos'),AT(187,68),USE(?LOC:TipoDocumento:Radio3),TRN,VALUE('3')
                       END
                       CHECK('Listar movimenta��es de baixas?'),AT(21,81),USE(LOC:ListarBaixas),FONT(,,,FONT:bold,CHARSET:ANSI)
                       GROUP('Per�odo'),AT(3,97,243,30),USE(?Group1),BOXED
                         LIST,AT(25,110,56,10),USE(LOC:TipoData),FLAT,DROP(5),FROM('Vencimento|#1|Previs�o|#2|Emiss�o|#3|Pagamento|#4')
                         ENTRY(@d06b),AT(99,110,46,10),USE(LOC:DataInicial),FLAT
                         BUTTON,AT(148,108,12,12),USE(?Calendar),FLAT,ICON('Calendario.ico')
                         ENTRY(@d06b),AT(180,110,46,10),USE(LOC:DataFinal),FLAT
                         BUTTON,AT(228,108,12,12),USE(?Calendar:2),FLAT,ICON('Calendario.ico')
                         PROMPT('Tipo:'),AT(8,110),USE(?LOC:TipoData:Prompt)
                         PROMPT('De:'),AT(85,110),USE(?LOC:DataInicial:Prompt),TRN
                         PROMPT('At�:'),AT(163,110),USE(?LOC:DataFinal:Prompt),TRN
                       END
                       GROUP('Clientes e Fornecedores'),AT(3,131,243,30),USE(?Group2),BOXED
                         COMBO(@s255),AT(9,144,231,10),USE(FAV:razaosocial),IMM,FLAT,VSCROLL,FORMAT('240L(2)|M@s60@'),DROP(10),FROM(Queue:RADSQLDrop),MSG('Raz�o social do cliente/fornecedor')
                       END
                       GROUP('Ordenar por'),AT(3,165,243,30),USE(?Group3),BOXED
                         LIST,AT(9,178,231,10),USE(LOC:ordem),FLAT,DROP(7),FROM('|#1|Clientes e Fornecedores|#2|Vencimento|#3|Pagamento|#4|Plano de Contas|#5|Previs�o|#6')
                       END
                       CHECK('Relacionar Documentos do i-MANAGER'),AT(6,198),USE(loc:docsimanager),FONT(,,,FONT:bold),VALUE('1','0')
                       PANEL,AT(2,211,573,2),USE(?Panel1:2)
                       PROGRESS,USE(Progress:Thermometer),HIDE,AT(4,217,63,12),RANGE(0,100)
                       BUTTON('&Imprimir'),AT(460,218,55,15),USE(?Pause),FLAT,LEFT,ICON('imprimir.ico')
                       BUTTON('&Cancelar'),AT(518,218,55,15),USE(?Progress:Cancel),FLAT,LEFT,ICON('cancelar.ico')
                     END

Report               REPORT,AT(4,30,282,160),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',8,,,CHARSET:ANSI),LANDSCAPE,MM
                       HEADER,AT(3,6,282,24),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         BOX,AT(81,2,198,9),USE(?Box1:4),ROUND,COLOR(COLOR:Black),FILL(COLOR:Gray)
                         BOX,AT(3,2,76,16),USE(?Box1:2),ROUND,COLOR(COLOR:Black),FILL(COLOR:Gray)
                         BOX,AT(80,1,198,9),USE(?Box1:3),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(2,1,76,16),USE(?Box1),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         STRING('Documentos a Pagar'),AT(80,2,199,7),USE(?String17),TRN,CENTER,FONT('Arial',18,,FONT:bold,CHARSET:ANSI)
                         STRING(@s50),AT(3,3,73,4),USE(LOC:RazaoSocial),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s50),AT(3,8,74,4),USE(LOC:EnderecoBairro),TRN,LEFT,FONT(,7,,)
                         STRING(@s50),AT(3,13,74,4),USE(LOC:CepTelefone),TRN,LEFT,FONT(,7,,)
                         STRING(''),AT(82,13,124,5),USE(?String37:3),TRN
                         STRING('De:'),AT(236,13),USE(?String37),TRN
                         STRING(@d06),AT(241,13),USE(LOC:DataInicial),LEFT
                         STRING('At�:'),AT(258,13),USE(?String37:2),TRN
                         STRING(@d06b),AT(264,13),USE(LOC:DataFinal),LEFT
                         LINE,AT(2,23,277,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('N<186> Doc'),AT(91,19),USE(?String18:8),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Emiss�o'),AT(117,19),USE(?String18:10),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Vencimento'),AT(132,19),USE(?String18),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Pagamento'),AT(151,19),USE(?String18:3),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Previs�o'),AT(170,19),USE(?String18:6),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Valor'),AT(184,19,16,4),USE(?String19),TRN,CENTER,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Desconto'),AT(227,19,16,4),USE(?String19:2),TRN,CENTER,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Juros'),AT(214,19,12,4),USE(?String19:3),TRN,CENTER,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Multa'),AT(201,19,12,4),USE(?String19:4),TRN,CENTER,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('O. Acr�sc.'),AT(244,19,16,4),USE(?String19:5),TRN,CENTER,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Pago'),AT(262,19,14,4),USE(?String19:7),TRN,CENTER,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Clientes e Fornecedores'),AT(2,19),USE(?String19:9),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                       END
detail1                DETAIL,AT(,,,8),USE(?Detail1)
                         STRING(@s16),AT(88,0,28,4),USE(FDP:campo4),TRN,LEFT
                         STRING(@d06),AT(115,0,,4),USE(FDP:campo6),TRN,CENTER
                         STRING(@d06),AT(132,0,,4),USE(FDP:campo7),TRN,CENTER
                         STRING(@d06b),AT(151,0,,4),USE(FDP:campo9),TRN,CENTER
                         STRING(@d06b),AT(168,0,,4),USE(FDP:campo21),TRN,CENTER
                         STRING(@n10.`2),AT(184,0,,4),USE(FDP:campo8),TRN,CENTER
                         STRING(@n7.`2),AT(228,0,,4),USE(FDP:campo10),TRN,CENTER
                         STRING(@n7.`2),AT(214,0,,4),USE(FDP:campo11),TRN,CENTER
                         STRING(@n7.`2),AT(201,0,,4),USE(FDP:campo12),TRN,CENTER
                         STRING(@n7.`2),AT(245,0,,4),USE(FDP:campo13),TRN,CENTER
                         STRING(@n9.`2),AT(262,0,,4),USE(FDP:campo15),TRN,RIGHT
                         STRING('Descri��o:'),AT(2,3),USE(?String75),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         LINE,AT(2,7,277,0),USE(?Line3),COLOR(COLOR:Black)
                         STRING(@s50),AT(18,3,81,4),USE(FDP:campo5),TRN,LEFT
                         STRING(@s10),AT(126,3,16,4),USE(FDP:campo3),TRN,CENTER
                         STRING('-'),AT(143,3),USE(?String77),TRN
                         STRING(@s70),AT(145,3,118,5),USE(FDP:campo17),TRN
                         STRING('Plano de Contas:'),AT(101,3),USE(?String76),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@s48),AT(2,0,83,4),USE(FDP:campo20),TRN,LEFT
                       END
detail2                DETAIL,AT(,,,8),USE(?detail2)
                         STRING('Movimenta��es/Baixas'),AT(83,0,,3),USE(?String39),TRN,FONT(,,,FONT:bold)
                         STRING('Data Pagamento'),AT(113,4,,4),USE(?String19:6),TRN
                         STRING('Valor'),AT(140,4,,4),USE(?String19:14),TRN
                         STRING('Valor Pago'),AT(222,4,,4),USE(?String19:8),TRN
                         STRING('Multa'),AT(157,4,,4),USE(?String19:10),TRN
                         STRING('Juros'),AT(174,4,,4),USE(?String19:11),TRN
                         STRING('Desconto'),AT(190,4,,4),USE(?String19:12),TRN
                         STRING('O. Acr�sc'),AT(207,4,,4),USE(?String19:15),TRN
                         STRING('Tipo de Baixa'),AT(242,4,,4),USE(?String19:13),TRN
                       END
detail3                DETAIL,AT(,,,4),USE(?detail3)
                         STRING(@n10.2),AT(135,0,,4),USE(loc:valor),TRN,CENTER
                         STRING(@n10`2),AT(188,0,,4),USE(FMD:Campo5),TRN,CENTER
                         STRING(@n10`2),AT(205,0,,4),USE(FMD:Campo6),TRN,CENTER
                         STRING(''),AT(242,0,19,4),USE(?String54),TRN
                         STRING(@n10`2),AT(170,0,,4),USE(FMD:Campo4),TRN,CENTER
                         STRING(@d06b),AT(117,0,,4),USE(FMD:Campo1),TRN
                         STRING(@n10`2),AT(222,0,,4),USE(FMD:Campo2),TRN,CENTER
                         STRING(@n10`2),AT(153,0,,4),USE(FMD:Campo3),TRN,CENTER
                       END
detail4                DETAIL,AT(,,,4),USE(?detail4)
                         BOX,AT(2,0,277,4),USE(?Box5),COLOR(COLOR:Black),FILL(0E4E4E4H)
                         STRING(@n10`2),AT(184,0,,3),USE(loc:totalvalor),TRN,CENTER
                         STRING(@n8`2),AT(200,0,,3),USE(loc:totalmulta),TRN,CENTER
                         STRING(@n8`2),AT(213,0,,3),USE(loc:totaljuros),TRN,CENTER
                         STRING(@n8`2),AT(227,0,,3),USE(loc:totaldesconto),TRN,CENTER
                         STRING(@n8`2),AT(244,0,,3),USE(loc:totaloutrosacrescimos),TRN,CENTER
                         STRING('Sub-Total'),AT(83,0,,3),USE(?String39:2),TRN,FONT(,,,FONT:bold)
                         STRING(@n10`2),AT(260,0,,3),USE(loc:subtotal),TRN,CENTER
                       END
                       FOOTER,AT(3,190,282,13),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         BOX,AT(3,1,277,4),USE(?Box5:2),COLOR(COLOR:Black),FILL(0E4E4E4H)
                         STRING('Data:'),AT(50,5),USE(?ReportDatePrompt),TRN,FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING(@d06b),AT(58,5),USE(LOC:Data),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING('Hora:'),AT(85,5),USE(?ReportTimePrompt),TRN,FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING(@T01B),AT(94,5),USE(LOC:Hora),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING(@n7.`2),AT(215,1,,4),USE(LOC:vlr_juros),TRN,CENTER
                         STRING(@n7.`2),AT(228,1,,4),USE(LOC:vlr_desconto),TRN,CENTER
                         STRING(@n7.`2),AT(246,1,,4),USE(LOC:vlr_outros),TRN,CENTER
                         STRING(@n13`2),AT(257,1,,4),USE(LOC:vlr_pago),TRN,RIGHT
                         STRING('Usu�rio:'),AT(2,5),USE(?String24),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s20),AT(13,5),USE(GLO:nomeusuario),FONT('Arial',8,,,CHARSET:ANSI)
                         LINE,AT(2,-1,278,0),USE(?Line1:2),COLOR(COLOR:Black)
                         STRING('Total Acumulado'),AT(84,1,,3),USE(?String39:3),TRN,FONT(,,,FONT:bold)
                         STRING(@n13`2),AT(181,1,,4),USE(LOC:vlr_documento),TRN,CENTER
                         STRING(@n7.`2),AT(202,1,,4),USE(LOC:vlr_multa),TRN,CENTER
                         STRING(@pP�gina <.<<#p),AT(261,8,18,4),PAGENO,USE(?PageCount),RIGHT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Interfocus Tecnologia'),AT(253,5),USE(?String23),TRN,FONT('Times New Roman',8,,FONT:bold+FONT:italic,CHARSET:ANSI)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Reset                  PROCEDURE(BYTE Force=0),DERIVED     ! Method added to host embed code
SetAlerts              PROCEDURE(),DERIVED                 ! Method added to host embed code
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
Calendar2            CalendarClass
Calendar3            CalendarClass
RADSQLDrop5          CLASS(RADSQLDrop)
GenerateOrderbyClause  PROCEDURE(),DERIVED                 ! Method added to host embed code
GenerateSelectStatement PROCEDURE(),DERIVED                ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
MontaIN routine
  LOC:In = '('
  loop a# = 1 to records(qPlano)
    get(qPlano,a#)
    if QPL:check = 1
      LOC:In = LOC:In & QPL:codigo & ','
    end
  end
  tam# = len(LOC:in)
  LOC:In = LOC:In[1:(tam#-1)] & ')'

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('RelatDocumentosPagar')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  COMPILE('C55',_C55_)
    Self.Filesopened = 1
  C55
  OMIT('C55',_C55_)
    Filesopened = 1
  C55
  Relate:favorecidos.Open()
  Access:favorecidos.UseFile()
  Relate:consultasql2.Open                                 ! File consultasql2 used by this procedure, so make sure it's RelationManager is open
  Relate:documentos.SetOpenRelated()
  Relate:documentos.Open                                   ! File documentos used by this procedure, so make sure it's RelationManager is open
  Access:empresa.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ThisReport.Init(Process:View, Relate:empresa, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:empresa.SetQuickScan(1,Propagate:OneMany)
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom=True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Imprimir'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  RADSQLDrop5.Listqueue      &= Queue:RADSQLDrop
  RADSQLDrop5.Position       &= Queue:RADSQLDrop.ViewPosition
  RADSQLDrop5.Setlistcontrol(?FAV:razaosocial)
  RADSQLDrop5.RefreshHotkey = F5Key
  RADSQLDrop5.AddDropVariables(?FAV:razaosocial,0,FAV:razaosocial,'FAV:razaosocial',FAV:razaosocial,FAV:razaosocial)
  RADSQLDrop5.Init(Access:favorecidos)
  RADSQLDrop5.AddField(FAV:razaosocial,FAV:razaosocial)
     RADSQLDrop5.useansijoin=1
  SELF.SetAlerts()
  ?FAV:razaosocial{PROP:From} = Queue:RADSQLDrop
  RADSQLDrop5.AddTable('favorecidos',1,0,'FAVORECIDOS')  ! Add the table to the list of tables
  RADSQLDrop5.ColumnCharAsc   = ''
  RADSQLDrop5.ColumnCharDes   = ''
  RADSQLDrop5.AddFieldpairs(Queue:RADSQLDrop.FAV:id,FAV:id,'favorecidos','id | READONLY','N',0,'B','A','I','N',1,2)
  RADSQLDrop5.AddFieldpairs(Queue:RADSQLDrop.FAV:razaosocial,FAV:razaosocial,'favorecidos','razaosocial','A',0,'B','A','I','N',3,1)
  RADSQLDrop5.Managedview &=RADSQLDrop5:View
  RADSQLDrop5.Openview
  RADSQLDrop5.SetFileLoad
  RADSQLDrop5.SetForceFetch (1)
  RADSQLDrop5.SetColColor = 0
  RADSQLDrop5.Resetsort(1,1)
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  COMPILE('C55',_C55_)
  if Self.filesopened
  C55
  OMIT('C55',_C55_)
  If filesopened then
  C55
  Relate:favorecidos.Close
  END
    Relate:consultasql2.Close
    Relate:documentos.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
    ! PROCESSAMENTO DO RELAT�RIO
    !Limpa vari�veis de total acumulado
    clear(LOC:vlr_documento)
    clear(LOC:vlr_multa)
    clear(LOC:vlr_juros)
    clear(LOC:vlr_desconto)
    clear(LOC:vlr_outros)
    clear(LOC:vlr_pago)
  
    Open(fDocumentosPagar,42h)
    Open(fMoveDocs,42h)
  
    clear(LOC:filtro)
    clear(loc:view_where)
    !Tipo de Relat�rio
    if LOC:TipoRelatorio = 1 ! A pagar
      report$?String17{prop:text} = 'Documentos a Pagar'
      LOC:filtro = ' where b.tipolancamento = <39>D<39>'
    elsif LOC:TipoRelatorio = 2 ! A Receber
      report$?String17{prop:text} = 'Documentos a Receber'
      LOC:filtro = ' where b.tipolancamento = <39>C<39>'
    end
  
    !Tipo de Documentos (1-Pagos 2-Abertos 3-Todos)
    if LOC:TipoDocumento = 1
      LOC:filtro = LOC:filtro & ' and a.d_datapagamento is not null'
      loc:view_where = ' where a.d_datapagamento is not null'
    elsif LOC:TipoDocumento = 2
      LOC:filtro = LOC:filtro & ' and a.d_datapagamento is null'
      loc:view_where = ' where a.d_datapagamento is null'
    else
      loc:view_where = ' where'
    end
  
    if LOC:TipoData = 1
      LOC:filtro = LOC:Filtro & ' and a.d_datavencimento between <39>'&format(LOC:DataInicial,@d012b)&'<39> and <39>'&format(LOC:DataFinal,@d012b)&'<39>'
      loc:view_where = loc:view_where & ' a.d_datavencimento between <39>'&format(LOC:DataInicial,@d012b)&'<39> and <39>'&format(LOC:DataFinal,@d012b)&'<39>'
    elsif LOC:TipoData = 2
      LOC:filtro = LOC:Filtro & ' and a.d_dataprovavel between <39>'&format(LOC:DataInicial,@d012b)&'<39> and <39>'&format(LOC:DataFinal,@d012b)&'<39>'
      loc:view_where = loc:view_where & ' a.d_dataprovavel between <39>'&format(LOC:DataInicial,@d012b)&'<39> and <39>'&format(LOC:DataFinal,@d012b)&'<39>'
    elsif LOC:TipoData = 3
      LOC:filtro = LOC:Filtro & ' and a.d_dataemissao between <39>'&format(LOC:DataInicial,@d012b)&'<39> and <39>'&format(LOC:DataFinal,@d012b)&'<39>'
      loc:view_where = loc:view_where & ' a.d_dataemissao between <39>'&format(LOC:DataInicial,@d012b)&'<39> and <39>'&format(LOC:DataFinal,@d012b)&'<39>'
    else
      LOC:filtro = LOC:Filtro & ' and a.d_datapagamento between <39>'&format(LOC:DataInicial,@d012b)&'<39> and <39>'&format(LOC:DataFinal,@d012b)&'<39>'
      loc:view_where = loc:view_where & ' a.d_datapagamento between <39>'&format(LOC:DataInicial,@d012b)&'<39> and <39>'&format(LOC:DataFinal,@d012b)&'<39>'
    end
  
    if FAV:razaosocial
      LOC:filtro = LOC:filtro & ' and c.id = '&FAV:id
      !loc:view_where = loc:view_where  & ' and a.id = '&FAV:id
    end
  
    !Verifica se tem alguma conta marcada
    QPL:check = 1
    get(qPlano,QPL:check)
    if ~Error()
      LOC:filtro = LOC:filtro & ' and b.id in '&LOC:In
    end
  
    !Filtra pela empresa seleciona
    LOC:Filtro = LOC:Filtro & ' and a.idempresa = '&GLO:CodEmpresa
  
    if LOC:ordem = 2
      LOC:filtro = LOC:filtro & ' order by c.razaosocial'
      if loc:docsimanager
          loc:view_where = loc:view_where & ' order by c.razaosocial, a.d_datavencimento'
      end
      report$?String37:3{prop:text} = 'Ordem: Favorecido'
    elsif LOC:ordem = 3
      LOC:filtro = LOC:filtro &' order by a.d_datavencimento, c.razaosocial'
      if loc:docsimanager
          loc:view_where = loc:view_where & ' order by a.d_datavencimento, c.razaosocial'
      end
      report$?String37:3{prop:text} = 'Ordem: Vencimento'
    elsif LOC:ordem = 4
      LOC:filtro = LOC:filtro&' order by a.d_datapagamento, c.razaosocial'
      if loc:docsimanager
          loc:view_where = loc:view_where & ' order by a.d_datapagamento, c.razaosocial'
      end
      report$?String37:3{prop:text} = 'Ordem: Pagamento'
    elsif LOC:ordem = 5
      LOC:filtro = LOC:filtro&' order by b.descricaoplano, c.razaosocial'
      if loc:docsimanager
          loc:view_where = loc:view_where & ' order by b.descricaoplano, c.razaosocial'
      end
      report$?String37:3{prop:text} = 'Ordem: Plano de Contas'
    elsif LOC:ordem = 6
      LOC:filtro = LOC:filtro &' order by a.d_dataprovavel, c.razaosocial'
      if loc:docsimanager
          loc:view_where = loc:view_where  &' order by a.d_dataprovavel, c.razaosocial'
      end
      report$?String37:3{prop:text} = 'Ordem: Previs�o'
    end
  
    clear(fDocumentosPagar)
    fDocumentosPagar{Prop:Sql} = 'select a.id, a.idcliente, a.idplano, a.numdocumento, a.descricao, '&|
                                 '  a.d_dataemissao, a.d_datavencimento, a.valordocumento, a.d_datapagamento, '&|
                                 '  a.valordesconto, a.valorjuros, a.valormulta, a.valoroutrosacrescimos, '&|
                                 '  a.valorloutrosdescontos, a.valorpago, b.id, b.descricaoplano, b.tipolancamento, '&|
                                 '  c.id, c.razaosocial '&|
                                 'from documentos a '&|
                                 'join planocontas b on a.idplano = b.id '&|
                                 'join favorecidos c on a.idcliente = c.id'&LOC:filtro
  
    !message(fDocumentosPagar{Prop:Sql},,,,,2)
  
    next(fDocumentosPagar)
    if LOC:ordem = 2    ! Favorecido
       teste2" = FDP:campo20
    elsif LOC:ordem = 3 ! Vencimento
       teste2" = FDP:campo7
       Report$?FDP:campo7{prop:fontstyle} = font:bold
    elsif LOC:ordem = 4 ! Pagamento
       teste2" = FDP:campo9
       Report$?FDP:campo9{prop:fontstyle} = font:bold
    elsif LOC:ordem = 5 ! Plano de contas
       teste2" = FDP:campo3
       Report$?FDP:campo3{prop:fontstyle} = font:bold
    elsif LOC:ordem = 6
       teste2" = FDP:campo21
       Report$?FDP:campo21{prop:fontstyle} = font:bold
    end
  
    clear(fDocumentosPagar)
    Loc:queryString = 'select a.id, a.idcliente, a.idplano, a.numdocumento, a.descricao, '&|
                                 'a.d_dataemissao, a.d_datavencimento, a.valordocumento, a.d_datapagamento, '&|
                                 'a.valordesconto, a.valorjuros, a.valormulta, a.valoroutrosacrescimos, '&|
                                 'a.valorloutrosdescontos, a.valorpago, b.id, b.descricaoplano, b.tipolancamento, '&|
                                 'c.id, c.razaosocial, a.d_dataprovavel '&|
                                 'from documentos a '&|
                                 'join planocontas b on a.idplano = b.id '&|
                                 'join favorecidos c on a.idcliente = c.id'&LOC:filtro
    if loc:docsimanager
  
            Loc:queryString =    'select * from ( ' &|
                                 'select * from ( ' &|
                                 '(select a.id, a.idcliente, a.idplano, a.numdocumento, a.descricao, '&|
                                 'a.d_dataemissao, a.d_datavencimento, a.valordocumento, a.d_datapagamento, '&|
                                 'a.valordesconto, a.valorjuros, a.valormulta, a.valoroutrosacrescimos, '&|
                                 'a.valorloutrosdescontos, a.valorpago, b.id, b.descricaoplano, b.tipolancamento, '&|
                                 'c.id, c.razaosocial, a.d_dataprovavel '&|
                                 'from documentos a '&|
                                 'join planocontas b on a.idplano = b.id '&|
                                 'join favorecidos c on a.idcliente = c.id'&LOC:filtro&|
                                 ') union all ('&|
                                 'select ' &|
                                 'a.id, a.idcliente, a.idplanodocumento, a.numdocumento, a.descricao, '&|
                                 'a.d_dataemissao, a.d_datavencimento, a.valordocumento, a.d_datapagamento, '&|
                                 'a.valordesconto, a.valorjuros, a.valormulta, a.valoroutrosacrescimos, '&|
                                 'a.valorloutrosdescontos, a.valorpago, a.planoid, a.planodescricao, a.tipolancamento, ' &|
                                 'a.codigocliente, a.razaosocial, a.d_dataprovavel ' &|
                                 'from public.fun_carrega_view_docreceber_imanager a)) as a ' &|
                                 loc:view_where & ') as tudo'
  
    end
  
    fDocumentosPagar{Prop:Sql} = Loc:queryString
    !message(fDocumentosPagar{Prop:Sql},,,,,2)
  
    clear(loc:totalvalor)
    clear(loc:totalmulta)
    clear(loc:totaljuros)
    clear(loc:totaldesconto)
    clear(loc:totaloutrosacrescimos)
    clear(loc:subtotal)
    loop
      next(fDocumentosPagar)
      if error() then
          if loc:totalvalor > 0
                print(RPT:detail4)
          end
          break
      end
      ! Sub Total
      if LOC:ordem
  
          if LOC:ordem = 2    ! Favorecido
             teste1" = FDP:campo20
          elsif LOC:ordem = 3 ! Vencimento
             teste1" = FDP:campo7
          elsif LOC:ordem = 4 ! Pagamento
             teste1" = FDP:campo9
          elsif LOC:ordem = 5 ! Plano de Contas
             teste1" = FDP:campo3
          elsif LOC:ordem = 6 ! Previsao
             teste1" = FDP:campo21
          end
  
          if teste2" ~= teste1"
             teste2" = teste1"
             if loc:totalvalor > 0
                print(RPT:detail4)
                
                clear(loc:totalvalor)
                clear(loc:totalmulta)
                clear(loc:totaljuros)
                clear(loc:totaldesconto)
                clear(loc:totaloutrosacrescimos)
                clear(loc:subtotal)
  
             end
             Report$?FDP:campo20{prop:hide} = 0
          end
          
      end
  
      print(RPT:detail1)
  
      !Soma valores dos t�tulos
      LOC:vlr_documento += FDP:campo8
      LOC:vlr_multa += FDP:campo12
      LOC:vlr_juros += FDP:campo11
      LOC:vlr_desconto += FDP:campo10
      LOC:vlr_outros += FDP:campo13
      LOC:vlr_pago += FDP:campo15
  
      if LOC:ordem = 2    ! Favorecido
         Report$?FDP:campo20{prop:hide} = 1
      end
  
      if LOC:ListarBaixas
        ! Movimenta��o
        clear(fMoveDocs)
        fMoveDocs{Prop:Sql} = 'select mov.d_datamovimentacao, ' &|
                              'mov.valormovimento, mov.valormulta, mov.valorjuros, ' &|
                              'mov.valordesconto, mov.valoroutrosacrescimos, mov.tipobaixa ' &|
                              'from movdocumentos mov ' &|
                              'where mov.iddocumento = ' & FDP:Campo1
  
        !message(fMoveDocs{Prop:Sql},,,,,2)
  
        p# = 0
        loop
          next(fMoveDocs)
          if error() then break .
          if p# = 0
             print(RPT:detail2)
             p# = 1
          end
          Report$?String54{prop:text} = '1-Total'
          if FMD:Campo7 = 2
             Report$?String54{prop:text} = '2-Parcial'
          end
          loc:valor = FMD:Campo2 - (FMD:Campo3 + FMD:Campo4 + FMD:Campo6) + FMD:Campo5
          print(RPT:detail3)
        end
      end
  
      if teste2" = teste1"
        loc:totalvalor            += FDP:campo8
        loc:totalmulta            += FDP:campo12
        loc:totaljuros            += FDP:campo11
        loc:totaldesconto         += FDP:campo10
        loc:totaloutrosacrescimos += FDP:campo13
        loc:subtotal              += FDP:campo15
      end
  
    end
  
    !if loc:subtotal
    !   print(RPT:detail4)
    !end
  
    close(fDocumentosPagar)
    close(fMoveDocs)
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF ProgressWindow{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
  If force = 2
    RADSQLDrop5.ResetBrowse (1)
  end


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  RADSQLDrop5.SetAlerts


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Parar'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Imprimir'
      END
      SELF.Paused = 1 - SELF.Paused
      if ~LOC:DataInicial or ~LOC:DataFinal
        message('O Campos Data Inicial e Data Final s�o obrigat�rios! Por favor, corrija.','Aten��o',Icon:Exclamation,'OK',,2)
        select(?LOC:DataInicial)
        ?Pause{prop:text} = 'Imprimir'
      end
      do MontaIn
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?lstPlano
      marca# = 0
      desmarca# = 0
      filho# = 0
      pai# = 0
      
      if keycode() = mouseleft2
        get(qPlano,choice(?lstPlano))
        if QPL:check = 1
          QPL:check = 2
          desmarca# = 1
        else
          QPL:check = 1
          marca# = 1
        end
        filho# = QPL:codigo
        pai# = QPL:codigopai
        put(qPlano)
      
        !Marca os n�veis pais
        if marca# = 1
          loop
            if pai# = 0 then break .
      
            QPL:codigo = pai#
            get(qPlano,QPL:codigo)
            if error() then break .
      
            QPL:check = 1
            put(qPlano)
            pai# = QPL:codigopai
            !stop(pai#&' = '&QPL:codigopai)
          end
        end
      
        !Marca todos os filhos
        if marca# = 1 and filho# > 0
          loop a# = 1 to records(qPlano)
            get(qPlano,a#)
            if QPL:check = 2 and QPL:codigopai = filho#
              QPL:check = 1
              put(qPlano)
            end
          end
        end
      
        !Desmarca todos os filhos
        if desmarca# = 1 and filho# > 0
          loop a# = 1 to records(qPlano)
            get(qPlano,a#)
            if QPL:check = 1 and QPL:codigopai = filho#
              QPL:check = 2
              put(qPlano)
            end
          end
        end
      end
      
      !Marcar e Desmarcar Todos
      if keycode() = mouseright
        pop# = 0
        LOC:popup = '[' & PROP:Icon & '(check.ico)]Marcar Todos|[' & PROP:Icon & '(uncheck.ico)]Desmarcar Todos'
        execute popup(LOC:popup)
          begin
            pop# = 1
          end
          begin
            pop# = 2
          end
        end
        loop a# = 1 to records(qPlano)
          get(qPlano,a#)
          if QPL:check > 0
            QPL:check = pop#
            put(qPlano)
          end
        end
      end
      
      display
    OF ?Calendar
      ThisWindow.Update
      Calendar2.SelectOnClose = True
      Calendar2.Ask('Calend�rio',LOC:DataInicial)
      IF Calendar2.Response = RequestCompleted THEN
      LOC:DataInicial=Calendar2.SelectedDate
      DISPLAY(?LOC:DataInicial)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update
      Calendar3.SelectOnClose = True
      Calendar3.Ask('Calend�rio',LOC:DataFinal)
      IF Calendar3.Response = RequestCompleted THEN
      LOC:DataFinal=Calendar3.SelectedDate
      DISPLAY(?LOC:DataFinal)
      END
      ThisWindow.Reset(True)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
  RADSQLDrop5.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      !!!Seta as Vari�veis do Report
      Clear(LOC:RazaoSocial)
      Clear(LOC:EnderecoBairro)
      Clear(LOC:CepTelefone)
      Clear(LOC:Data)
      Clear(LOC:Hora)
      
      Clear(LOC:DataInicial)
      Clear(LOC:DataFinal)
      Clear(FAV:Record)
      
      Clear(CSQ1:Record)
      consultasql2{prop:sql} = |
              'select emp.razaosocial, emp.logradouro, emp.numero, emp.bairro, emp.cep, emp.telefone from empresa emp where emp.codempresa = '&GLO:CodEmpresa
      
      !message(consultasql2{prop:sql},,,,,2)
      
      Next(consultasql2)
      
      LOC:RazaoSocial    = CSQ1:campo1
      LOC:EnderecoBairro = CSQ1:campo2&', '&CSQ1:campo3&'   '&CSQ1:campo4
      LOC:CepTelefone    = CSQ1:campo5&'   '&CSQ1:campo6
      LOC:Data           = DataServidor()
      LOC:Hora           = HoraServidor()
      
      
      !Define os �cones
      
      !Carrega o Plano de Contas
      ?lstPlano{prop:iconlist,1} = 'check.ico'
      ?lstPlano{prop:iconlist,2} = 'uncheck.ico'
      ?lstPlano{prop:iconlist,3} = 'headecontas.ico'
      ?lstPlano{prop:iconlist,4} = 'debito.ico'
      ?lstPlano{prop:iconlist,5} = 'credito.ico'
      srcRetornaPlanoContas(qPlano)
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  IF 1 = 2
    PRINT(RPT:detail1)
  END
  IF 1=2
    PRINT(RPT:detail2)
  END
  IF 1=2
    PRINT(RPT:detail3)
  END
  IF 1=2
    PRINT(RPT:detail4)
  END
  RETURN ReturnValue


RADSQLDrop5.GenerateOrderbyClause PROCEDURE


  CODE
      Self.OrderByclause = 'ORDER BY A.RAZAOSOCIAL'
      Self.ViewSqlStatement = Clip(Self.ViewSqlStatement) &' '& Clip(Self.Orderbyclause)
      Return
  PARENT.GenerateOrderbyClause


RADSQLDrop5.GenerateSelectStatement PROCEDURE


  CODE
  Self.ViewSqlStatement = 'SELECT A.ID, A.RAZAOSOCIAL FROM FAVORECIDOS A'
  Return
  PARENT.GenerateSelectStatement

RelatDocumentosPagar_2 PROCEDURE (PAR:DataInicial,PAR:DataFinal,PAR:TipoRelatorio,PAR:TipoDocumento,<PAR:idfavorecido>,PAR:ordem,PAR:SubTotal) ! Generated from procedure template - Report

Progress:Thermometer BYTE                                  !
LOC:DataInicial      DATE                                  !
LOC:DataFinal        DATE                                  !
LOC:RazaoSocial      CSTRING(101)                          !
LOC:EnderecoBairro   CSTRING(101)                          !
LOC:CepTelefone      CSTRING(101)                          !
LOC:Data             DATE                                  !
LOC:Hora             STRING(20)                            !
LOC:filtro           CSTRING(10001)                        !
Loc:DocumentosListados LONG                                !
Loc:ValorDocumento   DECIMAL(12,2)                         !
Loc:ValorDesconto    DECIMAL(12,2)                         !
Loc:ValorJuros       DECIMAL(12,2)                         !
Loc:ValorMulta       DECIMAL(12,2)                         !
LOC:ValorOAcrescimos DECIMAL(15,2)                         !
LOC:ValorODescontos  DECIMAL(15,2)                         !
Loc:ValorLiquido     DECIMAL(12,2)                         !
LOC:TotalValor       DECIMAL(15,2)                         !
LOC:TotalDesconto    DECIMAL(15,2)                         !
LOC:TotalJuros       DECIMAL(15,2)                         !
LOC:TotalMulta       DECIMAL(15,2)                         !
LOC:TotalOAcrescimos DECIMAL(15,2)                         !
LOC:TotalODescontos  DECIMAL(15,2)                         !
LOC:TotalPago        DECIMAL(15,2)                         !
LOC:CodControle      LONG                                  !
LOC:DataControle     DATE                                  !
LOC:ControleSubTotal LONG                                  !
Process:View         VIEW(documentos)
                     END
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
fDocumentosPagar FILE,DRIVER('ODBC','/Turbosql=1'),OWNER(GLO:conexao),PRE(FDP),Name('consultasql')
Record      Record
campo1      long
campo2      long
campo3      long
campo4      cstring(21)
campo5      cstring(51)
campo6      date
campo7      date
campo8      decimal(10,2)
campo9      date
campo10     decimal(10,2)
campo11     decimal(10,2)
campo12     decimal(10,2)
campo13     decimal(10,2)
campo14     decimal(10,2)
campo15     decimal(10,2)
campo16     long
campo17     cstring(71)
campo18     cstring(2)
campo19     long
campo20     cstring(61)
            End
        End
ProgressWindow       WINDOW('i-Money'),AT(,,273,27),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),HIDE,AT(239,1,15,12),RANGE(0,100)
                       PROMPT('Processando, aguarde!'),AT(57,6),USE(?Prompt1),TRN,FONT(,16,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                       BOX,AT(2,2,269,24),USE(?Box1),ROUND,COLOR(COLOR:Navy),FILL(COLOR:White)
                       STRING(''),AT(239,1,15,12),USE(?Progress:UserString),HIDE,CENTER
                       STRING(''),AT(239,1,15,12),USE(?Progress:PctText),HIDE,CENTER
                       BUTTON('Cancel'),AT(239,1,15,12),USE(?Progress:Cancel),HIDE
                     END

Report               REPORT,AT(3,33,282,128),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',8,,,CHARSET:ANSI),LANDSCAPE,MM
                       HEADER,AT(3,6,282,27),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         BOX,AT(81,2,198,9),USE(?Box1:4),ROUND,COLOR(COLOR:Black),FILL(COLOR:Gray)
                         BOX,AT(3,2,76,16),USE(?Box1:2),ROUND,COLOR(COLOR:Black),FILL(COLOR:Gray)
                         BOX,AT(80,1,198,9),USE(?Box1:3),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(2,1,76,16),USE(?Box1),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         STRING('T�TULO'),AT(80,2,199,7),USE(?strTituloRelatorio),TRN,CENTER,FONT('Arial',18,,FONT:bold,CHARSET:ANSI)
                         STRING(@s46),AT(3,3,76,4),USE(LOC:RazaoSocial),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s50),AT(3,8,74,4),USE(LOC:EnderecoBairro),TRN,LEFT,FONT(,7,,)
                         STRING(@s50),AT(3,13,74,4),USE(LOC:CepTelefone),TRN,LEFT,FONT(,7,,)
                         STRING('De:'),AT(236,13),USE(?String37),TRN
                         STRING(@d06),AT(241,13),USE(LOC:DataInicial),LEFT
                         STRING('At�:'),AT(258,13),USE(?String37:2),TRN
                         STRING(@d06),AT(264,13),USE(LOC:DataFinal),LEFT
                         LINE,AT(2,26,277,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('N<186> Documento'),AT(2,22),USE(?String18:8),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Emiss�o'),AT(98,22),USE(?String18:10),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Vencimento'),AT(114,22),USE(?String18),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Vlr. Documento'),AT(140,22),USE(?String19),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Desconto'),AT(166,22),USE(?String19:2),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Juros'),AT(185,22),USE(?String19:3),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Multa'),AT(200,22),USE(?String19:4),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('O. Acr�scimos'),AT(212,22),USE(?String19:5),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('O. Descontos'),AT(236,22),USE(?String19:6),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Vlr. Pago'),AT(265,22),USE(?String19:7),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Descri��o'),AT(50,22),USE(?String18:2),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                       END
detail1                DETAIL,AT(,,,4),USE(?Detail1)
                         STRING(@s30),AT(2,0,47,5),USE(FDP:campo4),TRN,LEFT
                         STRING(@d06),AT(98,0),USE(FDP:campo6),TRN,LEFT
                         STRING(@d06),AT(114,0),USE(FDP:campo7),TRN,LEFT
                         STRING(@n8.`2),AT(150,0),USE(FDP:campo8),TRN,RIGHT
                         STRING(@n6.`2),AT(170,0),USE(FDP:campo10),TRN,RIGHT
                         STRING(@n6.`2),AT(184,0),USE(FDP:campo11),TRN,RIGHT
                         STRING(@n6.`2),AT(198,0),USE(FDP:campo12),TRN,RIGHT
                         STRING(@n6.`2),AT(223,0),USE(FDP:campo13),TRN,RIGHT
                         STRING(@n6.`2),AT(245,0),USE(FDP:campo14),TRN,RIGHT
                         STRING(@n8.`2),AT(265,0),USE(FDP:campo15),TRN,RIGHT
                         STRING(@s30),AT(50,0,47,5),USE(FDP:campo5),TRN,LEFT
                         LINE,AT(1,8,277,0),USE(?Line1:3),COLOR(0DADADAH)
                       END
detail2                DETAIL,AT(,,,4),USE(?Detail2)
                         STRING('Favorecido:'),AT(2,0),USE(?String19:8),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@s70),AT(19,0,110,5),USE(FDP:campo20),TRN,LEFT
                       END
detail3                DETAIL,AT(,,,5),USE(?Detail3)
                         LINE,AT(149,0,132,0),USE(?Line1:6),COLOR(COLOR:Black)
                         STRING(@n-8.2),AT(167,1),USE(LOC:TotalDesconto),RIGHT
                         STRING(@n-8.2),AT(181,1),USE(LOC:TotalJuros),RIGHT
                         STRING(@n-8.2),AT(195,1),USE(LOC:TotalMulta),RIGHT
                         STRING(@n-12.2),AT(215,1),USE(LOC:TotalOAcrescimos),RIGHT
                         STRING(@n-12.2),AT(237,1),USE(LOC:ValorODescontos),RIGHT
                         STRING(@n-12.2),AT(260,1),USE(LOC:TotalPago),RIGHT
                         LINE,AT(2,5,278,0),USE(?Line1:5),COLOR(COLOR:Black)
                         STRING(@n-21.2),AT(133,1),USE(LOC:TotalValor),RIGHT
                       END
                       FOOTER,AT(3,161,282,38),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         LINE,AT(2,0,278,0),USE(?Line1:4),COLOR(COLOR:Black)
                         STRING(@n-14b),AT(39,1),USE(Loc:DocumentosListados),LEFT(1)
                         STRING(@n-15.`2b),AT(141,2),USE(Loc:ValorDocumento),RIGHT
                         STRING(@n-12.`2b),AT(162,2),USE(Loc:ValorDesconto),RIGHT
                         STRING(@n-12.`2b),AT(176,2),USE(Loc:ValorJuros),RIGHT
                         STRING(@n-12.`2b),AT(190,2),USE(Loc:ValorMulta),RIGHT
                         STRING(@n-12.2),AT(215,2),USE(LOC:ValorOAcrescimos),RIGHT
                         STRING(@n-12.2),AT(237,2),USE(LOC:ValorODescontos,,?LOC:ValorODescontos:2),RIGHT
                         STRING(@n-15.`2b),AT(256,2),USE(Loc:ValorLiquido),RIGHT
                         STRING('N<186> de Documentos Listados:'),AT(2,1),USE(?String41),TRN
                         STRING('Data:'),AT(50,29),USE(?ReportDatePrompt),TRN,FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING(@d06b),AT(59,29),USE(LOC:Data),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING('Hora:'),AT(85,29),USE(?ReportTimePrompt),TRN,FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING(@T01B),AT(94,29),USE(LOC:Hora),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING('Usu�rio:'),AT(2,29),USE(?String24),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s20),AT(13,29),USE(GLO:nomeusuario),FONT('Arial',8,,,CHARSET:ANSI)
                         LINE,AT(2,29,278,0),USE(?Line1:2),COLOR(COLOR:Black)
                         STRING(@pP�gina <.<<#p),AT(261,34,18,4),PAGENO,USE(?PageCount),RIGHT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Interfocus Tecnologia'),AT(253,29),USE(?String23),TRN,FONT('Times New Roman',8,,FONT:bold+FONT:italic,CHARSET:ANSI)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Previewer            PrintPreviewClass                     ! Print Previewer
Calendar2            CalendarClass
Calendar3            CalendarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
CarregaDocumentos Routine
  Loc:DocumentosListados += 1

  !Totais
  Loc:ValorDocumento   += FDP:Campo8
  Loc:ValorDesconto    += FDP:Campo10
  Loc:ValorJuros       += FDP:Campo11
  Loc:ValorMulta       += FDP:Campo12
  LOC:ValorOAcrescimos += FDP:Campo13
  LOC:ValorODescontos  += FDP:Campo14
  Loc:ValorLiquido     += FDP:Campo15

  if LOC:ControleSubTotal = 0
    Loc:CodControle = FDP:campo1
    if PAR:ordem = 3 or PAR:ordem = 5
      loc:datacontrole = FDP:campo7
    elsif PAR:ordem = 4 or PAR:ordem = 6
      loc:datacontrole = FDP:campo9
    end
  end

  if PAR:SubTotal = 2
    if LOC:ControleSubTotal <> 0 and PAR:ordem = 2 !Favorecido
      if LOC:CodControle <> FDP:campo1
        print(RPT:detail3)
        clear(LOC:TotalValor)
        clear(LOC:TotalDesconto)
        clear(LOC:TotalJuros)
        clear(LOC:TotalMulta)
        clear(LOC:TotalOAcrescimos)
        clear(LOC:TotalODescontos)
        clear(LOC:TotalPago)
        LOC:CodControle = FDP:campo1
      end
    elsif LOC:ControleSubTotal <> 0 and PAR:ordem = 3 !Vencimento
      if LOC:DataControle <> FDP:campo7
        print(RPT:detail3)
        clear(LOC:TotalValor)
        clear(LOC:TotalDesconto)
        clear(LOC:TotalJuros)
        clear(LOC:TotalMulta)
        clear(LOC:TotalOAcrescimos)
        clear(LOC:TotalODescontos)
        clear(LOC:TotalPago)
        LOC:DataControle = FDP:campo7
      end
    elsif LOC:ControleSubTotal <> 0 and PAR:ordem = 4 !Pagamento
      if LOC:DataControle <> FDP:campo9
        print(RPT:detail3)
        clear(LOC:TotalValor)
        clear(LOC:TotalDesconto)
        clear(LOC:TotalJuros)
        clear(LOC:TotalMulta)
        clear(LOC:TotalOAcrescimos)
        clear(LOC:TotalODescontos)
        clear(LOC:TotalPago)
        LOC:DataControle = FDP:campo9
      end
    elsif LOC:ControleSubTotal <> 0 and PAR:ordem = 5 !Favorecido/Vencimento
      if LOC:DataControle <> FDP:campo7 or LOC:codcontrole <> FDP:campo1
        print(RPT:detail3)
        clear(LOC:TotalValor)
        clear(LOC:TotalDesconto)
        clear(LOC:TotalJuros)
        clear(LOC:TotalMulta)
        clear(LOC:TotalOAcrescimos)
        clear(LOC:TotalODescontos)
        clear(LOC:TotalPago)
        loc:datacontrole = FDP:campo7
        loc:codcontrole = FDP:campo1
      end
    elsif LOC:ControleSubTotal <> 0 and PAR:ordem = 6 !Favorecido/Pagamento
      if LOC:DataControle <> FDP:campo9 or loc:codcontrole <> FDP:campo1
        print(RPT:detail3)
        clear(LOC:TotalValor)
        clear(LOC:TotalDesconto)
        clear(LOC:TotalJuros)
        clear(LOC:TotalMulta)
        clear(LOC:TotalOAcrescimos)
        clear(LOC:TotalODescontos)
        clear(LOC:TotalPago)
        loc:datacontrole = FDP:campo9
        loc:codcontrole = FDP:campo1
      end
    end
  end

  !SubTotais
  LOC:TotalValor       += FDP:Campo8
  LOC:TotalDesconto    += FDP:Campo10
  LOC:TotalJuros       += FDP:Campo11
  LOC:TotalMulta       += FDP:Campo12
  LOC:TotalOAcrescimos += FDP:Campo13
  LOC:TotalODescontos  += FDP:Campo14
  LOC:TotalPago        += FDP:Campo15

  LOC:ControleSubTotal += 1

  print(RPT:detail1)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('RelatDocumentosPagar_2')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:consultasql2.Open                                 ! File consultasql2 used by this procedure, so make sure it's RelationManager is open
  Relate:documentos.SetOpenRelated()
  Relate:documentos.Open                                   ! File documentos used by this procedure, so make sure it's RelationManager is open
  Access:empresa.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:favorecidos.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ThisReport.Init(Process:View, Relate:documentos, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:documentos.SetQuickScan(1,Propagate:OneMany)
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom=True
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:consultasql2.Close
    Relate:documentos.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      !!!Seta as Vari�veis do Report
      Clear(LOC:DataInicial)
      Clear(LOC:DataFinal)
      Clear(LOC:RazaoSocial)
      Clear(LOC:EnderecoBairro)
      Clear(LOC:CepTelefone)
      Clear(LOC:Data)
      Clear(LOC:Hora)
      
      
      Clear(CSQ1:Record)
      consultasql2{prop:sql} = |
              'select emp.razaosocial, emp.logradouro, emp.numero, emp.bairro, emp.cep, emp.telefone from empresa emp where emp.codempresa = '&GLO:CodEmpresa
      
      !message(fExtrato{prop:sql},,,,,2)
      
      Next(consultasql2)
      
      LOC:RazaoSocial    = CSQ1:campo1
      LOC:EnderecoBairro = CSQ1:campo2&', '&CSQ1:campo3&'   '&CSQ1:campo4
      LOC:CepTelefone    = CSQ1:campo5&'   '&CSQ1:campo6
      LOC:Data           = DataServidor()
      LOC:Hora           = HoraServidor()
      
      Open(fDocumentosPagar,42h)
      !!!
      !!!
      !!!
        if PAR:TipoRelatorio = 1
          ?strTituloRelatorio{prop:text} = 'Relat�rio de Documentos a Pagar'
          LOC:filtro = '<39>D<39>'
        else
          ?strTituloRelatorio{prop:text} = 'Relat�rio de Documentos a Receber'
          LOC:filtro = '<39>C<39>'
        end
      
        if PAR:TipoDocumento = 1
          LOC:filtro = LOC:filtro&' and a.d_datapagamento between <39>'&format(PAR:DataInicial,@d012b)&'<39> and <39>'&format(PAR:DataFinal,@d012b)&'<39>'
        elsif PAR:TipoDocumento = 2
          LOC:filtro = LOC:filtro&' and a.d_datapagamento is null and a.d_datavencimento between <39>'&format(PAR:DataInicial,@d012b)&'<39> and <39>'&format(PAR:DataFinal,@d012b)&'<39>'
        elsif PAR:TipoDocumento = 3
          LOC:filtro = LOC:filtro&' and a.d_datavencimento between <39>'&format(PAR:DataInicial,@d012b)&'<39> and <39>'&format(PAR:DataFinal,@d012b)&'<39>'
        end
      
        if PAR:idfavorecido
          LOC:filtro = LOC:filtro&' and c.id = '&PAR:idfavorecido
        end
      
        if PAR:ordem = 2
          LOC:filtro = LOC:filtro&' order by c.razaosocial'
        elsif PAR:ordem = 3
          LOC:filtro = LOC:filtro&' order by a.d_datavencimento'
        elsif PAR:ordem = 4
          LOC:filtro = LOC:filtro&' order by a.d_datapagamento'
        elsif PAR:ordem = 5
          LOC:filtro = LOC:filtro&' order by c.razaosocial, a.d_datavencimento'
        elsif PAR:ordem = 6
          LOC:filtro = LOC:filtro&' order by c.razaosocial, a.d_pagamento'
        end
      
      !  stop(LOC:filtro)
      
        !Limpa var�aveis de controle de subtotal
        clear(LOC:CodControle)
        clear(LOC:DataControle)
        clear(LOC:ControleSubTotal)
      
        fDocumentosPagar{Prop:Sql} = 'select a.id, a.idcliente, a.idplano, a.numdocumento, a.descricao, '&|
                                     '  a.d_dataemissao, a.d_datavencimento, a.valordocumento, a.d_datapagamento, '&|
                                     '  a.valordesconto, a.valorjuros, a.valormulta, a.valoroutrosacrescimos, '&|
                                     '  a.valorloutrosdescontos, a.valorpago, b.id, b.descricaoplano, b.tipolancamento, '&|
                                     '  c.id, c.razaosocial '&|
                                     'from documentos a '&|
                                     'join planocontas b on a.idplano = b.id '&|
                                     'join favorecidos c on a.idcliente = c.id '&|
                                     'where b.tipolancamento = '&LOC:filtro
      
      !  message(fDocumentosPagar{Prop:Sql},,,,,2)
      
        loop
          next(fDocumentosPagar)
          if error() then break .
      
          if Clip(razaosocial") ~= Clip(FDP:campo20)
            razaosocial" = Clip(FDP:campo20)
      
            print(RPT:detail2)
      
            Do CarregaDocumentos
          else
            Do CarregaDocumentos
          end
        end
      !!!
      !!!
      !!!
      Close(fDocumentosPagar)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  IF 1 = 2
    PRINT(RPT:detail1)
  END
  IF 1 = 2
    PRINT(RPT:detail2)
  END
  IF 1 = 2
    PRINT(RPT:detail3)
  END
  RETURN ReturnValue

WindowRelatDocumentosPagar_2 PROCEDURE                     ! Generated from procedure template - Window

LOC:DataInicial      DATE                                  !
LOC:DataFinal        DATE                                  !
LOC:TipoRelatorio    BYTE(1)                               !
LOC:TipoDocumento    BYTE(3)                               !
LOC:filtro           CSTRING(10001)                        !
LOC:ordem            BYTE(1)                               !
LOC:SubTotal         BYTE(1)                               !
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
RADSQLDrop1Locator CString(256)
RADSQLDrop1:View     VIEW(favorecidos)
                       PROJECT(FAV:razaosocial)
                       PROJECT(FAV:id)
                     END
Queue:RADSQLDrop     QUEUE                            !Queue declaration for browse/combo box using ?FAV:razaosocial
FAV:razaosocial        LIKE(FAV:razaosocial)          !List box control field - type derived from field
FAV:id                 LIKE(FAV:id)                   !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Window               WINDOW('i-Money'),AT(,,249,256),FONT('MS Sans Serif',8,,FONT:regular),CENTER,IMM,SYSTEM,GRAY,DOUBLE,AUTO
                       IMAGE('window.ico'),AT(3,2),USE(?Image1)
                       PROMPT('Relat�rio de Documentos'),AT(29,5),USE(?Prompt8),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(3,22,245,2),USE(?Panel1)
                       OPTION('Tipo de Documentos'),AT(3,31,243,30),USE(LOC:TipoRelatorio),BOXED
                         RADIO(' A Pagar'),AT(21,44),USE(?LOC:TipoRelatorio:Radio4),VALUE('1')
                         RADIO(' A Receber'),AT(103,44),USE(?LOC:TipoRelatorio:Radio5),VALUE('2')
                       END
                       OPTION('Listar Documentos'),AT(3,64,243,30),USE(LOC:TipoDocumento),BOXED
                         RADIO(' Pagos'),AT(21,77),USE(?Option1:Radio1),TRN,VALUE('1')
                         RADIO(' Em Aberto'),AT(103,77),USE(?LOC:TipoDocumento:Radio2),TRN,VALUE('2')
                         RADIO(' Todos'),AT(197,77),USE(?LOC:TipoDocumento:Radio3),TRN,VALUE('3')
                       END
                       GROUP('Per�odo'),AT(3,97,243,30),USE(?Group1),BOXED
                         PROMPT('Data Inicial:'),AT(9,110),USE(?LOC:DataInicial:Prompt)
                         ENTRY(@d06b),AT(49,110,60,10),USE(LOC:DataInicial),FLAT
                         BUTTON,AT(111,109,12,12),USE(?Calendar),FLAT,ICON('Calendario.ico')
                         PROMPT('Data Final:'),AT(129,110),USE(?LOC:DataFinal:Prompt)
                         ENTRY(@d06b),AT(166,110,60,10),USE(LOC:DataFinal),FLAT
                         BUTTON,AT(228,109,12,12),USE(?Calendar:2),FLAT,ICON('Calendario.ico')
                       END
                       GROUP('Favorecido'),AT(3,131,243,30),USE(?Group2),BOXED
                         COMBO(@s255),AT(9,144,231,10),USE(FAV:razaosocial),IMM,FLAT,VSCROLL,FORMAT('240L(2)@s60@'),DROP(10),FROM(Queue:RADSQLDrop),MSG('Raz�o social do cliente/fornecedor')
                       END
                       GROUP('Ordenar por'),AT(3,164,243,30),USE(?Group3),BOXED
                         LIST,AT(9,178,231,10),USE(LOC:ordem),FLAT,DROP(5),FROM('|#1|Favorecidos|#2|Vencimento|#3|Pagamento|#4')
                       END
                       GROUP('Ordenar por'),AT(3,198,243,30),USE(?Group4),BOXED
                         LIST,AT(9,211,231,10),USE(LOC:SubTotal),FLAT,DROP(5),FROM('Sem Sub-Total|#1|Com Sub-Total|#2')
                       END
                       PANEL,AT(2,231,245,2),USE(?Panel1:2)
                       BUTTON('&Imprimir'),AT(132,238,55,15),USE(?btnImprimir),FLAT,LEFT,ICON('imprimir.ico')
                       BUTTON('&Cancelar'),AT(190,238,55,15),USE(?btnCancelar),FLAT,LEFT,ICON('cancelar.ico'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Reset                  PROCEDURE(BYTE Force=0),DERIVED     ! Method added to host embed code
SetAlerts              PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
RADSQLDrop1          CLASS(RADSQLDrop)
GenerateOrderbyClause  PROCEDURE(),DERIVED                 ! Method added to host embed code
GenerateSelectStatement PROCEDURE(),DERIVED                ! Method added to host embed code
UpdateWindow           PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END

Calendar2            CalendarClass
Calendar3            CalendarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('WindowRelatDocumentosPagar_2')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  COMPILE('C55',_C55_)
    Self.Filesopened = 1
  C55
  OMIT('C55',_C55_)
    Filesopened = 1
  C55
  Relate:favorecidos.Open()
  Access:favorecidos.UseFile()
  Relate:favorecidos.Open                                  ! File favorecidos used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  RADSQLDrop1.Listqueue      &= Queue:RADSQLDrop
  RADSQLDrop1.Position       &= Queue:RADSQLDrop.ViewPosition
  RADSQLDrop1.Setlistcontrol(?FAV:razaosocial)
  RADSQLDrop1.RefreshHotkey = F5Key
  RADSQLDrop1.AddDropVariables(?FAV:razaosocial,0,FAV:razaosocial,'FAV:razaosocial',FAV:razaosocial,FAV:razaosocial)
  RADSQLDrop1.Init(Access:favorecidos)
  RADSQLDrop1.AddField(FAV:razaosocial,FAV:razaosocial)
     RADSQLDrop1.useansijoin=1
  SELF.SetAlerts()
  ?FAV:razaosocial{PROP:From} = Queue:RADSQLDrop
  RADSQLDrop1.AddTable('favorecidos',1,0,'FAVORECIDOS')  ! Add the table to the list of tables
  RADSQLDrop1.ColumnCharAsc   = ''
  RADSQLDrop1.ColumnCharDes   = ''
  RADSQLDrop1.AddFieldpairs(Queue:RADSQLDrop.FAV:id,FAV:id,'favorecidos','id | READONLY','N',0,'B','A','I','N',1,2)
  RADSQLDrop1.AddFieldpairs(Queue:RADSQLDrop.FAV:razaosocial,FAV:razaosocial,'favorecidos','razaosocial','A',0,'B','A','I','N',3,1)
  RADSQLDrop1.Managedview &=RADSQLDrop1:View
  RADSQLDrop1.Openview
  RADSQLDrop1.SetFileLoad
  RADSQLDrop1.SetForceFetch (1)
  RADSQLDrop1.SetColColor = 0
  RADSQLDrop1.Resetsort(1,1)
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  COMPILE('C55',_C55_)
  if Self.filesopened
  C55
  OMIT('C55',_C55_)
  If filesopened then
  C55
  Relate:favorecidos.Close
  END
    Relate:favorecidos.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF Window{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
  If force = 2
    RADSQLDrop1.ResetBrowse (1)
  end


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  RADSQLDrop1.SetAlerts


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Calendar
      ThisWindow.Update
      Calendar2.SelectOnClose = True
      Calendar2.Ask('Selecione a Data Inicial',LOC:DataInicial)
      IF Calendar2.Response = RequestCompleted THEN
      LOC:DataInicial=Calendar2.SelectedDate
      DISPLAY(?LOC:DataInicial)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update
      Calendar3.SelectOnClose = True
      Calendar3.Ask('Selecione a Data Final',LOC:DataFinal)
      IF Calendar3.Response = RequestCompleted THEN
      LOC:DataFinal=Calendar3.SelectedDate
      DISPLAY(?LOC:DataFinal)
      END
      ThisWindow.Reset(True)
    OF ?btnImprimir
      ThisWindow.Update
      if ~LOC:DataInicial or ~LOC:DataFinal
        message('O Campos Data Inicial e Data Final s�o obrigat�rios! Por favor, corrija-os.','Aten��o',Icon:Exclamation,'OK',,2)
        select(?LOC:DataInicial)
        cycle
      else
        RelatDocumentosPagar_2(LOC:DataInicial,LOC:DataFinal,LOC:TipoRelatorio,LOC:TipoDocumento,FAV:id,LOC:ordem,LOC:SubTotal)
      end
      
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
  RADSQLDrop1.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


RADSQLDrop1.GenerateOrderbyClause PROCEDURE


  CODE
      Self.OrderByclause = 'ORDER BY A.RAZAOSOCIAL'
      Self.ViewSqlStatement = Clip(Self.ViewSqlStatement) &' '& Clip(Self.Orderbyclause)
      Return
  PARENT.GenerateOrderbyClause


RADSQLDrop1.GenerateSelectStatement PROCEDURE


  CODE
  Self.ViewSqlStatement = 'SELECT A.ID, A.RAZAOSOCIAL FROM FAVORECIDOS A'
  Return
  PARENT.GenerateSelectStatement


RADSQLDrop1.UpdateWindow PROCEDURE


  CODE
  PARENT.UpdateWindow
  Clear(FAV:Record)

