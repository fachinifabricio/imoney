

   MEMBER('imoney.clw')                                    ! This is a MEMBER module


   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('IMONE014.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('IMONE004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE012.INC'),ONCE        !Req'd for module callout resolution
                     END


Relat_FluxoCaixa PROCEDURE                                 ! Generated from procedure template - Report

Progress:Thermometer BYTE                                  !
Loc:valorCredito     DECIMAL(11,2)                         !
Loc:valorDebito      DECIMAL(11,2)                         !
loc:SaldoR           DECIMAL(11,2)                         !
loc:mesanterior      BYTE                                  !
loc:stringQuery      CSTRING(5000)                         !
LOC:DataInicial      DATE                                  !
LOC:DataFinal        DATE                                  !
LOC:RazaoSocial      CSTRING(101)                          !
LOC:EnderecoBairro   CSTRING(101)                          !
LOC:CepTelefone      CSTRING(101)                          !
LOC:Data             DATE                                  !
LOC:Hora             STRING(20)                            !
loc:valor            DECIMAL(10,2)                         !
loc:receitas         DECIMAL(9,2),DIM(12)                  !
loc:totalreceita     DECIMAL(11,2)                         !
loc:despesas         DECIMAL(9,2),DIM(12)                  !
loc:totaldespesa     DECIMAL(11,2)                         !
loc:totalreceitames  DECIMAL(11,2),DIM(12)                 !
loc:saldomesanterior DECIMAL(11,2),DIM(12)                 !
loc:totaldespesames  DECIMAL(11,2),DIM(12)                 !
loc:totalgeralmes    DECIMAL(11,2),DIM(12)                 !
loc:mesescabecalho   STRING(9),DIM(12)                     !
loc:dtgeracao        BYTE                                  !
loc:docsImanager     BYTE                                  !
Process:View         VIEW(documentos)
                     END
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
fDocumentos FILE,DRIVER('ODBC','/Turbosql=1'),OWNER(GLO:conexao),PRE(DOC),Name('consultasql')
Record      Record
campo1      cstring(25)   ! C�digo evento
campo2      cstring(10)   ! Tipo, Cr�dito, D�bito
campo3      decimal(10,2) ! M�s 1
campo4      decimal(10,2) ! M�s 2
campo5      decimal(10,2) ! M�s 3
campo6      decimal(10,2) ! M�s 4
campo7      decimal(10,2) ! M�s 5
campo8      decimal(10,2) ! M�s 6
campo9      decimal(10,2) ! M�s 7
campo10     decimal(10,2) ! M�s 8
campo11     decimal(10,2) ! M�s 9
campo12     decimal(10,2) ! M�s 10
campo13     decimal(10,2) ! M�s 11
campo14     decimal(10,2) ! M�s 12
campo15     cstring(25) ! Descicao
            End
        End
ProgressWindow       WINDOW('i-Money'),AT(,,197,134),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,TIMER(1),GRAY,DOUBLE
                       IMAGE('window.ico'),AT(3,2),USE(?Image1)
                       PROMPT('Fluxo de Caixa'),AT(29,5),USE(?Prompt8),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,23,190,2),USE(?Panel1)
                       OPTION('Par�metro para gera��o'),AT(5,26,186,30),USE(loc:dtgeracao),BOXED
                         RADIO(' Data de pagamento'),AT(13,42),USE(?loc:dtgeracao:Radio1),HLP('O que foi pago at� o M�s de refer�ncia???'),MSG('O que foi pago at� o M�s de refer�ncia???'),TIP('O que foi pago at� o M�s de refer�ncia???'),VALUE('1')
                         RADIO(' Data de vencimento'),AT(103,42),USE(?loc:dtgeracao:Radio2),HLP('O que vai vencer at� o M�s de refer�ncia??'),MSG('O que vai vencer at� o M�s de refer�ncia??'),TIP('O que vai vencer at� o M�s de refer�ncia??'),VALUE('2')
                       END
                       GROUP('M�s de refer�ncia'),AT(5,58,186,30),USE(?Group1),BOXED
                         ENTRY(@D014B),AT(67,71,60,10),USE(LOC:DataFinal),FLAT,CENTER
                       END
                       CHECK('Relacionar Documentos do i-MANAGER'),AT(6,91),USE(loc:docsImanager),FONT(,,,FONT:bold),VALUE('1','0')
                       CHECK('Simular saldo do m�s anterior'),AT(6,101),USE(loc:mesanterior)
                       PANEL,AT(3,111,190,2),USE(?Panel1:2)
                       PROGRESS,USE(Progress:Thermometer),HIDE,AT(4,119,63,12),RANGE(0,100)
                       STRING(''),AT(39,119,15,12),USE(?Progress:UserString),HIDE,CENTER
                       STRING(''),AT(71,123,15,12),USE(?Progress:PctText),HIDE,CENTER
                       BUTTON('&Imprimir'),AT(77,116,55,15),USE(?Pause),FLAT,LEFT,ICON('imprimir.ico')
                       BUTTON('&Cancelar'),AT(135,116,55,15),USE(?Progress:Cancel),FLAT,LEFT,ICON('cancelar.ico')
                     END

Report               REPORT,AT(167,1583,11283,6008),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',8,,,CHARSET:ANSI),LANDSCAPE,THOUS
                       HEADER,AT(125,240,11283,1300),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         BOX,AT(3240,83,7917,365),USE(?Box1:4),ROUND,COLOR(COLOR:Black),FILL(COLOR:Gray)
                         BOX,AT(125,83,3042,635),USE(?Box1:2),ROUND,COLOR(COLOR:Black),FILL(COLOR:Gray)
                         BOX,AT(3198,42,7917,365),USE(?Box1:3),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(94,42,3042,635),USE(?Box1),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         STRING('Fluxo de Caixa'),AT(3198,83,7958,281),USE(?String17),TRN,CENTER,FONT('Arial',18,,FONT:bold,CHARSET:ANSI)
                         STRING(@s46),AT(156,125,3042,156),USE(LOC:RazaoSocial),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s50),AT(156,323,2958,156),USE(LOC:EnderecoBairro),TRN,LEFT,FONT(,7,,)
                         STRING(@s50),AT(156,521,2958,156),USE(LOC:CepTelefone),TRN,LEFT,FONT(,7,,)
                         STRING('De:'),AT(9438,521),USE(?String37),TRN
                         STRING(@d06),AT(9635,521),USE(LOC:DataInicial),LEFT
                         STRING('At�:'),AT(10323,521),USE(?String37:2),TRN
                         STRING(@d06b),AT(10563,521),USE(LOC:DataFinal),LEFT
                         STRING('Descri��o'),AT(125,875,1719,156),USE(?String18),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@s9),AT(1635,875,719,156),USE(loc:mesescabecalho[1]),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@s9),AT(2385,875,719,156),USE(loc:mesescabecalho[2]),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@s9),AT(3115,875,719,156),USE(loc:mesescabecalho[3]),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@s9),AT(3854,875,719,156),USE(loc:mesescabecalho[4]),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@s9),AT(4583,875,719,156),USE(loc:mesescabecalho[5]),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@s9),AT(5313,875,719,156),USE(loc:mesescabecalho[6]),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@s9),AT(6052,875,719,156),USE(loc:mesescabecalho[7]),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@s9),AT(6813,875,719,156),USE(loc:mesescabecalho[8]),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@s9),AT(7531,875,719,156),USE(loc:mesescabecalho[9]),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@s9),AT(8271,875,719,156),USE(loc:mesescabecalho[10]),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@s9),AT(9010,875,719,156),USE(loc:mesescabecalho[11]),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@s9),AT(9771,875,719,156),USE(loc:mesescabecalho[12]),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Total'),AT(10531,875,635,156),USE(?String18:14),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                         LINE,AT(94,1042,11083,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Receitas'),AT(125,1083,1719,156),USE(?String18:15),TRN,LEFT,FONT(,10,,FONT:bold,CHARSET:ANSI)
                         LINE,AT(94,1271,11083,0),USE(?Line1:3),COLOR(COLOR:Black),LINEWIDTH(2)
                       END
detail1                DETAIL,AT(,,,192),USE(?Detail1)
                         STRING(@n-13`2),AT(6010,21,719,156),USE(loc:receitas[7]),TRN,RIGHT(1)
                         STRING(@n-13`2),AT(8969,21,719,156),USE(loc:receitas[11]),TRN,RIGHT(1)
                         STRING(@n-13`2),AT(6750,21,719,156),USE(loc:receitas[8]),TRN,RIGHT(1)
                         STRING(@n-13`2),AT(9729,21,719,156),USE(loc:receitas[12]),TRN,RIGHT(1)
                         STRING(@n-14`2),AT(10500,21,719,156),USE(loc:totalreceita),TRN,RIGHT(1),FONT(,,,FONT:bold)
                         STRING(@n-13`2),AT(2344,21,719,156),USE(loc:receitas[2]),TRN,RIGHT(1)
                         STRING('String 36'),AT(94,21,1719,156),USE(?String36),TRN
                         STRING(@n-13`2),AT(3073,21,719,156),USE(loc:receitas[3]),TRN,RIGHT(1)
                         STRING(@n-13`2),AT(3813,21,719,156),USE(loc:receitas[4]),TRN,RIGHT(1)
                         STRING(@n-13`2),AT(4542,21,719,156),USE(loc:receitas[5]),TRN,RIGHT(1)
                         STRING(@n-13`2),AT(7490,21,719,156),USE(loc:receitas[9]),TRN,RIGHT(1)
                         STRING(@n-13`2),AT(8229,21,719,156),USE(loc:receitas[10]),TRN,RIGHT(1)
                         STRING(@n-13`2),AT(5271,21,719,156),USE(loc:receitas[6]),TRN,RIGHT(1)
                         STRING(@n-13`2),AT(1604,21,719,156),USE(loc:receitas[1]),RIGHT(1)
                       END
detail2                DETAIL,AT(,,,500),USE(?Detail2)
                         STRING(@n12`2),AT(1604,21,719,156),USE(loc:totalreceitames[1]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n12`2),AT(2344,21,719,156),USE(loc:totalreceitames[2]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n12`2),AT(3073,21,719,156),USE(loc:totalreceitames[3]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n12`2),AT(3813,21,719,156),USE(loc:totalreceitames[4]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n12`2),AT(4542,21,719,156),USE(loc:totalreceitames[5]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n12`2),AT(5271,21,719,156),USE(loc:totalreceitames[6]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n12`2),AT(6010,21,719,156),USE(loc:totalreceitames[7]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n12`2),AT(6750,21,719,156),USE(loc:totalreceitames[8]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n12`2),AT(7490,21,719,156),USE(loc:totalreceitames[9]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n12`2),AT(8229,21,719,156),USE(loc:totalreceitames[10]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n12`2),AT(8969,21,719,156),USE(loc:totalreceitames[11]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n12`2),AT(9729,21,719,156),USE(loc:totalreceitames[12]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Total de Receitas'),AT(83,21,1719,156),USE(?String18:16),TRN,LEFT,FONT(,10,,FONT:bold,CHARSET:ANSI)
                         STRING('Despesas'),AT(83,281,1719,156),USE(?String18:17),TRN,LEFT,FONT(,10,,FONT:bold,CHARSET:ANSI)
                         LINE,AT(94,469,11083,0),USE(?Line1:4),COLOR(COLOR:Black),LINEWIDTH(2)
                       END
detail3                DETAIL,AT(,,,192),USE(?Detail3)
                         STRING('String 36'),AT(83,10,1719,156),USE(?String36:2),TRN
                         STRING(@n-13`2),AT(1604,10,719,156),USE(loc:despesas[1]),TRN,RIGHT(1)
                         STRING(@n-13`2),AT(2344,0,719,156),USE(loc:despesas[2]),TRN,RIGHT(1)
                         STRING(@n-13`2),AT(3073,0,719,156),USE(loc:despesas[3]),TRN,RIGHT(1)
                         STRING(@n-13`2),AT(3813,0,719,156),USE(loc:despesas[4]),TRN,RIGHT(1)
                         STRING(@n-13`2),AT(4542,0,719,156),USE(loc:despesas[5]),TRN,RIGHT(1)
                         STRING(@n-13`2),AT(5271,0,719,156),USE(loc:despesas[6]),TRN,RIGHT(1)
                         STRING(@n-13`2),AT(6010,10,719,156),USE(loc:despesas[7]),TRN,RIGHT(1)
                         STRING(@n-13`2),AT(6750,0,719,156),USE(loc:despesas[8]),TRN,RIGHT(1)
                         STRING(@n-13`2),AT(7490,0,719,156),USE(loc:despesas[9]),TRN,RIGHT(1)
                         STRING(@n-13`2),AT(8229,0,719,156),USE(loc:despesas[10]),TRN,RIGHT(1)
                         STRING(@n-13`2),AT(8969,0,719,156),USE(loc:despesas[11]),TRN,RIGHT(1)
                         STRING(@n-13`2),AT(9729,0,719,156),USE(loc:despesas[12]),TRN,RIGHT(1)
                         STRING(@n-14`2),AT(10490,0,719,156),USE(loc:totaldespesa),TRN,RIGHT(1),FONT(,,,FONT:bold)
                       END
detail4                DETAIL,AT(,,,758),USE(?Detail4)
                         LINE,AT(94,21,11083,0),USE(?Line1:5),COLOR(COLOR:Black)
                         STRING(@n-11`2),AT(1604,42,719,156),USE(loc:totaldespesames[1]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-11`2),AT(2344,42,719,156),USE(loc:totaldespesames[2]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-11`2),AT(3073,42,719,156),USE(loc:totaldespesames[3]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-11`2),AT(3813,42,719,156),USE(loc:totaldespesames[4]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-11`2),AT(4542,42,719,156),USE(loc:totaldespesames[5]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-11`2),AT(5271,42,719,156),USE(loc:totaldespesames[6]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-11`2),AT(6010,42,719,156),USE(loc:totaldespesames[7]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-11`2),AT(6750,42,719,156),USE(loc:totaldespesames[8]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-11`2),AT(7490,42,719,156),USE(loc:totaldespesames[9]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-11`2),AT(8229,42,719,156),USE(loc:totaldespesames[10]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-11`2),AT(8969,42,719,156),USE(loc:totaldespesames[11]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-11`2),AT(9729,42,719,156),USE(loc:totaldespesames[12]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Total de Despesas'),AT(73,42,1719,156),USE(?String18:19),TRN,LEFT,FONT(,10,,FONT:bold,CHARSET:ANSI)
                         LINE,AT(94,260,11083,0),USE(?Line1:6),COLOR(COLOR:Black)
                         GROUP,AT(10750,333,500,400),USE(?Group1),HIDE
                           STRING(@n-15.2),AT(1600,308,719,156),USE(loc:saldomesanterior[1]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-15.2),AT(2342,308,719,156),USE(loc:saldomesanterior[2]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-15.2),AT(3075,308,719,156),USE(loc:saldomesanterior[3]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-15.2),AT(3817,308,719,156),USE(loc:saldomesanterior[4]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-15.2),AT(4542,308,719,156),USE(loc:saldomesanterior[5]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-15.2),AT(5275,308,719,156),USE(loc:saldomesanterior[6]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-15.2),AT(6008,308,719,156),USE(loc:saldomesanterior[7]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-15.2),AT(6750,308,719,156),USE(loc:saldomesanterior[8]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-15.2),AT(7492,308,719,156),USE(loc:saldomesanterior[9]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-15.2),AT(8225,308,719,156),USE(loc:saldomesanterior[10]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-15.2),AT(8967,308,719,156),USE(loc:saldomesanterior[11]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-15.2),AT(9725,308,719,156),USE(loc:saldomesanterior[12]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING('Saldo M�s anterior'),AT(75,317,1719,156),USE(?String18:2),TRN,LEFT,FONT(,10,,FONT:bold,CHARSET:ANSI)
                         END
                         STRING(@n-13`2),AT(1592,542,719,156),USE(loc:totalgeralmes[1]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-13`2),AT(2342,542,719,156),USE(loc:totalgeralmes[2]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-13`2),AT(3075,542,719,156),USE(loc:totalgeralmes[3]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-13`2),AT(3817,542,719,156),USE(loc:totalgeralmes[4]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-13`2),AT(4542,542,719,156),USE(loc:totalgeralmes[5]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-13`2),AT(5275,542,719,156),USE(loc:totalgeralmes[6]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-13`2),AT(6008,542,719,156),USE(loc:totalgeralmes[7]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-13`2),AT(6750,542,719,156),USE(loc:totalgeralmes[8]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-13`2),AT(7492,542,719,156),USE(loc:totalgeralmes[9]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-13`2),AT(8225,542,719,156),USE(loc:totalgeralmes[10]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-13`2),AT(8967,542,719,156),USE(loc:totalgeralmes[11]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING(@n-13`2),AT(9725,542,719,156),USE(loc:totalgeralmes[12]),TRN,RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Total Geral'),AT(75,550,1719,156),USE(?String18:18),TRN,LEFT,FONT(,10,,FONT:bold,CHARSET:ANSI)
                       END
                       FOOTER,AT(125,7604,11283,367),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Data:'),AT(2000,0),USE(?ReportDatePrompt),TRN,FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING(@d06b),AT(2323,0),USE(LOC:Data),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING('Hora:'),AT(3396,0),USE(?ReportTimePrompt),TRN,FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING(@T01B),AT(3760,0),USE(LOC:Hora),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING('Usu�rio:'),AT(83,0),USE(?String24),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s20),AT(521,0),USE(GLO:nomeusuario),FONT('Arial',8,,,CHARSET:ANSI)
                         LINE,AT(83,-42,11125,0),USE(?Line1:2),COLOR(COLOR:Black)
                         STRING(@pP�gina <.<<#p),AT(10438,198,719,156),PAGENO,USE(?PageCount),RIGHT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Interfocus Tecnologia'),AT(10125,0),USE(?String23),TRN,FONT('Times New Roman',8,,FONT:bold+FONT:italic,CHARSET:ANSI)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Previewer            PrintPreviewClass                     ! Print Previewer

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Busca_Saldo_Mes_Anterior routine

      DtMesAnterior_Final#   = LOC:DataInicial - 1
      DtMesAnterior_Inicial# = date(month(DtMesAnterior_Final#),1,year(DtMesAnterior_Final#))


      Clear(csq1:Record)
      consultasql2{prop:sql} =  'select sum(dc.valordocumento) as totalvrdoc, sum(dc.valorpago) as totalvrpago, ' &|
                                'sum(dc.valororiginalbaixado) as totalvrbaixado, pl.tipolancamento as tipo ' &|
                                'from documentos dc  ' &|
                                'left join planocontas pl on (pl.id = dc.idplano) ' &|
                                'where dc.d_datavencimento between <39>' & format(DtMesAnterior_Inicial#,@d12) & '<39> and <39>' & format(DtMesAnterior_Final#,@d12) &'<39> and ' &|
                                'dc.idempresa = ' & GLO:CodEmpresa &|
                                ' group by pl.tipolancamento ' &|
                                'order by pl.tipolancamento'

      !message(consultasql2{prop:sql},,,,,2)
      
      loop
        next(consultasql2)
        if error() then break end
                                                                            
        if CSQ1:campo4 = 'C'
           Loc:valorCredito = CSQ1:campo1
        else
           Loc:valorDebito = CSQ1:campo1
        end

      end

      loc:SaldoR = Loc:valorCredito - Loc:valorDebito


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Relat_FluxoCaixa')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:consultasql.Open                                  ! File consultasql used by this procedure, so make sure it's RelationManager is open
  Relate:consultasql2.Open                                 ! File consultasql2 used by this procedure, so make sure it's RelationManager is open
  Relate:documentos.SetOpenRelated()
  Relate:documentos.Open                                   ! File documentos used by this procedure, so make sure it's RelationManager is open
  Access:empresa.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ThisReport.Init(Process:View, Relate:documentos, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:documentos.SetQuickScan(1,Propagate:OneMany)
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom=True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Imprimir'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:consultasql.Close
    Relate:consultasql2.Close
    Relate:documentos.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.OpenReport()
    Open(fDocumentos,42h)
  
    clear(fDocumentos)
    !fDocumentos{Prop:Sql} = 'select * from Ret_DocDetalhes('&|
    !                        format(LOC:DataInicial,@d12) &', '& format(LOC:DataFinal,@d12) & ')'
  
    ano# = year(LOC:DataFinal)
  
    mes# = month(LOC:DataFinal) + 1
    if mes# = 12
       mes# = 1
       ano# = ano# + 1
    end
    LOC:DataFinal   = date(mes#,1,ano#) - 1
  
    if loc:dtgeracao = 1
  
          if loc:docsImanager = 0
  
            loc:stringQuery = 'select evento, tipo,  ' &|
                                    'valor[1], valor[2], valor[3], valor[4],   ' &|
                                    'valor[5], valor[6], valor[7], valor[8],   ' &|
                                    'valor[9], valor[10], valor[11], valor[12], descricao ' &|
                                    'from Ret_DocDetalhes(<39>'&|
                                    format(LOC:DataInicial,@d12) &'<39>, <39>'& format(LOC:DataFinal,@d12) & '<39>, '&GLO:CodEmpresa&' '&|
                                    ') order by tipo, descricao'
          else
  
            loc:stringQuery = 'select * from ((select evento, tipo,  ' &|
                                    'valor[1], valor[2], valor[3], valor[4], ' &|
                                    'valor[5], valor[6], valor[7], valor[8], ' &|
                                    'valor[9], valor[10], valor[11], valor[12], descricao ' &|
                                    'from ret_docdetalhes_view_imoney(<39>'&|
                                    format(LOC:DataInicial,@d12) &'<39>, <39>'& format(LOC:DataFinal,@d12) & '<39>) order by tipo, descricao' &|
                              ') union all (' &|
                              'select evento, tipo,  ' &|
                                    'valor[1], valor[2], valor[3], valor[4], ' &|
                                    'valor[5], valor[6], valor[7], valor[8], ' &|
                                    'valor[9], valor[10], valor[11], valor[12], descricao ' &|
                                    'from Ret_DocDetalhes(<39>'&|
                                    format(LOC:DataInicial,@d12) &'<39>, <39>'& format(LOC:DataFinal,@d12) & '<39>, '&GLO:CodEmpresa&' '&|
                                    ') order by tipo, descricao)) as q'
          end
          fDocumentos{Prop:Sql} = loc:stringQuery
  
    else
          if loc:docsImanager = 0
  
            loc:stringQuery = 'select evento, tipo,  ' &|
                                    'valor[1], valor[2], valor[3], valor[4],   ' &|
                                    'valor[5], valor[6], valor[7], valor[8],   ' &|
                                    'valor[9], valor[10], valor[11], valor[12], descricao ' &|
                                    'from public."ret_docdetalhes_Venc"(<39>'&|
                                    format(LOC:DataInicial,@d12) &'<39>, <39>'& format(LOC:DataFinal,@d12) & '<39>, '&GLO:CodEmpresa&' '&|
                                    ') order by tipo, descricao'
          else
            loc:stringQuery = 'select * from ((select evento, tipo,  ' &|
                                    'valor[1], valor[2], valor[3], valor[4], ' &|
                                    'valor[5], valor[6], valor[7], valor[8], ' &|
                                    'valor[9], valor[10], valor[11], valor[12], descricao ' &|
                                    'from ret_docdetalhes_view_imoney_Venc(<39>'&|
                                    format(LOC:DataInicial,@d12) &'<39>, <39>'& format(LOC:DataFinal,@d12) & '<39>) order by tipo, descricao' &|
                              ') union all (' &|
                              'select evento, tipo,  ' &|
                                    'valor[1], valor[2], valor[3], valor[4], ' &|
                                    'valor[5], valor[6], valor[7], valor[8], ' &|
                                    'valor[9], valor[10], valor[11], valor[12], descricao ' &|
                                    'from public."ret_docdetalhes_Venc"(<39>'&|
                                    format(LOC:DataInicial,@d12) &'<39>, <39>'& format(LOC:DataFinal,@d12) & '<39>, '&GLO:CodEmpresa&' '&|
                                    ') order by tipo, descricao)) as q'
          end
          fDocumentos{Prop:Sql} = loc:stringQuery
    end
  
    !message(fDocumentos{Prop:Sql},,,,,2)
  
  
    f# = 0
    loop
        next(fDocumentos)
        if error() then break end
        
        IF Doc:Campo2 = 'S'
          CYCLE
        END
        
        if Doc:Campo2 = 'D' and f# = 0
           print(rpt:detail2)
           f# = 1
        end
  
        b# = 1
        loop a# = 1 to 12
            case a#
            of 1
                if f# = 0
                   loc:receitas[a#] = DOC:Campo3
                else
                   loc:despesas[a#] = DOC:Campo3
                end
            of 2
                if f# = 0
                   loc:receitas[a#] = DOC:Campo4
                else
                   loc:despesas[a#] = DOC:Campo4
                end
            of 3
               if f# = 0
                   loc:receitas[a#] = DOC:Campo5
                else
                   loc:despesas[a#] = DOC:Campo5
                end
            of 4
                if f# = 0
                   loc:receitas[a#] = DOC:Campo6
                else
                   loc:despesas[a#] = DOC:Campo6
                end
            of 5
                if f# = 0
                   loc:receitas[a#] = DOC:Campo7
                else
                   loc:despesas[a#] = DOC:Campo7
                end
            of 6
                if f# = 0
                   loc:receitas[a#] = DOC:Campo8
                else
                   loc:despesas[a#] = DOC:Campo8
                end
            of 7
                if f# = 0
                   loc:receitas[a#] = DOC:Campo9
                else
                   loc:despesas[a#] = DOC:Campo9
                end
            of 8
                if f# = 0
                   loc:receitas[a#] = DOC:Campo10
                else
                   loc:despesas[a#] = DOC:Campo10
                end
  
            of 9
                if f# = 0
                   loc:receitas[a#] = DOC:Campo11
                else
                   loc:despesas[a#] = DOC:Campo11
                end
  
            of 10
                if f# = 0
                   loc:receitas[a#] = DOC:Campo12
                else
                   loc:despesas[a#] = DOC:Campo12
                end
  
            of 11
                if f# = 0
                   loc:receitas[a#] = DOC:Campo13
                else
                   loc:despesas[a#] = DOC:Campo13
                end
  
            of 12
                if f# = 0
                   loc:receitas[a#] = DOC:Campo14
                else
                   loc:despesas[a#] = DOC:Campo14
                end
  
            end
  
            if f# = 0
               loc:totalreceitames[a#] += loc:receitas[a#]
               loc:totalreceita += loc:receitas[a#]
            else
               loc:totaldespesames[a#] += loc:despesas[a#]
               loc:totaldespesa += loc:despesas[a#]
            end
  
            loc:totalgeralmes[a#] = loc:totalreceitames[a#] - loc:totaldespesames[a#]
  
  
        end
  
        if f# = 0
           Report$?String36{prop:text}   = DOC:Campo15
           print(rpt:detail1)
           loc:totalreceita = 0
        else
           Report$?String36:2{prop:text} = DOC:Campo15
           print(rpt:detail3)
           loc:totaldespesa = 0
        end
  
    end
  
    if loc:mesanterior = 1
       loc:SaldoR = 0
       do Busca_Saldo_Mes_Anterior
       loc:saldomesanterior[1] = loc:SaldoR
  
       loop x# = 1 to 11
  
              loc:saldomesanterior[x#] *= -1
  
              loc:totalgeralmes[x#]    = loc:totalgeralmes[x#] - loc:saldomesanterior[x#]
  
              loc:saldomesanterior[x#+1] = loc:totalgeralmes[x#]
              Report$?group1{prop:hide}  = 0
       end
    end
    print(rpt:detail4)
    
    Close(fDocumentos)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?loc:dtgeracao
      if loc:dtgeracao = 2 ! Data de pagamento
        LOC:DataFinal = date(month(today()),1,year(today())+1) -1
      else ! Data de vencimento
          LOC:DataFinal = date(month(today()),1,year(today()))
      end
      display
      
    OF ?Pause
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Parar'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Imprimir'
      END
      SELF.Paused = 1 - SELF.Paused
        ! Formata��o dos meses no cabe�alho
      
        ! Encontrar o m�s inicial
        mes# = month(LOC:DataFinal)
        ano# = year(LOC:DataFinal)
        
        loop a# = 1 to 11
             mes# = mes# - 1
             ano# = ano#
             if mes# = 0
                mes# = 12
                ano# -= 1
             end
             LOC:DataInicial = date(mes#,1,ano#)
        end
      
        loop a# = 1 to 12
             aux_data# = date(month(LOC:DataInicial)+(a#-1),day(LOC:DataInicial),year(LOC:DataInicial))
             case month(aux_data#)
             of 1
                mes" = 'JAN'
             of 2
                mes" = 'FEV'
             of 3
                mes" = 'MAR'
             of 4
                mes" = 'ABR'
             of 5
                mes" = 'MAI'
             of 6
                mes" = 'JUN'
             of 7
                mes" = 'JUL'
             of 8
                mes" = 'AGO'
             of 9
                mes" = 'SET'
             of 10
                mes" = 'OUT'
             of 11
                mes" = 'NOV'
             of 12
                mes" = 'DEZ'
             end
             loc:mesescabecalho[a#] = format(MES",@S3) & '/' & format(year(aux_data#),@n04)
        end
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      !!!Seta as Vari�veis do Report
      Clear(LOC:RazaoSocial)
      Clear(LOC:EnderecoBairro)
      Clear(LOC:CepTelefone)
      Clear(LOC:Data)
      Clear(LOC:Hora)
      
      Clear(LOC:DataInicial)
      Clear(LOC:DataFinal)
      Clear(FAV:Record)
      
      
      Clear(CSQ1:Record)
      consultasql2{prop:sql} = |
              'select emp.razaosocial, emp.logradouro, emp.numero, emp.bairro, emp.cep, emp.telefone from empresa emp where emp.codempresa = '&GLO:CodEmpresa
      
      !message(fExtrato{prop:sql},,,,,2)
      
      Next(consultasql2)
      
      LOC:RazaoSocial    = CSQ1:campo1
      LOC:EnderecoBairro = CSQ1:campo2&', '&CSQ1:campo3&'   '&CSQ1:campo4
      LOC:CepTelefone    = CSQ1:campo5&'   '&CSQ1:campo6
      LOC:Data           = DataServidor()
      LOC:Hora           = HoraServidor()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  IF 1 = 2
    PRINT(RPT:detail1)
  END
  IF 1 = 2
    PRINT(RPT:detail2)
  END
  IF 1 = 2
    PRINT(RPT:detail3)
  END
  IF 1 = 2
    PRINT(RPT:detail4)
  END
  RETURN ReturnValue

RelatPlanoSaldoAPagar PROCEDURE                            ! Generated from procedure template - Report

Progress:Thermometer BYTE                                  !
LOC:RazaoSocial      CSTRING(101)                          !
LOC:EnderecoBairro   CSTRING(101)                          !
LOC:CepTelefone      CSTRING(101)                          !
LOC:Data             DATE                                  !
LOC:Hora             STRING(20)                            !
Loc:dataDoc          DATE                                  !
loc:total            DECIMAL(15,2)                         !
Process:View         VIEW(movdocumentos)
                     END
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
fExtrato  File,Driver('ODBC','/TurboSql=1'),PRE(FEX),Name('consultasql'),Owner(GLO:conexao)
Record      Record
campo1        date
campo2        cstring(51)
campo3        decimal(15,2)
campo4        cstring(21)
            End
          End
ProgressWindow       WINDOW('i-Money'),AT(,,213,56),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,TIMER(1),GRAY,DOUBLE
                       PANEL,AT(2,2,207,35),USE(?Panel1),BEVEL(1,-1)
                       PROMPT('Selecione a conta'),AT(9,6),USE(?Prompt1),FONT(,,,FONT:bold)
                       PROMPT('Documentos com data at� ...'),AT(98,6),USE(?Prompt1:2),FONT(,,,FONT:bold)
                       ENTRY(@n15.`0b),AT(11,19,60,10),USE(CON:id),DECIMAL(12),MSG('Identifica��o da conta'),TIP('Identifica��o da conta'),REQ,READONLY
                       BUTTON('...'),AT(76,18,12,12),USE(?CallLookup)
                       ENTRY(@d06b),AT(121,19,60,10),USE(Loc:dataDoc),REQ
                       PROGRESS,USE(Progress:Thermometer),HIDE,AT(7,39,51,16),RANGE(0,100)
                       BUTTON('Imprimir'),AT(87,39,55,16),USE(?Pause),FLAT,LEFT,ICON('imprimir.ico')
                       STRING(''),AT(45,44,51,12),USE(?Progress:UserString),HIDE,CENTER
                       STRING(''),AT(3,44,51,12),USE(?Progress:PctText),HIDE,CENTER
                       BUTTON('Cancelar'),AT(153,39,55,16),USE(?Progress:Cancel),FLAT,HIDE,LEFT,ICON('cancelar.ico')
                     END

Report               REPORT,AT(240,1365,7750,9823),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',8,,,CHARSET:ANSI),THOUS
                       HEADER,AT(240,260,7750,1115),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         BOX,AT(3208,83,4458,354),USE(?Box1:4),ROUND,COLOR(COLOR:Black),FILL(COLOR:Gray)
                         BOX,AT(83,83,3052,635),USE(?Box1:2),ROUND,COLOR(COLOR:Black),FILL(COLOR:Gray)
                         BOX,AT(3188,52,4458,354),USE(?Box1:3),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(63,52,3052,635),USE(?Box1),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         STRING('Saldo a Pagar'),AT(3198,73,4438,281),USE(?String17),TRN,CENTER,FONT('Arial',18,,FONT:bold,CHARSET:ANSI)
                         STRING(@s46),AT(104,135,3031,177),USE(LOC:RazaoSocial),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s50),AT(94,323,2979,177),USE(LOC:EnderecoBairro),TRN,LEFT,FONT(,7,,)
                         STRING('Informa��es sobre a conta'),AT(3260,458),USE(?String18:5),TRN,FONT(,,,FONT:bold)
                         STRING(@s50),AT(94,500,2979,177),USE(LOC:CepTelefone),TRN,LEFT,FONT(,7,,)
                         STRING('Conta'),AT(3333,635,4198,177),USE(?String22),TRN
                         STRING('N<186> Cheque/N<186> Documento'),AT(5021,885),USE(?String18:4),TRN,FONT(,,,FONT:bold)
                         STRING('Descri��o'),AT(1500,885),USE(?String18:2),TRN,FONT(,,,FONT:bold)
                         STRING('Data'),AT(479,885),USE(?String18),TRN,FONT(,,,FONT:bold)
                         STRING('Valor'),AT(6938,885),USE(?String18:3),TRN,FONT(,,,FONT:bold)
                         LINE,AT(94,1052,7563,0),USE(?Line1),COLOR(COLOR:Black)
                       END
detail1                DETAIL,AT(,,,177),USE(?Detail1),FONT('Tahoma',,,,CHARSET:ANSI)
                         STRING(@s15),AT(5250,10),USE(fex:campo4),TRN,CENTER
                         STRING(@d06b),AT(479,10),USE(fex:campo1),TRN
                         STRING(@s50),AT(1500,10,3250,167),USE(fex:campo2),TRN
                         STRING(@n10.2),AT(6625,10,917,167),USE(fex:campo3),TRN,CENTER
                       END
                       FOOTER,AT(240,11177,7750,594),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Data:'),AT(2010,260),USE(?ReportDatePrompt),TRN,FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING(@d06b),AT(2333,260),USE(LOC:Data),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING('Hora:'),AT(3417,260),USE(?ReportTimePrompt),TRN,FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING(@T01B),AT(3750,260),USE(LOC:Hora),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING('Usu�rio:'),AT(83,250),USE(?String24),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s20),AT(531,250),USE(GLO:nomeusuario),FONT('Arial',8,,,CHARSET:ANSI)
                         LINE,AT(94,-10,7563,0),USE(?Line1:2),COLOR(COLOR:Black)
                         STRING('Total'),AT(6125,10,1510,177),USE(?String25),TRN,CENTER,FONT(,,,FONT:bold)
                         LINE,AT(94,208,7563,0),USE(?Line1:3),COLOR(COLOR:Black)
                         STRING(@pP�gina <.<<#p),AT(6906,438,729,146),PAGENO,USE(?PageCount),RIGHT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Interfocus Tecnologia'),AT(6604,250),USE(?String23),TRN,FONT('Times New Roman',8,,FONT:bold+FONT:italic,CHARSET:ANSI)
                       END
                     END
ThisWindow           CLASS(ReportManager)
AskPreview             PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED ! Method added to host embed code
Paused                 BYTE
Timer                  LONG
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Previewer            PrintPreviewClass                     ! Print Previewer

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
CarregaDocs Routine

  !Busca os movimentos n�o consolidados
  Clear(FEX:Record)
  fExtrato{prop:sql} = |
  'select mvd.d_datamovimentacao, mvd.descricao, mvd.valormovimento, mvd.numdocpagamento '&|
  'from movdocumentos mvd '&|
  'left join documentos dc on dc.id = mvd.iddocumento '&|
  'left Join planocontas pla on (pla.id = dc.idplano) '&|
  'Where mvd.d_dataconciliacao is null and mvd.idconta = ' & CON:id & ' and d_datamovimentacao <= <39>' & format(Loc:dataDoc,@d12) &|
  '<39> order by mvd.d_datamovimentacao asc'

  get(contas, CON:con_pk_id)
  if ~error()
     report$?string22{prop:text} = 'Banco: ' & clip(CON:numerodobanco) & ' - Ag�ncia: ' & clip(CON:agenciabancaria) & ' - Conta: ' & clip(CON:numeroconta)
     report$?String18:5{prop:text} = 'Informa��es sobre a conta at� ' & format(Loc:dataDoc,@d06b)
  else
     report$?string22{prop:text} = ''
  end

  !message(fExtrato{prop:sql},,,,,2)
  loc:total = 0
  Loop
    Next(fExtrato)
    If Error() Then Break .
      loc:total += fex:campo3
      Report$?string25{prop:text} = 'Total: ' & format(loc:total,@n12.2)
      print(RPT:detail1)
  end

ThisWindow.AskPreview PROCEDURE

  CODE
  Open(fextrato,42h)
  Do CarregaDocs
  Close(fextrato)
  PARENT.AskPreview


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('RelatPlanoSaldoAPagar')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:consultasql2.Open                                 ! File consultasql2 used by this procedure, so make sure it's RelationManager is open
  Relate:contas.SetOpenRelated()
  Relate:contas.Open                                       ! File contas used by this procedure, so make sure it's RelationManager is open
  Access:empresa.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:documentos.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:planocontas.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ThisReport.Init(Process:View, Relate:movdocumentos, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:movdocumentos.SetQuickScan(1,Propagate:OneMany)
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom=True
  ASSERT(~SELF.DeferWindow) ! A hidden Go button is not smart ...
  SELF.KeepVisible = 1
  SELF.DeferOpenReport = 1
  SELF.Timer = TARGET{PROP:Timer}
  TARGET{PROP:Timer} = 0
  ?Pause{PROP:Text} = 'Imprimir'
  SELF.Paused = 1
  ?Progress:Cancel{PROP:Key} = EscKey
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:consultasql2.Close
    Relate:contas.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    BrowseContas
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Pause
      if ~Loc:dataDoc or ~CON:id
        cycle
      end
      IF SELF.Paused
        TARGET{PROP:Timer} = SELF.Timer
        ?Pause{PROP:Text} = 'Pause'
      ELSE
        SELF.Timer = TARGET{PROP:Timer}
        TARGET{PROP:Timer} = 0
        ?Pause{PROP:Text} = 'Imprimir'
      END
      SELF.Paused = 1 - SELF.Paused
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CON:id
      IF CON:id OR ?CON:id{Prop:Req}
        CON:id = CON:id
        IF Access:contas.TryFetch(CON:con_pk_id)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            CON:id = CON:id
          ELSE
            SELECT(?CON:id)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      CON:id = CON:id
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        CON:id = CON:id
      END
      ThisWindow.Reset(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      !!!Seta as Vari�veis do Report
      Clear(LOC:RazaoSocial)
      Clear(LOC:EnderecoBairro)
      Clear(LOC:CepTelefone)
      Clear(LOC:Data)
      Clear(LOC:Hora)
      
      
      Clear(CSQ1:Record)
      consultasql2{prop:sql} = |
              'select emp.razaosocial, emp.logradouro, emp.numero, emp.bairro, emp.cep, emp.telefone from empresa emp where emp.codempresa = '&GLO:CodEmpresa
      
      !message(fExtrato{prop:sql},,,,,2)
      
      Next(consultasql2)
      
      LOC:RazaoSocial    = CSQ1:campo1
      LOC:EnderecoBairro = CSQ1:campo2&', '&CSQ1:campo3&'   '&CSQ1:campo4
      LOC:CepTelefone    = CSQ1:campo5&'   '&CSQ1:campo6
      LOC:Data           = DataServidor()
      LOC:Hora           = HoraServidor()
    OF EVENT:Timer
      IF SELF.Paused THEN RETURN Level:Benign .
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  IF 1 = 2
    PRINT(RPT:detail1)
  END
  RETURN ReturnValue

