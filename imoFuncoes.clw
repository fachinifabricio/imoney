

   MEMBER('imoney.clw')                                    ! This is a MEMBER module

                     MAP
                       INCLUDE('IMOFUNCOES.INC'),ONCE        !Local module procedure declarations
                     END


srcRetornaPlanoContas PROCEDURE  (qPlano)                  ! Declare Procedure
  CODE
  Do CarregaPlano
CarregaPlano Routine
  data
fPlanos   file,driver('ODBC','/TurboSql=1'),owner(glo:conexao),pre(FPL),name('consultasql')
record      record
linha         cstring(81),name('campo1')
id            long,name('campo1')
identacao     byte,name('campo1')
idpai         long,name('campo1')
tipo          cstring(5),name('campo1')
            end
          end
LOC:Campo any

  code
    free(qPlano)
    !Inclui o T�tulo do Plano
    !Codigo
    LOC:campo &= what(qPlano,1)
    LOC:campo = 0
    !Check
    LOC:campo &= what(qPlano,2)
    LOC:campo = 0
    !Descricao
    LOC:campo &= what(qPlano,3)
    LOC:campo = ''
    !CodigoPai
    LOC:campo &= what(qPlano,4)
    LOC:campo = 0
    !Display
    LOC:campo &= what(qPlano,5)
    LOC:campo = 'Contas'
    !Icone
    LOC:campo &= what(qPlano,6)
    LOC:campo = 3
    !Tree
    LOC:campo &= what(qPlano,7)
    LOC:campo = 0
    add(qPlano)

    open(fPlanos,42h)
    if error() then error() .
    Clear(FPL:Record)
    fPlanos{prop:sql} = |
      'select pl.descricaoplano, t.id, t.identacao, pl.idpai, pl.tipolancamento '&|
      'from ( '&|
        'select x.linha, btrim(substr(x.linha,1,position('' - '' in x.linha)-1))::bigint as id, '&|
        'length(x.linha) - length(btrim(x.linha)) as identacao '&|
        'from ( '&|
          'select func_relatorio_plano_contas as linha '&|
          'from func_relatorio_plano_contas() '&|
        ') as x '&|
      ') as t '&|
      'join public.planocontas pl on pl.id = t.id'
    !message(fPlanos{prop:sql},,,,,2)
    loop
      next(fPlanos)
      if error() then break .

      !Codigo
      LOC:campo &= what(qPlano,1)
      LOC:campo = FPL:id
      !Check
      LOC:campo &= what(qPlano,2)
      LOC:campo = 2
      !Descricao
      LOC:campo &= what(qPlano,3)
      LOC:campo = ''
      !CodigoPai
      LOC:campo &= what(qPlano,4)
      LOC:campo = FPL:idpai
      !Display
      LOC:campo &= what(qPlano,5)
      LOC:campo = format(FPL:id,@n08) & ' - ' & FPL:linha
      !Icone
      LOC:campo &= what(qPlano,6)
      if FPL:tipo = 'S'
        LOC:campo = 3
      elsif FPL:tipo = 'D'
        LOC:campo = 4
      else
        LOC:campo = 5
      end
      !Tree
      LOC:campo &= what(qPlano,7)
      if FPL:identacao = 0
        LOC:campo = 1
      else
        LOC:campo = (FPL:identacao / 2) + 1
      end
      add(qPlano)
    end
    close(fPlanos)
srcImprimeNotaFiscal PROCEDURE  (PAR:IdNota,PAR:CodEmpresa) ! Declare Procedure
fDadosNota  File,Driver('ODBC','/TurboSql=1'),Owner(GLO:conexao),PRE(FDN),Name('consultasql')
record        record
numeronf        long, name('campo1')
naturezaop      cstring(31), name('campo1')
cfop            cstring(7), name('campo1')
emissao         date, name('campo1')
razao           cstring(51), name('campo1')
endereco        cstring(46), name('campo1')
bairro          cstring(16), name('campo1')
cidade          cstring(46), name('campo1')
uf              cstring(3), name('campo1')
cep             cstring(11), name('campo1')
cnpj            cstring(21), name('campo1')
ie              cstring(21), name('campo1')
duplicata       cstring(21), name('campo1')
vencimento      date, name('campo1')
valor           decimal(15,2), name('campo1')
descricao       cstring(71), name('campo1')
              end
            end

LOC:impressora cstring(501)
LOC:DataEmissao cstring(11)
LOC:Data       date
LOC:ArquivoNF  cstring(501)
  CODE
  if not filedialog('Selecione o arquivo de notas!',LOC:arquivoNF,'Text|*.txt',FILE:Save+FILE:KeepDir+FILE:LongName) then return .

  clear(LOC:DataEmissao)
  LOC:DataEmissao = Inputbox('Informe a data de emiss�o para a Nota Fiscal no formato DD/MM/YYYY?','Data de Emiss�o da Nota Fiscal','Data de Emiss�o:')
  if ~LOC:DataEmissao
    return
  end

  LOC:Data = Deformat(LOC:DataEmissao,@d06b)
  if ~LOC:Data
    Message('Data inv�lida!')
    return
  end

  !stop(format(LOC:Data,@d012))
  !Abre os arquivos de consulta na base de dados
  open(fDadosNota,42h)

  !Inicia a impress�o
  LinePrint('',LOC:arquivoNF)
  LinePrint('',LOC:arquivoNF)
  LinePrint('',LOC:arquivoNF)
  !Inclui a nota
  clear(FDN:record)
  fDadosNota{prop:sql} = |
    'insert into temporarias.numeracaonf(iddocumento, numeronota, dataemissao, codempresa) '&|
    'select '&PAR:IdNota&', '&|
    'case when max(n.numeronota) is null then 1 else max(n.numeronota) + 1 end, '&|
    '<39>'&Format(LOC:data,@d012)&'<39>, '&PAR:CodEmpresa&' '&|
    'from temporarias.numeracaonf n where codempresa = '&PAR:CodEmpresa&';'

  !Busca os dados para impress�o
  clear(FDN:record)       
  fDadosNota{prop:sql} = |
    'select nf.numeronota, <39>PREST.SERVI�O DE INFORM�TICA<39>, <39> <39>, nf.dataemissao, '&|
    'f.razaosocial, f.endereco||<39>, <39>||f.numero, f.bairro, f.cidade, '&|
    'f.estado, f.cep, f.cpf_cnpj, f.rg_ie, d.numdocumento, d.d_datavencimento, '&|
    'd.valordocumento, d.descricao '&|
    'from public.documentos d '&|
    'join public.favorecidos f on f.id = d.idcliente '&|
    'join temporarias.numeracaonf nf on nf.iddocumento = d.id and nf.situacao = <39>N<39>'&|
    'where d.id = '&PAR:IdNota
  !message(fDadosNota{prop:sql},,,,,2)
  !stop(1)
  loop
    next(fDadosNota)
    if error() then break .
    !if error() then stop(error()&' - '&fileerror()) .

    !stop(FDN:descricao)
    LinePrint(all(' ',86) & format(FDN:numeronf,@n06),LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('  ' & Format(upper(trocaletras(FDN:naturezaop)),@s30) & '           ' & FDN:cfop & all(' ',38) & format(FDN:emissao,@d06b),LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('  ' & format(upper(trocaletras(FDN:razao)),@s62),LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('  ' & format(upper(trocaletras(FDN:endereco)),@s45) & '           ' & format(upper(trocaletras(FDN:bairro)),@s20),LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('  ' & format(upper(trocaletras(FDN:cidade)),@s45) & '           ' & format(upper(FDN:uf),@s2) & '  ' & format(FDN:cep,@p#####-###p),LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('  ' & format(FDN:cnpj,@s38) & '          ' & format(FDN:ie,@s38),LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('  ' & format(FDN:duplicata,@s12) & '   ' & format(FDN:vencimento,@d06b) & '         R$' & format(FDN:valor,@n10.`2),LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('0001  ' & format(upper(trocaletras(FDN:descricao)),@s50) & '            R$' & format(FDN:valor,@n10.`2) & '    R$' & format(FDN:valor,@n10.`2),LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('                  R$       0,00'&'                 '&'     R$       0,00'&'                  R$'&format(FDN:valor,@n10.`2),LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint(all(' ',9) & format(FDN:numeronf,@n06),LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    LinePrint('',LOC:arquivoNF)
    !LinePrint('',LOC:arquivoNF)
    !LinePrint('',LOC:arquivoNF)
    !LinePrint('',LOC:arquivoNF)
  end

  close(fDadosNota)
!  !Seleciona a impressora e inicializa
!  !If Not PrinterDialog('Selecione a impressora') Then Return .
!  !LOC:Impressora = Printer{propprint:device}
!  !message(LOC:Impressora,,,,,2)
!
!  clear(LOC:DataEmissao)
!  LOC:DataEmissao = Inputbox('Informe a data de emiss�o para a Nota Fiscal no formato DD/MM/YYYY?','Data de Emiss�o da Nota Fiscal','Data de Emiss�o:')
!  if ~LOC:DataEmissao
!    return
!  end
!
!  LOC:Data = Deformat(LOC:DataEmissao,@d06b)
!  if ~LOC:Data
!    Message('Data inv�lida!')
!    return
!  end
!
!  !stop(format(LOC:Data,@d012))
!  !Abre os arquivos de consulta na base de dados
!  open(fDadosNota,42h)
!
!  !Inicia a impress�o
!  LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!  LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!  LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!  !Inclui a nota
!  clear(FDN:record)
!  fDadosNota{prop:sql} = |
!    'insert into temporarias.numeracaonf(iddocumento, numeronota, dataemissao) '&|
!    'select '&PAR:IdNota&', '&|
!    'case when max(n.numeronota) is null then 1 else max(n.numeronota) + 1 end, '&|
!    '<39>'&Format(LOC:data,@d012)&'<39> from temporarias.numeracaonf n;'
!
!  !Busca os dados para impress�o
!  clear(FDN:record)       
!  fDadosNota{prop:sql} = |
!    'select nf.numeronota, <39>PREST.SERVI�O DE INFORM�TICA<39>, <39>5933<39>, nf.dataemissao, '&|
!    'f.razaosocial, f.endereco||<39>, <39>||f.numero, f.bairro, f.cidade, '&|
!    'f.estado, f.cep, f.cpf_cnpj, f.rg_ie, d.numdocumento, d.d_datavencimento, '&|
!    'd.valordocumento, d.descricao '&|
!    'from public.documentos d '&|
!    'join public.favorecidos f on f.id = d.idcliente '&|
!    'join temporarias.numeracaonf nf on nf.iddocumento = d.id '&|
!    'where d.id = '&PAR:IdNota
!  !message(fDadosNota{prop:sql},,,,,2)
!  !stop(1)
!  loop
!    next(fDadosNota)
!    if error() then break .
!    !if error() then stop(error()&' - '&fileerror()) .
!
!    !stop(2)
!    LinePrint(all(' ',86) & format(FDN:numeronf,@n06),path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('  ' & Format(upper(trocaletras(FDN:naturezaop)),@s30) & '           ' & FDN:cfop & all(' ',35) & format(LOC:dataemissao,@d06b),path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('  ' & format(upper(trocaletras(FDN:razao)),@s62),path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('  ' & format(upper(trocaletras(FDN:endereco)),@s45) & '           ' & format(upper(trocaletras(FDN:bairro)),@s20),path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('  ' & format(upper(trocaletras(FDN:cidade)),@s45) & '           ' & format(upper(FDN:uf),@s2) & '  ' & format(FDN:cep,@p#####-###p),path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('  ' & format(FDN:cnpj,@s38) & '          ' & format(FDN:ie,@s38),path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('  ' & format(FDN:duplicata,@s12) & '   ' & format(FDN:vencimento,@d06b) & '         R$' & format(FDN:valor,@n10.`2),path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('0001  ' & format(upper(trocaletras(FDN:descricao)),@s50) & '            R$' & format(FDN:valor,@n10.`2) & '    R$' & format(FDN:valor,@n10.`2),path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('                  R$       0,00'&'                 '&'     R$       0,00'&'                  R$'&format(FDN:valor,@n10.`2),path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint(all(' ',9) & format(FDN:numeronf,@n06),path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!    LinePrint('',path()&'\NF'&PAR:IdNota&'.txt')
!  end
!
!  close(fDadosNota)
FunNumSequencial     PROCEDURE  (prm:tabela, prm:campo)    ! Declare Procedure
  CODE
    open(consultasql)
    clear(consultasql:Record)
    consultasql{prop:sql} = 'select nextval(<39>'&clip(prm:tabela)&'_'&clip(prm:campo)&'_seq<39>)'
    
    next(ConsultaSql)

    return(CSQ:campo1)
    close(consultasql)
DataServidor         PROCEDURE                             ! Declare Procedure
LOC:Data Date

fturbo  FILE,DRIVER('ODBC','/TURBOSQL=1'),Owner(GLO:conexao),Name('consultasql')
Record      Record
campo1        Date
            End
        End
  CODE
  Open(fturbo)
  Clear(fturbo)
  fturbo{prop:sql} = 'Select current_date'
  Next(fturbo)
  Return(fturbo.campo1)
  Close(fturbo)
HoraServidor         PROCEDURE                             ! Declare Procedure
LOC:Hora  Time

fturbo  FILE,DRIVER('ODBC','/Turbosql=1'),OWNER(GLO:conexao),PRE(FTU),Name('consultasql')
Record      Record
campo1        cstring(21)
            End
        End
  CODE
  Open(fturbo,42h)
  Clear(FTU:Record)
  fturbo{prop:sql} = 'Select current_time(0)'
  Next(fturbo)
  LOC:Hora = Deformat(FTU:campo1[1:5],@t01)
  Return(LOC:Hora)
  Close(fturbo)
SetNullFields        PROCEDURE  (pFile)                    ! Declare Procedure
rField                  ANY                                 !Refer�ncia para o campo
rRecord                 &GROUP                              !Refer�ncia para a estrutura RECORD
lFields                 LONG                                !Quantos campos tem o arquivo
lCount                  LONG                                !Contador 
  CODE
  rRecord &= pFile{ PROP:RECORD }                           !Determina o handle para a estrutura de registro
  LOOP lCount = 1 TO pFile{ PROP:Fields }                   !Percorre todos os campos do arquivo
    rField &= WHAT( rRecord, lCount )                       !Determina o handle para o campo da vez
    If ~rField
      SETNULL( pFile, rField )                              !For�a conte�do para nulo
    End
  END
TiraCaracteres       PROCEDURE  (prm:variavel)             ! Declare Procedure
ezField              LONG                                  !
LOC:Work             CSTRING(20)                           !
LOC:Inscricao        CSTRING(20)                           !
LOC:Invalidos        CSTRING(20)                           !
LOC:Contador         LONG                                  !
LOC:Tira             BYTE                                  !
  CODE
    Letra" = '-()|\/.,!@#$%&*_-+=:;? '
    loop t# = 1 to len(Letra")
        loop while instring(Letra"[t#],clip(prm:variavel)) ~= 0
            prm:variavel = sub(prm:variavel,1,instring(letra"[t#],prm:variavel)-1)&|
            sub(prm:variavel,instring(letra"[t#],prm:variavel) +1, len(prm:variavel))
        end
    end
    return(clip(prm:variavel))
TiraEspacos          PROCEDURE  (prm:variavel)             ! Declare Procedure
  CODE
    Letra" = ' '
    loop t# = 1 to len(Letra")
        loop while instring(Letra"[t#],clip(prm:variavel)) ~= 0
            prm:variavel = sub(prm:variavel,1,instring(letra"[t#],prm:variavel)-1)&|
            sub(prm:variavel,instring(letra"[t#],prm:variavel) +1, len(prm:variavel))
        end
    end
    return(clip(prm:variavel))
TrocaDoisPontos      PROCEDURE  (String Variavel)          ! Declare Procedure
ezField              LONG                                  !
  CODE
   Letra" = ':'
   loop t# = 1 to 1
        loop
            posicao# = instring(Letra"[t#],variavel)
            if posicao# = 0 then break .
            Variavel[posicao#] = ' '
        end
   end
   Return(variavel)
TrocaLetras          PROCEDURE  (String Variavel,<String Adicional>) ! Declare Procedure
ezField              LONG                                  !
  CODE
   Letra" = '���������������'
   loop t# = 1 to 15
        loop
            posicao# = instring(Letra"[t#],variavel)
            if posicao# = 0 then break .
            if t# <= 6
                Variavel[posicao#] = 'A'
            else
                Variavel[posicao#] = 'a'
            end
        end
   end
   Letra" = '��'
   loop t# = 1 to 2
        loop
            posicao# = instring(Letra"[t#],variavel)
            if posicao# = 0 then break .
            if t# = 1
                Variavel[posicao#] = 'C'
            else
                Variavel[posicao#] = 'c'
            end
        end
   end
   Letra" = '��������'
   loop t# = 1 to 8
        loop
            posicao# = instring(Letra"[t#],variavel)
            if posicao# = 0 then break .
            if t# <= 4
                Variavel[posicao#] = 'E'
            else
                Variavel[posicao#] = 'e'
            end
        end
   end
   Letra" = '��������'
   loop t# = 1 to 8
        loop
            posicao# = instring(Letra"[t#],variavel)
            if posicao# = 0 then break .
            if t# <= 4
                Variavel[posicao#] = 'I'
            else
                Variavel[posicao#] = 'i'
            end
        end
   end
   Letra" = '��'
   loop t# = 1 to 2
        loop
            posicao# = instring(Letra"[t#],variavel)
            if posicao# = 0 then break .
            if t# = 1
                Variavel[posicao#] = 'N'
            else
                Variavel[posicao#] = 'n'
            end
        end
   end
   Letra" = '����������'
   loop t# = 1 to 10
        loop
            posicao# = instring(Letra"[t#],variavel)
            if posicao# = 0 then break .
            if t# <= 4
                Variavel[posicao#] = 'O'
            else
                Variavel[posicao#] = 'o'
            end
        end
   end
   Letra" = '��������'
   loop t# = 1 to 8
        loop
            posicao# = instring(Letra"[t#],variavel)
            if posicao# = 0 then break .
            if t# <= 4
                Variavel[posicao#] = 'U'
            else
                Variavel[posicao#] = 'u'
            end
        end
   end
   Letra" = '��'
   loop t# = 1 to 2
        loop
            posicao# = instring(Letra"[t#],variavel)
            if posicao# = 0 then break .
            Variavel[posicao#] = 'y'
        end
   end
   Letra" = '���'
   loop t# = 1 to 3
        loop
            posicao# = instring(Letra"[t#],variavel)
            if posicao# = 0 then break .
            Variavel[posicao#] = ' '
        end
   end
   Letra" = '['
   loop t# = 1 to 1
        loop
            posicao# = instring(Letra"[t#],variavel)
            if posicao# = 0 then break .
            Variavel[posicao#] = ' '
        end
   end
   Letra" = ']'
   loop t# = 1 to 1
        loop
            posicao# = instring(Letra"[t#],variavel)
            if posicao# = 0 then break .
            Variavel[posicao#] = ' '
        end
   end
   If Adicional
      Letra" = clip(Adicional)
      loop t# = 1 to Len(clip(Adicional))
           loop
               posicao# = instring(Letra"[t#],variavel)
               if posicao# = 0 then break .
               Variavel[posicao#] = ' '
           end
      end
   end
   Return(variavel)
