

   MEMBER('imoney.clw')                                    ! This is a MEMBER module


   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('IMONE008.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('IMONE003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE012.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE013.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseExtratoConta PROCEDURE                               ! Generated from procedure template - Window

LOC:DataInicial      DATE                                  !
LOC:DataFinal        DATE                                  !
qExtrato             QUEUE,PRE(QEX)                        !
idmov                DECIMAL(15)                           !
datamov              DATE                                  !
icone                LONG                                  !
descricao            CSTRING(51)                           !
valor                CSTRING(20)                           !
tipomov              STRING(1)                             !
numdocumento         CSTRING(21)                           !
numdocpagamento      CSTRING(21)                           !
seq                  LONG                                  !
id_documento         REAL                                  !
Saldo                DECIMAL(15,2)                         !
                     END                                   !
LOC:SaldoFinal       CSTRING(20)                           !
Loc:SaldoTotal       CSTRING(20)                           !
LOC:RazaoSocial      CSTRING(101)                          !
LOC:EnderecoBairro   CSTRING(101)                          !
LOC:CepTelefone      CSTRING(101)                          !
LOC:Data             DATE                                  !
LOC:Hora             STRING(20)                            !
loc:busca_doc        STRING(20)                            !
loc:busca_data_nao_conciliados LONG                        !
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
fExtrato  File,Driver('ODBC','/TurboSql=1'),PRE(FEX),Name('consultasql'),Owner(GLO:conexao)
Record      Record
campo1        decimal(15)
campo2        date
campo3        cstring(51)
campo4        cstring(51)
campo5        cstring(2)
campo6        byte
campo7        cstring(21)
campo8        cstring(21)
campo9        real
            End
          End

fFornecedor  File,Driver('ODBC','/TurboSql=1'),PRE(FFOR),Name('consultasql'),Owner(GLO:conexao)
Record      Record
campo1        cstring(20)
campo2        cstring(20)
campo3        cstring(150)
            End
          End
Window               WINDOW('i-Money'),AT(,,505,379),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,SYSTEM,GRAY,DOUBLE,AUTO
                       IMAGE('window.ico'),AT(2,2),USE(?imgLogo)
                       PROMPT('Extrato da Conta'),AT(26,5),USE(?proTitulo),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(2,23,500,2),USE(?Panel1)
                       GROUP('Filtros'),AT(3,26,498,69),USE(?Group1),BOXED,TRN
                         ENTRY(@n15.`0b),AT(32,39,54,10),USE(CON:id),FLAT,DECIMAL(2),MSG('Identifica��o da conta'),TIP('Identifica��o da conta'),REQ
                         BUTTON,AT(89,37,12,12),USE(?lkpConta),TRN,FLAT,ICON('busca.ico')
                         GROUP('Per�odo (Documentos Conciliados)'),AT(8,64,158,26),USE(?Group2),BOXED,TRN
                           PROMPT('De:'),AT(19,74),USE(?LOC:DataInicial:Prompt),TRN
                           ENTRY(@d06b),AT(32,74,52,10),USE(LOC:DataInicial),FLAT
                           PROMPT('At�:'),AT(88,74),USE(?LOC:DataFinal:Prompt),TRN
                           ENTRY(@d06b),AT(106,74,52,10),USE(LOC:DataFinal),FLAT
                         END
                         BUTTON('Atualizar'),AT(169,70,68,17),USE(?btnAtualizar),FLAT,LEFT,ICON('selecionar.ico')
                         PROMPT('Conta:'),AT(8,39),USE(?CON:id:Prompt),TRN
                         ENTRY(@s50),AT(105,39,390,10),USE(CON:descricaoconta),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Descri��o da conta'),TIP('Descri��o da conta'),REQ,READONLY
                         STRING('Banco:'),AT(8,52),USE(?String1)
                         ENTRY(@s50),AT(32,52,249,9),USE(BAN:nomedobanco),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Nome do banco'),TIP('Nome do banco'),REQ,READONLY
                         PROMPT('Ag�ncia:'),AT(286,52),USE(?CON:agenciabancaria:Prompt),TRN
                         ENTRY(@s10),AT(316,52,60,10),USE(CON:agenciabancaria),SKIP,FLAT,RIGHT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Ag�ncia banc�ria, caso seja uma conta banc�ria'),TIP('Ag�ncia banc�ria, caso seja uma conta banc�ria'),READONLY
                         PROMPT('Conta:'),AT(381,52),USE(?CON:numeroconta:Prompt),TRN
                         ENTRY(@s20),AT(405,52,90,10),USE(CON:numeroconta),SKIP,FLAT,RIGHT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('N�mero da conta, caso seja uma ag�ncia banc�ria'),TIP('N�mero da conta, caso seja uma ag�ncia banc�ria'),READONLY
                       END
                       GROUP('Busca'),AT(3,95,498,38),USE(?Group5),BOXED
                         GROUP('Por N<186> Doc. Pagamento'),AT(10,103,170,26),USE(?Group4),BOXED
                           ENTRY(@s20),AT(18,114,91,10),USE(loc:busca_doc),FLAT,UPR
                           BUTTON('Buscar'),AT(118,110,56,15),USE(?Button7),FLAT,LEFT,ICON('busca.ico')
                         END
                         GROUP('Por Data (Inclusive n�o conciliados)'),AT(323,103,170,26),USE(?Group6),BOXED
                           ENTRY(@d06b),AT(330,114,91,10),USE(loc:busca_data_nao_conciliados),FLAT
                           BUTTON('Buscar'),AT(426,110,56,15),USE(?Button7:2),FLAT,LEFT,ICON('busca.ico')
                         END
                       END
                       LIST,AT(4,135,497,193),USE(?lstExtrato),FLAT,VSCROLL,FORMAT('60L_FI~Data~L(11)@d06b@#2#57L(1)_F~N<186> Documento~L(12)@s20@#7#180L(4)_F~Descri��o' &|
   '~S(120)@s50@#4#84R_F~Valor~@n-20`2@#5#84R_F~N<186> Doc. Pagamento~@s20@#8#4C_F~Tipo~' &|
   'C(4)@s1@#6#'),FROM(qExtrato)
                       PROMPT('Saldo Geral (Conciliados + N�o Conciliados)'),AT(222,331,279,10),USE(?Prompt7),RIGHT
                       PANEL,AT(2,342,500,2),USE(?Panel1:2)
                       GROUP('Lan�amento Avulso'),AT(169,346,177,30),USE(?Group3),BOXED
                         BUTTON('Inclui&r'),AT(189,357,56,15),USE(?btnLancamento),TRN,FLAT,LEFT,ICON('incluir.ico')
                         BUTTON('E&xcluir'),AT(267,357,56,15),USE(?Button6),DISABLE,FLAT,LEFT,ICON('excluir.ico')
                       END
                       BUTTON('&Imprimir'),AT(4,356,63,15),USE(?btnImprimir),FLAT,LEFT,ICON('imprimir.ico')
                       BUTTON('Concilia��o por lote'),AT(68,357,89,15),USE(?Button8),FLAT,LEFT,ICON('principal.ico')
                       BUTTON('Cancelar Documento'),AT(355,353,88,19),USE(?Button9),DISABLE,FLAT,LEFT,ICON('cancelar.ico')
                       BUTTON('&Fechar'),AT(451,360,50,15),USE(?btnFechar),TRN,FLAT,LEFT,ICON('fechar.ico'),DEFAULT
                     END

Report               REPORT,AT(240,1365,7750,9825),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',8,,,CHARSET:ANSI),THOUS
                       HEADER,AT(240,260,7750,1117),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         BOX,AT(3208,83,4458,354),USE(?Box1:4),ROUND,COLOR(COLOR:Black),FILL(COLOR:Gray)
                         BOX,AT(83,83,3052,635),USE(?Box1:2),ROUND,COLOR(COLOR:Black),FILL(COLOR:Gray)
                         BOX,AT(3188,52,4458,354),USE(?Box1:3),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(63,52,3052,635),USE(?Box1),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         STRING('Extrato da Conta'),AT(3198,73,4438,281),USE(?String17),TRN,CENTER,FONT('Arial',18,,FONT:bold,CHARSET:ANSI)
                         STRING(@s100),AT(73,135,3031,177),USE(LOC:RazaoSocial),TRN,CENTER,FONT(,8,,FONT:bold)
                         STRING(@s50),AT(94,323,2979,177),USE(LOC:EnderecoBairro),TRN,CENTER,FONT(,7,,)
                         STRING(@s50),AT(94,500,2979,177),USE(LOC:CepTelefone),TRN,CENTER,FONT(,7,,)
                         STRING(''),AT(3717,683,3858,167),USE(?String29),TRN,RIGHT
                         LINE,AT(94,1052,7563,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING(@d06b),AT(5500,467),USE(LOC:DataInicial),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING(@d06b),AT(6750,469),USE(LOC:DataFinal),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING('at�'),AT(6500,469),USE(?String32),TRN,FONT('Verdana',8,,FONT:bold,CHARSET:ANSI)
                         STRING('De:'),AT(5275,467),USE(?String31),TRN,FONT('Verdana',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Data'),AT(83,896),USE(?String18:8),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('N<186> Documento'),AT(750,896),USE(?String18:9),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Descri��o'),AT(1646,896),USE(?String18:10),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Valor'),AT(6117,900),USE(?String18:11),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Saldo'),AT(7033,900),USE(?String18:12),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Tipo'),AT(7406,896),USE(?String18:7),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                       END
detail1                DETAIL,AT(,,,158),USE(?Detail1)
                         STRING(@d06b),AT(94,10,,145),USE(QEX:datamov),LEFT
                         STRING(@s20),AT(750,10,802,145),USE(QEX:numdocumento),LEFT
                         STRING(@s50),AT(1650,8,2983,142),USE(QEX:descricao),LEFT
                         STRING(@s1),AT(7469,10,,145),USE(QEX:tipomov),CENTER
                         STRING(@n-14.2b),AT(6508,8,833,142),USE(QEX:Saldo),RIGHT(2)
                         STRING(@n15`2),AT(5475,8,950,142),USE(QEX:valor),RIGHT(2)
                       END
DetailTotalGeral       DETAIL,AT(,,,175)
                         STRING('Saldo Geral'),AT(3875,17,3467,150),USE(?String30),TRN,RIGHT
                       END
DadosEmpresa           DETAIL,AT(,,,108)
                         STRING('String 33'),AT(2058,8,5508,100),USE(?String33),TRN,FONT(,6,,FONT:italic,CHARSET:ANSI)
                       END
                       FOOTER,AT(240,11177,7750,350),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Data:'),AT(2010,21),USE(?ReportDatePrompt),TRN,FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING(@d06b),AT(2333,21),USE(LOC:Data),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING('Hora:'),AT(3417,21),USE(?ReportTimePrompt),TRN,FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING(@T01B),AT(3750,21),USE(LOC:Hora),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING('Usu�rio:'),AT(83,10),USE(?String24),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s20),AT(531,10),USE(GLO:nomeusuario),FONT('Arial',8,,,CHARSET:ANSI)
                         LINE,AT(94,-10,7563,0),USE(?Line1:2),COLOR(COLOR:Black)
                         STRING(@pP�gina <.<<#p),AT(6906,198,729,146),PAGENO,USE(?PageCount),RIGHT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Interfocus Tecnologia'),AT(6604,10),USE(?String23),TRN,FONT('Times New Roman',8,,FONT:bold+FONT:italic,CHARSET:ANSI)
                       END
                     END
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
CarregaExtrato Routine

  !No Extrato � feita a invers�o de contas, toda conta cr�dito � d�bito e a d�bito � cr�dito. Somente para os que tem documento
  sq# = 0
  Free(qExtrato)
  Clear(LOC:SaldoFinal)
  !Busca o Saldo Inicial                                                                            
  Clear(FEX:Record)
  clear(Loc:SaldoTotal)

  ! A1, Inicio - 19/05/2009 - Fabricio Fachini

  ! C�digo Antigo, Inicio
     !  fExtrato{prop:sql} = |
     !  'select mvd.idconta, 0, <39>Saldo Inicial<39>, sum(mvd.valormovimento), pla.tipolancamento, '&|
     !  'case when mvd.iddocumento is null Then 0 Else 1 End as doc '&|
     !  'from movdocumentos mvd '&|
     !  'Join planocontas pla on (pla.id = mvd.idplano) '&|
     !  'Where mvd.d_dataconciliacao < <39>'&Format(LOC:DataInicial,@d012)&'<39> and mvd.idconta = '&CON:id&' '&|
     !  'Group By mvd.idconta, pla.tipolancamento, doc'
  ! C�digo Antigo, FIM          doctos.idplano       'Join documentos doctos on (doctos.id = mvd.iddocumento) ' &|

  ! C�digo Novo, Inicio
  fExtrato{prop:sql} = |
  'select mvd.idconta, 0, <39>Saldo Inicial<39>, sum(mvd.valormovimento), pla.tipolancamento, '&|
  'case when mvd.iddocumento is null Then 0 Else 1 End as doc ' &|
  'from movdocumentos mvd ' &|
  'Join planocontas pla on (pla.id = mvd.idplano) ' &|
  'Where mvd.d_dataconciliacao < <39>'&Format(LOC:DataInicial,@d012)&'<39> and ' &|
  'mvd.idconta = ' &CON:id& ' '&|
  'Group By mvd.idconta, pla.tipolancamento, doc '
  ! C�digo Novo, Fim

  !Message(fExtrato{prop:sql},,,,,2)

  ! A1, FIM - 19/05/2009 - Fabricio Fachini
  !stop('1-LOC:SaldoFinal: ' & LOC:SaldoFinal)
  Loop
    next(fExtrato)
    If Error() Then Break .

    !If (FEX:campo5 = 'C' And FEX:campo6) Or (FEX:campo5 = 'D' and ~FEX:campo6)

    If FEX:campo5 = 'D'
      LOC:SaldoFinal -= FEX:campo4
    Else
      LOC:SaldoFinal += FEX:campo4
    End
    !stop('SAldo Inicial: '& LOC:SaldoFinal &' - '& FEX:campo4 &' - '& FEX:campo4)
  End
  !stop(FEX:campo4     &' - '& format(FEX:campo4,@n15`2))
  !stop(LOC:SaldoFinal &' - '& format(LOC:SaldoFinal,@n-20`2))
  !Inclui o Saldo Inicial
  do Busca_Ultimo_Reg_Queue
  Clear(qExtrato)
  QEX:seq = sq# + 1
  QEX:icone = 0
  QEX:descricao = 'Saldo Inicial'
  QEX:valor = LOC:SaldoFinal
  If LOC:SaldoFinal >= 0
    QEX:tipomov = 'C'
  Else
    QEX:tipomov = 'D'
  End
  Add(qExtrato)
  !Busca os movimentos conciliados
  Clear(FEX:Record)

  ! A2, Inicio - 19/05/2009 - Fabricio Fachini

  ! C�digo Antigo, Inicio
  !fExtrato{prop:sql} = |
  !'select mvd.id, mvd.d_dataconciliacao, mvd.descricao, mvd.valormovimento, pla.tipolancamento, '&|
  !'case when mvd.iddocumento is null Then 0 Else 1 End as doc, dc.numdocumento, mvd.numdocpagamento '&|
  !'from movdocumentos mvd '&|
  !'left join documentos dc on dc.id = mvd.iddocumento '&|
  !'Join planocontas pla on (pla.id = mvd.idplano) '&|
  !'Where mvd.d_dataconciliacao between <39>'&Format(LOC:DataInicial,@d012)&'<39> and <39>'&Format(LOC:DataFinal,@d012)&'<39> '&|
  !'and mvd.idconta = '&CON:id&' order by mvd.d_dataconciliacao asc'
  ! C�digo Antigo, Fim

  ! C�digo Novo, Inicio
  fExtrato{prop:sql} = |
  'select mvd.id, mvd.d_dataconciliacao, '&|
  'case when dc.descricao is not null then dc.descricao else mvd.descricao end, '&|
  'mvd.valormovimento, pla.tipolancamento, '&|
  'case when dc.id is null Then 0 Else 1 End as doc, dc.numdocumento, mvd.numdocpagamento, '&|
  'dc.id from movdocumentos mvd '&|
  'left join documentos dc on dc.id = mvd.iddocumento '&|
  'left Join planocontas pla on pla.id = mvd.idplano '&|
  'Where mvd.d_dataconciliacao between <39>' &Format(LOC:DataInicial,@d012)&'<39> and <39>'&Format(LOC:DataFinal,@d012)&'<39> '&|
  'and mvd.idconta = ' & CON:id & ' '&|
  'order by mvd.d_dataconciliacao asc'
  ! C�digo Novo, Fim
  !Message('1: ' & fExtrato{prop:sql},,,,,2)
  ! A2, Fim - 19/05/2009 - Fabricio Fachini

  Loop
    Next(fExtrato)
    If Error() Then Break .

    do Busca_Ultimo_Reg_Queue
    Clear(qExtrato)
    QEX:seq = sq# + 1

    QEX:idmov = FEX:campo1
    QEX:datamov = FEX:campo2
    if FEX:campo6 = 1
      QEX:icone = 3
    else
      QEX:icone = 2
    end
    QEX:descricao = FEX:campo3
    
    QEX:valor = FEX:campo4

    !If (FEX:campo5 = 'C' And FEX:campo6) Or (FEX:campo5 = 'D' And FEX:campo6 = 0)
    !stop(FEX:campo5)
    If FEX:campo5 = 'D'
      QEX:tipomov = 'D'
      LOC:SaldoFinal += FEX:campo4 * (-1)
      QEX:Saldo = LOC:SaldoFinal
    Else
      QEX:tipomov = 'C'
      LOC:SaldoFinal += FEX:campo4
      QEX:Saldo = LOC:SaldoFinal
    End
    !stop('Movimentos: '& FEX:campo4 &' - '& Format(FEX:campo4,@n-15.`2) &' - '& LOC:SaldoFinal &' - '& QEX:Saldo)

    QEX:numdocumento = FEX:campo7
    QEX:numdocpagamento = FEX:campo8
    QEX:id_documento = FEX:campo9
    
    Add(qExtrato)
  End
  !Inclui Saldo Final, 2 Linhas em Branco e 1 linha de t�tulo de n�o conciliados
  
  Loop a# = 1 To 4

    do Busca_Ultimo_Reg_Queue
    Clear(qExtrato)
    QEX:seq = sq# + 1

    If a# = 1
      QEX:icone = 0
      QEX:descricao = 'Saldo Final'
      If LOC:SaldoFinal >= 0
        QEX:tipomov = 'C'
      Else
        QEX:tipomov = 'D'
      End
      QEX:valor = LOC:SaldoFinal
    ElsIf a# <> 4
      QEX:icone = 0
    Else a# = 4
      QEX:icone = 0
      QEX:descricao = 'N � O   C O N C I L I A D O S'

    End
    Add(qExtrato)
  End
  Loc:SaldoTotal = LOC:SaldoFinal
  Clear(LOC:SaldoFinal)
  !Busca os movimentos n�o consolidados
  Clear(FEX:Record)


  fExtrato{prop:sql} = |
  'select mvd.id, mvd.d_datamovimentacao, ' &|
  'case when dc.descricao is not null then dc.descricao else mvd.descricao end, '&|
  'mvd.valormovimento, pla.tipolancamento, '&|
  'case when mvd.iddocumento is null Then 0 Else 1 End as doc, dc.numdocumento, mvd.numdocpagamento '&|
  'from movdocumentos mvd '&|
  'left join documentos dc on dc.id = mvd.iddocumento '&|
  'left Join planocontas pla on (pla.id = mvd.idplano) '&|
  'Where mvd.d_dataconciliacao is null and mvd.idconta = '&CON:id&' order by mvd.d_datamovimentacao asc'
  !message(fExtrato{prop:sql},,,,,2)
  Loop
    Next(fExtrato)
    If Error() Then Break .

    do Busca_Ultimo_Reg_Queue
    Clear(qExtrato)
    QEX:seq = sq# + 1

    QEX:idmov = FEX:campo1
    QEX:datamov = FEX:campo2
    QEX:icone = 1
    QEX:descricao = FEX:campo3
    !QEX:valor = Format(FEX:campo4,@n-15.`2)
    QEX:valor = FEX:campo4

    !If (FEX:campo5 = 'C' And FEX:campo6) or (FEX:campo5 = 'D' And FEX:campo6 = 0)
    If FEX:campo5 = 'D'
      QEX:tipomov = 'D'
      LOC:SaldoFinal += FEX:campo4 * (-1)
    Else
      QEX:tipomov = 'C'
      LOC:SaldoFinal += FEX:campo4
    End

    QEX:numdocumento    = FEX:campo7
    QEX:numdocpagamento = FEX:campo8
    Add(qExtrato)
  End

  !Inclui Saldo n�o consolidado

  do Busca_Ultimo_Reg_Queue
  Clear(qExtrato)
  QEX:seq = sq# + 1

  QEX:icone = 0
  QEX:descricao = 'Saldo N�o Conciliado'
  QEX:valor = LOC:SaldoFinal
  If LOC:SaldoFinal >= 0
    QEX:tipomov = 'C'
  Else
    QEX:tipomov = 'D'
  End
  Add(qExtrato)
  Loc:SaldoTotal += LOC:SaldoFinal
  !
  !Display(?lstExtrato)

  ! Posicionando registro
  QEX:descricao = 'N � O   C O N C I L I A D O S'
  get(qExtrato, QEX:descricao)
  select(?lstExtrato, QEX:seq + 1)

  ?Prompt7{prop:text} = 'Saldo Geral (Conciliados + N�o Conciliados): R$' & Format(Loc:SaldoTotal,@n-15.`2)

  display

!  IMPRIME A QUEUE EM UM ARQUIVOS ( NA PASTAS DO EXECUT�VEL )
!  remove('testequeue.txt')
!  sort(qExtrato,1)
!  loop bg# = 1 to records(qExtrato)
!    get(qExtrato, bg#)
!    if error() then break end
!    lineprint(QEX:seq &' - '& QEX:idmov &' - '& format(QEX:datamov,@d06b) &' - '& QEX:descricao &' - '& QEX:valor,'testequeue.txt')
!  end
!CarregaExtrato Routine
!  !No Extrato � feita a invers�o de contas, toda conta cr�dito � d�bito e a d�bito � cr�dito. Somente para os que tem documento
!  sq# = 0
!  Free(qExtrato)
!  Clear(LOC:SaldoFinal)
!  !Busca o Saldo Inicial
!  Clear(FEX:Record)
!
!  ! A1, Inicio - 19/05/2009 - Fabricio Fachini
!
!  ! C�digo Antigo, Inicio
!     !  fExtrato{prop:sql} = |
!     !  'select mvd.idconta, 0, <39>Saldo Inicial<39>, sum(mvd.valormovimento), pla.tipolancamento, '&|
!     !  'case when mvd.iddocumento is null Then 0 Else 1 End as doc '&|
!     !  'from movdocumentos mvd '&|
!     !  'Join planocontas pla on (pla.id = mvd.idplano) '&|
!     !  'Where mvd.d_dataconciliacao < <39>'&Format(LOC:DataInicial,@d012)&'<39> and mvd.idconta = '&CON:id&' '&|
!     !  'Group By mvd.idconta, pla.tipolancamento, doc'
!  ! C�digo Antigo, FIM
!
!  ! C�digo Novo, Inicio
!  fExtrato{prop:sql} = |
!  'select mvd.idconta, 0, <39>Saldo Inicial<39>, sum(mvd.valormovimento), pla.tipolancamento, '&|
!  'case when mvd.iddocumento is null Then 0 Else 1 End as doc ' &|
!  'from movdocumentos mvd ' &|
!  'Join documentos doctos on (doctos.id = mvd.iddocumento) ' &|
!  'Join planocontas pla on (pla.id = doctos.idplano) ' &|
!  'Where mvd.d_dataconciliacao < <39>'&Format(LOC:DataInicial,@d012)&'<39> and ' &|
!  'mvd.idconta = ' &CON:id& ' '&|
!  'Group By mvd.idconta, pla.tipolancamento, doc '
!  ! C�digo Novo, Fim
!  !Message(fExtrato{prop:sql},,,,,2)
!
!  ! A1, FIM - 19/05/2009 - Fabricio Fachini
!
!  Loop
!    next(fExtrato)
!    If Error() Then Break .
!
!    If (FEX:campo5 = 'C' And FEX:campo6) Or (FEX:campo5 = 'D' and ~FEX:campo6)
!      LOC:SaldoFinal -= FEX:campo4
!    Else
!      LOC:SaldoFinal += FEX:campo4
!    End
!  End
!  !Inclui o Saldo Inicial
!  do Busca_Ultimo_Reg_Queue
!  Clear(qExtrato)
!  QEX:seq = sq# + 1
!  QEX:icone = 2
!  QEX:descricao = 'Saldo Inicial'
!  QEX:valor = Format(LOC:SaldoFinal,@n15.`2)
!  If LOC:SaldoFinal >= 0
!    QEX:tipomov = 'C'
!  Else
!    QEX:tipomov = 'D'
!  End
!  Add(qExtrato)
!  !Busca os movimentos conciliados
!  Clear(FEX:Record)
!
!  ! A2, Inicio - 19/05/2009 - Fabricio Fachini
!
!  ! C�digo Antigo, Inicio
!  !fExtrato{prop:sql} = |
!  !'select mvd.id, mvd.d_dataconciliacao, mvd.descricao, mvd.valormovimento, pla.tipolancamento, '&|
!  !'case when mvd.iddocumento is null Then 0 Else 1 End as doc, dc.numdocumento, mvd.numdocpagamento '&|
!  !'from movdocumentos mvd '&|
!  !'left join documentos dc on dc.id = mvd.iddocumento '&|
!  !'Join planocontas pla on (pla.id = mvd.idplano) '&|
!  !'Where mvd.d_dataconciliacao between <39>'&Format(LOC:DataInicial,@d012)&'<39> and <39>'&Format(LOC:DataFinal,@d012)&'<39> '&|
!  !'and mvd.idconta = '&CON:id&' order by mvd.d_dataconciliacao asc'
!  ! C�digo Antigo, Fim
!
!  ! C�digo Novo, Inicio
!  fExtrato{prop:sql} = |
!  'select mvd.id, mvd.d_dataconciliacao, mvd.descricao, mvd.valormovimento, pla.tipolancamento, '&|
!  'case when mvd.iddocumento is null Then 0 Else 1 End as doc, dc.numdocumento, mvd.numdocpagamento '&|
!  'from movdocumentos mvd '&|
!  'left join documentos dc on dc.id = mvd.iddocumento '&|
!  'left Join planocontas pla on (pla.id = dc.idplano) '&|
!  'Where mvd.d_dataconciliacao between <39>' &Format(LOC:DataInicial,@d012)&'<39> and <39>'&Format(LOC:DataFinal,@d012)&'<39> '&|
!  'and mvd.idconta = ' & CON:id & ' '&|
!  'order by mvd.d_dataconciliacao asc'
!  ! C�digo Novo, Fim
!  !Message(fExtrato{prop:sql},,,,,2)
!  ! A2, Fim - 19/05/2009 - Fabricio Fachini
!
!  Loop
!    Next(fExtrato)
!    If Error() Then Break .
!
!    do Busca_Ultimo_Reg_Queue
!    Clear(qExtrato)
!    QEX:seq = sq# + 1
!
!    QEX:idmov = FEX:campo1
!    QEX:datamov = FEX:campo2
!    QEX:icone = 2
!    QEX:descricao = FEX:campo3
!    QEX:valor = Format(FEX:campo4,@n15.`2)
!
!    If (FEX:campo5 = 'C' And FEX:campo6) Or (FEX:campo5 = 'D' And FEX:campo6 = 0)
!      QEX:tipomov = 'D'
!      LOC:SaldoFinal += FEX:campo4 * (-1)
!    Else
!      QEX:tipomov = 'C'       
!      LOC:SaldoFinal += FEX:campo4
!    End
!
!    QEX:numdocumento = FEX:campo7
!    QEX:numdocpagamento = FEX:campo8
!    Add(qExtrato)
!  End
!  !Inclui Saldo Final, 2 Linhas em Branco e 1 linha de t�tulo de n�o conciliados
!  
!  Loop a# = 1 To 4
!
!    do Busca_Ultimo_Reg_Queue
!    Clear(qExtrato)
!    QEX:seq = sq# + 1
!
!    If a# = 1
!      QEX:icone = 2
!      QEX:descricao = 'Saldo Final'
!      If LOC:SaldoFinal >= 0
!        QEX:tipomov = 'C'
!      Else
!        QEX:tipomov = 'D'
!      End
!      QEX:valor = Format(LOC:SaldoFinal,@n15.`2)
!    ElsIf a# <> 4
!      QEX:icone = 2
!    Else a# = 4
!      QEX:icone = 2
!      QEX:descricao = 'N � O   C O N C I L I A D O S'
!
!    End
!    Add(qExtrato)
!  End
!  Clear(LOC:SaldoFinal)
!  !Busca os movimentos n�o consolidados
!  Clear(FEX:Record)
!  fExtrato{prop:sql} = |
!  'select mvd.id, mvd.d_datamovimentacao, mvd.descricao, mvd.valormovimento, pla.tipolancamento, '&|
!  'case when mvd.iddocumento is null Then 0 Else 1 End as doc, dc.numdocumento, mvd.numdocpagamento '&|
!  'from movdocumentos mvd '&|
!  'left join documentos dc on dc.id = mvd.iddocumento '&|
!  'left Join planocontas pla on (pla.id = dc.idplano) '&|
!  'Where mvd.d_dataconciliacao is null and mvd.idconta = '&CON:id&' order by mvd.d_datamovimentacao asc'
!  !message(fExtrato{prop:sql},,,,,2)
!  Loop
!    Next(fExtrato)
!    If Error() Then Break .
!
!    do Busca_Ultimo_Reg_Queue
!    Clear(qExtrato)
!    QEX:seq = sq# + 1
!
!    QEX:idmov = FEX:campo1
!    QEX:datamov = FEX:campo2
!    QEX:icone = 1
!    QEX:descricao = FEX:campo3
!    QEX:valor = Format(FEX:campo4,@n15.`2)
!
!    If (FEX:campo5 = 'C' And FEX:campo6) or (FEX:campo5 = 'D' And FEX:campo6 = 0)
!      QEX:tipomov = 'D'
!      LOC:SaldoFinal += FEX:campo4 * (-1)
!    Else
!      QEX:tipomov = 'D'
!      LOC:SaldoFinal += FEX:campo4
!    End
!
!    QEX:numdocumento    = FEX:campo7
!    QEX:numdocpagamento = FEX:campo8
!    Add(qExtrato)
!  End
!
!  !Inclui Saldo n�o consolidado
!
!  do Busca_Ultimo_Reg_Queue
!  Clear(qExtrato)
!  QEX:seq = sq# + 1
!
!  QEX:icone = 2
!  QEX:descricao = 'Saldo N�o Conciliado'
!  QEX:valor = Format(LOC:SaldoFinal,@n15.`2)
!  If LOC:SaldoFinal >= 0
!    QEX:tipomov = 'C'
!  Else
!    QEX:tipomov = 'D'
!  End
!  Add(qExtrato)
!  !
!  !Display(?lstExtrato)
!
!  ! Posicionando registro
!  QEX:descricao = 'N � O   C O N C I L I A D O S'
!  get(qExtrato, QEX:descricao)
!  select(?lstExtrato, QEX:seq + 1)
!  display
!
!!  IMPRIME A QUEUE EM UM ARQUIVOS ( NA PASTAS DO EXECUT�VEL )
!!  remove('testequeue.txt')
!!  sort(qExtrato,1)
!!  loop bg# = 1 to records(qExtrato)
!!    get(qExtrato, bg#)
!!    if error() then break end
!!    lineprint(QEX:seq &' - '& QEX:idmov &' - '& format(QEX:datamov,@d06b) &' - '& QEX:descricao &' - '& QEX:valor,'testequeue.txt')
!!  end
!!
!!  
Busca_Ultimo_Reg_Queue routine

  sort(qExtrato, QEX:seq)
  loop z# = 1 to records(qExtrato)
       get(qExtrato, z#)
       if errorcode() then break end
       sq# = QEX:seq
  end
imprimir_fornecedor routine
  if QEX:id_documento
      open(fFornecedor)
      Clear(FFOR:Record)

      fFornecedor{prop:sql} = 'select fav.id, ' &|
                              'case when fav.cpf_cnpj <> <39><39> then fav.cpf_cnpj  when fav.rg_ie <> <39><39> then fav.rg_ie else <39><39> end as documento,  ' &|
                              'fav.razaosocial from favorecidos fav join documentos doc on doc.idcliente = fav.id ' &|
                              'where doc.id = ' & QEX:id_documento

      !Message(fFornecedor{prop:sql},,,,,2)

      next(fFornecedor)
      if error() then stop(error()) end

      ?String33{prop:text} =  'EMPRESA: ' & FFOR:campo2 &' - '& FFOR:campo3

      print(RPT:DadosEmpresa)
      close(fFornecedor)
  end


!fFornecedor  File,Driver('ODBC','/TurboSql=1'),PRE(FFOR),Name('consultasql'),Owner(GLO:conexao)
!Record      Record
!campo1        cstring(20)
!campo2        cstring(20)
!campo3        cstring(150)
!            End
!          End

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseExtratoConta')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?imgLogo
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:bancos.SetOpenRelated()
  Relate:bancos.Open                                       ! File bancos used by this procedure, so make sure it's RelationManager is open
  Relate:consultasql2.Open                                 ! File consultasql2 used by this procedure, so make sure it's RelationManager is open
  Access:contas.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:movdocumentos.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:empresa.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  Open(fExtrato,42h)
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:bancos.Close
    Relate:consultasql2.Close
  Close(fExtrato)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    BrowseContas
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button7
      ! Busca DOC
      
      QEX:numdocpagamento = loc:busca_doc
      get(qExtrato, QEX:numdocpagamento)
      
      select(?lstExtrato, QEX:seq)
      display
    OF ?Button7:2
      ! Busca DOC por data
      
      QEX:datamov = loc:busca_data_nao_conciliados
      get(qExtrato, QEX:datamov)
      
      select(?lstExtrato, QEX:seq)
      display
    OF ?btnLancamento
      If ~CON:id
        Message('N�o foi selecionada a conta!','Aten��o',Icon:Asterisk,'OK',,2)
        Cycle
      End
    OF ?btnImprimir
      ! I M P R E S S � O
      
      fornecedores# = 0
      case message('Deseja relacionar informa��es de fornecedores ao extrato?','Aten��o',icon:exclamation,'&Sim|&N�o',2)
      of 1
        fornecedores# = 1
      end
      
      !!!Seta as Configura��es do Report
      Previewer.INIT(PrintPreviewQueue)
      Previewer.AllowUserZoom = True
      Previewer.Maximize = True
      
      Open(Report)
      Settarget(Report)
      Report{PROP:Preview} = PrintPreviewQueue.Filename
      
      ?String29{prop:text} = BAN:nomedobanco &' - Ag�ncia: '& CON:agenciabancaria &' - Conta: '&  CON:numeroconta
      
      !L� a Queue e Imprime
      Loop a# = 1 To Records(qExtrato)
        Get(qExtrato,a#)
        Print(RPT:Detail1)
        if fornecedores# = 1
            do imprimir_fornecedor
        end
      End
      ?String30{prop:text} = 'Saldo Geral (Conciliados + N�o Conciliados): R$' & Format(Loc:SaldoTotal,@n-15.`2)
      Print(RPT:DetailTotalGeral)
      
      LOC:Data = DataServidor()
      LOC:Hora = HoraServidor()
      
      Endpage(Report)
      If Previewer.Display() Then Report{ PROP:FlushPreview } = True .
      Settarget()
      Close(Report)
      Free(PrintPreviewQueue)
    OF ?Button9
      ! Cancela/Zera valor do documento
        if QEX:idmov
          case message('O documento receber� valor 0(ZERO).||Deseja realmente cancelar o documento?','Aten��o',icon:question,'&Sim|&N�o',2)
          of 1
            Clear(FEX:Record)
            fExtrato{prop:sql} = |
              'update public.movdocumentos set valormovimento = 0 where id = ' & QEX:idmov
      
            ! Message(fExtrato{prop:sql},,,,,2)
      
            do CarregaExtrato
      
            message('Documento Cancelado!','Aten��o',icon:exclamation)
          end
      
        else
          message('Selecione um documento','Aten��o',icon:exclamation)
        end
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CON:id
      IF CON:id OR ?CON:id{Prop:Req}
        CON:id = CON:id
        IF Access:contas.TryFetch(CON:con_pk_id)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            CON:id = CON:id
          ELSE
            SELECT(?CON:id)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      if CON:idempresa <> GLO:CodEmpresa
        message('A conta selecionada n�o pertence a empresa logada, corrija!','Aten��o',Icon:Exclamation,'OK',,2)
        select(?CON:id)
      else
        BAN:numerobanco = CON:numerodobanco
        Get(bancos,BAN:ban_numerodobanco)
      
        !if ~LOC:DataInicial or ~LOC:Datafinal
          !Passa data dos �ltimos 10 dias para o extrato
          LOC:DataFinal = LOC:Data
          LOC:DataInicial = LOC:DataFinal - 10
        !end
      
        If LOC:DataInicial And LOC:DataFinal
          Do CarregaExtrato
        Else
          Free(qExtrato)
        End
        Display
      end
    OF ?lkpConta
      ThisWindow.Update
      CON:id = CON:id
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        CON:id = CON:id
      END
      ThisWindow.Reset(1)
      Post(Event:Accepted,?CON:id)
    OF ?btnAtualizar
      ThisWindow.Update
      If ~CON:id
        Message('Selecione a conta!','Aten��o',Icon:Asterisk,'OK',,2)
      ElsIf ~LOC:DataInicial Or ~LOC:DataFinal
        Message('Per�odo informado inv�lido!','Aten��o',Icon:Asterisk,'OK',,2)
      Else
        Do CarregaExtrato
      End
    OF ?lstExtrato
      ! Accept do brw, habilita btn excluir lanc avulso
      get(qExtrato,choice(?lstExtrato))
      
      Clear(FEX:Record)
      fExtrato{prop:sql} = |
              'select pmov.tipobaixa from public.movdocumentos pmov where pmov.id = ' & QEX:idmov
      
      !message(fExtrato{prop:sql},,,,,2)
      
      Next(fExtrato)
      
      if sub(FEX:campo1,1,1) = '4'
         enable(?Button6)
      else
         disable(?Button6)
      end
      
      if sub(FEX:campo1,1,1) = '4' or sub(FEX:campo1,1,1) = '1'
        enable(?Button9)
      else
        disable(?Button9)
      end
      
      display
      
      If KeyCode() = MouseLeft2
        Get(qExtrato,Choice(?lstExtrato))
        !Se �cone 1, quer dizer que � n�o conciliado
        If QEX:icone = 1
          !Posiciona no registro
          MOV:id = QEX:idmov
          Get(movdocumentos,MOV:mov_pk_id)
          If ~Error()
            !Chama a Janela de Data de Concilia��o e caso retorne a data, concilia o movimento
            MOV:d_dataconciliacao = WindowDataConciliacao()
            If MOV:d_dataconciliacao
              Put(movdocumentos)
              Post(Event:Accepted,?btnAtualizar)
            End
          End
        elsif QEX:icone = 3
            winVisualizaDocumento(QEX:id_documento)
        elsif QEX:icone = 2
            GlobalRequest = ViewRecord
            UpdateLancamentoAvulso(QEX:idmov)
        End
      End
      clear(movdocumentos)
    OF ?btnLancamento
      ThisWindow.Update
      GlobalRequest = InsertRecord
      UpdateLancamentoAvulso()
      ThisWindow.Reset
      Post(Event:Accepted,?btnAtualizar)
    OF ?Button6
      ThisWindow.Update
      !Excluir lanc avulso
      
      case message('Deseja realmente excluir o lan�amento?','Aten��o',icon:question,'&Sim|&N�o',2)
      of 1
      
          Clear(FEX:Record)
      
          fExtrato{prop:sql} = |
                  'delete from public.movdocumentos where id = ' & QEX:idmov
      
      
          Do CarregaExtrato
      
          disable(?Button6)
      
      end
    OF ?Button8
      ThisWindow.Update
      Conciliacao_LOTE()
      ThisWindow.Reset
      do CarregaExtrato
    OF ?btnFechar
      ThisWindow.Update
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      Clear(CON:Record)
      Clear(BAN:Record)
      Clear(MOV:Record)
      
      ?lstExtrato{prop:iconlist,1} = path()&'\uncheck.ico'
      !?lstExtrato{prop:iconlist,2} = path()&'\check.ico'
      ?lstExtrato{prop:iconlist,2} = path()&'\lupazinha.ico'
      ?lstExtrato{prop:iconlist,3} = path()&'\lupazinha.ico'
      ?lstExtrato{prop:lineheight} = 10
      
      !!!Seta as Vari�veis do Report
      Clear(LOC:RazaoSocial)
      Clear(LOC:EnderecoBairro)
      Clear(LOC:CepTelefone)
      
      
      !Clear(csq1:Record)
      !consultasql2{prop:sql} = |
      !        'select emp.razaosocial, emp.logradouro, emp.numero, emp.bairro, emp.cep, emp.telefone from empresa emp limit 1'

      Clear(csq1:Record)
      consultasql2{prop:sql} = |
              'select emp.razaosocial, emp.logradouro, emp.numero, emp.bairro, emp.cep, emp.telefone from empresa emp where emp.codempresa = '&GLO:CodEmpresa


      !message(consultasql2{prop:sql},,,,,2)
      
      Next(consultasql2)
      
      LOC:RazaoSocial    = csq1:campo1
      LOC:EnderecoBairro = csq1:campo2&', '&csq1:campo3&'   '&csq1:campo4
      LOC:CepTelefone    = csq1:campo5&'   '&csq1:campo6
      LOC:Data           = DataServidor()
      LOC:Hora           = HoraServidor()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

WindowDataConciliacao PROCEDURE                            ! Generated from procedure template - Window

LOC:DataConciliacao  DATE                                  !
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
Window               WINDOW('i-Money'),AT(,,167,65),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,SYSTEM,GRAY,DOUBLE,AUTO
                       IMAGE('window.ico'),AT(2,2),USE(?Image1)
                       PROMPT('Concilia��o Banc�ria'),AT(26,5),USE(?Prompt1),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(2,23,163,2),USE(?Panel1)
                       PROMPT('Data de Concilia��o:'),AT(20,28),USE(?LOC:DataConciliacao:Prompt),TRN
                       ENTRY(@d06b),AT(87,28,60,10),USE(LOC:DataConciliacao),FLAT
                       PANEL,AT(2,41,163,2),USE(?Panel1:2)
                       BUTTON('&Confirmar'),AT(47,48,55,15),USE(?btnConfirma),TRN,FLAT,LEFT,ICON('gravar.ico'),DEFAULT
                       BUTTON('&Cancelar'),AT(108,48,55,15),USE(?btnCancelar),TRN,FLAT,LEFT,ICON('cancelar.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(LOC:DataConciliacao)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('WindowDataConciliacao')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?btnConfirma
      ThisWindow.Update
      Post(Event:CloseWindow)
    OF ?btnCancelar
      ThisWindow.Update
      Clear(LOC:DataConciliacao)
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

UpdateLancamentoAvulso PROCEDURE (<PAR:Id>)                ! Generated from procedure template - Window

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Valor            DECIMAL(12,2)                         !
qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
History::MOV:Record  LIKE(MOV:RECORD),THREAD
QuickWindow          WINDOW('i-Money'),AT(,,334,124),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,HLP('UpdateMovDocumentos'),SYSTEM,GRAY,DOUBLE,AUTO
                       IMAGE('window.ico'),AT(2,2),USE(?Image1)
                       PROMPT('Lan�amento Avulso'),AT(26,5),USE(?Prompt6),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(2,23,329,2),USE(?Panel1)
                       PROMPT('Plano:'),AT(4,25),USE(?MOV:idplano:Prompt)
                       ENTRY(@n-16b),AT(4,36,54,10),USE(MOV:idplano),FLAT,DECIMAL(12)
                       BUTTON('...'),AT(60,35,12,12),USE(?CallLookup),FLAT,ICON('busca.ico')
                       ENTRY(@s70),AT(76,36,255,10),USE(PLA:descricaoplano),SKIP,FLAT,MSG('Descri��o do lan�amento no plano de contas'),TIP('Descri��o do lan�amento no plano de contas'),READONLY
                       PROMPT('Descri��o:'),AT(4,50),USE(?MOV:descricao:Prompt),TRN
                       ENTRY(@s50),AT(4,60,327,10),USE(MOV:descricao),FLAT,MSG('Descri��o do movimento'),TIP('Descri��o do movimento'),REQ
                       PROMPT('Data:'),AT(4,73),USE(?MOV:d_datamovimentacao:Prompt),TRN
                       PROMPT('Valor:'),AT(81,73),USE(?MOV:valormovimento:Prompt),TRN
                       PROMPT('N<186> Doc. Pagamento:'),AT(166,73),USE(?MOV:numdocpagamento:Prompt),TRN,LEFT
                       ENTRY(@d06b),AT(4,82,73,10),USE(MOV:d_datamovimentacao),FLAT,MSG('Data'),TIP('Data'),REQ
                       ENTRY(@n13.`2),AT(81,82,80,10),USE(MOV:valormovimento),FLAT,DECIMAL(12),MSG('Valor do movimento'),TIP('Valor do movimento')
                       ENTRY(@s20),AT(166,82,80,10),USE(MOV:numdocpagamento),FLAT,RIGHT(2),MSG('N�mero do Documento do Pagamento (N<186> Cheque ou Tranfer�ncia Banc�ria)'),TIP('N�mero do Documento do Pagamento (N<186> Cheque ou Tranfer�ncia Banc�ria)'),UPR
                       PANEL,AT(2,95,329,2),USE(?Panel1:2)
                       BUTTON('&Gravar'),AT(214,104,55,15),USE(?btnGravar),TRN,FLAT,LEFT,MSG('Accept data and close the window'),TIP('Accept data and close the window'),ICON('gravar.ico'),DEFAULT
                       BUTTON('&Cancelar'),AT(274,104,55,15),USE(?btnCancelar),TRN,FLAT,LEFT,MSG('Cancel operation'),TIP('Cancel operation'),ICON('cancelar.ico')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Reset                  PROCEDURE(BYTE Force=0),DERIVED     ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
LimpaCampos routine
  clear(MOV:idplano)
  clear(PLA:descricaoplano)
  clear(MOV:descricao)
  clear(MOV:d_datamovimentacao)
  clear(MOV:valormovimento)
  clear(MOV:numdocpagamento)
  display

PegaCampos routine
  MOV:id = PAR:Id
  get(movdocumentos,MOV:mov_pk_id)
  PLA:id = MOV:idplano
  get(planocontas,PLA:pla_pk_id)
  display

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'Visualizando'
  OF InsertRecord
    ActionMessage = 'Incluindo'
  OF ChangeRecord
    GlobalErrors.Throw(Msg:UpdateIllegal)
    RETURN
  OF DeleteRecord
    GlobalErrors.Throw(Msg:DeleteIllegal)
    RETURN
  END
  QuickWindow{Prop:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateLancamentoAvulso')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(MOV:Record,History::MOV:Record)
  SELF.AddHistoryField(?MOV:idplano,3)
  SELF.AddHistoryField(?MOV:descricao,4)
  SELF.AddHistoryField(?MOV:d_datamovimentacao,5)
  SELF.AddHistoryField(?MOV:valormovimento,6)
  SELF.AddHistoryField(?MOV:numdocpagamento,8)
  SELF.AddUpdateFile(Access:movdocumentos)
  SELF.AddItem(?btnCancelar,RequestCancelled)              ! Add the cancel control to the window manager
  Relate:contas.SetOpenRelated()
  Relate:contas.Open                                       ! File contas used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:movdocumentos
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.DeleteAction = Delete:None                        ! Deletes not allowed
    SELF.ChangeAction = Change:None                        ! Changes not allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?btnGravar
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>UpdateLancamentoAvulso<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>UpdateLancamentoAvulso<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?MOV:idplano{PROP:ReadOnly} = True
    DISABLE(?CallLookup)
    ?PLA:descricaoplano{PROP:ReadOnly} = True
    ?MOV:descricao{PROP:ReadOnly} = True
    ?MOV:d_datamovimentacao{PROP:ReadOnly} = True
    ?MOV:valormovimento{PROP:ReadOnly} = True
    ?MOV:numdocpagamento{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:contas.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  PLA:id = MOV:idplano                                     ! Assign linking field value
  Access:planocontas.Fetch(PLA:pla_pk_id)
  PARENT.Reset(Force)


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    BrowsePlanoContas
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?btnGravar
      MOV:d_dataconciliacao = MOV:d_datamovimentacao
      MOV:idconta = CON:id
      
      SetNullFields(movdocumentos)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?MOV:idplano
      IF MOV:idplano OR ?MOV:idplano{Prop:Req}
        PLA:id = MOV:idplano
        IF Access:planocontas.TryFetch(PLA:pla_pk_id)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            MOV:idplano = PLA:id
          ELSE
            SELECT(?MOV:idplano)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      PLA:id = MOV:idplano
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        MOV:idplano = PLA:id
      END
      ThisWindow.Reset(1)
    OF ?MOV:valormovimento
      IF Access:movdocumentos.TryValidateField(6)          ! Attempt to validate MOV:valormovimento in movdocumentos
        SELECT(?MOV:valormovimento)
        QuickWindow{Prop:AcceptAll} = False
        CYCLE
      ELSE
        FieldColorQueue.Feq = ?MOV:valormovimento
        GET(FieldColorQueue, FieldColorQueue.Feq)
        IF ~ERRORCODE()
          ?MOV:valormovimento{Prop:FontColor} = FieldColorQueue.OldColor
          DELETE(FieldColorQueue)
        END
      END
    OF ?btnGravar
      ThisWindow.Update
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      MOV:tipobaixa = '4-Avulso'
      if ~PAR:ID
        Do LimpaCampos
      else
        Do PegaCampos
      end
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('UpdateLancamentoAvulso')
          End
        End
      End
      Close(HWGroups)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

UpdateTransferenciaContas PROCEDURE                        ! Generated from procedure template - Window

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
LOC:Valor            DECIMAL(12,2)                         !
LOC:ContaDebito      DECIMAL(15)                           !
LOC:DescContaDeb     CSTRING(51)                           !
LOC:BancoDeb         CSTRING(51)                           !
LOC:AgenciaDeb       CSTRING(11)                           !
LOC:NumContaDeb      CSTRING(21)                           !
LOC:PlanoDeb         DECIMAL(15)                           !
LOC:PlanoDescDeb     CSTRING(71)                           !
LOC:ContaCredito     DECIMAL(15)                           !
LOC:DescContaCred    CSTRING(51)                           !
LOC:BancoCred        CSTRING(51)                           !
LOC:AgenciaCred      CSTRING(11)                           !
LOC:NumContaCred     CSTRING(21)                           !
LOC:PlanoCred        DECIMAL(15)                           !
LOC:PlanoDescCred    CSTRING(71)                           !
qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
QuickWindow          WINDOW('i-Money'),AT(,,389,229),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,HLP('UpdateMovDocumentos'),SYSTEM,GRAY,DOUBLE,AUTO
                       IMAGE('window.ico'),AT(2,2),USE(?Image1)
                       PROMPT('Transfer�ncia entre Contas'),AT(26,5),USE(?Prompt6),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(2,23,383,2),USE(?Panel1)
                       GROUP('Conta a D�bitar'),AT(4,28,379,71),USE(?Group1),BOXED,TRN
                         PROMPT('Conta:'),AT(10,39),USE(?CON:id:Prompt),TRN
                         ENTRY(@n9.`0b),AT(10,48,54,10),USE(LOC:ContaDebito),FLAT,DECIMAL(12),MSG('Identifica��o da conta'),TIP('Identifica��o da conta'),REQ
                         BUTTON,AT(67,47,12,12),USE(?lkpContaDeb),TRN,FLAT,ICON('busca.ico')
                         ENTRY(@s50),AT(83,48,293,10),USE(LOC:DescContaDeb),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Descri��o da conta'),TIP('Descri��o da conta'),REQ,READONLY
                         ENTRY(@s50),AT(10,61,177,10),USE(LOC:BancoDeb),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Nome do banco'),TIP('Nome do banco'),REQ,READONLY
                         PROMPT('Ag�ncia:'),AT(190,61),USE(?CON:agenciabancaria:Prompt),TRN
                         ENTRY(@s10),AT(220,61,46,10),USE(LOC:AgenciaDeb),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Ag�ncia banc�ria, caso seja uma conta banc�ria'),TIP('Ag�ncia banc�ria, caso seja uma conta banc�ria'),READONLY
                         PROMPT('Conta:'),AT(269,61),USE(?CON:numeroconta:Prompt),TRN
                         ENTRY(@s20),AT(293,61,83,10),USE(LOC:NumContaDeb),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('N�mero da conta, caso seja uma ag�ncia banc�ria'),TIP('N�mero da conta, caso seja uma ag�ncia banc�ria'),READONLY
                         PROMPT('Plano:'),AT(10,73),USE(?MOV:idplano:Prompt),TRN
                         ENTRY(@n9.`0b),AT(10,82,54,10),USE(LOC:PlanoDeb),FLAT,DECIMAL(14),MSG('Identificador do plano de contas'),TIP('Identificador do plano de contas'),REQ
                         BUTTON,AT(67,81,12,12),USE(?lkpPlano),TRN,FLAT,ICON('busca.ico')
                         ENTRY(@s70),AT(83,82,293,10),USE(LOC:PlanoDescDeb),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Descri��o do lan�amento no plano de contas'),TIP('Descri��o do lan�amento no plano de contas'),REQ,READONLY
                       END
                       GROUP('Conta a Creditar'),AT(4,101,379,71),USE(?Group1:2),BOXED,TRN
                         PROMPT('Conta:'),AT(10,111),USE(?CON:id:Prompt:2),TRN
                         ENTRY(@n9.`0b),AT(10,120,54,10),USE(LOC:ContaCredito),FLAT,DECIMAL(12),MSG('Identifica��o da conta'),TIP('Identifica��o da conta'),REQ
                         BUTTON,AT(67,119,12,12),USE(?lkpContaCred),TRN,FLAT,ICON('busca.ico')
                         ENTRY(@s50),AT(84,120,293,10),USE(LOC:DescContaCred),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Descri��o da conta'),TIP('Descri��o da conta'),REQ,READONLY
                         ENTRY(@s50),AT(10,133,177,10),USE(LOC:BancoCred),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Nome do banco'),TIP('Nome do banco'),REQ,READONLY
                         PROMPT('Ag�ncia:'),AT(190,133),USE(?CON:agenciabancaria:Prompt:2),TRN
                         ENTRY(@s10),AT(220,133,46,10),USE(LOC:AgenciaCred),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Ag�ncia banc�ria, caso seja uma conta banc�ria'),TIP('Ag�ncia banc�ria, caso seja uma conta banc�ria'),READONLY
                         PROMPT('Conta:'),AT(270,133),USE(?CON:numeroconta:Prompt:2),TRN
                         ENTRY(@s20),AT(294,133,83,10),USE(LOC:NumContaCred),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('N�mero da conta, caso seja uma ag�ncia banc�ria'),TIP('N�mero da conta, caso seja uma ag�ncia banc�ria'),READONLY
                         PROMPT('Plano:'),AT(10,146),USE(?MOV:idplano:Prompt:2),TRN
                         ENTRY(@n9.`0b),AT(10,155,54,10),USE(LOC:PlanoCred),FLAT,DECIMAL(14),MSG('Identificador do plano de contas'),TIP('Identificador do plano de contas'),REQ
                         BUTTON,AT(67,154,12,12),USE(?lkpPlanoCred),TRN,FLAT,ICON('busca.ico')
                         ENTRY(@s70),AT(84,155,293,10),USE(LOC:PlanoDescCred),SKIP,FLAT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI),MSG('Descri��o do lan�amento no plano de contas'),TIP('Descri��o do lan�amento no plano de contas'),REQ,READONLY
                       END
                       PANEL,AT(2,176,383,2),USE(?Panel1:3)
                       PROMPT('Descri��o:'),AT(4,180),USE(?MOV:descricao:Prompt),TRN
                       PROMPT('Data:'),AT(262,180),USE(?MOV:d_datamovimentacao:Prompt),TRN
                       PROMPT('Valor:'),AT(318,180),USE(?MOV:valormovimento:Prompt),TRN
                       ENTRY(@s50),AT(4,190,252,10),USE(MOV:descricao),FLAT,SCROLL,MSG('Descri��o do movimento'),TIP('Descri��o do movimento'),REQ
                       ENTRY(@d06b),AT(262,190,51,10),USE(MOV:d_datamovimentacao),FLAT,MSG('Data'),TIP('Data'),REQ
                       ENTRY(@n13.`2),AT(318,190,66,10),USE(MOV:valormovimento),FLAT,DECIMAL(12),MSG('Valor do movimento'),TIP('Valor do movimento')
                       PANEL,AT(2,205,383,2),USE(?Panel1:2)
                       BUTTON('&Gravar'),AT(270,211,55,15),USE(?btnGravar),TRN,FLAT,LEFT,ICON('gravar.ico')
                       BUTTON('&Cancelar'),AT(328,211,55,15),USE(?btnCancelar),TRN,FLAT,LEFT,ICON('cancelar.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateTransferenciaContas')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:bancos.SetOpenRelated()
  Relate:bancos.Open                                       ! File bancos used by this procedure, so make sure it's RelationManager is open
  Access:contas.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:movdocumentos.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:planocontas.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>UpdateTransferenciaContas<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>UpdateTransferenciaContas<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?LOC:ContaDebito{PROP:ReadOnly} = True
    DISABLE(?lkpContaDeb)
    ?LOC:DescContaDeb{PROP:ReadOnly} = True
    ?LOC:BancoDeb{PROP:ReadOnly} = True
    ?LOC:AgenciaDeb{PROP:ReadOnly} = True
    ?LOC:NumContaDeb{PROP:ReadOnly} = True
    ?LOC:PlanoDeb{PROP:ReadOnly} = True
    DISABLE(?lkpPlano)
    ?LOC:PlanoDescDeb{PROP:ReadOnly} = True
    ?LOC:ContaCredito{PROP:ReadOnly} = True
    DISABLE(?lkpContaCred)
    ?LOC:DescContaCred{PROP:ReadOnly} = True
    ?LOC:BancoCred{PROP:ReadOnly} = True
    ?LOC:AgenciaCred{PROP:ReadOnly} = True
    ?LOC:NumContaCred{PROP:ReadOnly} = True
    ?LOC:PlanoCred{PROP:ReadOnly} = True
    DISABLE(?lkpPlanoCred)
    ?LOC:PlanoDescCred{PROP:ReadOnly} = True
    ?MOV:descricao{PROP:ReadOnly} = True
    ?MOV:d_datamovimentacao{PROP:ReadOnly} = True
    ?MOV:valormovimento{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:bancos.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      BrowseContas
      BrowsePlanoContas
      BrowseContas
      BrowsePlanoContas
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LOC:ContaDebito
      IF LOC:ContaDebito OR ?LOC:ContaDebito{Prop:Req}
        CON:id = LOC:ContaDebito
        IF Access:contas.TryFetch(CON:con_pk_id)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            LOC:ContaDebito = CON:id
            LOC:DescContaDeb = CON:descricaoconta
            LOC:AgenciaDeb = CON:agenciabancaria
            LOC:NumContaDeb = CON:numeroconta
          ELSE
            CLEAR(LOC:DescContaDeb)
            CLEAR(LOC:AgenciaDeb)
            CLEAR(LOC:NumContaDeb)
            SELECT(?LOC:ContaDebito)
            CYCLE
          END
        ELSE
          LOC:DescContaDeb = CON:descricaoconta
          LOC:AgenciaDeb = CON:agenciabancaria
          LOC:NumContaDeb = CON:numeroconta
        END
      END
      ThisWindow.Reset()
      If ~0{prop:acceptall}
        If CON:id
          BAN:numerobanco = CON:numerodobanco
          Get(bancos,BAN:ban_numerodobanco)
          LOC:BancoDeb = BAN:nomedobanco
        End
      End
    OF ?lkpContaDeb
      ThisWindow.Update
      CON:id = LOC:ContaDebito
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LOC:ContaDebito = CON:id
        LOC:DescContaDeb = CON:descricaoconta
        LOC:AgenciaDeb = CON:agenciabancaria
        LOC:NumContaDeb = CON:numeroconta
      END
      ThisWindow.Reset(1)
      If ~0{prop:acceptall}
        Post(Event:Accepted,?LOC:ContaDebito)
      End
    OF ?LOC:PlanoDeb
      IF LOC:PlanoDeb OR ?LOC:PlanoDeb{Prop:Req}
        PLA:id = LOC:PlanoDeb
        IF Access:planocontas.TryFetch(PLA:pla_pk_id)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            LOC:PlanoDeb = PLA:id
            LOC:PlanoDescDeb = PLA:descricaoplano
          ELSE
            CLEAR(LOC:PlanoDescDeb)
            SELECT(?LOC:PlanoDeb)
            CYCLE
          END
        ELSE
          LOC:PlanoDescDeb = PLA:descricaoplano
        END
      END
      ThisWindow.Reset()
      If PLA:tipolancamento = 'S' Or PLA:tipolancamento = 'C'
        Message('Tipo de conta inv�lida!','Aten��o',Icon:Asterisk,'OK',,2)
        Clear(PLA:Record)
        Clear(LOC:PlanoDeb)
        Clear(LOC:PlanoDescDeb)
        Select(?LOC:PlanoDeb)
      End
    OF ?lkpPlano
      ThisWindow.Update
      PLA:id = LOC:PlanoDeb
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        LOC:PlanoDeb = PLA:id
        LOC:PlanoDescDeb = PLA:descricaoplano
      END
      ThisWindow.Reset(1)
      If ~0{prop:acceptall}
        Post(Event:Accepted,?LOC:PlanoDeb)
      End
    OF ?LOC:ContaCredito
      IF LOC:ContaCredito OR ?LOC:ContaCredito{Prop:Req}
        CON:id = LOC:ContaCredito
        IF Access:contas.TryFetch(CON:con_pk_id)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            LOC:ContaCredito = CON:id
            LOC:DescContaCred = CON:descricaoconta
            LOC:AgenciaCred = CON:agenciabancaria
            LOC:NumContaCred = CON:numeroconta
          ELSE
            CLEAR(LOC:DescContaCred)
            CLEAR(LOC:AgenciaCred)
            CLEAR(LOC:NumContaCred)
            SELECT(?LOC:ContaCredito)
            CYCLE
          END
        ELSE
          LOC:DescContaCred = CON:descricaoconta
          LOC:AgenciaCred = CON:agenciabancaria
          LOC:NumContaCred = CON:numeroconta
        END
      END
      ThisWindow.Reset()
      If ~0{prop:acceptall}
        If CON:id
          BAN:numerobanco = CON:numerodobanco
          Get(bancos,BAN:ban_numerodobanco)
          LOC:BancoCred = BAN:nomedobanco
        End
      End
    OF ?lkpContaCred
      ThisWindow.Update
      CON:id = LOC:ContaCredito
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        LOC:ContaCredito = CON:id
        LOC:DescContaCred = CON:descricaoconta
        LOC:AgenciaCred = CON:agenciabancaria
        LOC:NumContaCred = CON:numeroconta
      END
      ThisWindow.Reset(1)
      If ~0{prop:acceptall}
        Post(Event:Accepted,?LOC:ContaCredito)
      End
    OF ?LOC:PlanoCred
      IF LOC:PlanoCred OR ?LOC:PlanoCred{Prop:Req}
        PLA:id = LOC:PlanoCred
        IF Access:planocontas.TryFetch(PLA:pla_pk_id)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            LOC:PlanoCred = PLA:id
            LOC:PlanoDescCred = PLA:descricaoplano
          ELSE
            CLEAR(LOC:PlanoDescCred)
            SELECT(?LOC:PlanoCred)
            CYCLE
          END
        ELSE
          LOC:PlanoDescCred = PLA:descricaoplano
        END
      END
      ThisWindow.Reset()
      If PLA:tipolancamento = 'S' Or PLA:tipolancamento = 'D'
        Message('Tipo de conta inv�lida!','Aten��o',Icon:Asterisk,'OK',,2)
        Clear(PLA:Record)
        Clear(LOC:PlanoCred)
        Clear(LOC:PlanoDescCred)
        Select(?LOC:PlanoCred)
      End
    OF ?lkpPlanoCred
      ThisWindow.Update
      PLA:id = LOC:PlanoCred
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        LOC:PlanoCred = PLA:id
        LOC:PlanoDescCred = PLA:descricaoplano
      END
      ThisWindow.Reset(1)
      If ~0{prop:acceptall}
        Post(Event:Accepted,?LOC:PlanoCred)
      End
    OF ?MOV:valormovimento
      IF Access:movdocumentos.TryValidateField(6)          ! Attempt to validate MOV:valormovimento in movdocumentos
        SELECT(?MOV:valormovimento)
        QuickWindow{Prop:AcceptAll} = False
        CYCLE
      ELSE
        FieldColorQueue.Feq = ?MOV:valormovimento
        GET(FieldColorQueue, FieldColorQueue.Feq)
        IF ~ERRORCODE()
          ?MOV:valormovimento{Prop:FontColor} = FieldColorQueue.OldColor
          DELETE(FieldColorQueue)
        END
      END
    OF ?btnGravar
      ThisWindow.Update
      Case Message('Este processo efetua a transfer�ncia entre contas, deseja continuar?','Aten��o',icon:question,'Sim|N�o',2,2)
      Of 1
        !Grava o d�bito
        MOV:idplano = LOC:PlanoDeb
        MOV:idconta = LOC:ContaDebito
        MOV:d_dataconciliacao = MOV:d_datamovimentacao
        MOV:tipobaixa = '4-Avulso'
        SetNullFields(movdocumentos)
        Access:movdocumentos.Insert()
        !
        !Grava o Cr�dito
        MOV:idplano = LOC:PlanoCred
        MOV:idconta = LOC:ContaCredito
        MOV:d_dataconciliacao = MOV:d_datamovimentacao
        MOV:tipobaixa = '4-Avulso'
        SetNullFields(movdocumentos)
        Access:movdocumentos.Insert()
        !
        Post(Event:CloseWindow)
      End
    OF ?btnCancelar
      ThisWindow.Update
      Case Message('Cancelar todas as altera��es e sair?','Aten��o',icon:question,'Sim|N�o',2,2)
      Of 1
        Post(Event:CloseWindow)
      End
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('UpdateTransferenciaContas')
          End
        End
      End
      Close(HWGroups)
    OF EVENT:OpenWindow
      Clear(MOV:Record)
      Clear(PLA:Record)
      Clear(CON:Record)
      Clear(BAN:Record)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

RelatExtratoConta PROCEDURE                                ! Generated from procedure template - Report

Progress:Thermometer BYTE                                  !
LOC:RazaoSocial      CSTRING(51)                           !
LOC:EnderecoBairro   CSTRING(51)                           !
LOC:CepTelefone      CSTRING(51)                           !
LOC:Data             DATE                                  !
LOC:Hora             STRING(20)                            !
LOC:DataInicial      DATE                                  !
LOC:DataFinal        DATE                                  !
Process:View         VIEW(movdocumentos)
                       PROJECT(MOV:id)
                     END
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
ProgressWindow       WINDOW('i-Money'),AT(,,273,27),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),HIDE,AT(239,1,15,12),RANGE(0,100)
                       PROMPT('Processando, aguarde!'),AT(57,6),USE(?Prompt1),TRN,FONT(,16,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                       BOX,AT(2,2,269,24),USE(?Box1),ROUND,COLOR(COLOR:Navy),FILL(COLOR:White)
                       STRING(''),AT(239,1,15,12),USE(?Progress:UserString),HIDE,CENTER
                       STRING(''),AT(239,1,15,12),USE(?Progress:PctText),HIDE,CENTER
                       BUTTON('Cancel'),AT(239,1,15,12),USE(?Progress:Cancel),HIDE
                     END

Report               REPORT,AT(240,1000,7750,10115),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',8,,,CHARSET:ANSI),THOUS
                       HEADER,AT(240,260,7750,729),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         BOX,AT(3219,83,4448,354),USE(?Box1:4),ROUND,COLOR(COLOR:Black),FILL(COLOR:Gray)
                         STRING('Extrato da Conta'),AT(3208,73,4427,281),USE(?String17),TRN,CENTER,FONT('Arial',18,,FONT:bold,CHARSET:ANSI)
                         BOX,AT(115,83,3052,635),USE(?Box1:2),ROUND,COLOR(COLOR:Black),FILL(COLOR:Gray)
                         BOX,AT(3198,52,4448,354),USE(?Box1:3),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(94,52,3052,635),USE(?Box1),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         STRING(@d06b),AT(5385,469),USE(LOC:DataInicial),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING(@d06b),AT(6729,469),USE(LOC:DataFinal),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING('at�'),AT(6500,469),USE(?String32),TRN,FONT('Verdana',8,,FONT:bold,CHARSET:ANSI)
                         STRING('De:'),AT(5156,469),USE(?String31),TRN,FONT('Verdana',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s46),AT(146,135,2979,177),USE(LOC:RazaoSocial),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s50),AT(146,323,2979,177),USE(LOC:EnderecoBairro),TRN,LEFT,FONT(,7,,)
                         STRING(@s50),AT(146,500,2979,177),USE(LOC:CepTelefone),TRN,LEFT,FONT(,7,,)
                       END
detail1                DETAIL,USE(?Detail1)
                         STRING(@n15.`0b),AT(146,104),USE(MOV:id),DECIMAL(12)
                       END
                       FOOTER,AT(240,7625,7750,333),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Data:'),AT(63,177),USE(?ReportDatePrompt),TRN,FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING(@d06b),AT(417,177),USE(LOC:Data),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING('Hora:'),AT(1458,177),USE(?ReportTimePrompt),TRN,FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING(@T01B),AT(1833,177),USE(LOC:Hora),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING('Usu�rio:'),AT(2510,10),USE(?String24),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s20),AT(2948,10),USE(GLO:nomeusuario),FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@pP�gina <.<<#p),AT(6948,73,729,146),PAGENO,USE(?PageCount),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Interfocus Tecnologia - (14) 3454-2681'),AT(63,10),USE(?String23),TRN,FONT('Times New Roman',8,,FONT:bold+FONT:italic,CHARSET:ANSI)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Previewer            PrintPreviewClass                     ! Print Previewer

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
CarregaExtratoConta Routine
  LOC:RazaoSocial    = 'TOCA IM�VEIS'
  LOC:EnderecoBairro = ''
  LOC:CepTelefone    = ''
  LOC:Data           = DataServidor()
  LOC:Hora           = HoraServidor()

  Print(RPT:Detail1)

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('RelatExtratoConta')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:movdocumentos.SetOpenRelated()
  Relate:movdocumentos.Open                                ! File movdocumentos used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ThisReport.Init(Process:View, Relate:movdocumentos, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:movdocumentos.SetQuickScan(1,Propagate:OneMany)
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom=True
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:movdocumentos.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  Do CarregaExtratoConta
  ReturnValue = PARENT.TakeRecord()
  IF 1 = 2
    PRINT(RPT:detail1)
  END
  RETURN ReturnValue

Conciliacao_LOTE PROCEDURE                                 ! Generated from procedure template - Window

Loc:DocInicial       LONG                                  !
Loc:DocFinal         LONG                                  !
Loc:DataConciliacao  LONG                                  !
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
fExtrato  File,Driver('ODBC','/TurboSql=1'),PRE(FEX),Name('consultasql'),Owner(GLO:conexao)
Record      Record
campo1        decimal(15)
campo2        date
campo3        cstring(51)
campo4        decimal(15,2)
campo5        cstring(2)
campo6        byte
campo7        cstring(21)
campo8        cstring(21)
            End
          End
Window               WINDOW('i-Money'),AT(,,206,145),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY
                       IMAGE('window.ico'),AT(3,3),USE(?Image1)
                       PROMPT('Concilia��o por lotes'),AT(27,6),USE(?Prompt1),FONT('Impact',16,,FONT:bold)
                       PANEL,AT(3,26,198,2),USE(?Panel1)
                       GROUP('Informe o N<186> Doc. Pagamento'),AT(21,31,163,36),USE(?Group1),BOXED
                         PROMPT('N<186> Inicial'),AT(41,42),USE(?Prompt2)
                         PROMPT('N<186> Final'),AT(114,42),USE(?Prompt2:2)
                         ENTRY(@n10b),AT(41,52,55,10),USE(Loc:DocInicial),FLAT,CENTER,REQ
                         ENTRY(@n10b),AT(114,52,55,10),USE(Loc:DocFinal),FLAT,CENTER,REQ
                       END
                       GROUP('Data para Concilia��o'),AT(21,74,163,36),USE(?Group2),BOXED
                         ENTRY(@d06b),AT(75,90,55,10),USE(Loc:DataConciliacao),FLAT,CENTER,REQ
                       END
                       PANEL,AT(4,119,198,2),USE(?Panel1:2)
                       BUTTON('&Conciliar Documentos'),AT(10,125,104,14),USE(?OkButton),FLAT,LEFT,ICON('gravar.ico'),DEFAULT
                       BUTTON('&Cancelar'),AT(142,125,53,14),USE(?CancelButton),FLAT,LEFT,ICON('cancelar.ico'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Conciliacao_LOTE')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Loc:DocFinal
      if Loc:DocFinal <= Loc:DocInicial
        message('O N� de Documento Final deve ser " M A I O R " que o Documento Inicial','Aten��o',icon:exclamation)
        clear(Loc:DocFinal)
      end
      display
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      if ~Loc:DocInicial or ~Loc:DocFinal or ~Loc:DataConciliacao
        message('Todos os campos da janela s�o requeridos!','Aten��o',icon:exclamation)
        cycle
      else
      
        Clear(FEX:Record)
        fExtrato{prop:sql} = |
          'update public.movdocumentos set d_dataconciliacao = <39>' & format(Loc:DataConciliacao,@d12) & '<39> where numdocpagamento BETWEEN <39>' & Loc:DocInicial & '<39> and <39>' & Loc:DocFinal & '<39>'
      
        !Message(fExtrato{prop:sql},,,,,2)
      
        message('Concilia��o realizada!','Aten��o',icon:exclamation)
      
        post(Event:CloseWindow)
      
      end
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

