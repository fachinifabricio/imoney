  MEMBER('imoney.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
IMONEBC1:DctInit    PROCEDURE
IMONEBC1:DctKill    PROCEDURE
IMONEBC1:FilesInit  PROCEDURE
  END

Hide:Access:cfopnf   CLASS(FileManager),TYPE               ! FileManager for cfopnf
Init                   PROCEDURE(),DERIVED                 ! Method added to host embed code
Kill                   PROCEDURE(),DERIVED                 ! Method added to host embed code
PrimeFields            PROCEDURE(),PROC,DERIVED            ! Method added to host embed code
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED ! Method added to host embed code
ValidateFieldServer    PROCEDURE(UNSIGNED Id,BYTE HandleErrors),BYTE,PROC,DERIVED ! Method added to host embed code
                     END


Hide:Relate:cfopnf   CLASS(RelationManager),TYPE           ! RelationManager for cfopnf
Init                   PROCEDURE
DeferedAddRelations    PROCEDURE(),DERIVED                 ! Method added to host embed code
Kill                   PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END

Hide:Access:aliasfavorecidos CLASS(FileManager),TYPE       ! FileManager for aliasfavorecidos
Init                   PROCEDURE(),DERIVED                 ! Method added to host embed code
Kill                   PROCEDURE(),DERIVED                 ! Method added to host embed code
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED ! Method added to host embed code
                     END


Hide:Relate:aliasfavorecidos CLASS(RelationManager),TYPE   ! RelationManager for aliasfavorecidos
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END

Hide:Access:aliasplanocontas CLASS(FileManager),TYPE       ! FileManager for aliasplanocontas
Init                   PROCEDURE(),DERIVED                 ! Method added to host embed code
Kill                   PROCEDURE(),DERIVED                 ! Method added to host embed code
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED ! Method added to host embed code
                     END


Hide:Relate:aliasplanocontas CLASS(RelationManager),TYPE   ! RelationManager for aliasplanocontas
Init                   PROCEDURE
DeferedAddRelations    PROCEDURE(),DERIVED                 ! Method added to host embed code
Kill                   PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END


_Hide:Access:cfopnf  &Hide:Access:cfopnf,AUTO,THREAD
_Hide:Relate:cfopnf  &Hide:Relate:cfopnf,AUTO,THREAD
_Hide:Access:aliasfavorecidos &Hide:Access:aliasfavorecidos,AUTO,THREAD
_Hide:Relate:aliasfavorecidos &Hide:Relate:aliasfavorecidos,AUTO,THREAD
_Hide:Access:aliasplanocontas &Hide:Access:aliasplanocontas,AUTO,THREAD
_Hide:Relate:aliasplanocontas &Hide:Relate:aliasplanocontas,AUTO,THREAD


IMONEBC1:DctInit PROCEDURE
  CODE
  _Hide:Access:cfopnf &= NEW(Hide:Access:cfopnf)
  _Hide:Relate:cfopnf &= NEW(Hide:Relate:cfopnf)
  _Hide:Access:aliasfavorecidos &= NEW(Hide:Access:aliasfavorecidos)
  _Hide:Relate:aliasfavorecidos &= NEW(Hide:Relate:aliasfavorecidos)
  _Hide:Access:aliasplanocontas &= NEW(Hide:Access:aliasplanocontas)
  _Hide:Relate:aliasplanocontas &= NEW(Hide:Relate:aliasplanocontas)
  Relate:cfopnf &= _Hide:Relate:cfopnf
  Relate:aliasfavorecidos &= _Hide:Relate:aliasfavorecidos
  Relate:aliasplanocontas &= _Hide:Relate:aliasplanocontas


IMONEBC1:FilesInit PROCEDURE
  CODE
  _Hide:Relate:cfopnf.Init
  _Hide:Relate:aliasfavorecidos.Init
  _Hide:Relate:aliasplanocontas.Init


IMONEBC1:DctKill PROCEDURE
  CODE
  _Hide:Relate:cfopnf.Kill
  DISPOSE(_Hide:Relate:cfopnf)
  _Hide:Relate:aliasfavorecidos.Kill
  DISPOSE(_Hide:Relate:aliasfavorecidos)
  _Hide:Relate:aliasplanocontas.Kill
  DISPOSE(_Hide:Relate:aliasplanocontas)


Hide:Relate:cfopnf.Init PROCEDURE
  CODE
  _Hide:Access:cfopnf.Init
  SELF.Init(Access:cfopnf,1)


Hide:Access:cfopnf.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= CFO:Record
  SELF.FileNameValue = 'cfopnf'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= cfopnf
  PARENT.Init
  Access:cfopnf &= SELF


Hide:Access:cfopnf.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:cfopnf &= NULL                                    ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:cfopnf.PrimeFields PROCEDURE

  CODE
  CFO:tipo = 2                                             ! Assign initial field value
  PARENT.PrimeFields


Hide:Access:cfopnf.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(cfopnf,GlobalErrors)
    SELF.Create = 0
    SELF.LockRecover = 10
    SELF.AddKey(CFO:cfo_pk_id,'id',0)
    SELF.AddKey(CFO:cfo_codcfop,'codcfop',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Access:cfopnf.ValidateFieldServer PROCEDURE(UNSIGNED Id,BYTE HandleErrors)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateFieldServer(Id,HandleErrors)
  CASE Id
  OF 4
    GlobalErrors.SetField('Tipo de CFOP (1-Entrada 2-Sa�da)')
    IF INSTRING(']' & CFO:tipo & '[', ']1[]2[', 1, 1) = 0
      ReturnValue = Level:Notify
    END
    IF ReturnValue <> Level:Benign
      IF HandleErrors
        ReturnValue = GlobalErrors.ThrowMessage(Msg:FieldNotInList,'''Entrada'',''Sa�da''')
      END
    END
  END
  RETURN ReturnValue


Hide:Relate:cfopnf.DeferedAddRelations PROCEDURE

  CODE
  DO AddRelations_1
  
  PARENT.DeferedAddRelations

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:notasfiscais,RI:CASCADE_SERVER,RI:RESTRICT_SERVER,NFS:nfs_fk_cfop)
  SELF.AddRelationLink(CFO:id,NFS:idcfop)


Hide:Relate:cfopnf.Kill PROCEDURE

  CODE
  _Hide:Access:cfopnf.Kill                                 ! Kill the file manager
  PARENT.Kill
  Relate:cfopnf &= NULL                                    ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:cfopnf)                             ! destroy the file manager


Hide:Relate:aliasfavorecidos.Init PROCEDURE
  CODE
  _Hide:Access:aliasfavorecidos.Init
  SELF.Init(Access:aliasfavorecidos,1)
  SELF.SetAlias(Relate:favorecidos)


Hide:Access:aliasfavorecidos.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= AFV:Record
  SELF.AliasedFile &= Access:favorecidos                   !This is a File Alias, so assign aliased file manager
  SELF.FileNameValue = 'aliasfavorecidos'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= aliasfavorecidos
  PARENT.Init
  Access:aliasfavorecidos &= SELF


Hide:Access:aliasfavorecidos.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:aliasfavorecidos &= NULL                          ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:aliasfavorecidos.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(aliasfavorecidos,GlobalErrors)
    SELF.Create = 0
    SELF.LockRecover = 10
    SELF.AddKey(AFV:cli_pk_id,'id',0)
    SELF.AddKey(AFV:cli_razaosocial,'razaosocial',0)
    SELF.AddKey(AFV:sk_nomefantasia,'Nome Fantasia',0)
    SELF.AddKey(AFV:cli_tipocliente,'tipocliente',0)
    SELF.AddKey(AFV:cli_usuario,'usuario',0)
    SELF.AddKey(AFV:cli_cadastro,'d_datacadastro e t_horacadastro',0)
    SELF.AddKey(AFV:fav_cpf_cnpj,'cpf_cnpj',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:aliasfavorecidos.Kill PROCEDURE

  CODE
  _Hide:Access:aliasfavorecidos.Kill                       ! Kill the file manager
  PARENT.Kill
  Relate:aliasfavorecidos &= NULL                          ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:aliasfavorecidos)                   ! destroy the file manager


Hide:Relate:aliasplanocontas.Init PROCEDURE
  CODE
  _Hide:Access:aliasplanocontas.Init
  SELF.Init(Access:aliasplanocontas,1)
  SELF.SetAlias(Relate:planocontas)


Hide:Access:aliasplanocontas.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= APC:Record
  SELF.AliasedFile &= Access:planocontas                   !This is a File Alias, so assign aliased file manager
  SELF.FileNameValue = 'aliasplanocontas'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= aliasplanocontas
  PARENT.Init
  Access:aliasplanocontas &= SELF


Hide:Access:aliasplanocontas.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:aliasplanocontas &= NULL                          ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:aliasplanocontas.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(aliasplanocontas,GlobalErrors)
    SELF.Create = 0
    SELF.LockRecover = 10
    SELF.AddKey(APC:pla_pk_id,'id',0)
    SELF.AddKey(APC:pla_descricao,'descricaoplano',0)
    SELF.AddKey(APC:pla_fk_idpai,'idpai',0)
    SELF.AddKey(APC:pla_tipolancamento,'tipolancamento',0)
    SELF.AddKey(APC:pla_usuario,'usuario',0)
    SELF.AddKey(APC:pla_cadastro,'d_datacadastro e t_horacadastro',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:aliasplanocontas.DeferedAddRelations PROCEDURE

  CODE
  DO AddRelations_1
  
  PARENT.DeferedAddRelations

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:planocontas,RI:CASCADE_SERVER,RI:RESTRICT_SERVER,PLA:pla_fk_idpai)
  SELF.AddRelationLink(APC:id,PLA:idpai)


Hide:Relate:aliasplanocontas.Kill PROCEDURE

  CODE
  _Hide:Access:aliasplanocontas.Kill                       ! Kill the file manager
  PARENT.Kill
  Relate:aliasplanocontas &= NULL                          ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:aliasplanocontas)                   ! destroy the file manager

