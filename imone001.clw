

   MEMBER('imoney.clw')                                    ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('IMONE001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('IMONE002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE005.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE006.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE008.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE009.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE010.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE012.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE013.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE014.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE015.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('IMONE017.INC'),ONCE        !Req'd for module callout resolution
                     END


WindowParametros PROCEDURE                                 ! Generated from procedure template - Window

LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
Window               WINDOW('i-Money'),AT(,,423,209),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('principal.ico'),SYSTEM,GRAY,DOUBLE,AUTO
                       IMAGE('window.ico'),AT(1,0),USE(?Image1)
                       PROMPT('Par�metros'),AT(26,2),USE(?Prompt1),TRN,FONT('Impact',16,COLOR:Black,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(1,19,256,2),USE(?Panel1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('WindowParametros')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Principal PROCEDURE                                        ! Generated from procedure template - Window

qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
Window               WINDOW('i-Money 1.1'),AT(,,509,368),COLOR(COLOR:White),CENTER,IMM,ICON('principal.ico'),WALLPAPER('FundoNovo.jpg'),SYSTEM,GRAY,MAX,MAXIMIZE,DOUBLE,AUTO
                       REGION,AT(0,0,66,342),USE(?OutlookBar),IMM
                       ENTRY(@n05),AT(70,3,53,15),USE(GLO:CodEmpresa),SKIP,FLAT,CENTER,FONT(,16,COLOR:Black,FONT:bold,CHARSET:ANSI),COLOR(0D0D0A2H),READONLY
                       ENTRY(@s70),AT(126,3,229,15),USE(GLO:NomeEmpresa),SKIP,FLAT,FONT(,16,COLOR:Black,FONT:bold,CHARSET:ANSI),COLOR(0D0D0A2H),READONLY
                       BUTTON('Pend�ncias'),AT(404,4,61,14),USE(?btnPendencias),TRN,FLAT,LEFT,ICON('dolar.ico')
                       BUTTON,AT(356,4,14,14),USE(?btnTrocaEmpresa),TRN,FLAT,ICON('Lupazinha.ico')
                       PANEL,AT(-11,-6,1500,29),USE(?Panel1),FILL(0808040H),BEVEL(-1)
                     END
OutlookBar1        Class(POOutlookBarClass)
ExecuteAction       Procedure(Long HeaderID, Long TaskID, Long MouseBtn, Long Feq),VIRTUAL

Cadastros                              LONG
Cadastros:Empresa                      LONG       
Cadastros:Bancos                       LONG       
Cadastros:Centro_de_Custo              LONG       
Cadastros:Contas                       LONG       
Cadastros:Favorecidos                  LONG       
Cadastros:Plano_de_Contas              LONG       
Configuracoes                          LONG
Configuraes:Alterar_Senha              LONG       
Configuracoes:Parametros               LONG       
Lancamentos                            LONG
Lancamentos:Documentos                 LONG       
Lancamentos:Extrato                    LONG       
Lancamentos:Transferencias             LONG       
Relatrios                              LONG
Relatrios:Centro_de_Custo              LONG       
Relatrios:Documentos_a_Pagar           LONG       
Relatrios:Fluxo_de_Caixa               LONG       
Relatrios:Plano_de_Contas              LONG       
Relatrios:Saldo_a_pagar                LONG       
Grficos                                LONG
Grficos:Receitas_e_Despesas            LONG       
Grficos:Plano_de_Contas                LONG       
Sair                                   LONG
Sair:Sair                              LONG       
                End



ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Open                   PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
UDF                  CLASS
UniResolucaoVideo      PROCEDURE(),String,Virtual
                     END

  CODE
  GlobalResponse = ThisWindow.Run()    ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
rtnAlertas routine
  data
fAlertas  File,Driver('ODBC','/TurboSql=1'),PRE(FAL),Name('consultasql'),Owner(GLO:conexao)
Record      Record
quantidade    long,name('campo1')
            End
          End
  code
    !Abre o arquivo
    open(fAlertas,42h)
    !
    clear(FAL:Record)
    fAlertas{prop:sql} = |
      'select count(*) from public.documentos d '&|
      'join public.planocontas pl on pl.id = d.idplano '&|
      'join public.favorecidos fav on fav.id = d.idcliente '&|
      'where d.d_datavencimento <= current_date and d.d_datapagamento is null'
    next(fAlertas)
    if FAL:quantidade > 0
      WindowAvisoDocumentos
    end
    close(fAlertas)
OutlookBar1.ExecuteAction   Procedure(Long HeaderID, Long TaskID, Long MouseBtn, Long Feq)
    Code

    Case HeaderID 
    Of Self.Cadastros
        Case TaskID
        Of 0
            Self.SetExpanded(HeaderID, True)         

        Of Self.Cadastros:Empresa

            BrowseEmpresas()
        Of Self.Cadastros:Bancos

            BrowseBancos()
        Of Self.Cadastros:Centro_de_Custo

            BrowseCentroDeCusto()
        Of Self.Cadastros:Contas

            BrowseContas()
        Of Self.Cadastros:Favorecidos

            BrowseFavorecidos()
        Of Self.Cadastros:Plano_de_Contas

            BrowsePlanoContas()
        End
    Of Self.Configuracoes
        Case TaskID
        Of 0
            Self.SetExpanded(HeaderID, True)         

        Of Self.Configuraes:Alterar_Senha

            WindowChangePassword()
        Of Self.Configuracoes:Parametros

            WindowParametros()
        End
    Of Self.Lancamentos
        Case TaskID
        Of 0
            Self.SetExpanded(HeaderID, True)         

        Of Self.Lancamentos:Documentos

            BrowseDocumentos()
        Of Self.Lancamentos:Extrato

            BrowseExtratoConta()
        Of Self.Lancamentos:Transferencias

            UpdateTransferenciaContas()
        End
    Of Self.Relatrios
        Case TaskID
        Of 0
            Self.SetExpanded(HeaderID, True)         

        Of Self.Relatrios:Centro_de_Custo

            RelatCentroDeCusto()
        Of Self.Relatrios:Documentos_a_Pagar

            RelatDocumentosPagar()
        Of Self.Relatrios:Fluxo_de_Caixa

            Relat_FluxoCaixa()
        Of Self.Relatrios:Plano_de_Contas

            RelatPlanoContas()
        Of Self.Relatrios:Saldo_a_pagar

            RelatPlanoSaldoAPagar()
        End
    Of Self.Grficos
        Case TaskID
        Of 0
            Self.SetExpanded(HeaderID, True)         

        Of Self.Grficos:Receitas_e_Despesas

            grafico_Receitas_Despesas()
        Of Self.Grficos:Plano_de_Contas

            grafico_Plano_Contas()
        End
    Of Self.Sair
        Case TaskID
        Of 0
            Self.SetExpanded(HeaderID, True)         

        Of Self.Sair:Sair

            Post(EVENT:CloseWindow, , )
        End
    End

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Principal')
  SELF.Request = GlobalRequest         ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?OutlookBar
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors          ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                 ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(Window)                    ! Open window
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>Principal<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>Principal<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Open PROCEDURE

  CODE
  OutlookBar1.Cadastros = OutlookBar1.AddHeader('Cadastros', 0, )
  OutlookBar1.Cadastros:Empresa = OutlookBar1.AddTask('Empresa', 0, False, 'Empresa.ico')
  OutlookBar1.Cadastros:Bancos = OutlookBar1.AddTask('Bancos', 0, False, 'Bancos.ico')
  OutlookBar1.Cadastros:Centro_de_Custo = OutlookBar1.AddTask('Centro de Custo', 0, False, 'CentroDeCusto.ico')
  OutlookBar1.Cadastros:Contas = OutlookBar1.AddTask('Contas', 0, False, 'contas.ico')
  OutlookBar1.Cadastros:Favorecidos = OutlookBar1.AddTask('Clientes e Fornecedores', 0, False, 'favorecidos.ico')
  OutlookBar1.Cadastros:Plano_de_Contas = OutlookBar1.AddTask('Plano de Contas', 0, False, 'plano.ico')
  OutlookBar1.Configuracoes = OutlookBar1.AddHeader('Configura��es', 0, )
  OutlookBar1.Configuraes:Alterar_Senha = OutlookBar1.AddTask('Alterar Senha', 0, False, 'seguranca.ico')
  OutlookBar1.Configuracoes:Parametros = OutlookBar1.AddTask('Par�metros', 0, False, 'configuracoes.ico')
  OutlookBar1.SetVisible(False)
  OutlookBar1.SetEnabled(False)
  OutlookBar1.Lancamentos = OutlookBar1.AddHeader('Lan�amentos', 0, )
  OutlookBar1.Lancamentos:Documentos = OutlookBar1.AddTask('Documentos', 0, False, 'documentos.ico')
  OutlookBar1.Lancamentos:Extrato = OutlookBar1.AddTask('Extrato', 0, False, 'extrato.ico')
  OutlookBar1.Lancamentos:Transferencias = OutlookBar1.AddTask('Transfer�ncias', 0, False, 'transferencia.ico')
  OutlookBar1.Relatrios = OutlookBar1.AddHeader('Relat�rios', 0, )
  OutlookBar1.Relatrios:Centro_de_Custo = OutlookBar1.AddTask('Centros de Custo', 0, False, 'CentroDeCusto.ico')
  OutlookBar1.Relatrios:Documentos_a_Pagar = OutlookBar1.AddTask('Documentos', 0, False, 'documentos.ico')
  OutlookBar1.Relatrios:Fluxo_de_Caixa = OutlookBar1.AddTask('Fluxo de Caixa', 0, False, 'money.ico')
  OutlookBar1.Relatrios:Plano_de_Contas = OutlookBar1.AddTask('Plano de Contas', 0, False, 'plano.ico')
  OutlookBar1.Relatrios:Saldo_a_pagar = OutlookBar1.AddTask('Saldo da Conta', 0, False, 'imprimir.ico')
  OutlookBar1.SetVisible(False)
  OutlookBar1.SetEnabled(False)
  OutlookBar1.Grficos = OutlookBar1.AddHeader('Gr�ficos', 0, )
  OutlookBar1.Grficos:Receitas_e_Despesas = OutlookBar1.AddTask('Receitas e Despesas', 0, False, 'faturado.ico')
  OutlookBar1.Grficos:Plano_de_Contas = OutlookBar1.AddTask('Plano de Contas', 0, False, 'plano.ico')
  OutlookBar1.Sair = OutlookBar1.AddHeader('Sair', 0, )
  OutlookBar1.Sair:Sair = OutlookBar1.AddTask('Sair', 0, False, 'fechar.ico')
  OutlookBar1.SetFont('Tahoma')
  OutlookBar1.SetBackgroundColors(9405237, 15265493, 9405237)
  OutlookBar1.SetHeaderColors(65535, 15265493, 9405237, 16777215, 9405237)
  OutlookBar1.SetHoveredHeaderColors(-1502252229, 9405237, 15265493, 0, 9405237)
  OutlookBar1.SetTaskColors(0, 16777215, 16777215, 16777215, 9405237)
  OutlookBar1.SetAppName('imoney.exe')
  OutlookBar1.Init(?OutlookBar, PSTYLE_OUTLOOK2003, 0, 0)
  PARENT.Open


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                 ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?btnPendencias
      ThisWindow.Update
      WindowAvisoDocumentos()
      ThisWindow.Reset
    OF ?btnTrocaEmpresa
      ThisWindow.Update
      winSelecionarEmpresa()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                 ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                 ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      !Testa a Vertical do Monitor, aumentando o tamanho do Painel de Bot�es
      video" = UDF.UniResolucaoVideo()
      res# = video"[Instring('x',video",1,1)+1:10]
      If Res# = 800
        ?OutlookBar{prop:height} = 370
      ElsIf Res# = 768
        ?OutlookBar{prop:height} = 354
      ElsIf Res# = 600
        ?OutlookBar{prop:height} = 270
      ElsIf Res# = 720
        ?OutlookBar{prop:height} = 330
      ElsIf Res# = 1024
        ?OutlookBar{prop:height} = 470
      End
      
      !Verfica Alertas
      do rtnAlertas
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('Principal')
          End
        End
      End
      Close(HWGroups)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

UDF.UniResolucaoVideo PROCEDURE()
_Loc:HRes    LONG
_Loc:VRes    LONG
_Loc:HWnd    LONG
_Loc:Retorna String(20)
   Code

   _Loc:HWnd    = GetDC(0)
   _Loc:HRes    = GetDeviceCaps(_Loc:HWnd,8)
   _Loc:VRes    = GetDeviceCaps(_Loc:HWnd,10)

   _Loc:Retorna = Clip(_Loc:HRes) & 'x' & Clip(_Loc:VRes)

   Return _Loc:Retorna
