

   MEMBER('imoney.clw')                                    ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('IMONE015.INC'),ONCE        !Local module procedure declarations
                     END


grafico_Receitas_Despesas PROCEDURE                        ! Generated from procedure template - Window

loc:data_inicial     LONG                                  !
LOC:tipodata         BYTE(1)                               !
loc:data_final       LONG                                  !
qValores             QUEUE,PRE(queue)                      !
referencia           LONG                                  !
receitas             DECIMAL(10,2)                         !
despesas             DECIMAL(10,2)                         !
                     END                                   !
ClickedOnPointName   STRING(255)                           !
ClickedOnPointNumber REAL                                  !
ClickedOnSetNumber   LONG                                  !
LOC:DocImanager      BYTE                                  !Relacionar Documentos do i-MANAGER
LOC:CampoValor       CSTRING(31)                           !
LOC:CampoData        CSTRING(31)                           !
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
  !ChartNotHere=0 - NoCharts=0 - Family=abc - q1
ThisGraph1    Class(GlobalInsight)
ValidateRecord  PROCEDURE(long graphID),Long ,VIRTUAL
SetPointName    PROCEDURE (Long graphSet),String ,VIRTUAL
Reset           PROCEDURE (Byte graphForce=0),VIRTUAL
              End
ThisGraph1:Popup  Class(PopupClass)
               End
ThisGraph1:ClickedOnPointName   String(255)
ThisGraph1:ClickedOnPointNumber Real
ThisGraph1:ClickedOnSetNumber   Long
ThisGraph1:Color                Group(iColorGroupType), PRE(ThisGraph1:Color)
                                End
ThisGraph1:FileName              String(255)
!-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
fDocumentos FILE,DRIVER('ODBC','/Turbosql=1'),OWNER(GLO:conexao),PRE(DOC),Name('consultasql')
Record      Record
campo1      cstring(25)     ! M�s Refer�ncia
campo2      decimal(13,2)   ! Valor
campo3      cstring(25)     ! Tipo
            End
        End
Window               WINDOW('Receitas e Despesas'),AT(,,621,322),FONT('MS Sans Serif',8,,FONT:regular),CENTER,ICON('principal.ico'),GRAY,IMM
                       GROUP('Per�odo'),AT(159,2,303,37),USE(?Group1),BOXED,FONT(,,COLOR:Blue,,CHARSET:ANSI)
                         ENTRY(@d014b),AT(185,13,62,10),USE(loc:data_inicial),FLAT,RIGHT(1)
                         ENTRY(@d014b),AT(270,13,62,10),USE(loc:data_final),FLAT,RIGHT
                         LIST,AT(357,13,74,10),USE(LOC:tipodata),FLAT,DROP(5),FROM('Vencimento|#1|Previs�o|#2|Emiss�o|#3|Pagamento|#4')
                         CHECK('Relacionar Documentos do i-MANAGER'),AT(171,26),USE(LOC:DocImanager),FONT(,,,FONT:bold,CHARSET:ANSI),MSG('Relacionar Documentos do i-MANAGER'),TIP('Relacionar Documentos do i-MANAGER')
                         BUTTON,AT(435,10,20,15),USE(?Button1),FLAT,LEFT,ICON('selecionar.ico')
                         PROMPT('De:'),AT(171,13),USE(?loc:data_inicial:Prompt),TRN
                         PROMPT('At�:'),AT(255,13),USE(?Prompt1:2),TRN
                         PROMPT('Tipo:'),AT(337,13),USE(?LOC:tipodata:Prompt),TRN
                       END
                       REGION,AT(5,43,611,256),USE(?Insight),IMM,BEVEL(1,-1)
                       STRING('Gerando gr�fico, aguarde!'),AT(141,162,340,26),USE(?strMensagem),HIDE,CENTER,FONT('Arial',24,COLOR:Navy,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       BUTTON('&Fechar'),AT(559,303,55,14),USE(?Button2),FLAT,LEFT,ICON('fechar.ico'),STD(STD:Close)
                     END
  !ChartNotHere=0 - NoCharts=0 - Family=abc - q2

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Open                   PROCEDURE(),DERIVED                 ! Method added to host embed code
Reset                  PROCEDURE(BYTE Force=0),DERIVED     ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
DocIManager routine
  clear(fDocumentos)

  fDocumentos{Prop:Sql} = |
    'select t.mesref, t.valor, t.tipo '&|
    'from ( '&|
      'select to_char('&clip(LOC:CampoData)&',<39>MM/YYYY<39>) as mesref, '&|
      'to_char('&clip(LOC:CampoData)&',<39>YYYY-MM-01<39>)::date as ordem, '&|
      '<39>C<39> as tipo, '&LOC:CampoValor&' as valor '&|
      'from public.fun_carrega_view_docreceber_imanager dc '&|
      'where '&clip(LOC:CampoData)&' between <39>'&format(loc:data_inicial,@d012)&'<39> and <39>'&format(loc:data_final,@d012)&'<39> '&|
      'group by mesref, ordem, tipo '&|
      'order by ordem, tipo '&|
    ') as t'

  !message(fDocumentos{Prop:Sql},,,,,2)
  loop
    next(fDocumentos)
    if error() then break end

    clear(qValores)

    queue:referencia = date(sub(Doc:Campo1,1,2),1,sub(Doc:Campo1,4,4))
    get(qValores, queue:referencia)
    if error() then
      if Doc:Campo3 = 'C'
        queue:receitas += Doc:Campo2
      elsif Doc:Campo3 = 'D'
        queue:despesas += Doc:Campo2
      end
      add(qValores)
    else
      if Doc:Campo3 = 'C'
        queue:receitas += Doc:Campo2
      elsif Doc:Campo3 = 'D'
        queue:despesas += Doc:Campo2
      end
      put(qValores)
    end
  end

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('grafico_Receitas_Despesas')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?loc:data_inicial
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
    ! Start Of Insight InitStuff Group
  Bind('ThisGraph1:ClickedOnPointNumber',ThisGraph1:ClickedOnPointNumber) ! Insight ClickedOnVariable 
  Bind('ThisGraph1:ClickedOnPointName',ThisGraph1:ClickedOnPointName)       ! Insight ClickedOnVariable 
  Bind('ThisGraph1:ClickedOnSetNumber',ThisGraph1:ClickedOnSetNumber)     ! Insight ClickedOnVariable 
      ! loc:data_inicial - LONG -
      ! LOC:tipodata - BYTE(1) -
      ! loc:data_final - LONG -
      ! ClickedOnPointName - STRING(255) -
      ! ClickedOnPointNumber - REAL -
      ! ClickedOnSetNumber - LONG -
      ! LOC:DocImanager - BYTE -
      ! LOC:CampoValor - CSTRING(31) -
      ! LOC:CampoData - CSTRING(31) -
  if ThisGraph1.Init(?Insight,Insight:Bar).
  if not ThisGraph1:Color:Fetched then ThisGraph1.GetWindowsColors(ThisGraph1:Color).
  Window{prop:buffer} = 1
  ThisGraph1.LegendPosition = 2
  ThisGraph1.LegendText = 1
  ThisGraph1.LegendValue = 1
  ThisGraph1.LegendPercent = 1
  ThisGraph1.Stacked=0
  ThisGraph1.BlackAndWhite=0
  ThisGraph1.BackgroundPicture=''
  ThisGraph1.BackgroundColor=Color:white
  ThisGraph1.BackgroundShadeColor=Color:None
  ThisGraph1.BorderColor=Color:Black
  ThisGraph1.MinPointWidth=0
  ThisGraph1.PaperZoom=4
  ThisGraph1.ActiveInvisible=0
  ThisGraph1.PrintPortrait=0
  ThisGraph1.RightBorder=10
  ThisGraph1.MaxXGridTicks=16
  ThisGraph1.MaxYGridTicks=16
  ThisGraph1.WorkSpaceWidth=0
  ThisGraph1.WorkSpaceHeight=0
  ThisGraph1.AutoShade=1
  ThisGraph1.TopShade=0
  ThisGraph1.RightShade=0
  ThisGraph1.LongShade=0
  ThisGraph1.Pattern=Insight:None
  ThisGraph1.Float=0
  ThisGraph1.ZCluster=0
  ThisGraph1.Fill=0
  ThisGraph1.FillToZero=0
  ThisGraph1.SquareWave=0
  ThisGraph1.InnerRadius=0
  ThisGraph1.MaxPieRadius=0
  ThisGraph1.PieAngle=0
  ThisGraph1.PieLabelLineColor=Color:None
  ThisGraph1.AspectRatio=1
  ThisGraph1.PieLabelLines=0
  ThisGraph1.Shape=Insight:Auto
  ThisGraph1.LineWidth = 1
      ! Fonts
  ThisGraph1.HeaderFontStyle    = FONT:BOLD
  ThisGraph1.LegendFontStyle    = FONT:BOLD
  ThisGraph1.LegendAngle   = 0
  ThisGraph1.XFontStyle    = FONT:BOLD
  ThisGraph1.XNameFontStyle    = FONT:BOLD
  ThisGraph1.YFontStyle    = FONT:BOLD
  ThisGraph1.YNameFontStyle    = FONT:BOLD
  ThisGraph1.DataFontStyle    = FONT:BOLD
  ThisGraph1.ShowXLabelsEvery = 1
  ThisGraph1.DisplayOnX = 2   ! 0= don't display, 1= display number, 2= display name.
  ThisGraph1.AutoXLabels = 0
  ThisGraph1.SeparateYAxis = 0
  ThisGraph1.SpreadXLabels       = 0
  ThisGraph1.AutoXGridTicks = 1
  ThisGraph1.AutoScale = Scale:Low + Scale:High + Scale:Zero
  ThisGraph1.YFormat = '@n10`2'
  ThisGraph1.ShowDataLabels = 1
  ThisGraph1.DataLabelFormat = '@n14`2'
  ThisGraph1.ShowDataLabelsEvery = 1
  ThisGraph1.ColorDataLabels = True
  ThisGraph1.Depth = 0
          
  ThisGraph1.AddItem(1,1,qValores,Insight:GraphField,queue:receitas,,,,,,)
  ThisGraph1.SetSetYAxis(1,Scale:Low + Scale:High + Scale:Zero,,,)
  ThisGraph1.SetSetColors(1,-1,32768,-1,16777215)
  ThisGraph1.SetSetType(1,Insight:None)
  If ThisGraph1.GetSet(1) = 0 then ThisGraph1.AddSetQ(1).
  ThisGraph1.SetQ.SquareWave = -1
  ThisGraph1.SetQ.Fill       = -1
  ThisGraph1.SetQ.FillToZero = -1
  ThisGraph1.SetQ.PointWidth = 66
  ThisGraph1.SetQ.Points     = 0
  Put(ThisGraph1.SetQ)
          
  ThisGraph1.AddItem(2,2,qValores,Insight:GraphField,queue:despesas,,,,,,)
  ThisGraph1.SetSetYAxis(2,Scale:Low + Scale:High + Scale:Zero,,,)
  ThisGraph1.SetSetColors(2,-1,255,-1,16777215)
  ThisGraph1.SetSetType(2,Insight:None)
  If ThisGraph1.GetSet(2) = 0 then ThisGraph1.AddSetQ(2).
  ThisGraph1.SetQ.SquareWave = -1
  ThisGraph1.SetQ.Fill       = -1
  ThisGraph1.SetQ.FillToZero = -1
  ThisGraph1.SetQ.PointWidth = 66
  ThisGraph1.SetQ.Points     = 0
  Put(ThisGraph1.SetQ)
  !Mouse
  ThisGraph1.setMouseMoveParameters(1, '')
  if ThisGraph1.SavedGraphType<>0 then ThisGraph1.Type = ThisGraph1.SavedGraphType.
  ThisGraph1:Popup.Init()
  ThisGraph1:Popup.AddItem('Diminuir','ZoomOut','',1)
  ThisGraph1:Popup.AddItemEvent('ZoomOut',INSIGHT:ZoomOut,?Insight)
  ThisGraph1:Popup.AddItem('Ampliar','ZoomIn','',1)
  ThisGraph1:Popup.AddItemEvent('ZoomIn',INSIGHT:ZoomIn,?Insight)
  ThisGraph1:Popup.AddItem('Copiar','Copy','',1)
  ThisGraph1:Popup.AddItemEvent('Copy',INSIGHT:Copy,?Insight)
  ThisGraph1:Popup.AddItem('Salvar Como...','SaveAs','',1)
  ThisGraph1:Popup.AddItemEvent('SaveAs',INSIGHT:SaveAs,?Insight)
  ThisGraph1:Popup.AddItem('Imprimir Gr�fico','Print','',1)
  ThisGraph1:Popup.AddItemEvent('Print',INSIGHT:PrintGraph,?Insight)
  ThisGraph1:Popup.AddItem('Tipo do Gr�fico','SetGraphType','',1)
  ThisGraph1:Popup.AddItem('Linha','LineType','SetGraphType',2)
  ThisGraph1:Popup.AddItem('Barra','BarType','SetGraphType',2)
  ThisGraph1:Popup.AddItem('Pareto','ParetoType','SetGraphType',2)
  ThisGraph1:Popup.AddItemEvent('LineType',INSIGHT:SelectGraphType+Insight:Line,?Insight)
  ThisGraph1:Popup.AddItemEvent('BarType',INSIGHT:SelectGraphType+Insight:Bar,?Insight)
  ThisGraph1:Popup.AddItemEvent('ParetoType',INSIGHT:SelectGraphType+Insight:Pareto,?Insight)
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
   ThisGraph1.Kill()  
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Open PROCEDURE

  CODE
  PARENT.Open
    ThisGraph1.Reset()
    Post(Event:accepted,?Insight)


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF Window{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
     ThisGraph1.Reset()
     Post(Event:accepted,?Insight)


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button1
        ?strMensagem{prop:hide} = false
        display(?strMensagem)
      
        loc:data_inicial = date(month(loc:data_inicial),1,year(loc:data_inicial))
        loc:data_final = date(month(loc:data_final)+1,1,year(loc:data_final)) - 1
      
        if LOC:tipodata = 1
          LOC:CampoData = 'dc.d_datavencimento'
          LOC:CampoValor = 'sum(dc.valordocumento)'
        elsif LOC:tipodata = 2
          LOC:CampoData = 'dc.d_dataprovavel'
          LOC:CampoValor = 'sum(dc.valordocumento)'
        elsif LOC:tipodata = 3
          LOC:CampoData = 'dc.d_dataemissao'
          LOC:CampoValor = 'sum(dc.valordocumento)'
        else
          LOC:CampoData = 'dc.d_datapagamento'
          LOC:CampoValor = 'sum(dc.valorpago)'
        end
      
        Open(fDocumentos,42h)
        clear(fDocumentos)
      
        fDocumentos{Prop:Sql} = |
          'select t.mesref, t.totalvrpago, t.tipolancamento '&|
          'from ( '&|
            'select to_char('&clip(LOC:CampoData)&',<39>MM/YYYY<39>) as mesref, '&|
            'to_char('&clip(LOC:CampoData)&',<39>YYYY-MM-01<39>)::date as ordem, '&|
            'pl.tipolancamento, '&LOC:CampoValor&' as totalvrpago '&|
            'from documentos dc '&|
            'join planocontas pl on pl.id = dc.idplano '&|
            'where '&clip(LOC:CampoData)&' between <39>'&format(loc:data_inicial,@d012)&'<39> and <39>'&format(loc:data_final,@d012)&'<39> '&|
            'and dc.idempresa = '&GLO:CodEmpresa&' '&|
            'group BY mesref, pl.tipolancamento, ordem '&|
            'order by ordem, pl.tipolancamento '&|
          ') as t'
      
        !message(fDocumentos{Prop:Sql},,,,,2)
      
        free(qValores)
        loop
          next(fDocumentos)
          if error() then break end
      
          clear(qValores)
            
          queue:referencia = date(sub(Doc:Campo1,1,2),1,sub(Doc:Campo1,4,4))
          get(qValores, queue:referencia)
          if error() then
            if Doc:Campo3 = 'C'
              queue:receitas += Doc:Campo2
            elsif Doc:Campo3 = 'D'
              queue:despesas += Doc:Campo2
            end
            add(qValores)
          else
            if Doc:Campo3 = 'C'
              queue:receitas += Doc:Campo2
            elsif Doc:Campo3 = 'D'
              queue:despesas += Doc:Campo2
            end
            put(qValores)
          end
        end
      
        if LOC:DocImanager
          do DocIManager
        end
      
        ThisGraph1.Reset()
        ThisGraph1.Draw()
      
        ?strMensagem{prop:hide} = true
        display(?strMensagem)
      
        Close(fDocumentos)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Insight
      If ((?Insight{prop:visible} or ThisGraph1.ActiveInvisible) and not 0{prop:acceptall})
        ThisGraph1.Draw()
      End
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Insight
    CASE EVENT()
    Of INSIGHT:ResetAndDraw
      ThisGraph1.Reset()
      Post(event:accepted,?Insight)
    Of INSIGHT:PrintGraph
      ThisGraph1.PrintGraph('',255)
    Of INSIGHT:Copy
         ThisGraph1.ToClipBoard()
    Of INSIGHT:SaveAs
      if FileDialog('Salvar Como...',ThisGraph1:FileName,'Windows Bitmap|*.bmp|Portable Network Graphics|*.png',10011b) = 1
        ThisGraph1.SaveAs(ThisGraph1:FileName)
      End
    Of INSIGHT:ZoomIn
      ThisGraph1.Zoom(,-25,ThisGraph1:ClickedOnPointNumber)
      if ThisGraph1.zoom < 5 then ThisGraph1:Popup.SetItemEnable('ZoomIn',0).
      if ThisGraph1.zoom < 100 then ThisGraph1:Popup.SetItemEnable('ZoomOut',1).
    Of INSIGHT:ZoomOut
      ThisGraph1.Zoom(,+25,ThisGraph1:ClickedOnPointNumber)
      if ThisGraph1.zoom = 100 then ThisGraph1:Popup.SetItemEnable('ZoomOut',0).
      if ThisGraph1.zoom > 4 then ThisGraph1:Popup.SetItemEnable('ZoomIn',1).
    Of INSIGHT:SelectGraphType + Insight:Bar to INSIGHT:SelectGraphType + Insight:Gantt
      ThisGraph1.Type = event() - INSIGHT:SelectGraphType
      ThisGraph1.Draw()
    OF EVENT:MouseUp
        If ThisGraph1.GetPoint(ThisGraph1:ClickedOnSetNumber,ThisGraph1:ClickedOnPointNumber) = true
          ThisGraph1:ClickedOnPointName = ThisGraph1.GetPointName(ThisGraph1:ClickedOnSetNumber,ThisGraph1:ClickedOnPointNumber)
        End
      Case Keycode()
      Of MouseRight !Up
        ThisGraph1:Popup.Ask()
      End
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
        loc:data_final   = date(month(today())+1,1,year(today())) -1
        loc:data_inicial = loc:data_final - (day(loc:data_final)-1)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph1.ValidateRecord  PROCEDURE (long graphID)
ReturnValue  Long  ! set to > 0 to reject record
  Code
  ReturnValue = Parent.ValidateRecord(GraphID)
  Return ReturnValue
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph1.SetPointName     PROCEDURE(Long graphID)
ReturnValue  String(255)
  Code
  ReturnValue = format(queue:referencia,@d014b)
  Return ReturnValue
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph1.Reset     PROCEDURE (Byte graphForce=0)
  Code
  self.HeaderName = 'Receitas e Despesas'
  self.XAxisName = 'Compet�ncia'
  self.YAxisName = 'Valor em Reais'
  Sort(qValores,queue:referencia)
      
  Self.SetSetDescription(1,'Receitas')
  Self.SetSetDataLabels(1,0,'', 1,0)
  ThisGraph1.SetSetYAxis(1,Scale:Low + Scale:High + Scale:Zero,,,)
      
  Self.SetSetDescription(2,'Despesas')
  Self.SetSetDataLabels(2,0,'', 1,0)
  ThisGraph1.SetSetYAxis(2,Scale:Low + Scale:High + Scale:Zero,,,)
  Parent.Reset(graphForce)
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
grafico_Plano_Contas PROCEDURE                             ! Generated from procedure template - Window

loc:data_inicial     LONG                                  !
LOC:DocImanager      BYTE                                  !
LOC:CampoValor       CSTRING(31)                           !
LOC:FiltroData       CSTRING(201)                          !
LOC:tipodata         BYTE(1)                               !
Loc:TotalReceitas    DECIMAL(10,2)                         !
Loc:TotalDespesas    DECIMAL(10,2)                         !
loc:data_final       LONG                                  !
ClickedOnPointName   STRING(255)                           !
ClickedOnPointNumber REAL                                  !
ClickedOnSetNumber   LONG                                  !
queueReceber         QUEUE,PRE(q_r)                        !
pos                  BYTE                                  !
descricao            CSTRING(60)                           !
valor                DECIMAL(10,2)                         !
                     END                                   !
queuePagar           QUEUE,PRE(q_p)                        !
pos                  BYTE                                  !
descricao            CSTRING(40)                           !
valor                DECIMAL(10,2)                         !
                     END                                   !
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
  !ChartNotHere=0 - NoCharts=0 - Family=abc - q1
ThisGraph1    Class(GlobalInsight)
ValidateRecord  PROCEDURE(long graphID),Long ,VIRTUAL
Reset           PROCEDURE (Byte graphForce=0),VIRTUAL
              End
ThisGraph1:Popup  Class(PopupClass)
               End
ThisGraph1:ClickedOnPointName   String(255)
ThisGraph1:ClickedOnPointNumber Real
ThisGraph1:ClickedOnSetNumber   Long
ThisGraph1:Color                Group(iColorGroupType), PRE(ThisGraph1:Color)
                                End
ThisGraph1:FileName              String(255)
!-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  !ChartNotHere=0 - NoCharts=0 - Family=abc - q1
ThisGraph2    Class(GlobalInsight)
ValidateRecord  PROCEDURE(long graphID),Long ,VIRTUAL
Reset           PROCEDURE (Byte graphForce=0),VIRTUAL
              End
ThisGraph2:Popup  Class(PopupClass)
               End
ThisGraph2:ClickedOnPointName   String(255)
ThisGraph2:ClickedOnPointNumber Real
ThisGraph2:ClickedOnSetNumber   Long
ThisGraph2:Color                Group(iColorGroupType), PRE(ThisGraph2:Color)
                                End
ThisGraph2:FileName              String(255)
!-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
fDocumentos FILE,DRIVER('ODBC','/Turbosql=1'),OWNER(GLO:conexao),PRE(DOC),Name('consultasql')
Record      Record
campo1      cstring(25)   ! idPlanoContas
campo2      cstring(40)   ! Descri��o do Plano
campo3      cstring(25)   ! Tipo do lan�amento
campo4      cstring(25)   ! MesRef
campo5      decimal(13,2) ! Valor Documento
            End
        End
Window               WINDOW('Plano de Contas'),AT(,,682,404),FONT('MS Sans Serif',8,,FONT:regular),CENTER,ICON('principal.ico'),GRAY,IMM
                       GROUP('Per�odo'),AT(198,3,285,37),USE(?Group1),BOXED,FONT(,,COLOR:Blue,,CHARSET:ANSI)
                         ENTRY(@d014b),AT(218,13,62,10),USE(loc:data_inicial),FLAT,RIGHT(1)
                         ENTRY(@d014b),AT(298,13,62,10),USE(loc:data_final),FLAT,RIGHT
                         LIST,AT(384,13,69,10),USE(LOC:tipodata),FLAT,DROP(5),FROM('Vencimento|#1|Previs�o|#2|Emiss�o|#3|Pagamento|#4')
                         CHECK('Relacionar Documentos do i-MANAGER'),AT(204,27),USE(LOC:DocImanager),FONT(,,,FONT:bold,CHARSET:ANSI)
                         BUTTON,AT(458,11,20,15),USE(?Button1),FLAT,LEFT,ICON('selecionar.ico')
                         PROMPT('De:'),AT(204,13),USE(?loc:data_inicial:Prompt),TRN
                         PROMPT('At�:'),AT(284,13),USE(?Prompt1:2),TRN
                         PROMPT('Tipo:'),AT(364,13),USE(?LOC:tipodata:Prompt)
                       END
                       STRING('Gerando gr�fico, aguarde!'),AT(171,202,340,26),USE(?strMensagem),HIDE,CENTER,FONT('Arial',24,COLOR:Navy,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       SHEET,AT(5,31,671,354),USE(?Sheet1)
                         TAB('Despesas'),USE(?Tab1)
                           REGION,AT(10,63,661,318),USE(?Insight),IMM,BEVEL(1,-1)
                           STRING('Despesas'),AT(10,50,661,12),USE(?String2),CENTER,FONT(,10,,FONT:bold)
                         END
                         TAB('Receitas'),USE(?Tab2)
                           STRING('Receitas'),AT(10,50,661,12),USE(?String1),CENTER,FONT(,10,,FONT:bold)
                           REGION,AT(10,63,661,318),USE(?Insight:2),IMM,BEVEL(1,-1)
                         END
                       END
                       BUTTON('&Fechar'),AT(621,388,55,14),USE(?Button2),FLAT,LEFT,ICON('fechar.ico'),STD(STD:Close)
                     END
  !ChartNotHere=0 - NoCharts=0 - Family=abc - q2
  !ChartNotHere=0 - NoCharts=0 - Family=abc - q2

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Open                   PROCEDURE(),DERIVED                 ! Method added to host embed code
Reset                  PROCEDURE(BYTE Force=0),DERIVED     ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
DocumentosIManager routine
  clear(fDocumentos)

  fDocumentos{Prop:Sql} = |
    'select dc.planoid, dc.planodescricao, dc.tipolancamento, '&|
    '<39>2010-01-01<39> as dia, ' & LOC:CampoValor & ' as valor '&|
    'from public.fun_carrega_view_docreceber_imanager dc ' & LOC:FiltroData & ' ' &|
    'group by dc.planoid, dc.planodescricao, dc.tipolancamento, dia '&|
    'order by dc.tipolancamento'

  !message(fDocumentos{Prop:Sql},,,,,2)
  a# = 0
  b# = 0
  loop
    next(fDocumentos)
    if error() then break end

    clear(queueReceber) ; clear(queuePagar)

    if Doc:Campo3 = 'C'
      a# += 1
      q_r:pos = a#
      q_r:descricao = Doc:Campo2
      q_r:valor     = Doc:Campo5
      Loc:TotalReceitas += q_r:valor
      add(queueReceber)
    elsif Doc:Campo3 = 'D'
      b# += 1
      q_p:pos = b#
      q_p:descricao = Doc:Campo2
      q_p:valor     = Doc:Campo5
      Loc:TotalDespesas += q_p:valor
      add(queuePagar)
    end
  end

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('grafico_Plano_Contas')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?loc:data_inicial
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(Window)                                        ! Open window
  COMPILE ('**CW7**',_CWVER_=7000)
  ?Sheet1{PROP:TabSheetStyle} = TabStyle:Colored
  !**CW7**
  Do DefineListboxStyle
    ! Start Of Insight InitStuff Group
  Bind('ThisGraph1:ClickedOnPointNumber',ThisGraph1:ClickedOnPointNumber) ! Insight ClickedOnVariable 
  Bind('ThisGraph1:ClickedOnPointName',ThisGraph1:ClickedOnPointName)       ! Insight ClickedOnVariable 
  Bind('ThisGraph1:ClickedOnSetNumber',ThisGraph1:ClickedOnSetNumber)     ! Insight ClickedOnVariable 
      ! loc:data_inicial - LONG -
      ! LOC:DocImanager - BYTE -
      ! LOC:CampoValor - CSTRING(31) -
      ! LOC:FiltroData - CSTRING(201) -
      ! LOC:tipodata - BYTE(1) -
      ! Loc:TotalReceitas - DECIMAL(10,2) -
      ! Loc:TotalDespesas - DECIMAL(10,2) -
      ! loc:data_final - LONG -
      ! ClickedOnPointName - STRING(255) -
      ! ClickedOnPointNumber - REAL -
      ! ClickedOnSetNumber - LONG -
  if ThisGraph1.Init(?Insight,Insight:Pareto).
  if not ThisGraph1:Color:Fetched then ThisGraph1.GetWindowsColors(ThisGraph1:Color).
  Window{prop:buffer} = 1
  ThisGraph1.Stacked=0
  ThisGraph1.BlackAndWhite=0
  ThisGraph1.BackgroundPicture=''
  ThisGraph1.BackgroundColor=Color:white
  ThisGraph1.BackgroundShadeColor=Color:None
  ThisGraph1.BorderColor=Color:Black
  ThisGraph1.MinPointWidth=0
  ThisGraph1.PaperZoom=4
  ThisGraph1.ActiveInvisible=0
  ThisGraph1.PrintPortrait=0
  ThisGraph1.RightBorder=10
  ThisGraph1.MaxXGridTicks=16
  ThisGraph1.MaxYGridTicks=16
  ThisGraph1.WorkSpaceWidth=0
  ThisGraph1.WorkSpaceHeight=0
  ThisGraph1.AutoShade=1
  ThisGraph1.TopShade=0
  ThisGraph1.RightShade=0
  ThisGraph1.LongShade=0
  ThisGraph1.Pattern=Insight:None
  ThisGraph1.Float=0
  ThisGraph1.ZCluster=0
  ThisGraph1.Fill=0
  ThisGraph1.FillToZero=0
  ThisGraph1.SquareWave=0
  ThisGraph1.InnerRadius=0
  ThisGraph1.MaxPieRadius=0
  ThisGraph1.PieAngle=0
  ThisGraph1.PieLabelLineColor=Color:None
  ThisGraph1.AspectRatio=1
  ThisGraph1.PieLabelLines=0
  ThisGraph1.Shape=Insight:Auto
  ThisGraph1.LineWidth = 1
      ! Fonts
  ThisGraph1.LegendAngle   = 0
  ThisGraph1.ShowXLabelsEvery = 1
  ThisGraph1.DisplayOnX = 2   ! 0= don't display, 1= display number, 2= display name.
  ThisGraph1.AutoXLabels = 0
  ThisGraph1.SeparateYAxis = 0
  ThisGraph1.SpreadXLabels       = 0
  ThisGraph1.AutoXGridTicks = 1
  ThisGraph1.AutoScale = Scale:Low + Scale:High
  ThisGraph1.YFormat = '@n14.`2'
  ThisGraph1.ShowDataLabels = 1
  ThisGraph1.DataLabelFormat = '@n14.`2'
  ThisGraph1.PiePercentFormat = '@n10`2'
  ThisGraph1.ShowDataLabelsEvery = 1
  ThisGraph1.Depth = 0
  ThisGraph1.SortStyle = -1
  ThisGraph1.SortSet = 1
          
  ThisGraph1.AddItem(1,1,queuePagar,Insight:GraphField,q_p:valor,,,,,q_p:pos,q_p:descricao)
  ThisGraph1.SetSetYAxis(1,Scale:Low + Scale:High,,,)
  ThisGraph1.SetSetColors(1,-1556861281,255,-1,16777215)
  ThisGraph1.SetSetType(1,Insight:None)
  If ThisGraph1.GetSet(1) = 0 then ThisGraph1.AddSetQ(1).
  ThisGraph1.SetQ.SquareWave = -1
  ThisGraph1.SetQ.Fill       = -1
  ThisGraph1.SetQ.FillToZero = -1
  ThisGraph1.SetQ.PointWidth = 66
  ThisGraph1.SetQ.Points     = 0
  Put(ThisGraph1.SetQ)
  !Mouse
  ThisGraph1.setMouseMoveParameters(1, '')
  if ThisGraph1.SavedGraphType<>0 then ThisGraph1.Type = ThisGraph1.SavedGraphType.
  ThisGraph1:Popup.Init()
  ThisGraph1:Popup.AddItem('Diminuir','ZoomOut','',1)
  ThisGraph1:Popup.AddItemEvent('ZoomOut',INSIGHT:ZoomOut,?Insight)
  ThisGraph1:Popup.AddItem('Ampliar','ZoomIn','',1)
  ThisGraph1:Popup.AddItemEvent('ZoomIn',INSIGHT:ZoomIn,?Insight)
  ThisGraph1:Popup.AddItem('Definir Ciclo','CycleSets','',1)
  ThisGraph1:Popup.AddItemEvent('CycleSets',INSIGHT:CycleSets,?Insight)
  ThisGraph1:Popup.AddItem('Copiar','Copy','',1)
  ThisGraph1:Popup.AddItemEvent('Copy',INSIGHT:Copy,?Insight)
  ThisGraph1:Popup.AddItem('Salvar Como...','SaveAs','',1)
  ThisGraph1:Popup.AddItemEvent('SaveAs',INSIGHT:SaveAs,?Insight)
  ThisGraph1:Popup.AddItem('Imprimir Gr�fico','Print','',1)
  ThisGraph1:Popup.AddItemEvent('Print',INSIGHT:PrintGraph,?Insight)
    ! Start Of Insight InitStuff Group
  Bind('ThisGraph2:ClickedOnPointNumber',ThisGraph2:ClickedOnPointNumber) ! Insight ClickedOnVariable 
  Bind('ThisGraph2:ClickedOnPointName',ThisGraph2:ClickedOnPointName)       ! Insight ClickedOnVariable 
  Bind('ThisGraph2:ClickedOnSetNumber',ThisGraph2:ClickedOnSetNumber)     ! Insight ClickedOnVariable 
      ! loc:data_inicial - LONG -
      ! LOC:DocImanager - BYTE -
      ! LOC:CampoValor - CSTRING(31) -
      ! LOC:FiltroData - CSTRING(201) -
      ! LOC:tipodata - BYTE(1) -
      ! Loc:TotalReceitas - DECIMAL(10,2) -
      ! Loc:TotalDespesas - DECIMAL(10,2) -
      ! loc:data_final - LONG -
      ! ClickedOnPointName - STRING(255) -
      ! ClickedOnPointNumber - REAL -
      ! ClickedOnSetNumber - LONG -
  if ThisGraph2.Init(?Insight:2,Insight:Pareto).
  if not ThisGraph2:Color:Fetched then ThisGraph2.GetWindowsColors(ThisGraph2:Color).
  Window{prop:buffer} = 1
  ThisGraph2.Stacked=0
  ThisGraph2.BlackAndWhite=0
  ThisGraph2.BackgroundPicture=''
  ThisGraph2.BackgroundColor=Color:white
  ThisGraph2.BackgroundShadeColor=Color:None
  ThisGraph2.BorderColor=Color:Black
  ThisGraph2.MinPointWidth=0
  ThisGraph2.PaperZoom=4
  ThisGraph2.ActiveInvisible=0
  ThisGraph2.PrintPortrait=0
  ThisGraph2.RightBorder=10
  ThisGraph2.MaxXGridTicks=16
  ThisGraph2.MaxYGridTicks=16
  ThisGraph2.WorkSpaceWidth=0
  ThisGraph2.WorkSpaceHeight=0
  ThisGraph2.AutoShade=1
  ThisGraph2.TopShade=0
  ThisGraph2.RightShade=0
  ThisGraph2.LongShade=0
  ThisGraph2.Pattern=Insight:None
  ThisGraph2.Float=0
  ThisGraph2.ZCluster=0
  ThisGraph2.Fill=0
  ThisGraph2.FillToZero=0
  ThisGraph2.SquareWave=0
  ThisGraph2.InnerRadius=0
  ThisGraph2.MaxPieRadius=0
  ThisGraph2.PieAngle=0
  ThisGraph2.PieLabelLineColor=Color:None
  ThisGraph2.AspectRatio=1
  ThisGraph2.PieLabelLines=0
  ThisGraph2.Shape=Insight:Auto
  ThisGraph2.LineWidth = 1
      ! Fonts
  ThisGraph2.LegendAngle   = 0
  ThisGraph2.ShowXLabelsEvery = 1
  ThisGraph2.DisplayOnX = 2   ! 0= don't display, 1= display number, 2= display name.
  ThisGraph2.AutoXLabels = 0
  ThisGraph2.SeparateYAxis = 0
  ThisGraph2.SpreadXLabels       = 0
  ThisGraph2.AutoXGridTicks = 1
  ThisGraph2.AutoScale = Scale:Low + Scale:High
  ThisGraph2.ShowDataLabels = 1
  ThisGraph2.DataLabelFormat = '@n14.`2'
  ThisGraph2.ShowDataLabelsEvery = 1
  ThisGraph2.Depth = 0
  ThisGraph2.SortStyle = -1
  ThisGraph2.SortSet = 1
          
  ThisGraph2.AddItem(1,1,queueReceber,Insight:GraphField,q_r:valor,,,,,q_r:pos,q_r:descricao)
  ThisGraph2.SetSetYAxis(1,Scale:Low + Scale:High,,,)
  ThisGraph2.SetSetColors(1,-1556861281,32768,-1,16777215)
  ThisGraph2.SetSetType(1,Insight:None)
  If ThisGraph2.GetSet(1) = 0 then ThisGraph2.AddSetQ(1).
  ThisGraph2.SetQ.SquareWave = -1
  ThisGraph2.SetQ.Fill       = -1
  ThisGraph2.SetQ.FillToZero = -1
  ThisGraph2.SetQ.PointWidth = 66
  ThisGraph2.SetQ.Points     = 0
  Put(ThisGraph2.SetQ)
  !Mouse
  ThisGraph2.setMouseMoveParameters(1, '')
  if ThisGraph2.SavedGraphType<>0 then ThisGraph2.Type = ThisGraph2.SavedGraphType.
  ThisGraph2:Popup.Init()
  ThisGraph2:Popup.AddItem('Diminuir','ZoomOut','',1)
  ThisGraph2:Popup.AddItemEvent('ZoomOut',INSIGHT:ZoomOut,?Insight:2)
  ThisGraph2:Popup.AddItem('Ampliar','ZoomIn','',1)
  ThisGraph2:Popup.AddItemEvent('ZoomIn',INSIGHT:ZoomIn,?Insight:2)
  ThisGraph2:Popup.AddItem('Definir Ciclo','CycleSets','',1)
  ThisGraph2:Popup.AddItemEvent('CycleSets',INSIGHT:CycleSets,?Insight:2)
  ThisGraph2:Popup.AddItem('Copiar','Copy','',1)
  ThisGraph2:Popup.AddItemEvent('Copy',INSIGHT:Copy,?Insight:2)
  ThisGraph2:Popup.AddItem('Salvar Como...','SaveAs','',1)
  ThisGraph2:Popup.AddItemEvent('SaveAs',INSIGHT:SaveAs,?Insight:2)
  ThisGraph2:Popup.AddItem('Imprimir Gr�fico','Print','',1)
  ThisGraph2:Popup.AddItemEvent('Print',INSIGHT:PrintGraph,?Insight:2)
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
   ThisGraph1.Kill()  
   ThisGraph2.Kill()  
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Open PROCEDURE

  CODE
  PARENT.Open
    ThisGraph1.Reset()
    Post(Event:accepted,?Insight)
    ThisGraph2.Reset()
    Post(Event:accepted,?Insight:2)


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF Window{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
     ThisGraph1.Reset()
     Post(Event:accepted,?Insight)
     ThisGraph2.Reset()
     Post(Event:accepted,?Insight:2)


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button1
        ?strMensagem{prop:hide} = false
        display(?strMensagem)
      
        loc:data_inicial = date(month(loc:data_inicial),1,year(loc:data_inicial))
        loc:data_final = date(month(loc:data_final)+1,1,year(loc:data_final)) - 1
      
        if LOC:tipodata = 1
          LOC:FiltroData = 'where dc.d_datavencimento between <39>' & format(loc:data_inicial,@d12) & '<39> and <39>' & format(loc:data_final,@d12) & '<39>'
          LOC:CampoValor = 'sum(dc.valordocumento)'
        elsif LOC:tipodata = 2
          LOC:FiltroData = 'where dc.d_dataprovavel between <39>' & format(loc:data_inicial,@d12) & '<39> and <39>' & format(loc:data_final,@d12) & '<39>'
          LOC:CampoValor = 'sum(dc.valordocumento)'
        elsif LOC:tipodata = 3
          LOC:FiltroData = 'where dc.d_dataemissao between <39>' & format(loc:data_inicial,@d12) & '<39> and <39>' & format(loc:data_final,@d12) & '<39>'
          LOC:CampoValor = 'sum(dc.valordocumento)'
        else
          LOC:FiltroData = 'where dc.d_datapagamento between <39>' & format(loc:data_inicial,@d12) & '<39> and <39>' & format(loc:data_final,@d12) & '<39>'
          LOC:CampoValor = 'sum(dc.valorpago)'
        end
      
        LOC:FiltroData = LOC:FiltroData & ' and dc.idempresa = '&GLO:CodEmpresa
      
        Open(fDocumentos,42h)
        clear(fDocumentos)
      
        fDocumentos{Prop:Sql} = 'select dc.idplano, pl.descricaoplano, pl.tipolancamento, ' &|
                                '<39>2010-01-01<39>, '&LOC:CampoValor&' as valordocumento ' &|
                                'from documentos dc ' &|
                                'join planocontas pl on (pl.id = dc.idplano) ' & LOC:FiltroData & ' ' &|
                                'group BY dc.idplano, pl.descricaoplano, pl.tipolancamento ' &|
                                'order by pl.tipolancamento'
      
        !message(fDocumentos{Prop:Sql},,,,,2)
        a# = 0
        b# = 0
        Loc:TotalReceitas = 0
        Loc:TotalDespesas = 0
        free(queueReceber) ; free(queuePagar)
        loop
            next(fDocumentos)
            if error() then break end
      
            clear(queueReceber) ; clear(queuePagar)
      
            if Doc:Campo3 = 'C'
                a# += 1
                q_r:pos = a#
                q_r:descricao = Doc:Campo2 !&' [R$ '&format(Doc:Campo5,@n14.`2)&'] => '
                q_r:valor     = Doc:Campo5
                Loc:TotalReceitas += q_r:valor
                add(queueReceber)
            elsif Doc:Campo3 = 'D'
              b# += 1
              q_p:pos = b#
              q_p:descricao = Doc:Campo2 !&' [R$ '&format(Doc:Campo5,@n14.`2)&'] => '
              q_p:valor     = Doc:Campo5
              Loc:TotalDespesas += q_p:valor
              add(queuePagar)
            end
        end
      
        !Buscar Documentos do i-Manager
        if LOC:DocImanager
          do DocumentosIManager
        end
      
        sort(queuePagar,-q_p:valor)
        sort(queueReceber,-q_r:valor)
      
        ?string1{prop:text} = ' Receitas - R$ ' & format(Loc:TotalReceitas,@n10`2)
        ?string2{prop:text} = ' Despesas - R$ ' & format(Loc:TotalDespesas,@n10`2)
      
        Close(fDocumentos)
      
        ThisGraph1.Reset()
        ThisGraph1.Draw()
        ThisGraph2.Reset()
        ThisGraph2.Draw()
      
        ?strMensagem{prop:hide} = true
        display(?strMensagem)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Insight
      If ((?Insight{prop:visible} or ThisGraph1.ActiveInvisible) and not 0{prop:acceptall})
        ThisGraph1.Draw()
      End
    OF ?Insight:2
      If ((?Insight:2{prop:visible} or ThisGraph2.ActiveInvisible) and not 0{prop:acceptall})
        ThisGraph2.Draw()
      End
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Insight
    CASE EVENT()
    Of INSIGHT:ResetAndDraw
      ThisGraph1.Reset()
      Post(event:accepted,?Insight)
    Of INSIGHT:PrintGraph
      ThisGraph1.PrintGraph('',255)
    Of INSIGHT:Copy
         ThisGraph1.ToClipBoard()
    Of INSIGHT:SaveAs
      if FileDialog('Salvar Como...',ThisGraph1:FileName,'Windows Bitmap|*.bmp|Portable Network Graphics|*.png',10011b) = 1
        ThisGraph1.SaveAs(ThisGraph1:FileName)
      End
    Of INSIGHT:ZoomIn
      ThisGraph1.Zoom(,-25,ThisGraph1:ClickedOnPointNumber)
      if ThisGraph1.zoom < 5 then ThisGraph1:Popup.SetItemEnable('ZoomIn',0).
      if ThisGraph1.zoom < 100 then ThisGraph1:Popup.SetItemEnable('ZoomOut',1).
    Of INSIGHT:ZoomOut
      ThisGraph1.Zoom(,+25,ThisGraph1:ClickedOnPointNumber)
      if ThisGraph1.zoom = 100 then ThisGraph1:Popup.SetItemEnable('ZoomOut',0).
      if ThisGraph1.zoom > 4 then ThisGraph1:Popup.SetItemEnable('ZoomIn',1).
    Of INSIGHT:CycleSets
      ThisGraph1.SwapSets(1)
      ThisGraph1.Draw()
    OF EVENT:MouseUp
        If ThisGraph1.GetPoint(ThisGraph1:ClickedOnSetNumber,ThisGraph1:ClickedOnPointNumber) = true
          ThisGraph1:ClickedOnPointName = ThisGraph1.GetPointName(ThisGraph1:ClickedOnSetNumber,ThisGraph1:ClickedOnPointNumber)
        End
      Case Keycode()
      Of MouseRight !Up
        ThisGraph1:Popup.Ask()
      End
    END
  OF ?Insight:2
    CASE EVENT()
    Of INSIGHT:ResetAndDraw
      ThisGraph2.Reset()
      Post(event:accepted,?Insight:2)
    Of INSIGHT:PrintGraph
      ThisGraph2.PrintGraph('',255)
    Of INSIGHT:Copy
         ThisGraph2.ToClipBoard()
    Of INSIGHT:SaveAs
      if FileDialog('Salvar Como...',ThisGraph2:FileName,'Windows Bitmap|*.bmp|Portable Network Graphics|*.png',10011b) = 1
        ThisGraph2.SaveAs(ThisGraph2:FileName)
      End
    Of INSIGHT:ZoomIn
      ThisGraph2.Zoom(,-25,ThisGraph2:ClickedOnPointNumber)
      if ThisGraph2.zoom < 5 then ThisGraph2:Popup.SetItemEnable('ZoomIn',0).
      if ThisGraph2.zoom < 100 then ThisGraph2:Popup.SetItemEnable('ZoomOut',1).
    Of INSIGHT:ZoomOut
      ThisGraph2.Zoom(,+25,ThisGraph2:ClickedOnPointNumber)
      if ThisGraph2.zoom = 100 then ThisGraph2:Popup.SetItemEnable('ZoomOut',0).
      if ThisGraph2.zoom > 4 then ThisGraph2:Popup.SetItemEnable('ZoomIn',1).
    Of INSIGHT:CycleSets
      ThisGraph2.SwapSets(1)
      ThisGraph2.Draw()
    OF EVENT:MouseUp
        If ThisGraph2.GetPoint(ThisGraph2:ClickedOnSetNumber,ThisGraph2:ClickedOnPointNumber) = true
          ThisGraph2:ClickedOnPointName = ThisGraph2.GetPointName(ThisGraph2:ClickedOnSetNumber,ThisGraph2:ClickedOnPointNumber)
        End
      Case Keycode()
      Of MouseRight !Up
        ThisGraph2:Popup.Ask()
      End
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
        loc:data_final   = date(month(today())+1,1,year(today())) -1
        loc:data_inicial = loc:data_final - (day(loc:data_final)-1)
      
      !  ! Encontrar o m�s inicial
      !  mes# = month(loc:data_final)
      !  ano# = year(loc:data_final)
      !  
      !  loop a# = 1 to 11
      !       mes# = mes# - 1
      !       ano# = ano#
      !       if mes# = 0
      !          mes# = 12
      !          ano# -= 1
      !       end
      !       loc:data_inicial = date(mes#,1,ano#)
      !  end
      
        !stop(format(loc:data_inicial,@d06b) &' - '& format(loc:data_final,@d06b))
        display
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph1.ValidateRecord  PROCEDURE (long graphID)
ReturnValue  Long  ! set to > 0 to reject record
  Code
  ReturnValue = Parent.ValidateRecord(GraphID)
  Return ReturnValue
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph1.Reset     PROCEDURE (Byte graphForce=0)
  Code
      
  Self.SetSetDataLabels(1,0,'', 1,0)
  ThisGraph1.SetSetYAxis(1,Scale:Low + Scale:High,,,)
  Parent.Reset(graphForce)
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph2.ValidateRecord  PROCEDURE (long graphID)
ReturnValue  Long  ! set to > 0 to reject record
  Code
  ReturnValue = Parent.ValidateRecord(GraphID)
  Return ReturnValue
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
ThisGraph2.Reset     PROCEDURE (Byte graphForce=0)
  Code
      
  Self.SetSetDataLabels(1,0,'', 1,0)
  ThisGraph2.SetSetYAxis(1,Scale:Low + Scale:High,,,)
  Parent.Reset(graphForce)
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
! -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
