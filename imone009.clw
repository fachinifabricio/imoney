

   MEMBER('imoney.clw')                                    ! This is a MEMBER module


   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('radsqlb.inc'),ONCE

                     MAP
                       INCLUDE('IMONE009.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('IMONE012.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseCentroDeCusto PROCEDURE                              ! Generated from procedure template - Window

qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
BR1:View             VIEW(centrocusto)
                       PROJECT(CCU:id)
                       PROJECT(CCU:descricao)
                     END
Queue:RADSQLBrowse   QUEUE                            !Queue declaration for browse/combo box using ?lstCentroDeCusto
CCU:id                 LIKE(CCU:id)                   !List box control field - type derived from field
CCU:descricao          LIKE(CCU:descricao)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Window               WINDOW('i-Money'),AT(,,314,231),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('principal.ico'),SYSTEM,GRAY,DOUBLE,AUTO
                       IMAGE('window.ico'),AT(2,2),USE(?Image1)
                       PROMPT('Centro de Custo'),AT(26,5),USE(?Prompt1),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(2,22,309,2),USE(?Panel1)
                       LIST,AT(2,26,309,163),USE(?lstCentroDeCusto),IMM,FLAT,VSCROLL,MSG('Scrolling records...'),FORMAT('60R(2)_F~C�digo~C(0)@n15.`0b@200L(2)_F~Descri��o~@s50@'),FROM(Queue:RADSQLBrowse)
                       BUTTON('&Incluir'),AT(158,193,50,15),USE(?btnIncluir),TRN,FLAT,LEFT,ICON('incluir.ico')
                       BUTTON('&Alterar'),AT(210,193,50,15),USE(?btnAlterar),TRN,FLAT,LEFT,ICON('alterar.ico')
                       BUTTON('&Excluir'),AT(262,193,50,15),USE(?btnExcluir),TRN,FLAT,LEFT,ICON('excluir.ico')
                       BUTTON('&Imprimir'),AT(64,193,60,15),USE(?btnImprimir),FLAT,LEFT,ICON('imprimir.ico')
                       PANEL,AT(2,210,309,2),USE(?Panel1:2)
                       BUTTON('&Fechar'),AT(255,215,55,15),USE(?btnFechar),TRN,FLAT,LEFT,ICON('fechar.ico')
                       BUTTON('&Selecionar'),AT(2,193,60,15),USE(?btnSelecionar),TRN,FLAT,HIDE,LEFT,ICON('selecionar.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Reset                  PROCEDURE(BYTE Force=0),DERIVED     ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED ! Method added to host embed code
SetAlerts              PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
BR1                  CLASS(EXTSQLB)
RunForm                PROCEDURE(Byte in_Request),DERIVED  ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseCentroDeCusto')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?btnFechar,RequestCancelled)             ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?btnFechar,RequestCompleted)             ! Add the close control to the window manger
  END
  COMPILE('C55',_C55_)
    Self.Filesopened = 1
  C55
  OMIT('C55',_C55_)
    Filesopened = 1
  C55
  Relate:centrocusto.Open()
  Access:centrocusto.UseFile()
  SELF.Open(Window)                                        ! Open window
  BR1.RefreshHotkey = F5Key
  BR1.Setlistcontrol (?lstCentroDeCusto,10)
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>BrowseCentroDeCusto<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>BrowseCentroDeCusto<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  If Self.Request = SelectRecord then BR1.Selecting = 1.
  BR1.PopUpActive = True
  BR1.Init(Access:centrocusto)
  BR1.useansijoin=1
  BR1.MaintainProcedure = 'UpdateCentroDeCusto'
  BR1.SetupdateButtons (?btnIncluir,?btnAlterar,?btnExcluir,0)
  BR1.Selectcontrol = ?btnSelecionar
  if Self.Request = Selectrecord
    Unhide(?btnSelecionar)
  else
    Disable(?btnSelecionar)  !Needed if using Toolbar
  end
  SELF.SetAlerts()
  ?lstCentroDeCusto{PROP:From} = Queue:RADSQLBrowse
  BR1.AddTable('centrocusto',1,0,'centrocusto')  ! Add the table to the list of tables
  BR1.Listqueue      &= Queue:RADSQLBrowse
  BR1.Position       &= Queue:RADSQLBrowse.ViewPosition
  BR1.ColumnCharAsc   = ''
  BR1.ColumnCharDes   = ''
  BR1.AddFieldpairs(Queue:RADSQLBrowse.CCU:id,CCU:id,'centrocusto','id','N',0,'B','A','M','N',1,1)
  BR1.AddFieldpairs(Queue:RADSQLBrowse.CCU:descricao,CCU:descricao,'centrocusto','descricao','A',0,'B','A','M','N',2,2)
  BR1.Managedview &=BR1:View
  BR1.Openview()
  BR1.SetFileLoad()
  BR1.SetForceFetch (1)
  BR1.SetColColor = 0
  BR1.Resetsort(1)
  EnterByTabManager.Init(False)
  BR1.RefreshMethod = 1
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  COMPILE('C55',_C55_)
  if Self.filesopened
  C55
  OMIT('C55',_C55_)
  If filesopened then
  C55
    Relate:centrocusto.Close
  END
  END
  if  BR1.Response = RequestCompleted then Globalresponse = RequestCompleted; ReturnValue = RequestCompleted.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF Window{Prop:AcceptAll} THEN RETURN.
  PARENT.Reset(Force)
     If force = 1  then
        BR1.ResetBrowse (1)
     end


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF BR1.Response <> 0 then ReturnValue = BR1.Response.
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF BR1.Response <> 0 then ReturnValue = BR1.Response.
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  PARENT.SetAlerts
  BR1.SetAlerts()


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?btnImprimir
      ThisWindow.Update
      RelatCentroDeCusto()
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
  BR1.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('BrowseCentroDeCusto')
          End
        End
      End
      Close(HWGroups)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BR1.RunForm PROCEDURE(Byte in_Request)


  CODE
     GlobalRequest = in_Request
  PARENT.RunForm(in_Request)
     if in_Request = ChangeRecord or in_Request = DeleteRecord or in_Request = ViewRecord
        if Access:centrocusto.Fetch(CCU:ccu_pk_id) <> Level:Benign ! Fetch centrocusto on key 
          Message('Error on primary key fetch CCU:ccu_pk_id|Error:'& Error() &' FileError:'& FileError() &'|SQL:'& centrocusto{Prop:SQL})
          GlobalResponse = RequestCancelled
          Return
        end
     else
     end
  UpdateCentroDeCusto 
     BR1.Response = Globalresponse
  
   BR1.ListControl{Prop:Selected} = BR1.CurrentChoice
   Display

UpdateCentroDeCusto PROCEDURE                              ! Generated from procedure template - Window

CurrentTab           STRING(80)                            !
ActionMessage        CSTRING(40)                           !
qControls   Queue
feq                     Long
name                    Cstring(51)
                        End
                        
fSecurity       File,Driver('ODBC','/TurboSql=1'),PRE(FSEC),Owner(GLO:Conexao),Name('consultasql')
Record              Record
campo1                  Cstring(51) !controlname
campo2                  Byte !controlaccess
campo3                  Byte !controlinsert
campo4                  Byte !controlchange
campo5                  Byte !controldelete
                            End
                        End                     
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
History::CCU:Record  LIKE(CCU:RECORD),THREAD
QuickWindow          WINDOW('i-Money'),AT(,,306,70),FONT('Tahoma',8,COLOR:Black,FONT:regular),CENTER,IMM,ICON('principal.ico'),HLP('UpdateBancos'),SYSTEM,GRAY,DOUBLE,AUTO
                       IMAGE('window.ico'),AT(2,1),USE(?Image1)
                       PROMPT('Centro de Custo'),AT(26,5),USE(?Prompt4),TRN,FONT('Impact',16,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(2,21,302,2),USE(?Panel1)
                       PROMPT('Descri��o:'),AT(4,25,43,10),USE(?BAN:nomedobanco:Prompt),TRN
                       ENTRY(@s50),AT(4,35,300,10),USE(CCU:descricao),FLAT,MSG('Nome do banco'),TIP('Nome do banco'),REQ
                       PANEL,AT(2,48,302,2),USE(?Panel1:2)
                       BUTTON('&Gravar'),AT(189,53,55,15),USE(?btnGravar),FLAT,LEFT,MSG('Grava o registro'),TIP('Grava o registro'),ICON('gravar.ico'),DEFAULT
                       BUTTON('&Cancelar'),AT(248,53,55,15),USE(?btnCancelar),FLAT,LEFT,MSG('Cancela a opera��o'),TIP('Cancela a opera��o'),ICON('cancelar.ico')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'Visualizando'
  OF InsertRecord
    ActionMessage = 'Incluindo'
  OF ChangeRecord
    ActionMessage = 'Alterando'
  END
  QuickWindow{Prop:StatusText,2} = ActionMessage           ! Display status message in status bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateCentroDeCusto')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(CCU:Record,History::CCU:Record)
  SELF.AddHistoryField(?CCU:descricao,2)
  SELF.AddUpdateFile(Access:centrocusto)
  SELF.AddItem(?btnCancelar,RequestCancelled)              ! Add the cancel control to the window manager
  Relate:centrocusto.Open                                  ! File centrocusto used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:centrocusto
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?btnGravar
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Alert(CTRLF12)
  !Ready controls
  Free(qControls)
  Loop i# = 1 To LastField()
      qControls.feq = i#
      qControls.name = lower(GetFieldName(i#))
      Add(qControls)
  End
  !Ready permissions
  Open(fSecurity,42h)
  Clear(FSEC:Record)
  fSecurity{prop:sql} = 'Select hwp.procedurename, hwa.a_access from hwprocedures hwp ' & |
                                              'Join hwaccess hwa on hwa.idprocedure = hwp.id ' & |
                                              'where hwp.controlname is null and hwp.procedurename = <39>UpdateCentroDeCusto<39> and ' & |
                                              'hwa.idgroup = ' & GLO:IDGroup
  Next(fSecurity)
  If ~Error()
      IF FSEC:campo2 = 0
          Message('Voc� n�o tem acesso a este procedimento!','Acesso negado',Icon:Exclamation,'OK',,2)
          Post(Event:CloseWindow)
      Else
          Clear(FSEC:Record)
          fSecurity{prop:sql} = 'Select hwp.controlname, hwa.a_access, hwa.a_insert, hwa.a_change, ' & |
                                                      'hwa.a_delete from hwaccess hwa ' & |
                                                      'join hwprocedures hwp on hwa.idprocedure = hwp.id ' & |
                                                      'where hwp.procedurename = <39>UpdateCentroDeCusto<39> and hwp.controlname is not null ' & |
                                                      'and (hwa.a_access = 0 Or hwa.a_insert = 0 Or hwa.a_change = 0 Or ' & |
                                                      'hwa.a_delete = 0) and hwa.idgroup = ' & GLO:IDGroup
          Loop
              Next(fSecurity)
              If Error() Then Break .
              
              If FSEC:campo2 = 0
                  qControls.name = lower(FSEC:campo1)
                  Get(qControls,+qControls.name)
                  If ~Error()
                      qControls.feq{prop:disable} = 1
                  End
              ElsIf Self.Request = InsertRecord
                  If FSEC:campo3 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = ChangeRecord
                  If FSEC:campo4 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End
              ElsIf Self.Request = DeleteRecord
                  If FSEC:campo5 = 0
                      qControls.name = lower(FSEC:campo1)
                      Get(qControls,+qControls.name)
                      If ~Error()
                          qControls.feq{prop:disable} = 1
                      End
                  End 
              End
          End
      End                                             
  End                                         
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?CCU:descricao{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:centrocusto.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?btnGravar
      SetNullFields(centrocusto)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?btnGravar
      ThisWindow.Update
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      open(HWGroups,42h)
      If KeyCode() = CTRLF12
        HWG:Id = GLO:IDGroup
        Get(HWGroups,HWG:hwg_pk_id)
        If ~Error()
          If HWG:accessgroup = 1
                  BrowseGroupsUsersControls('UpdateCentroDeCusto')
          End
        End
      End
      Close(HWGroups)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

RelatCentroDeCusto PROCEDURE                               ! Generated from procedure template - Report

Progress:Thermometer BYTE                                  !
LOC:RazaoSocial      CSTRING(101)                          !
LOC:EnderecoBairro   CSTRING(101)                          !
LOC:CepTelefone      CSTRING(101)                          !
LOC:Data             DATE                                  !
LOC:Hora             STRING(20)                            !
Process:View         VIEW(centrocusto)
                       PROJECT(CCU:descricao)
                       PROJECT(CCU:id)
                     END
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
ProgressWindow       WINDOW('i-Money'),AT(,,273,27),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),HIDE,AT(239,1,15,12),RANGE(0,100)
                       PROMPT('Processando, aguarde!'),AT(57,6),USE(?Prompt1),TRN,FONT(,16,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                       BOX,AT(2,2,269,24),USE(?Box1),ROUND,COLOR(COLOR:Navy),FILL(COLOR:White)
                       STRING(''),AT(239,1,15,12),USE(?Progress:UserString),HIDE,CENTER
                       STRING(''),AT(239,1,15,12),USE(?Progress:PctText),HIDE,CENTER
                       BUTTON('Cancel'),AT(239,1,15,12),USE(?Progress:Cancel),HIDE
                     END

Report               REPORT,AT(240,1365,7750,9823),PAPER(PAPER:A4),PRE(RPT),FONT('Arial',8,,,CHARSET:ANSI),THOUS
                       HEADER,AT(240,260,7750,1115),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         BOX,AT(3208,83,4458,354),USE(?Box1:4),ROUND,COLOR(COLOR:Black),FILL(COLOR:Gray)
                         BOX,AT(83,83,3052,635),USE(?Box1:2),ROUND,COLOR(COLOR:Black),FILL(COLOR:Gray)
                         BOX,AT(3188,52,4458,354),USE(?Box1:3),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(63,52,3052,635),USE(?Box1),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         STRING('Centro de Custo'),AT(3198,73,4438,281),USE(?String17),TRN,CENTER,FONT('Arial',18,,FONT:bold,CHARSET:ANSI)
                         STRING(@s100),AT(115,135,2926,177),USE(LOC:RazaoSocial),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING(@s50),AT(115,323,2979,177),USE(LOC:EnderecoBairro),TRN,LEFT,FONT(,7,,)
                         STRING(@s50),AT(115,500,2979,177),USE(LOC:CepTelefone),TRN,LEFT,FONT(,7,,)
                         LINE,AT(94,1052,7563,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('C�digo'),AT(573,896),USE(?String18:8),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         STRING('Descri��o'),AT(1125,896),USE(?String18:10),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                       END
detail1                DETAIL,AT(,,,177),USE(?Detail1)
                         STRING(@n15.`0b),AT(83,21),USE(CCU:id),RIGHT(12)
                         STRING(@s50),AT(1125,21,3135,177),USE(CCU:descricao)
                       END
                       FOOTER,AT(240,11177,7750,354),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Data:'),AT(2010,21),USE(?ReportDatePrompt),TRN,FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING(@d06b),AT(2333,21),USE(LOC:Data),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING('Hora:'),AT(3417,21),USE(?ReportTimePrompt),TRN,FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING(@T01B),AT(3750,21),USE(LOC:Hora),RIGHT(1),FONT('Verdana',8,,,CHARSET:ANSI)
                         STRING('Usu�rio:'),AT(83,10),USE(?String24),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@s20),AT(531,10),USE(GLO:nomeusuario),FONT('Arial',8,,,CHARSET:ANSI)
                         LINE,AT(94,-10,7563,0),USE(?Line1:2),COLOR(COLOR:Black)
                         STRING(@pP�gina <.<<#p),AT(6906,198,729,146),PAGENO,USE(?PageCount),RIGHT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         STRING('Interfocus Tecnologia'),AT(6604,10),USE(?String23),TRN,FONT('Times New Roman',8,,FONT:bold+FONT:italic,CHARSET:ANSI)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Previewer            PrintPreviewClass                     ! Print Previewer

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('RelatCentroDeCusto')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:centrocusto.SetOpenRelated()
  Relate:centrocusto.Open                                  ! File centrocusto used by this procedure, so make sure it's RelationManager is open
  Relate:consultasql2.Open                                 ! File consultasql2 used by this procedure, so make sure it's RelationManager is open
  Access:empresa.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ThisReport.Init(Process:View, Relate:centrocusto, ?Progress:PctText, Progress:Thermometer)
  ThisReport.AddSortOrder()
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:centrocusto.SetQuickScan(1,Propagate:OneMany)
  SELF.SkipPreview = False
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom=True
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:centrocusto.Close
    Relate:consultasql2.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
      !!!Seta as Vari�veis do Report
      Clear(LOC:RazaoSocial)
      Clear(LOC:EnderecoBairro)
      Clear(LOC:CepTelefone)
      Clear(LOC:Data)
      Clear(LOC:Hora)
      
      
      Clear(CSQ1:Record)
      consultasql2{prop:sql} = |
              'select emp.razaosocial, emp.logradouro, emp.numero, emp.bairro, emp.cep, emp.telefone from empresa emp where emp.codempresa = '&GLO:CodEmpresa
      
      !message(fExtrato{prop:sql},,,,,2)
      
      Next(consultasql2)
      
      LOC:RazaoSocial    = CSQ1:campo1
      LOC:EnderecoBairro = CSQ1:campo2&', '&CSQ1:campo3&'   '&CSQ1:campo4
      LOC:CepTelefone    = CSQ1:campo5&'   '&CSQ1:campo6
      LOC:Data           = DataServidor()
      LOC:Hora           = HoraServidor()
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:detail1)
  RETURN ReturnValue

