CREATE TYPE "public"."detalhes" AS (
  "evento" INTEGER,
  "descricao" VARCHAR(100),
  "tipo" VARCHAR(10),
  "valor" NUMERIC(10,2)[]
);

CREATE TABLE "public"."doc_detalhes" (
  "evento" VARCHAR(10), 
  "descricao" VARCHAR(50), 
  "tipo" VARCHAR(10), 
  "valor" NUMERIC(10,2)[]
) WITHOUT OIDS;