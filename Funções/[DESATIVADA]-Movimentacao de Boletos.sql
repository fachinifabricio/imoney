CREATE OR REPLACE FUNCTION "public"."func_movdocumentos_atualizadocumento"() RETURNS trigger AS
$body$
-- Fun��o para atualizar os documentos de acordo com as movimenta��es
DECLARE
  reg_planodoc record;
  reg_planomov record;
  reg_documento record;
  var_op varchar;
  var_id bigint;
  var_idplanomov bigint;
  var_valordocumento numeric(12,2);
  var_atualiza smallint;
BEGIN
    var_atualiza := 0;
    var_op := substr(TG_OP,1,1);
    If var_op = 'I' Or var_op = 'U' Then
      If NEW.iddocumento is not null Then
        var_atualiza = 1;
      End If;
    ElsIf var_op = 'D' Then
      If OLD.iddocumento is not null Then
        var_atualiza = 1;
      End If;
    End If;
	-- Executa a fun��o somente se o documento n�o for nulo
	If var_atualiza = 1 Then
	  If var_op = 'I' Then
	    var_id := NEW.iddocumento;
	    var_idplanomov := NEW.idplano;
	  Else
	    var_id := OLD.iddocumento;
	    var_idplanomov := OLD.idplano;
	  End If;
	  -- Busca o documento a ser utilizado
	  Select into reg_documento doc.id, doc.valordocumento, doc.valordesconto, doc.valorjuros,
	    doc.valormulta, doc.valorloutrosdescontos, doc.valoroutrosacrescimos, doc.valorpago,
	    doc.d_datapagamento, idplano from documentos doc where doc.id = var_id;
	  If Not Found Then
	     Raise Exception 'Documento n�o encontrado!';
	  End If;
	  -- Busca o plano do documento
	  Select into reg_planodoc pla.tipolancamento, pla.tipovalor from planocontas pla
	    where pla.id = reg_documento.idplano;
	  If Not Found Then
	     Raise Exception 'Plano de contas do documento n�o encontrado!';
	  End If;
	  -- Busca o plano do movimento
	  Select into reg_planomov pla.tipolancamento, pla.tipovalor from planocontas pla
	    where pla.id = var_idplanomov;
	  If Not Found Then
	     Raise Exception 'Plano de contas do movimento n�o encontrado!';
	  End If;
	  If var_op = 'I' Then
	    -- Verifica o tipo de lan�amento do plano de contas e o tipo de valor que deve ser atualizado
	    If (reg_planomov.tipolancamento = 'D' And reg_planodoc.tipolancamento = 'D')
	    Or (reg_planomov.tipolancamento = 'C' And reg_planodoc.tipolancamento = 'C') Then
	      If reg_planomov.tipovalor = 2 Then
	        reg_documento.valormulta := reg_documento.valormulta + NEW.valormovimento;
	        Update documentos set valormulta = reg_documento.valormulta where id = reg_documento.id;
	      ElsIf reg_planomov.tipovalor = 3 Then
	        reg_documento.valorjuros := reg_documento.valorjuros + NEW.valormovimento;
	        Update documentos set valorjuros = reg_documento.valorjuros where id = reg_documento.id;
	      ElsIf reg_planomov.tipovalor = 4 Then
	        reg_documento.valoroutrosacrescimos := reg_documento.valoroutrosacrescimos + NEW.valormovimento;
	        Update documentos set valoroutrosacrescimos = reg_documento.valoroutrosacrescimos where id = reg_documento.id;
	      End If;
	    ElsIf (reg_planomov.tipolancamento = 'D' And reg_planodoc.tipolancamento = 'C')
	    Or (reg_planomov.tipolancamento = 'C' And reg_planodoc.tipolancamento = 'D') Then
	      If reg_planomov.tipovalor = 1 Then
	        reg_documento.valordesconto := reg_documento.valordesconto + NEW.valormovimento;
	        Update documentos set valordesconto = reg_documento.valordesconto where id = reg_documento.id;
	      ElsIf reg_planomov.tipovalor = 5 Then
	        reg_documento.valorloutrosdescontos := reg_documento.valorloutrosdescontos + NEW.valormovimento;
	        Update documentos set valoroutrosdescontos = reg_documento.valoroutrosdescontos where id = reg_documento.id;
	      ElsIf reg_planomov.tipovalor = 6 Then
	        reg_documento.valorpago := reg_documento.valorpago + NEW.valormovimento;
	        Update documentos set valorpago = reg_documento.valorpago where id = reg_documento.id;
	      End If;
	    End If;
	    -- Testa se o documento pode ser considerado como pago e coloca data de pagamento
	    If (reg_documento.valordocumento - reg_documento.valordesconto + reg_documento.valorjuros +
	    reg_documento.valormulta - reg_documento.valorloutrosdescontos +
	    reg_documento.valoroutrosacrescimos) = reg_documento.valorpago Then
	      Update documentos set d_datapagamento = NEW.d_datamovimentacao where id = reg_documento.id;
	    ElsIf reg_documento.d_datapagamento is not null Then
	      Update documentos set d_datapagamento = null where id = reg_documento.id;
	    End If;
	    Return NEW;
	  ElsIf var_op = 'U' Then
	    -- Verifica o tipo de lan�amento do plano de contas e o tipo de valor que deve ser atualizado
	    If (reg_planomov.tipolancamento = 'D' And reg_planodoc.tipolancamento = 'D')
	    Or (reg_planomov.tipolancamento = 'C' And reg_planodoc.tipolancamento = 'C') Then
	      If reg_planomov.tipovalor = 2 Then
	        reg_documento.valormulta := (reg_documento.valormulta - OLD.valormovimento) + NEW.valormovimento;
	        Update documentos set valormulta = reg_documento.valormulta where id = reg_documento.id;
	      ElsIf reg_planomov.tipovalor = 3 Then
	        reg_documento.valorjuros := (reg_documento.valorjuros - OLD.valormovimento) + NEW.valormovimento;
	        Update documentos set valorjuros = reg_documento.valorjuros where id = reg_documento.id;
	      ElsIf reg_planomov.tipovalor = 4 Then
	        reg_documento.valoroutrosacrescimos := (reg_documento.valoroutrosacrescimos - OLD.valormovimento) + NEW.valormovimento;
	        Update documentos set valoroutrosacrescimos = reg_documento.valoroutrosacrescimos where id = reg_documento.id;
	      End If;
	    ElsIf (reg_planomov.tipolancamento = 'D' And reg_planodoc.tipolancamento = 'C')
	    Or (reg_planomov.tipolancamento = 'C' And reg_planodoc.tipolancamento = 'D') Then
	      If reg_planomov.tipovalor = 1 Then
	        reg_documento.valordesconto := (reg_documento.valordesconto - OLD.valormovimento) + NEW.valormovimento;
	        Update documentos set valordesconto = reg_documento.valordesconto where id = reg_documento.id;
	      ElsIf reg_planomov.tipovalor = 5 Then
	        reg_documento.valorloutrosdescontos := (reg_documento.valorloutrosdescontos - OLD.valormovimento) + NEW.valormovimento;
	        Update documentos set valoroutrosdescontos = reg_documento.valoroutrosdescontos where id = reg_documento.id;
	      ElsIf reg_planomov.tipovalor = 6 Then
	        reg_documento.valorpago := (reg_documento.valorpago - OLD.valormovimento) + NEW.valormovimento;
	        Update documentos set valorpago = reg_documento.valorpago where id = reg_documento.id;
	      End If;
	    End If;
	    -- Testa se o documento pode ser considerado como pago e coloca data de pagamento
	    If (reg_documento.valordocumento - reg_documento.valordesconto + reg_documento.valorjuros +
	    reg_documento.valormulta - reg_documento.valorloutrosdescontos +
	    reg_documento.valoroutrosacrescimos) = reg_documento.valorpago Then
	      Update documentos set d_datapagamento = NEW.d_datamovimentacao where id = reg_documento.id;
	    ElsIf reg_documento.d_datapagamento is not null Then
	      Update documentos set d_datapagamento = null where id = reg_documento.id;
	    End If;
	    Return NEW;
	  ElsIf var_op = 'D' Then
	    -- Verifica o tipo de lan�amento do plano de contas e o tipo de valor que deve ser atualizado
	    If (reg_planomov.tipolancamento = 'D' And reg_planodoc.tipolancamento = 'D')
	    Or (reg_planomov.tipolancamento = 'C' And reg_planodoc.tipolancamento = 'C') Then
	      If reg_planomov.tipovalor = 2 Then
	        reg_documento.valormulta := reg_documento.valormulta - OLD.valormovimento;
	        Update documentos set valormulta = reg_documento.valormulta where id = reg_documento.id;
	      ElsIf reg_planomov.tipovalor = 3 Then
	        reg_documento.valorjuros := reg_documento.valorjuros - OLD.valormovimento;
	        Update documentos set valorjuros = reg_documento.valorjuros where id = reg_documento.id;
	      ElsIf reg_planomov.tipovalor = 4 Then
	        reg_documento.valoroutrosacrescimos := reg_documento.valoroutrosacrescimos - OLD.valormovimento;
	        Update documentos set valoroutrosacrescimos = reg_documento.valoroutrosacrescimos where id = reg_documento.id;
	      End If;
	    ElsIf (reg_planomov.tipolancamento = 'D' And reg_planodoc.tipolancamento = 'C')
	    Or (reg_planomov.tipolancamento = 'C' And reg_planodoc.tipolancamento = 'D') Then
	      If reg_planomov.tipovalor = 1 Then
	        reg_documento.valordesconto := reg_documento.valordesconto - OLD.valormovimento;
	        Update documentos set valordesconto = reg_documento.valordesconto where id = reg_documento.id;
	      ElsIf reg_planomov.tipovalor = 5 Then
	        reg_documento.valorloutrosdescontos := reg_documento.valorloutrosdescontos - OLD.valormovimento;
	        Update documentos set valoroutrosdescontos = reg_documento.valoroutrosdescontos where id = reg_documento.id;
	      ElsIf reg_planomov.tipovalor = 6 Then
	        reg_documento.valorpago := reg_documento.valorpago - OLD.valormovimento;
	        Update documentos set valorpago = reg_documento.valorpago where id = reg_documento.id;
	      End If;
	    End If;
	    -- Testa se o documento pode ser considerado como pago e coloca data de pagamento
	    If (reg_documento.valordocumento - reg_documento.valordesconto + reg_documento.valorjuros +
	    reg_documento.valormulta - reg_documento.valorloutrosdescontos +
	    reg_documento.valoroutrosacrescimos) = reg_documento.valorpago Then
	      Update documentos set d_datapagamento = OLD.d_datamovimentacao where id = reg_documento.id;
	    ElsIf reg_documento.d_datapagamento is not null Then
	      Update documentos set d_datapagamento = null where id = reg_documento.id;
	    End If;
	    Return OLD;
	  End If;
	Else
	  Return OLD;
	End If;
END;
$body$
LANGUAGE 'plpgsql' VOLATILE CALLED ON NULL INPUT SECURITY INVOKER;

CREATE TRIGGER "tri_movdocumentos_atualizadocumento" AFTER INSERT OR UPDATE OR DELETE
ON "public"."movdocumentos"
FOR EACH ROW EXECUTE PROCEDURE "public"."func_movdocumentos_atualizadocumento"();