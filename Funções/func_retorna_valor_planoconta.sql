CREATE OR REPLACE FUNCTION "public"."func_retorna_valor_planoconta" (
  "pid" bigint,
  "pdatainicial" date,
  "pdatafinal" date,
  "ptipodata" smallint,
  "pidempresa" bigint
)
RETURNS numeric AS
$body$
declare
  tSql text;
  rReg record;
  vCampoValor varchar(30);
  vCampoData varchar(30);
  nRetorno numeric(15,2) := 0;
begin
  if pTipoData = 1 then
     vCampoData := 'd_datavencimento';
     vCampoValor := 'valordocumento';
  elsif pTipoData = 2 then
     vCampoData := 'd_dataprovavel';
     vCampoValor := 'valordocumento';
  elsif pTipoData = 3 then
     vCampoData := 'd_dataemissao';
     vCampoValor := 'valordocumento';
  else
     vCampoData := 'd_datapagamento';
     vCampoValor := 'valorpago';
  end if;

  tSql := 'select d.idplano, sum(d.'||vCampoValor||') as total '||
    'from public.documentos d '||
    'join public.planocontas p on p.id = d.idplano '||
    'where d.'||vCampoData||' between '''||pDataInicial||''' and '''||pDataFinal||''' '||
    ' and d.idplano = '||pId||' and d.idempresa = '||pIdEmpresa||' '||
    'group by d.idplano;';
  for rReg in execute tSql
  loop
    nRetorno := rReg.total;      
  end loop;
  
  return nRetorno;
end;
$body$
LANGUAGE 'plpgsql';