/* Deve ser trocado o nome do esquema para o esquema que est� sendo
   utilizado para a cria��o das tabelas a serem criadas as triggers de us�rios */
CREATE OR REPLACE FUNCTION "public"."fun_cria_trigger_usuarios"(paresquema text, partabela text)
RETURNS "pg_catalog"."void" AS
$body$
/*
paresquema   -> esquema para ser criada a trigger, deve ser informado
partabela    -> tabela para ser criada a trigger, se n�o informado cria para
                todas as tabelas do esquema
*/

/* Cria a Trigger de usu�rios para todas as tabelas do esquema informado */
DECLARE
  tabelas record; -- record dos nomes das tabelas
  var_consulta text; -- cosulta a ser executada para as tabelas
  var_nometrigger text; -- nome da trigger a ser criada
  var_trigger text; -- consulta para cria��o da trigger
  var_nomefuncao text; -- nome da fun��o de replica��o
BEGIN
  if paresquema = '' then
    raise notice 'N�o criada, falta par�metros';
  else
    /* Busca os nomes das tabelas no information_schema lendo cada uma
       para que seja feita a cria��o das triggers para estas */
    if partabela = '' then
      var_consulta := 'select table_name as nome from information_schema.tables '||
                      'where table_schema = '''||paresquema||''' and '||
                      'table_type <> ''VIEW'' and substr(table_name,1,2) <> ''hw''';
    else
      var_consulta := 'select table_name as nome from information_schema.tables '||
                      'where table_schema = '''||paresquema||''' and '||
                      'table_name = '''||partabela||''' and table_type <> ''VIEW'''||
                      ' and substr(table_name,1,2) <> ''hw''';
    end if;
    for tabelas in execute var_consulta
    loop
      var_nomefuncao := paresquema||'.fun_usuarios';
      var_nometrigger := 'tri_usuarios_'||tabelas.nome;
      var_trigger :=
        'CREATE TRIGGER '||var_nometrigger||
        ' BEFORE INSERT OR UPDATE ON '||
        paresquema||'.'||tabelas.nome||' FOR EACH ROW EXECUTE PROCEDURE '||
        var_nomefuncao||'()';

      execute var_trigger;
      raise notice '% Criada com Sucesso', var_trigger;
    end loop;
  end if;
END;
$body$
LANGUAGE 'plpgsql' VOLATILE CALLED ON NULL INPUT SECURITY INVOKER;
