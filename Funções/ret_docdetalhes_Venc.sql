CREATE OR REPLACE FUNCTION "public"."ret_docdetalhes_Venc" (
  "par_inicio" text,
  "par_termino" text,
  "par_idempresa" bigint
)
RETURNS SETOF "public"."detalhes" AS
$body$
DECLARE
  reg detalhes%ROWTYPE;
  selrec RECORD;
  selrec1 RECORD;
  conf_competencia date;
  contador integer;
  mes integer;
  ano integer;
  data_texto text;
BEGIN        
  delete from doc_detalhes;

  FOR selrec IN       
    select pl.id as evtselect, pl.descricaoplano as evtdesc,
    to_char(dc.d_datavencimento, 'YYYY-MM-01')::date as mesref,                               
    sum(dc.valordocumento) as totalvrdoc,
    sum(dc.valorpago) as totalvrpago,
    sum(dc.valororiginalbaixado) as totalvrbaixado,
    pl.tipolancamento as tipo
    from documentos dc
    left join planocontas pl on (pl.id = dc.idplano)
    where dc.d_datavencimento between par_inicio::date and par_termino::date
    and dc.idempresa = par_idempresa
    group by mesref, pl.id, pl.descricaoplano, pl.tipolancamento
    order by pl.tipolancamento, mesref 
  LOOP                                  
    --raise notice '----> %,%,%,%.',selrec.evtselect,selrec.mesref,selrec.tipo,selrec.totalvrpago; 
    contador := 0;

    select count(*) into contador from doc_detalhes WHERE doc_detalhes.evento::integer = selrec.evtselect; 

    If contador = 0 then                                                 
      insert into doc_detalhes (evento, descricao, tipo) values (selrec.evtselect, selrec.evtdesc, selrec.tipo);
    end if;

    contador := 0;

    conf_competencia := to_char(par_inicio::date, 'YYYY-MM-01');

    LOOP
      contador := contador + 1;

      if contador > 12 then
        exit;
      end if;                                       

      if selrec.mesref = conf_competencia THEN                        
        update doc_detalhes set valor[contador] = selrec.totalvrdoc where evento::INTEGER = selrec.evtselect;
        exit;                        
      end if;

      ano:=0;
      mes:=0;
      data_texto:=0;                                      
      mes := extract(month from conf_competencia) + 1;

      if mes > 12 then
        mes := 01;
        ano := extract(year from conf_competencia) + 1;
      else    
        ano := extract(year from conf_competencia) ;
      end if;                   

      data_texto := ano||lpad(mes::text,2,'0')||'01';                 
      conf_competencia := to_char(data_texto::date, 'YYYY-MM-DD');                                        
    end loop;                
  END LOOP;    

  FOR selrec1 IN SELECT dc.evento, dc.descricao, dc.tipo, dc.valor FROM doc_detalhes dc
  LOOP
    reg.evento      := selrec1.evento;
    reg.descricao   := selrec1.descricao;
    reg.tipo        := selrec1.tipo;
    reg.valor       := selrec1.valor;                
 
    RETURN NEXT reg;
  END LOOP;       

  RETURN;  
END
$body$
LANGUAGE 'plpgsql';