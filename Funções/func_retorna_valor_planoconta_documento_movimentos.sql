CREATE OR REPLACE FUNCTION "public"."func_retorna_valor_plano_documento_movimento" (
  "pid" bigint,
  "pdatainicial" date,
  "pdatafinal" date,
  "pidempresa" bigint
)
RETURNS numeric AS
$body$
declare
  rReg record;
  nRetorno numeric(15,2) := 0;
begin
  for rReg in
    select sum(t.total) as total from (
      select sum(d.valorpago) as total
      from public.documentos d 
      join public.planocontas p on p.id = d.idplano 
      where d.d_datapagamento between pDataInicial and pDataFinal
      and d.idplano = pId
      and d.idempresa = pidempresa
      union all
      select sum(m.valormovimento) as total
      from public.movdocumentos m
      join public.contas cc on cc.id = m.idconta
      where m.d_datamovimentacao between pDataInicial and pDataFinal
      and m.iddocumento is null and m.idplano = pId
      and cc.idempresa = pidempresa
    ) as t
  loop
    nRetorno := rReg.total;      
  end loop;
  
  return nRetorno;
end;
$body$
LANGUAGE 'plpgsql';