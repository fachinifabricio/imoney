CREATE OR REPLACE FUNCTION "public"."fun_hwusers_usuariosbanco" () RETURNS trigger AS
$body$
declare
  vl_cont     integer;
  vl_op       char;
  vl_cons     text;
  vl_oldpass  text;
  vl_newpass  text;
  vl_olddata  date;
  vl_newdata  date;
  vl_senhapadrao text;
begin
  vl_op := substr(TG_OP,1,1);
  vl_senhapadrao := public.func_retornasenhapadrao();
  If vl_op = 'I' Then -- Inclus�o
    -- Verifica se j� existe um usu�rio cadastrado com o login
    vl_cont := 0;
    Select into vl_cont count(*) from pg_user where usename = lower(to_ascii(NEW.login));
    If vl_cont = 0 Then
      -- Cria o usu�rio
      vl_cons := 'CREATE USER "'||lower(to_ascii(NEW.login))||
                 '" WITH PASSWORD '''||vl_senhapadrao||''' superuser;';
      Execute vl_cons;
    End If;
    Return NEW;
  ElsIf vl_op = 'U' Then -- Altera��o
    vl_olddata := OLD.d_blockdate;
    vl_newdata := NEW.d_blockdate;
    If vl_newdata is not null And (vl_newdata <> vl_olddata or vl_olddata is null) Then
      -- Verifica se j� existe um usu�rio cadastrado com o login
      vl_cont := 0;
      Select into vl_cont count(*) from pg_user where usename = lower(to_ascii(NEW.login));
      If vl_cont <> 0 Then
        vl_cons := 'ALTER USER "'||lower(to_ascii(NEW.login))||
                   '" VALID UNTIL '''||NEW.d_blockdate||''';';
        Execute vl_cons;
      End If;
    elsif vl_newdata is null and vl_olddata is not null then
      -- Verifica se j� existe um usu�rio cadastrado com o login
      vl_cont := 0;
      Select into vl_cont count(*) from pg_user where usename = lower(to_ascii(NEW.login));
      If vl_cont <> 0 Then
        vl_cons := 'ALTER USER "'||lower(to_ascii(NEW.login))||
                   '" VALID UNTIL ''infinity'';';
        Execute vl_cons;
      End If;
    End If;
    Return NEW;
  ElsIf vl_op = 'D' Then -- Exclus�o
    -- Verifica se j� existe um usu�rio cadastrado com o login
    vl_cont := 0;
    Select into vl_cont count(*) from pg_user where usename = lower(to_ascii(OLD.login));
    If vl_cont <> 0 Then
      --Verifica se o usu�rio est� conectado neste momento
      vl_cont := 0;
      Select into vl_cont count(*) from pg_stat_activity where usename = lower(to_ascii(OLD.login));
      If vl_cont = 0 Then
        vl_cons := 'DROP USER "'||lower(to_ascii(OLD.login))||'";';
        Execute vl_cons;
      Else
        Raise Exception 'N�o foi poss�vel excluir, usu�rio conectado!';
      End If;
    End If;
    Return OLD;
  End If;
end;
$body$
LANGUAGE 'plpgsql' VOLATILE CALLED ON NULL INPUT SECURITY INVOKER;

CREATE TRIGGER "hwusers_tri_usuariosbanco" AFTER INSERT OR UPDATE OR DELETE 
ON "public"."hwusers" FOR EACH ROW 
EXECUTE PROCEDURE "public"."fun_hwusers_usuariosbanco"();