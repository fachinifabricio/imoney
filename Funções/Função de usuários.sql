CREATE OR REPLACE FUNCTION "public"."fun_usuarios"() RETURNS trigger AS
$body$
begin
  NEW.usuario := current_user;
  Return NEW;
end;
$body$
LANGUAGE 'plpgsql' VOLATILE CALLED ON NULL INPUT SECURITY INVOKER;
