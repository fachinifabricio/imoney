CREATE OR REPLACE FUNCTION "public"."func_relatorio_plano_contas"()
RETURNS SETOF text AS
$body$
declare
  smTamanho smallint;
  tRet   text;
  rReg1  record;
  rReg2  record;
  rReg3  record;
  rReg4  record;
  rReg5  record;
  rReg6  record;
  rReg7  record;
  rReg8  record;
  rReg9  record;
  rReg10 record;
begin
  -- Pega o Tamanho M�ximo da conta
  select into smTamanho max(length(p.id::text))from public.planocontas p;

  -- Primeiro N�vel
  for rReg1 in select pl.id, pl.descricaoplano,
      case when pl.tipolancamento = 'D' then 'D�bito'
           when pl.tipolancamento = 'C' then 'Cr�dito'
           else 'Sem A��o'
      end
    from planocontas pl
    where pl.idpai is null order by pl.id
  loop
    tRet := lpad(rReg1.id::text,smTamanho,' ')||' - '||rReg1.descricaoplano;
    return next tRet;

    -- Segundo N�vel
    for rReg2 in select pl.id, pl.descricaoplano,
        case when pl.tipolancamento = 'D' then 'D�bito'
             when pl.tipolancamento = 'C' then 'Cr�dito'
             else 'Sem A��o'
        end as lanc
      from planocontas pl
      where pl.idpai = rReg1.id order by pl.id
    loop   
      tRet := rpad('  '||lpad(rReg2.id::text,smTamanho,' ')||' - '||rReg2.descricaoplano,104,' ')||rReg2.lanc;
      return next tRet;

      -- Terceiro N�vel
      for rReg3 in select pl.id, pl.descricaoplano,
          case when pl.tipolancamento = 'D' then 'D�bito'
             when pl.tipolancamento = 'C' then 'Cr�dito'
             else 'Sem A��o'
          end as lanc
        from planocontas pl
        where pl.idpai = rReg2.id order by pl.id
      loop
        tRet := rpad('    '||lpad(rReg3.id::text,smTamanho,' ')||' - '||rReg3.descricaoplano,104,' ')||rReg3.lanc;
        return next tRet;

        -- Quarto N�vel
        for rReg4 in select pl.id, pl.descricaoplano,
            case when pl.tipolancamento = 'D' then 'D�bito'
                 when pl.tipolancamento = 'C' then 'Cr�dito'
                 else 'Sem A��o'
            end as lanc
          from planocontas pl
          where pl.idpai = rReg3.id order by pl.id
        loop
          tRet := rpad('      '||lpad(rReg4.id::text,smTamanho,' ')||' - '||rReg4.descricaoplano,104,' ')||rReg4.lanc;
          return next tRet;

          -- Quinto N�vel
          for rReg5 in select pl.id, pl.descricaoplano,
              case when pl.tipolancamento = 'D' then 'D�bito'
                   when pl.tipolancamento = 'C' then 'Cr�dito'
                   else 'Sem A��o'
              end as lanc
            from planocontas pl
            where pl.idpai = rReg4.id order by pl.id
          loop
            tRet := rpad('        '||lpad(rReg5.id::text,smTamanho,' ')||' - '||rReg5.descricaoplano,104,' ')||rReg5.lanc;
            return next tRet;

            -- Sexto N�vel
            for rReg6 in select pl.id, pl.descricaoplano,
                case when pl.tipolancamento = 'D' then 'D�bito'
                     when pl.tipolancamento = 'C' then 'Cr�dito'
                     else 'Sem A��o'
                end
              from planocontas pl
              where pl.idpai = rReg5.id order by pl.id
            loop
              tRet := rpad('          '||lpad(rReg6.id::text,smTamanho,' ')||' - '||rReg6.descricaoplano,104,' ')||rReg6.lanc;
              return next tRet;

              -- S�timo N�vel
              for rReg7 in select pl.id, pl.descricaoplano,
                  case when pl.tipolancamento = 'D' then 'D�bito'
                       when pl.tipolancamento = 'C' then 'Cr�dito'
                       else 'Sem A��o'
                  end as lanc
                from planocontas pl
                where pl.idpai = rReg6.id order by pl.id
              loop
                tRet := rpad('            '||lpad(rReg7.id::text,smTamanho,' ')||' - '||rReg7.descricaoplano,104,' ')||rReg7.lanc;
                return next tRet;

                -- Oitavo N�vel
                for rReg8 in select pl.id, pl.descricaoplano,
                    case when pl.tipolancamento = 'D' then 'D�bito'
                         when pl.tipolancamento = 'C' then 'Cr�dito'
                         else 'Sem A��o'
                    end as lanc
                  from planocontas pl
                  where pl.idpai = rReg7.id order by pl.id
                loop
                  tRet := rpad('              '||lpad(rReg8.id::text,smTamanho,' ')||' - '||rReg8.descricaoplano,104,' ')||rReg8.lanc;
                  return next tRet;

                  -- Nono N�vel
                  for rReg9 in select pl.id, pl.descricaoplano,
                      case when pl.tipolancamento = 'D' then 'D�bito'
                           when pl.tipolancamento = 'C' then 'Cr�dito'
                           else 'Sem A��o'
                      end as lanc
                    from planocontas pl
                    where pl.idpai = rReg8.id order by pl.id
                  loop
                    tRet := rpad('                '||lpad(rReg9.id::text,smTamanho,' ')||' - '||rReg9.descricaoplano,104,' ')||rReg9.lanc;
                    return next tRet;

                    -- D�cimo N�vel
                    for rReg10 in select pl.id, pl.descricaoplano,
                        case when pl.tipolancamento = 'D' then 'D�bito'
                             when pl.tipolancamento = 'C' then 'Cr�dito'
                             else 'Sem A��o'
                        end as lanc
                      from planocontas pl
                      where pl.idpai = rReg9.id order by pl.id
                    loop
                      tRet := rpad('                  '||lpad(rReg10.id::text,smTamanho,' ')||' - '||rReg10.descricaoplano,104,' ')||rReg10.lanc;
                      return next tRet;
                    end loop;  
                  end loop;  
                end loop;  
              end loop;  
            end loop;  
          end loop;  
        end loop;  
      end loop;
    end loop;
  end loop;
  return;
end;
$body$
LANGUAGE 'plpgsql';