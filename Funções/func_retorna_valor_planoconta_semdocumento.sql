CREATE OR REPLACE FUNCTION "public"."func_retorna_valor_planoconta_semdocumento" (
  "pid" bigint,
  "pdatainicial" date,
  "pdatafinal" date,
  "pidempresa" bigint
)
RETURNS numeric AS
$body$
declare
  tSql text;
  rReg record;
  nRetorno numeric(15,2) := 0;
begin
  for rReg in 
    select sum(m.valormovimento) as total
    from public.movdocumentos m
    join public.contas cc on cc.id = m.idconta
    where m.d_datamovimentacao between pDataInicial and pDataFinal
    and m.iddocumento is null and m.idplano = pId
    and cc.idempresa = pidempresa
  loop
    nRetorno := rReg.total;      
  end loop;
  
  return nRetorno;
end;
$body$
LANGUAGE 'plpgsql';