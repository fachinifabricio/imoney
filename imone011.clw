

   MEMBER('imoney.clw')                                    ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('IMONE011.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('IMONE007.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowsemovdocumentosByMOV:mov_fk_documento PROCEDURE        ! Generated from procedure template - Window

CurrentTab           STRING(80)                            !
BRW1::View:Browse    VIEW(movdocumentos)
                       PROJECT(MOV:id)
                       PROJECT(MOV:iddocumento)
                       PROJECT(MOV:descricao)
                       PROJECT(MOV:d_datamovimentacao)
                       PROJECT(MOV:valormovimento)
                       PROJECT(MOV:idconta)
                       PROJECT(MOV:usuario)
                       PROJECT(MOV:d_datacadastro)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
MOV:id                 LIKE(MOV:id)                   !List box control field - type derived from field
MOV:iddocumento        LIKE(MOV:iddocumento)          !List box control field - type derived from field
MOV:descricao          LIKE(MOV:descricao)            !List box control field - type derived from field
MOV:d_datamovimentacao LIKE(MOV:d_datamovimentacao)   !List box control field - type derived from field
MOV:valormovimento     LIKE(MOV:valormovimento)       !List box control field - type derived from field
MOV:idconta            LIKE(MOV:idconta)              !List box control field - type derived from field
MOV:usuario            LIKE(MOV:usuario)              !List box control field - type derived from field
MOV:d_datacadastro     LIKE(MOV:d_datacadastro)       !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
LocEnableEnterByTab  BYTE(1)                               !Used by the ENTER Instead of Tab template
EnterByTabManager    EnterByTabClass
QuickWindow          WINDOW('Browse the movdocumentos file by MOV:mov_fk_documento'),AT(,,358,199),FONT('Tahoma',8,COLOR:Navy,FONT:regular),CENTER,IMM,HLP('BrowsemovdocumentosByMOV:mov_fk_documento'),SYSTEM,GRAY,RESIZE,MDI
                       LIST,AT(8,30,342,123),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing the movdocumentos file'),FORMAT('64R(2)|M~ID~C(0)@n15.`0b@64R(2)|M~Documento~C(0)@n15.`0b@80L(2)|M~Descri��o~@s50' &|
   '@80R(2)|M~Data~C(0)@d06b@56D(24)|M~Valor~C(0)@n13.`2@64R(2)|M~Conta~C(0)@n15.`0b' &|
   '@80L(2)|M~Usu�rio~@s20@80R(2)|M~Cadastro~C(0)@d06b@'),FROM(Queue:Browse:1)
                       BUTTON('&Incluir'),AT(177,157,55,15),USE(?Insert:2),FLAT,LEFT,MSG('Incluir o registro'),TIP('Incluir o registro'),ICON('insert.ICO')
                       BUTTON('&Alterar'),AT(236,157,55,15),USE(?Change:2),FLAT,LEFT,MSG('Alterar o registro'),TIP('Alterar o registro'),ICON('edit.ICO'),DEFAULT
                       BUTTON('&Excluir'),AT(295,157,55,15),USE(?Delete:2),FLAT,LEFT,MSG('Excluir o Registro'),TIP('Excluir o Registro'),ICON('delete.ICO')
                       SHEET,AT(4,4,350,172),USE(?CurrentTab)
                         TAB('&1) iddocumento'),USE(?Tab:2)
                         END
                       END
                       BUTTON('Sai&r'),AT(299,180,55,15),USE(?Close),FLAT,LEFT,MSG('Sair da janela'),TIP('Sair da janela'),ICON('sair.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Fetch                  PROCEDURE(BYTE Direction),DERIVED   ! Method added to host embed code
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM) ! Method added to host embed code
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowsemovdocumentosByMOV:mov_fk_documento')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('MOV:id',MOV:id)                                    ! Added by: BrowseBox(ABC)
  BIND('MOV:iddocumento',MOV:iddocumento)                  ! Added by: BrowseBox(ABC)
  BIND('MOV:descricao',MOV:descricao)                      ! Added by: BrowseBox(ABC)
  BIND('MOV:d_datamovimentacao',MOV:d_datamovimentacao)    ! Added by: BrowseBox(ABC)
  BIND('MOV:valormovimento',MOV:valormovimento)            ! Added by: BrowseBox(ABC)
  BIND('MOV:idconta',MOV:idconta)                          ! Added by: BrowseBox(ABC)
  BIND('MOV:usuario',MOV:usuario)                          ! Added by: BrowseBox(ABC)
  BIND('MOV:d_datacadastro',MOV:d_datacadastro)            ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:documentos.SetOpenRelated()
  Relate:documentos.Open                                   ! File documentos used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:movdocumentos,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  COMPILE ('**CW7**',_CWVER_=7000)
  ?CurrentTab{PROP:TabSheetStyle} = TabStyle:Colored
  !**CW7**
  Do DefineListboxStyle
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,MOV:mov_fk_documento)                 ! Add the sort order for MOV:mov_fk_documento for sort order 1
  BRW1.AddRange(MOV:iddocumento,Relate:movdocumentos,Relate:documentos) ! Add file relationship range limit for sort order 1
  BRW1.AddField(MOV:id,BRW1.Q.MOV:id)                      ! Field MOV:id is a hot field or requires assignment from browse
  BRW1.AddField(MOV:iddocumento,BRW1.Q.MOV:iddocumento)    ! Field MOV:iddocumento is a hot field or requires assignment from browse
  BRW1.AddField(MOV:descricao,BRW1.Q.MOV:descricao)        ! Field MOV:descricao is a hot field or requires assignment from browse
  BRW1.AddField(MOV:d_datamovimentacao,BRW1.Q.MOV:d_datamovimentacao) ! Field MOV:d_datamovimentacao is a hot field or requires assignment from browse
  BRW1.AddField(MOV:valormovimento,BRW1.Q.MOV:valormovimento) ! Field MOV:valormovimento is a hot field or requires assignment from browse
  BRW1.AddField(MOV:idconta,BRW1.Q.MOV:idconta)            ! Field MOV:idconta is a hot field or requires assignment from browse
  BRW1.AddField(MOV:usuario,BRW1.Q.MOV:usuario)            ! Field MOV:usuario is a hot field or requires assignment from browse
  BRW1.AddField(MOV:d_datacadastro,BRW1.Q.MOV:d_datacadastro) ! Field MOV:d_datacadastro is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  BRW1.AskProcedure = 1
  BRW1.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  SELF.SetAlerts()
  EnterByTabManager.Init(False)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:documentos.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    UpdateMovDocumentos
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF EnterByTabManager.TakeEvent()
     RETURN(Level:Notify)
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Fetch PROCEDURE(BYTE Direction)

GreenBarIndex   LONG
  CODE
  PARENT.Fetch(Direction)


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

